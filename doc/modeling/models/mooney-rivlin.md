# Mooney-Rivlin Materials

While the Neo-Hookean model depends on just two scalar invariants, $\mathbb{I}_1 = \operatorname{trace} \bm{C} = 3 + 2 \operatorname{trace} \bm{E}$ and $J$, Mooney-Rivlin models depend on the additional invariant, $\mathbb{I}_2 = \frac 1 2 \left( \mathbb{I}_1^2 - \bm{C} \tcolon \bm{C} \right)$.
A coupled Mooney-Rivlin strain energy density (cf. Neo-Hookean {eq}`neo-hookean-energy`) is {cite}`holzapfel2000nonlinear`

$$
  \psi \left( \mathbb{I_1}, \mathbb{I_2}, J \right) = \frac{\lambda}{4} \left( J^2 - 1 -2 \log J \right) - \left( \mu_1 + 2\mu_2 \right) \log J + \frac{\mu_1}{2} \left( \mathbb{I_1} - 3 \right) + \frac{\mu_2}{2} \left( \mathbb{I_2} - 3 \right).
$$ (mooney-rivlin-energy_coupled)

We differentiate $\psi$ as in the Neo-Hookean case {eq}`neo-hookean-stress` to yield the second Piola-Kirchoff tensor,

$$
  \begin{aligned}
    \bm{S} &=  \frac{\lambda}{2} \left( J^2 - 1 \right)\bm{C}^{-1} - \left( \mu_1 + 2\mu_2 \right) \bm{C}^{-1} + \mu_1\bm{I}_3 + \mu_2 \left( \mathbb{I_1} \bm{I}_3 - \bm{C} \right) \\
           &= \frac{\lambda}{2} \left( J^2 - 1 \right)\bm{C}^{-1} + \mu_1 \left( \bm{I}_3 - \bm{C}^{-1} \right) + \mu_2 \left( \mathbb{I_1} \bm{I}_3 - 2 \bm{C}^{-1} - \bm{C} \right),
  \end{aligned}
$$ (mooney-rivlin-stress_coupled)

where we have used

$$
  \begin{aligned}
    \frac{\partial \mathbb{I_1}}{\partial \bm{E}} &= 2 \bm{I}_3, & \frac{\partial \mathbb{I_2}}{\partial \bm{E}} &= 2 \mathbb{I}_1 \bm{I}_3 - 2 \bm{C}, & \frac{\partial \log J}{\partial \bm{E}} &= \bm{C}^{-1}.
  \end{aligned}
$$ (None)

This is a common model for vulcanized rubber, with a shear modulus (defined for the small-strain limit) of $\mu_1 + \mu_2$ that should be significantly smaller than the first Lamé parameter $\lambda$.

:::{dropdown} Mooney-Rivlin strain energy comparison
We apply traction to a block and plot integrated strain energy $\psi$ as a function of the loading parameter.

```{altair-plot}
:hide-code:

import altair as alt
import pandas as pd
def source_path(rel):
    import os
    return os.path.join(os.path.dirname(os.environ["DOCUTILSCONFIG"]), rel)

nh = pd.read_csv(source_path("examples/data/NH-strain.csv"))
nh["model"] = "Neo-Hookean"
nh["parameters"] = "E=2.8, nu=0.4"

mr = pd.read_csv(source_path("examples/data/MR-strain.csv"))
mr["model"] = "Mooney-Rivlin; Neo-Hookean equivalent"
mr["parameters"] = "mu_1=1, mu_2=0, nu=.4"

mr1 = pd.read_csv(source_path("examples/data/MR-strain1.csv"))
mr1["model"] = "Mooney-Rivlin"
mr1["parameters"] = "mu_1=0.5, mu_2=0.5, nu=.4"

df = pd.concat([nh, mr, mr1])
highlight = alt.selection_single(
   on = "mouseover",
   nearest = True,
   fields=["model", "parameters"],
)
base = alt.Chart(df).encode(
   alt.X("increment"),
   alt.Y("energy", scale=alt.Scale(type="sqrt")),
   alt.Color("model"),
   alt.Tooltip(("model", "parameters")),
   opacity=alt.condition(highlight, alt.value(1), alt.value(.5)),
   size=alt.condition(highlight, alt.value(2), alt.value(1)),
)
base.mark_point().add_selection(highlight) + base.mark_line()
```
:::

:::{tip}
Similar to the Neo-Hookean materials, the stable form for the Mooney-Rivlin model in initial configuration {eq}`mooney-rivlin-stress_coupled` can be written as

$$
\bm{S} = \frac{\lambda}{2} \mathtt{J_{-1}} \left(\mathtt{J_{-1}} + 2 \right) \bm{C}^{-1} + 2 \left( \mu_1 + 2\mu_2 \right)\bm{C}^{-1} \bm{E} + 2\mu_2 \left(\operatorname{trace} \left(\bm{E} \right) \bm{I}_3 - \bm{E} \right).
$$ (mooney-rivlin-stress-stable)
:::

The Kirchhoff stress tensor $\bm{\tau}$ for Mooney-Rivilin model is given by

$$
    \bm{\tau} = \bm{F}\bm{S}\bm{F}^T = \frac{\lambda}{2} \left( J^2 - 1 \right)\bm{I}_{3} + \mu_1 \left( \bm{b} - \bm{I}_3 \right) + \mu_2 \left( \mathbb{I_1} \bm{b} - 2 \bm{I}_{3} - \bm{b}^2 \right).
$$ (mooney-rivlin-tau)

Note that $\mathbb{I_i}\left(\bm{b}\right) = \mathbb{I_i} \left(\bm{C} \right),$ for $i=1,2,3$.

:::{tip}
The stable Kirchhoff stress tensor version of {eq}`mooney-rivlin-tau` is given by

$$
\bm{\tau} = \frac{\lambda}{2} \mathtt{J_{-1}} \left(\mathtt{J_{-1}} + 2 \right) \bm{I}_{3} + 2 \left( \mu_1 + 2\mu_2 \right)\bm{e} + 2\mu_2 \left(\operatorname{trace} \left(\bm{e}\right) \bm{I}_3 - \bm{e} \right) \bm{b}.
$$ (mooney-rivlin-tau-stable)

where $\mathtt{J_{-1}}$ is computed by {eq}`JM1`
:::
