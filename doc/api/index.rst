**************************************
API Documentation
**************************************

The Ratel API sets up a PETSc `DM <https://petsc.org/release/docs/manualpages/DM/DM/>`_ object with the data to run a `SNES <https://petsc.org/release/docs/manual/snes/>`_ or `TS <https://petsc.org/release/docs/manual/ts/>`_ solver for solid mechanics problems.
This `DM <https://petsc.org/release/docs/manualpages/DM/DM/>`_ is set for a `SNES <https://petsc.org/release/docs/manual/snes/>`_ or `TS <https://petsc.org/release/docs/manual/ts/>`_ via `SNESSetDM <https://petsc.org/release/docs/manualpages/SNES/SNESSetDM/>`_ or `TSSetDM <https://petsc.org/release/docs/manualpages/TS/TSSetDM/>`_ to provide a complete solver for a solid mechanics problem.

The API also includes functions to set sensible defaults for the `TS <https://petsc.org/release/docs/manual/ts/>`_, `SNES <https://petsc.org/release/docs/manual/snes/>`_, and `KSP <https://petsc.org/release/docs/manual/ksp/>`_ as well as set up geometric multigrid with `PCMG <https://petsc.org/release/docs/manualpages/PC/PCMG/>`_.
The default options depend upon the :c:enum:`RatelSolverType`.

Lastly, the Ratel API includes convenience functions to facilitate computing diagnostic quantities of interest.
These quantities of interest can be monitored on every time step via command line options or computed at the end of the solve with the convenience functions.

Ratel Public API
======================================

The public API provides core functionality for users.

.. toctree::

   core

Ratel Internal API
======================================

The internal API builds composite `CeedOperator <https://libceed.org/en/latest/api/CeedOperator/>`_ for the residual, Jacobian, and diagnostic quantity evaluation.
These composite `CeedOperator <https://libceed.org/en/latest/api/CeedOperator/>`_ are used to set the appropriate `DMSNES <https://petsc.org/release/docs/manualpages/SNES/DMGetDMSNES/>`_ or `DMTS <https://petsc.org/release/docs/manualpages/TS/DMGetDMTS/>`_ options, depending upon the :c:enum:`RatelSolverType`.

.. figure:: ../img/internal-api.png
  :alt: Ratel internal API

The internal API is organized around :c:type:`RatelMaterial`, separate material regions in the domain.
See the :ref:`Using Ratel section<multiple-materials>` of the documentation for details on how the user specifies a material region with model parameters for the mesh.

Each :c:type:`RatelMaterial` is responsible for adding volumetric terms to the residual, Jacobian, and diagnostic quantity evaluation operators as well as any surface terms that require volumetric or material model values.
Additionally, each :c:type:`RatelMaterial` is responsible for building and modifying corresponding preconditioner components, as needed.

Boundary and forcing terms that do not require volumetric or material model parameters, such as Dirichlet boundary conditions, are handled separately from a :c:type:`RatelMaterial`.

New :c:type:`RatelMaterial` are added by creating a file `src/materials/new-material.c` with the material model data, material creation function, and material registration function.
Any new QFunction source files go in `include/ratel/qfunctions`, and the material is added to `src/materials/ratel-model-list.h`.

Numerical solvers
--------------------------------------

These functions setup the numerical solver and precondioner.

.. toctree::

   solver

Material models
--------------------------------------

These functions setup the material regions in the mesh.

.. toctree::

   material


Boundary conditions
--------------------------------------

These functions apply boundary conditions.

.. toctree::

   boundary


Internal functions
--------------------------------------

These functions are internal setup and application functions.

.. toctree::

   internal
