Internal Functions
**************************************

These are additional functions that do not clearly fit into another category.

.. doxygengroup:: RatelInternal
   :project: Ratel
   :path: ../../../../xml
   :content-only:
   :members:
