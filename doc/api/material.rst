Material Models
**************************************

Each :c:type:`RatelMaterial` is responsible for adding volumetric terms to the residual, Jacobian, and diagnostic quantity evaluation operators as well as any surface terms that require volumetric or material model values.
Additionally, each :c:type:`RatelMaterial` is responsible for building and modifying corresponding preconditioner components, as needed.

.. doxygengroup:: RatelMaterials
   :project: Ratel
   :path: ../../../../xml
   :content-only:
   :members:
