/// @file
/// Hyperelastic material at finite strain using Mooney Rivlin model in initial configuration

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/models/mooney-rivlin.h>
#include <ratel/qfunctions/geometry/surface-force-geometry.h>
#include <ratel/qfunctions/geometry/surface-geometry.h>
#include <ratel/qfunctions/geometry/volumetric-geometry.h>
#include <ratel/qfunctions/models/elasticity-mooney-rivlin-initial.h>
#include <ratel/qfunctions/scaled-mass.h>

static const PetscInt    active_field_sizes[]     = {3};
static const char *const active_field_names[]     = {"displacement_x", "displacement_y", "displacement_z"};
static const char *const diagnostic_field_names[] = {"displacement_x",
                                                     "displacement_y",
                                                     "displacement_z",
                                                     "Cauchy_stress_xx",
                                                     "Cauchy_stress_xy",
                                                     "Cauchy_stress_xz",
                                                     "Cauchy_stress_yy",
                                                     "Cauchy_stress_yz",
                                                     "Cauchy_stress_zz",
                                                     "pressure",
                                                     "volumetric_strain",
                                                     "trace_E2",
                                                     "J",
                                                     "strain_energy_density",
                                                     "von_Mises_stress"};
static const char *const quadrature_field_names[] = {"grad u"};
static CeedInt           quadrature_field_sizes[] = {9};

struct RatelModelData_private elasticity_Mooney_Rivlin_initial_data_private = {
    .name                          = "hyperelasticity at finite strain, initial configuration Mooney-Rivlin",
    .type                          = RATEL_MODEL_FEM,
    .setup_q_data_volume           = SetupVolumeGeometry,
    .setup_q_data_volume_loc       = SetupVolumeGeometry_loc,
    .setup_q_data_surface          = SetupSurfaceGeometry,
    .setup_q_data_surface_loc      = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad     = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size            = 10,
    .q_data_surface_size           = 4,
    .q_data_surface_grad_size      = 13,
    .num_active_fields             = 1,
    .active_field_sizes            = active_field_sizes,
    .active_field_names            = active_field_names,
    .active_field_eval_modes       = {CEED_EVAL_GRAD},
    .num_comp_diagnostic           = 15,
    .diagnostic_field_names        = diagnostic_field_names,
    .quadrature_mode               = CEED_GAUSS,
    .residual_u                    = ElasticityResidual_MooneyRivlinInitial,
    .residual_u_loc                = ElasticityResidual_MooneyRivlinInitial_loc,
    .residual_utt                  = ScaledMass,
    .residual_utt_loc              = ScaledMass_loc,
    .num_quadrature_fields         = sizeof(quadrature_field_sizes) / sizeof(*quadrature_field_sizes),
    .quadrature_field_names        = quadrature_field_names,
    .quadrature_field_sizes        = quadrature_field_sizes,
    .jacobian                      = ElasticityJacobian_MooneyRivlinInitial,
    .jacobian_loc                  = ElasticityJacobian_MooneyRivlinInitial_loc,
    .platen_residual_u             = PlatenBCsResidual_MooneyRivlinInitial,
    .platen_residual_u_loc         = PlatenBCsResidual_MooneyRivlinInitial_loc,
    .platen_jacobian               = PlatenBCsJacobian_MooneyRivlinInitial,
    .platen_jacobian_loc           = PlatenBCsJacobian_MooneyRivlinInitial_loc,
    .energy                        = Energy_MooneyRivlinInitial,
    .energy_loc                    = Energy_MooneyRivlinInitial_loc,
    .diagnostic                    = Diagnostic_MooneyRivlinInitial,
    .diagnostic_loc                = Diagnostic_MooneyRivlinInitial_loc,
    .surface_force                 = SurfaceForce_MooneyRivlinInitial,
    .surface_force_loc             = SurfaceForce_MooneyRivlinInitial_loc,
    .flops_qf_jacobian_u           = 767,
    .flops_qf_jacobian_utt         = 9,
};
RatelModelData elasticity_Mooney_Rivlin_initial_data = &elasticity_Mooney_Rivlin_initial_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create RatelMaterial for Mooney-Rivlin hyperelasticity at finite strain in initial configuration

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityMooneyRivlinInitial(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Mooney-Rivlin Initial");

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_Mooney_Rivlin_initial_data));
  material->model_data = elasticity_Mooney_Rivlin_initial_data;

  // QFunction contexts
  PetscCall(RatelMaterialPhysicsContextCreate_MooneyRivlin(material, &material->ctx_physics));
  PetscCall(RatelMaterialPhysicsSmootherDataSetup_MooneyRivlin(material));
  material->RatelMaterialPlatenContextCreate = RatelMaterialPlatenContextCreate_MooneyRivlin;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Mooney-Rivlin Initial Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Register Mooney-Rivlin hyperelasticity at finite strain in initial configuration

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityMooneyRivlinInitial(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityMooneyRivlinInitial);

  PetscFunctionReturn(0);
}

/// @}
