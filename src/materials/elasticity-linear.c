/// @file
/// Linear elastic material using Neo-Hookean model

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <ratel/qfunctions/error/linear-manufactured-error.h>
#include <ratel/qfunctions/forcing/linear-manufactured-force.h>
#include <ratel/qfunctions/geometry/surface-force-geometry.h>
#include <ratel/qfunctions/geometry/surface-geometry.h>
#include <ratel/qfunctions/geometry/volumetric-geometry.h>
#include <ratel/qfunctions/models/elasticity-linear.h>
#include <ratel/qfunctions/scaled-mass.h>

static const PetscInt    active_field_sizes[]     = {3};
static const char *const active_field_names[]     = {"displacement_x", "displacement_y", "displacement_z"};
static const char *const diagnostic_field_names[] = {"displacement_x",
                                                     "displacement_y",
                                                     "displacement_z",
                                                     "Cauchy_stress_xx",
                                                     "Cauchy_stress_xy",
                                                     "Cauchy_stress_xz",
                                                     "Cauchy_stress_yy",
                                                     "Cauchy_stress_yz",
                                                     "Cauchy_stress_zz",
                                                     "pressure",
                                                     "volumetric_strain",
                                                     "trace_E2",
                                                     "J",
                                                     "strain_energy_density",
                                                     "von_Mises_stress"};

/**
  @brief Boundary function for linear elasticity manufactured solution

  @param[in]   dim         Dimension of the mesh
  @param[in]   t           Current time
  @param[in]   coords      Coordinates of point
  @param[in]   num_comp_u  Number of components in solution vector
  @param[out]  u           Solution vector at point
  @param[in]   ctx         Context data

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelBoundaryMMS_ElasticityLinear(PetscInt dim, PetscReal t, const PetscReal coords[], PetscInt num_comp_u, PetscScalar *u,
                                                        void *ctx) {
  const PetscScalar x = coords[0];
  const PetscScalar y = coords[1];
  const PetscScalar z = coords[2];

  PetscFunctionBeginUser;

  u[0] = exp(2 * x) * sin(3 * y) * cos(4 * z) / 1e8 * t;
  u[1] = exp(3 * y) * sin(4 * z) * cos(2 * x) / 1e8 * t;
  u[2] = exp(4 * z) * sin(2 * x) * cos(3 * y) / 1e8 * t;

  PetscFunctionReturn(0);
}

struct RatelModelData_private elasticity_linear_data_private = {
    .name                          = "linear elasticity",
    .type                          = RATEL_MODEL_FEM,
    .setup_q_data_volume           = SetupVolumeGeometry,
    .setup_q_data_volume_loc       = SetupVolumeGeometry_loc,
    .setup_q_data_surface          = SetupSurfaceGeometry,
    .setup_q_data_surface_loc      = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad     = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size            = 10,
    .q_data_surface_size           = 4,
    .q_data_surface_grad_size      = 13,
    .num_active_fields             = 1,
    .active_field_sizes            = active_field_sizes,
    .active_field_names            = active_field_names,
    .active_field_eval_modes       = {CEED_EVAL_GRAD},
    .num_comp_diagnostic           = 15,
    .diagnostic_field_names        = diagnostic_field_names,
    .quadrature_mode               = CEED_GAUSS,
    .residual_u                    = ElasticityResidual_Linear,
    .residual_u_loc                = ElasticityResidual_Linear_loc,
    .residual_utt                  = ScaledMass,
    .residual_utt_loc              = ScaledMass_loc,
    .num_quadrature_fields         = 0,
    .jacobian                      = ElasticityJacobian_Linear,
    .jacobian_loc                  = ElasticityJacobian_Linear_loc,
    .energy                        = Energy_Linear,
    .energy_loc                    = Energy_Linear_loc,
    .diagnostic                    = Diagnostic_Linear,
    .diagnostic_loc                = Diagnostic_Linear_loc,
    .surface_force                 = SurfaceForce_Linear,
    .surface_force_loc             = SurfaceForce_Linear_loc,
    .mms_error                     = MMSError_Linear,
    .mms_error_loc                 = MMSError_Linear_loc,
    .mms_forcing                   = MMSForce_Linear,
    .mms_forcing_loc               = MMSForce_Linear_loc,
    .flops_qf_jacobian_u           = 176,
    .flops_qf_jacobian_utt         = 9,
    .BoundaryMMS                   = {RatelBoundaryMMS_ElasticityLinear},
};
RatelModelData elasticity_linear_data = &elasticity_linear_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create RatelMaterial for linear elasticity

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityLinear(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Linear");

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_linear_data));
  material->model_data = elasticity_linear_data;

  // QFunction contexts
  PetscCall(RatelMaterialPhysicsContextCreate_NeoHookean(material, &material->ctx_physics));
  PetscCall(RatelMaterialPhysicsSmootherDataSetup_NeoHookean(material));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Linear Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Register linear elastic model

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityLinear(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityLinear);

  PetscFunctionReturn(0);
}

/// @}
