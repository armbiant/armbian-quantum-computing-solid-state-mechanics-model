/// @file
/// Setup Ratel models

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <ratel-boundary.h>
#include <ratel-fem.h>
#include <ratel-material.h>
#include <ratel.h>

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create a RatelMaterial object

  @param[in]   ratel          Ratel context
  @param[in]   material_name  Material name for command line prefix
  @param[out]  material       Address to store newly created RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate(Ratel ratel, const char *material_name, RatelMaterial *material) {
  PetscErrorCode (*MaterialCreate)(Ratel, RatelMaterial);

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create");

  // Common setup
  PetscCall(PetscCalloc1(1, material));
  (*material)->ratel = ratel;
  {
    // Material name
    char  *name_copy;
    size_t name_len = strlen(material_name) + 1;

    PetscCall(PetscCalloc1(name_len, &name_copy));
    PetscCall(PetscStrncpy(name_copy, material_name, name_len));
    (*material)->name = name_copy;
  }
  {
    // CL options
    char *cl_prefix, *cl_message, *model_name[1];

    PetscCall(RatelMaterialGetCLPrefix(*material, &cl_prefix));
    PetscCall(RatelMaterialGetCLMessage(*material, &cl_message));
    PetscOptionsBegin(ratel->comm, cl_prefix, cl_message, NULL);

    // -- Material model
    PetscInt max_strings = 1;
    PetscCall(PetscOptionsStringArray("-model", "Material model", NULL, model_name, &max_strings, NULL));
    PetscCall(PetscFunctionListFind(ratel->material_create_functions, model_name[0], &MaterialCreate));
    if (!MaterialCreate) SETERRQ(ratel->comm, 1, "Model data '%s' for material '%s' not found", model_name[0], material_name);
    PetscCall(PetscFree(model_name[0]));

    // -- Name
    PetscBool volume_label_name_set = PETSC_FALSE;
    PetscCall(PetscOptionsStringArray("-label_name", "Mesh partitioning name for material", NULL, &(*material)->volume_label_name, &max_strings,
                                      &volume_label_name_set));

    // -- Use default, if needed
    if (!volume_label_name_set) {
      char       *volume_label_name_copy;
      const char *volume_label_name     = "Cell Sets";
      size_t      volume_label_name_len = strlen(volume_label_name) + 1;

      PetscCall(PetscCalloc1(volume_label_name_len, &volume_label_name_copy));
      PetscCall(PetscStrncpy(volume_label_name_copy, volume_label_name, volume_label_name_len));
      (*material)->volume_label_name = volume_label_name_copy;
    }
    // -- Check for mismatch with previous label, if any
    if (ratel->material_volume_label_name) {
      size_t    name_len    = strlen(ratel->material_volume_label_name);
      PetscBool names_match = PETSC_TRUE;

      PetscCall(PetscStrncmp((*material)->volume_label_name, ratel->material_volume_label_name, name_len, &names_match));
      if (!names_match) {
        // LCOV_EXCL_START
        SETERRQ(ratel->comm, PETSC_ERR_SUP, "Label name set for material %s different than previous name set", material_name);
        // LCOV_EXCL_STOP
      }
    } else {
      char  *volume_label_name_copy;
      size_t volume_label_name_len = strlen((*material)->volume_label_name) + 1;

      PetscCall(PetscCalloc1(volume_label_name_len, &volume_label_name_copy));
      PetscCall(PetscStrncpy(volume_label_name_copy, (*material)->volume_label_name, volume_label_name_len));
      ratel->material_volume_label_name = volume_label_name_copy;
    }

    // -- Label values
    PetscBool volume_label_values_set    = PETSC_FALSE;
    (*material)->num_volume_label_values = RATEL_MAX_MATERIAL_LABEL_VALUES;
    PetscCalloc((*material)->num_volume_label_values, &(*material)->volume_label_values);
    PetscOptionsIntArray("-label_value", "Mesh partitioning values for multi-materials", NULL, (*material)->volume_label_values,
                         &(*material)->num_volume_label_values, &volume_label_values_set);
    if (!volume_label_values_set) {
      if (ratel->num_materials > 1) {
        // LCOV_EXCL_START
        SETERRQ(ratel->comm, PETSC_ERR_SUP, "Label values must be set for %s material!", material_name);
        // LCOV_EXCL_STOP
      } else {
        (*material)->num_volume_label_values = 1;
        (*material)->volume_label_values[0]  = 3;
      }
    }

    PetscOptionsEnd();
    PetscCall(PetscFree(cl_prefix));
    PetscCall(PetscFree(cl_message));
  }
  // Set flag values
  (*material)->residual_index                          = -1;
  (*material)->num_face_orientations                   = -1;
  (*material)->num_surface_grad_dm_face_ids            = -1;
  (*material)->num_surface_grad_diagnostic_dm_face_ids = -1;

  // Material specific setup
  PetscCall((*MaterialCreate)(ratel, *material));

  // Model type specific setup
  switch ((*material)->model_data->type) {
    case RATEL_MODEL_FEM:
      PetscCall(RatelMaterialCreate_FEM(ratel, *material));
      break;
    // LCOV_EXCL_START
    default:
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Model %s not implemented for material %s", (*material)->model_data->name, (*material)->name);
      // LCOV_EXCL_STOP
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Success!");
  PetscFunctionReturn(0);
}

/**
  @brief View a RatelMaterial object

  @param[in]  material  RatelMaterial to view
  @param[in]  viewer    Visualization context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialView(RatelMaterial material, PetscViewer viewer) {
  const char *material_name, *model_name = NULL;

  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material View");

  // Get names
  PetscCall(RatelMaterialGetMaterialName(material, &material_name));
  PetscCall(RatelMaterialGetModelName(material, &model_name));

  // View
  PetscCall(PetscViewerASCIIPrintf(viewer, "    Ratel Material:\n"));
  PetscCall(PetscViewerASCIIPrintf(viewer, "      Name: %s\n", material_name));
  PetscCall(PetscViewerASCIIPrintf(viewer, "      Model: %s\n", model_name));

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material View Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Destroy a RatelMaterial object

  @param[in,out]  material  RatelMaterial to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialDestroy(RatelMaterial *material) {
  Ratel ratel = (*material)->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Destroy");

  // PETSc data
  PetscCall(PetscFree((*material)->name));
  PetscCall(PetscFree((*material)->jacobian_indices));

  // DM labels
  PetscCall(PetscFree((*material)->volume_label_values));
  PetscCall(PetscFree((*material)->volume_label_name));
  PetscCall(PetscFree((*material)->surface_grad_dm_face_ids));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_dm_face_ids; i++) PetscCall(PetscFree((*material)->surface_grad_label_names[i]));
  PetscCall(PetscFree((*material)->surface_grad_label_names));
  PetscCall(PetscFree((*material)->surface_grad_diagnostic_dm_face_ids));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_diagnostic_dm_face_ids; i++) {
    PetscCall(PetscFree((*material)->surface_grad_diagnostic_label_names[i]));
  }
  PetscCall(PetscFree((*material)->surface_grad_diagnostic_label_names));

  // Multigrid setup functions array
  PetscCall(PetscFree((*material)->RatelMaterialSetupMultigridLevels));

  // Smoother data
  for (PetscInt i = 0; i < (*material)->num_physics_smoother_values; i++) PetscCall(PetscFree((*material)->ctx_physics_label_names[i]));
  PetscCall(PetscFree((*material)->ctx_physics_values));
  PetscCall(PetscFree((*material)->ctx_physics_smoother_values));
  PetscCall(PetscFree((*material)->ctx_physics_label_names));
  PetscCall(PetscFree((*material)->ctx_physics_labels));

  // libCEED data
  RatelCeedCall(ratel, CeedQFunctionContextDestroy(&(*material)->ctx_physics));
  RatelCeedCall(ratel, CeedVectorDestroy(&(*material)->q_data_volume));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&(*material)->restriction_q_data_volume));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_diagnostic_dm_face_ids; i++) {
    for (PetscInt j = 0; j < (*material)->num_face_orientations; j++) {
      RatelCeedCall(ratel, CeedVectorDestroy(&(*material)->q_data_surface_grad_diagnostic[i][j]));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&(*material)->restrictions_q_data_surface_grad_diagnostic[i][j]));
    }
    PetscCall(PetscFree((*material)->q_data_surface_grad_diagnostic[i]));
    PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad_diagnostic[i]));
  }
  PetscCall(PetscFree((*material)->q_data_surface_grad_diagnostic));
  PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad_diagnostic));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_dm_face_ids; i++) {
    for (PetscInt j = 0; j < (*material)->num_face_orientations; j++) {
      RatelCeedCall(ratel, CeedVectorDestroy(&(*material)->q_data_surface_grad[i][j]));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&(*material)->restrictions_q_data_surface_grad[i][j]));
    }
    PetscCall(PetscFree((*material)->q_data_surface_grad[i]));
    PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad[i]));
  }
  PetscCall(PetscFree((*material)->q_data_surface_grad));
  PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad));

  // Struct itself
  PetscCall(PetscFree(*material));
  *material = NULL;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Destroy Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get number of components for a RatelMaterial object

  @param[in]   material            RatelMaterial context
  @param[out]  num_active_fields   Number of components in RatelMaterial
  @param[out]  active_field_sizes  Number of components in RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetActiveFieldSizes(RatelMaterial material, PetscInt *num_active_fields, const PetscInt **active_field_sizes) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Active Field Sizes");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  if (num_active_fields) *num_active_fields = material->model_data->num_active_fields;
  if (active_field_sizes) *active_field_sizes = material->model_data->active_field_sizes;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Active Field Sizes Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get number of diagnostic components for a RatelMaterial object

  @param[in]   material        RatelMaterial context
  @param[out]  num_components  Number of diagnostic components in RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetNumDiagnosticComponents(RatelMaterial material, PetscInt *num_components) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Number of Diagnostic Components");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *num_components = material->model_data->num_comp_diagnostic;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Number of Diagnostic Components Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get number of state fields for a RatelMaterial object

  @param[in]   material          RatelMaterial context
  @param[out]  num_state_fields  Number of state fields in RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetNumStateFields(RatelMaterial material, PetscInt *num_state_fields) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Number of State Fields");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *num_state_fields = material->model_data->num_state_fields;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Number of State Fields Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get active field names for a RatelMaterial object

  @param[in]   material     RatelMaterial context
  @param[out]  field_names  Names of fields in RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetActiveFieldNames(RatelMaterial material, const char ***field_names) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Field Names");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *field_names = (const char **)material->model_data->active_field_names;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Field Names Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get diagnostic field names for a RatelMaterial object

  @param[in]   material     RatelMaterial context
  @param[out]  field_names  Names of diagnostic fields in RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetDiagnosticFieldNames(RatelMaterial material, const char ***field_names) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Diagnostic Field Names");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *field_names = (const char **)material->model_data->diagnostic_field_names;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Diagnostic Field Names Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get state field names for a RatelMaterial object

  @param[in]   material     RatelMaterial context
  @param[out]  field_names  Names of state fields in RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetStateFieldNames(RatelMaterial material, const char ***field_names) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get State Field Names");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *field_names = (const char **)material->model_data->state_field_names;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get State Field Names Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get volume label values for a RatelMaterial object

  @param[in]   material          RatelMaterial context
  @param[out]  num_label_values  Number of label values for RatelMaterial
  @param[out]  label_values      Volume label values for RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetLabelValues(RatelMaterial material, PetscInt *num_label_values, PetscInt **label_values) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Label Values");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  if (num_label_values) *num_label_values = material->num_volume_label_values;
  *label_values = material->volume_label_values;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Label Values Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get RatelModelType for a RatelMaterial object

  @param[in]   material    RatelMaterial context
  @param[out]  model_type  RatelModelType for RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetModelType(RatelMaterial material, RatelModelType *model_type) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Model Type");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *model_type = material->model_data->type;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Model Type Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get material name for a RatelMaterial, or NULL if none set

  @param[in]   material       RatelMaterial context
  @param[out]  material_name  Char array holding model name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetMaterialName(RatelMaterial material, const char **material_name) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Material Name");

  *material_name = material->name;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Material Name Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get model name for a RatelMaterial

  @param[in]   material    RatelMaterial context
  @param[out]  model_name  Char array holding model name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetModelName(RatelMaterial material, const char **model_name) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Model Name");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *model_name = material->model_data->name;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Model Name Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get label name for solution DM for a RatelMaterial by face number

  @param[in]   material    RatelMaterial context
  @param[in]   dm_face     DMPlex face number
  @param[out]  label_name  Char array holding label name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Surface Gradient Label Name");

  // Check for labels
  if (material->num_surface_grad_dm_face_ids == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "DMPlex face labels not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_dm_face_ids[j] == dm_face) i = j;
  }
  if (i == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
            material->name);
    // LCOV_EXCL_STOP
  }
  *label_name = material->surface_grad_label_names[i];

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Surface Gradient Label Name Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get label name for diagnostic DM for a RatelMaterial by face number

  @param[in]   material    RatelMaterial context
  @param[in]   dm_face     DMPlex face number
  @param[out]  label_name  Char array holding label name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Surface Gradient Diagnostic Label Name");

  // Check for labels
  if (material->num_surface_grad_dm_face_ids == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "DMPlex face labels not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_diagnostic_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_diagnostic_dm_face_ids[j] == dm_face) i = j;
  }
  if (i == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
            material->name);
    // LCOV_EXCL_STOP
  }
  *label_name = material->surface_grad_diagnostic_label_names[i];

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Surface Gradient Diagnostic Label Name Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Check if RatelMaterial is on current process

  @param[in]   material       RatelMaterial context
  @param[out]  is_on_process  Boolean flag indicating if RatelMaterial is on current process

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialIsOnProcess(RatelMaterial material, PetscBool *is_on_process) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material is on Process");

  if (!material->is_on_process_set) {
    DMLabel  domain_label;
    PetscInt height = 0, first_point;
    PetscInt num_label_values, *label_values = NULL;

    PetscCall(RatelMaterialGetLabelValues(material, &num_label_values, &label_values));
    PetscCall(DMGetLabel(ratel->dm_orig, ratel->material_volume_label_name, &domain_label));
    PetscCall(DMGetFirstLabeledPoint(ratel->dm_hierarchy[0], ratel->dm_hierarchy[0], domain_label, 1, label_values, height, &first_point, NULL));

    material->is_on_process     = first_point != -1;
    material->is_on_process_set = PETSC_TRUE;
  }
  *is_on_process = material->is_on_process;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material is on Process Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get RatelModelType for a RatelMaterial object

  @param[in]   material  RatelMaterial context
  @param[out]  has_mms   Boolean flag indicating if RatelMaterial has MMS solution

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialHasMMS(RatelMaterial material, PetscBool *has_mms) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Has MMS");

  if (!material->model_data) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s", material->name);
    // LCOV_EXCL_STOP
  }
  *has_mms = !!material->model_data->mms_error;

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Has MMS Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get command line option prefix for a RatelMaterial object

  @param[in]   material   RatelMaterial context
  @param[out]  cl_prefix  Command line option prefix for RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetCLPrefix(RatelMaterial material, char **cl_prefix) {
  size_t    name_len   = strlen(material->name);
  PetscBool has_name   = name_len > 0;
  size_t    prefix_len = (has_name ? name_len + 1 : 0) + 1;

  PetscFunctionBeginUser;

  PetscCall(PetscCalloc1(prefix_len, cl_prefix));
  PetscCall(PetscSNPrintf(*cl_prefix, prefix_len, "%s%s", has_name ? material->name : "", has_name ? "_" : ""));
  RatelDebug(material->ratel, "---- RatelMaterial options prefix: %s", *cl_prefix);

  PetscFunctionReturn(0);
}

/**
  @brief Get command line option message for a RatelMaterial object

  @param[in]   material    RatelMaterial context
  @param[out]  cl_message  Command line option message for RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetCLMessage(RatelMaterial material, char **cl_message) {
  size_t      name_len      = strlen(material->name);
  PetscBool   has_name      = name_len > 0;
  const char *message_start = "Material model options";
  size_t      message_len   = strlen(message_start) + (has_name ? name_len : 0) + 1;

  PetscFunctionBeginUser;

  PetscCall(PetscCalloc1(message_len, cl_message));
  PetscCall(PetscSNPrintf(*cl_message, message_len, "%s%s%s", message_start, has_name ? " for " : "", has_name ? material->name : ""));
  RatelDebug(material->ratel, "---- Material options message: %s", *cl_message);

  PetscFunctionReturn(0);
}

/**
  @brief Build CeedOperator name for material
           Note: Caller is responsible for freeing the allocated name string with
                 PetscFree().

  @param[in]   material       RatelMaterial context
  @param[in]   base_name      String holding base name for operator
  @param[out]  operator_name  String holding operator name for RatelMaterial

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreateOperatorName(RatelMaterial material, const char *base_name, char **operator_name) {
  PetscInt  name_len          = strlen(material->name);
  PetscBool has_name          = name_len > 0;
  size_t    operator_name_len = strlen(base_name) + (has_name ? 5 : 0) + name_len + 1;

  PetscFunctionBeginUser;

  PetscCall(PetscCalloc1(operator_name_len, operator_name));
  PetscCall(PetscSNPrintf(*operator_name, operator_name_len, "%s%s%s", base_name, has_name ? " for " : "", has_name ? material->name : ""));
  RatelDebug(material->ratel, "---- Material operator name: %s", *operator_name);

  PetscFunctionReturn(0);
}

/**
  @brief Get CeedElemRestriction and CeedBasis objects corresponding to Residual evaluation

  @param[in]   material      RatelMaterial context
  @param[out]  restrictions  CeedElemRestrictions corresponding to Residual evaluation
  @param[out]  bases         CeedBases corresponding to Residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSolutionData(RatelMaterial material, PetscInt *num_active_fields, CeedElemRestriction **restrictions,
                                            CeedBasis **bases) {
  Ratel               ratel = material->ratel;
  CeedElemRestriction restriction;
  CeedBasis           basis;
  CeedOperator       *sub_ops;
  CeedOperatorField  *input_fields;

  PetscFunctionBeginUser;

  // Check if Residual setup
  if (material->residual_index == -1) SETERRQ(ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator not set up for material %s", material->name);

  // Allocate arrays
  *num_active_fields = material->model_data->num_active_fields;
  PetscCall(PetscCalloc1(*num_active_fields, restrictions));
  PetscCall(PetscCalloc1(*num_active_fields, bases));

  // Retrieve objects and copy reference
  RatelCeedCall(ratel, CeedCompositeOperatorGetSubList(material->ratel->ctx_residual_u->op, &sub_ops));
  RatelCeedCall(ratel, CeedOperatorGetFields(sub_ops[material->residual_index], NULL, &input_fields, NULL, NULL));
  for (PetscInt i = 0; i < *num_active_fields; i++) {
    RatelCeedCall(ratel, CeedOperatorFieldGetElemRestriction(input_fields[i + 1], &restriction));
    RatelCeedCall(ratel, CeedElemRestrictionReferenceCopy(restriction, &(*restrictions)[i]));
    RatelCeedCall(ratel, CeedOperatorFieldGetBasis(input_fields[i + 1], &basis));
    RatelCeedCall(ratel, CeedBasisReferenceCopy(basis, &(*bases)[i]));
  }

  PetscFunctionReturn(0);
}

/**
  @brief Build CeedOperator name for material

  @param[in]      material   RatelMaterial context
  @param[in]      base_name  String holding base name for operator
  @param[in,out]  op         CeedOperator to set the name for

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetOperatorName(RatelMaterial material, const char *base_name, CeedOperator op) {
  char *operator_name;

  PetscFunctionBeginUser;

  PetscCall(RatelMaterialCreateOperatorName(material, base_name, &operator_name));
  RatelCeedCall(material->ratel, CeedOperatorSetName(op, operator_name));
  PetscCall(PetscFree(operator_name));

  PetscFunctionReturn(0);
}

/**
  @brief Get CeedOperator volumetric QData for RatelMaterial

  @param[in]   material     RatelMaterial context
  @param[out]  restriction  CeedElemRestriction for QData
  @param[out]  q_data       CeedVector holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetVolumeQData(RatelMaterial material, CeedElemRestriction *restriction, CeedVector *q_data) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Volumetric QData");

  if (!material->q_data_volume) {
    PetscCall((*material->SetupVolumeQData)(material, material->volume_label_name, material->volume_label_values[0],
                                            &material->restriction_q_data_volume, &material->q_data_volume));
  }
  RatelCeedCall(material->ratel, CeedElemRestrictionReferenceCopy(material->restriction_q_data_volume, restriction));
  RatelCeedCall(material->ratel, CeedVectorReferenceCopy(material->q_data_volume, q_data));

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Volumetric QData Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get CeedOperator surface gradient QData for RatelMaterial

  @param[in]   material     RatelMaterial context
  @param[in]   dm_face      DMPlex face number
  @param[in]   orientation  Face orientation number, giving DMLabel value
  @param[out]  restriction  CeedElemRestriction for QData
  @param[out]  q_data       CeedVector holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientQData(RatelMaterial material, PetscInt dm_face, PetscInt orientation, CeedElemRestriction *restriction,
                                                    CeedVector *q_data) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Surface Gradient QData");

  // Check for labels
  if (material->num_surface_grad_dm_face_ids == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "DMPlex face labels not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Check for implementation
  if (!material->SetupSurfaceGradientQData) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Surface QData not supported for material model %s", material->model_data->name);
    // LCOV_EXCL_STOP
  }

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_dm_face_ids[j] == dm_face) i = j;
  }
  if (i == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
            material->name);
    // LCOV_EXCL_STOP
  }

  // Get face QData
  if (material->surface_grad_label_names[i]) {
    if (!material->q_data_surface_grad[i][orientation]) {
      PetscCall((*material->SetupSurfaceGradientQData)(material, material->ratel->dm_hierarchy[0], material->surface_grad_label_names[i], orientation,
                                                       &material->restrictions_q_data_surface_grad[i][orientation],
                                                       &material->q_data_surface_grad[i][orientation]));
    }
    RatelCeedCall(material->ratel, CeedElemRestrictionReferenceCopy(material->restrictions_q_data_surface_grad[i][orientation], restriction));
    RatelCeedCall(material->ratel, CeedVectorReferenceCopy(material->q_data_surface_grad[i][orientation], q_data));
  } else {
    // LCOV_EXCL_START
    *restriction = NULL;
    *q_data      = NULL;
    RatelDebug(material->ratel, "Warn: Label not found for index %" PetscInt_FMT " and face %" PetscInt_FMT, i, dm_face);
    // LCOV_EXCL_STOP
  }

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Surface Gradient QData Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Get CeedOperator diagnostic surface gradient QData for RatelMaterial

  @param[in]   material     RatelMaterial context
  @param[in]   dm_face      DMPlex face number
  @param[in]   orientation  Face orientation number, giving DMLabel value
  @param[out]  restriction  CeedElemRestriction for QData
  @param[out]  q_data       CeedVector holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticQData(RatelMaterial material, PetscInt dm_face, PetscInt orientation,
                                                              CeedElemRestriction *restriction, CeedVector *q_data) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Diagnostic Surface Gradient QData");

  // Check for labels
  if (material->num_surface_grad_dm_face_ids == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "DMPlex face labels not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Check for implementation
  if (!material->SetupSurfaceGradientQData) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Surface QData not supported for material model %s", material->model_data->name);
    // LCOV_EXCL_STOP
  }

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_diagnostic_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_diagnostic_dm_face_ids[j] == dm_face) i = j;
  }
  if (i == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
            material->name);
    // LCOV_EXCL_STOP
  }

  // Get face QData
  if (material->surface_grad_diagnostic_label_names[i]) {
    if (!material->q_data_surface_grad_diagnostic[i][orientation]) {
      PetscCall((*material->SetupSurfaceGradientQData)(
          material, material->ratel->dm_surface_displacement, material->surface_grad_diagnostic_label_names[i], orientation,
          &material->restrictions_q_data_surface_grad_diagnostic[i][orientation], &material->q_data_surface_grad_diagnostic[i][orientation]));
    }
    RatelCeedCall(material->ratel,
                  CeedElemRestrictionReferenceCopy(material->restrictions_q_data_surface_grad_diagnostic[i][orientation], restriction));
    RatelCeedCall(material->ratel, CeedVectorReferenceCopy(material->q_data_surface_grad_diagnostic[i][orientation], q_data));
  } else {
    // LCOV_EXCL_START
    *restriction = NULL;
    *q_data      = NULL;
    // LCOV_EXCL_STOP
  }

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Get Diagnostic Surface Gradient QData Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup Residual and Jacobian CeedOperators for RatelMaterial

  @param[in]   material         RatelMaterial context
  @param[out]  op_residual_u    Composite residual u term CeedOperator to add RatelMaterial suboperator
  @param[out]  op_residual_ut   Composite residual u_t term CeedOperator to add RatelMaterial suboperator
  @param[out]  op_residual_utt  Composite residual u_tt term CeedOperator to add RatelMaterial suboperator
  @param[out]  op_jacobian      Composite Jacobian CeedOperator to add RatelMaterial suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupResidualJacobianSuboperators(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                              CeedOperator op_residual_utt, CeedOperator op_jacobian) {
  CeedInt residual_index, jacobian_index;

  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Residual and Jacobian Suboperators");

  // Skip if not on process
  {
    PetscBool is_on_process = PETSC_TRUE;

    PetscCall(RatelMaterialIsOnProcess(material, &is_on_process));
    if (!is_on_process) PetscFunctionReturn(0);
  }

  // Set operator indices
  RatelCeedCall(material->ratel, CeedCompositeOperatorGetNumSub(op_residual_u, &residual_index));
  material->residual_index       = residual_index;
  material->num_jacobian_indices = 1;
  PetscCall(PetscCalloc1(material->num_jacobian_indices, &material->jacobian_indices));
  RatelCeedCall(material->ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &jacobian_index));
  material->jacobian_indices[0] = jacobian_index;
  PetscCall(PetscCalloc1(material->num_jacobian_indices, &material->RatelMaterialSetupMultigridLevels));
  material->RatelMaterialSetupMultigridLevels[0] = RatelMaterialSetupJacobianMultigridLevel_FEM;

  // Setup suboperators
  PetscCall((*material->SetupResidualJacobianSuboperators)(material, op_residual_u, op_residual_ut, op_residual_utt, op_jacobian));

  // Smoother values
  if (material->num_physics_smoother_values > 0) {
    CeedOperator *sub_operators;

    RatelCeedCall(material->ratel, CeedCompositeOperatorGetSubList(op_jacobian, &sub_operators));
    PetscCall(PetscCalloc1(material->num_physics_smoother_values, &material->ctx_physics_labels));
    for (PetscInt i = 0; i < material->num_physics_smoother_values; i++) {
      RatelCeedCall(material->ratel, CeedOperatorContextGetFieldLabel(sub_operators[jacobian_index], material->ctx_physics_label_names[i],
                                                                      &material->ctx_physics_labels[i]));
    }
  }

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Residual and Jacobian Suboperators Sucess!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup Residual and Jacobian CeedOperators for RatelMaterial

  @param[in]   material         RatelMaterial context
  @param[out]  op_residual_u    Composite residual u term CeedOperator to add RatelMaterial suboperator
  @param[out]  op_jacobian      Composite Jacobian CeedOperator to add RatelMaterial suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupPlatenSuboperators(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_jacobian) {
  CeedInt old_num_sub_operators, new_num_sub_operators, num_jacobian_indices, num_platen_jacobian;

  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Platen Suboperators");

  // Skip if not on process
  {
    PetscBool is_on_process = PETSC_TRUE;

    PetscCall(RatelMaterialIsOnProcess(material, &is_on_process));
    if (!is_on_process) PetscFunctionReturn(0);
  }

  RatelCeedCall(material->ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &old_num_sub_operators));

  // Setup platen suboperators
  PetscCall(RatelMaterialCeedAddBoundariesPlaten(material, op_residual_u, op_jacobian));

  // Get platen BC Jacobian suboperator index
  RatelCeedCall(material->ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &new_num_sub_operators));

  num_platen_jacobian = new_num_sub_operators - old_num_sub_operators;
  if (num_platen_jacobian < 0) {
    num_jacobian_indices = material->num_jacobian_indices + num_platen_jacobian;
    // Reallocate arrays
    {
      PetscSizeT int_size = sizeof(PetscInt);
      PetscCall(PetscRealloc(num_jacobian_indices * int_size, &material->jacobian_indices));
      PetscCall(
          PetscRealloc(num_jacobian_indices * sizeof(*material->RatelMaterialSetupMultigridLevels), &material->RatelMaterialSetupMultigridLevels));
      if (!material->ratel->jacobian_multiplicity_skip_indices) {
        material->ratel->num_jacobian_multiplicity_skip_indices = 0;
        PetscCall(PetscCalloc1(num_platen_jacobian, &material->ratel->jacobian_multiplicity_skip_indices));
      } else {
        PetscInt num_skip = material->ratel->num_jacobian_multiplicity_skip_indices + num_platen_jacobian;
        PetscCall(PetscRealloc(num_skip * int_size, &material->ratel->jacobian_multiplicity_skip_indices));
      }
      if (material->num_physics_smoother_values > 0) {
        PetscInt num_labels = num_jacobian_indices * material->num_physics_smoother_values;
        PetscCall(PetscRealloc(num_labels * sizeof(*material->ctx_physics_labels), &material->ctx_physics_labels));
      }
    }
    CeedOperator *sub_operators;
    RatelCeedCall(material->ratel, CeedCompositeOperatorGetSubList(op_jacobian, &sub_operators));

    for (PetscInt jacobian_index = old_num_sub_operators; jacobian_index < new_num_sub_operators; jacobian_index++) {
      material->jacobian_indices[material->num_jacobian_indices]                                                   = jacobian_index;
      material->RatelMaterialSetupMultigridLevels[material->num_jacobian_indices]                                  = NULL;
      // Set Jacobian multiplicity skip index
      material->ratel->jacobian_multiplicity_skip_indices[material->ratel->num_jacobian_multiplicity_skip_indices] = jacobian_index;
      // Smoother values
      if (material->num_physics_smoother_values > 0) {
        for (PetscInt j = 0; j < material->num_physics_smoother_values; j++) {
          PetscInt label_index = material->num_jacobian_indices * material->num_physics_smoother_values + j;
          RatelCeedCall(material->ratel, CeedOperatorContextGetFieldLabel(sub_operators[jacobian_index], material->ctx_physics_label_names[j],
                                                                          &material->ctx_physics_labels[label_index]));
        }
      }
      material->num_jacobian_indices++;
      material->ratel->num_jacobian_multiplicity_skip_indices++;
    }
  }

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Platen Suboperators Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid level CeedOperators for RatelMaterial

  @param[in]      material            RatelMaterial context
  @param[in]      level               Multigrid level to setup
  @param[in]      m_loc               libCEED vector holding multiplicity
  @param[in]      op_jacobian_fine    Composite Jacobian CeedOperator holding fine grid suboperators
  @param[in,out]  op_jacobian_coarse  Composite Jacobian CeedOperator to add RatelMaterial suboperators
  @param[in,out]  op_prolong          Composite prolongation CeedOperator to add RatelMaterial suboperators
  @param[in,out]  op_restrict         Composite restriction CeedOperator to add RatelMaterial suboperators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMultigridLevel(RatelMaterial material, PetscInt level, CeedVector m_loc, CeedOperator op_jacobian_fine,
                                                CeedOperator op_jacobian_coarse, CeedOperator op_prolong, CeedOperator op_restrict) {
  CeedOperator *sub_operators;

  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Multigrid Level");

  // Skip if not on process
  if (!material->is_on_process) PetscFunctionReturn(0);

  // Check for residual setup
  if (material->residual_index == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Setup suboperators
  RatelCeedCall(material->ratel, CeedCompositeOperatorGetSubList(op_jacobian_fine, &sub_operators));
  for (PetscInt i = 0; i < material->num_jacobian_indices; i++) {
    if (material->RatelMaterialSetupMultigridLevels[i] != NULL) {
      PetscCall((*material->RatelMaterialSetupMultigridLevels[i])(material, level, m_loc, sub_operators[material->jacobian_indices[i]],
                                                                  op_jacobian_coarse, op_prolong, op_restrict));
    }
  }

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Multigrid Level Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Set smoother contexts for Jacobian operators for RatelMaterial

  @param[in]      material      RatelMaterial context
  @param[in]      set_or_unset  Boolean flag to set (TRUE) or unset (FALSE) the smoother context
  @param[in,out]  op_jacobian   Composite Jacobian CeedOperator to update
  @param[out]     was_set       Boolean flag indicating if any smoother context was set.

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetJacobianSootherContext(RatelMaterial material, PetscBool set_or_unset, CeedOperator op_jacobian, PetscBool *was_set) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;

  if (material->num_physics_smoother_values > 0) {
    CeedOperator *sub_operators;

    RatelCeedCall(ratel, CeedCompositeOperatorGetSubList(op_jacobian, &sub_operators));

    // Loop over all Jacobians set by this material
    for (PetscInt i = 0; i < material->num_jacobian_indices; i++) {
      for (PetscInt j = 0; j < material->num_physics_smoother_values; j++) {
        CeedScalar value = set_or_unset ? material->ctx_physics_smoother_values[j] : material->ctx_physics_values[j];

        RatelCeedCall(ratel, CeedOperatorContextSetDouble(sub_operators[material->jacobian_indices[i]],
                                                          material->ctx_physics_labels[i * material->num_physics_smoother_values + j], &value));
      }
      if (was_set) *was_set = PETSC_TRUE;
    }
  } else {
    if (was_set) *was_set = PETSC_FALSE;
  }

  PetscFunctionReturn(0);
}

/**
  @brief Setup energy CeedOperator for RatelMaterial

  @param[in]   material   RatelMaterial context
  @param[out]  op_energy  Composite energy CeedOperator to add RatelMaterial suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupEnergySuboperator(RatelMaterial material, CeedOperator op_energy) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Energy Suboperator");

  // Skip if not on process
  if (!material->is_on_process) PetscFunctionReturn(0);

  // Skip if no energy QFunction
  if (!material->model_data->energy) PetscFunctionReturn(0);

  // Check for residual setup
  if (material->residual_index == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Setup energy suboperator
  PetscCall((*material->SetupEnergySuboperator)(material, op_energy));

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Energy Suboperator Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup diagnostic value CeedOperator for RatelMaterial

  @param[in]   material            RatelMaterial context
  @param[out]  op_mass_diagnostic  Composite diagnostic value projection CeedOperator to add RatelMaterial suboperator
  @param[out]  op_diagnostic       Composite diagnostic value CeedOperator to add RatelMaterial suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupDiagnosticSuboperators(RatelMaterial material, CeedOperator op_mass_diagnostic, CeedOperator op_diagnostic) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Diagnostic Suboperators");

  // Skip if not on process
  if (!material->is_on_process) PetscFunctionReturn(0);

  // Check for residual setup
  if (material->residual_index == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Setup volume diagnostic value suboperator
  PetscCall((*material->SetupDiagnosticSuboperators)(material, op_mass_diagnostic, op_diagnostic));

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Diagnostic Suboperators Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup surface force CeedOperator for RatelMaterial

  @param[in]   material           RatelMaterial context
  @param[out]  ops_surface_force  Composite surface force CeedOperators to add RatelMaterial suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSurfaceForceSuboperators(RatelMaterial material, CeedOperator *ops_surface_force) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Surface Force Suboperators");

  // Skip if not on process
  if (!material->is_on_process) PetscFunctionReturn(0);

  // Check for residual setup
  if (material->residual_index == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Setup surface force calculation suboperator
  PetscCall((*material->SetupSurfaceForceSuboperators)(material, ops_surface_force));

  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Surface Force Suboperators Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup MMS error CeedOperator for RatelMaterial

  @param[in]   material      RatelMaterial context
  @param[out]  op_mms_error  Composite MMS error CeedOperator to add RatelMaterial suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMMSErrorSuboperator(RatelMaterial material, CeedOperator op_mms_error) {
  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup MMS Error Suboperator");

  // Skip if not on process
  if (!material->is_on_process) PetscFunctionReturn(0);

  // Check for residual setup
  if (material->residual_index == -1) {
    // LCOV_EXCL_START
    SETERRQ(material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s", material->name);
    // LCOV_EXCL_STOP
  }

  // Check for MMS error operator
  if (!material->model_data->mms_error) SETERRQ(material->ratel->comm, PETSC_ERR_SUP, "No MMS solution for material %s", material->model_data->name);

  // Setup MMS error suboperator
  PetscCall((*material->SetupMMSErrorSuboperator)(material, op_mms_error));

  PetscFunctionBeginUser;
  RatelDebug256(material->ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup MMS Error Suboperator Success!");
  PetscFunctionReturn(0);
}

/// @}
