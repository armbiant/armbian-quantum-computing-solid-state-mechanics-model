// List each model registration function once here.
// This will be expanded inside RatelRegisterModels() to call each registration function in the order listed, and also to define weak symbol aliases
// for models that are not configured.

#include <ratel-types.h>
#include <ratel.h>

// CEED BPs
RATEL_MODEL("ceed-bp3", CEED_BP3)
RATEL_MODEL("ceed-bp4", CEED_BP4)

// Elasticity
RATEL_MODEL("elasticity-linear", ElasticityLinear)
RATEL_MODEL("elasticity-neo-hookean-current", ElasticityNeoHookeanCurrent)
RATEL_MODEL("elasticity-neo-hookean-current-ad", ElasticityNeoHookeanCurrentAD)
RATEL_MODEL("elasticity-neo-hookean-initial", ElasticityNeoHookeanInitial)
RATEL_MODEL("elasticity-neo-hookean-initial-ad", ElasticityNeoHookeanInitialAD)
RATEL_MODEL("elasticity-mooney-rivlin-initial", ElasticityMooneyRivlinInitial)
