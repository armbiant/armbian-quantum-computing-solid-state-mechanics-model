/// @file
/// FEM material DM and operator setup

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdm.h>
#include <petscsnes.h>
#include <petscts.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-petsc-ops.h>
#include <ratel-types.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/models/common-parameters.h>
#include <ratel/models/scaled-mass.h>
#include <ratel/qfunctions/mass.h>

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create a RatelMaterial object for a finite element model

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_FEM(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material FEM Create");

  // -- Geometry
  material->SetupVolumeQData          = RatelMaterialSetupVolumeQData_FEM;
  material->SetupSurfaceGradientQData = RatelMaterialSetupSurfaceGradientQData_FEM;

  // -- Residual and Jacobian operators
  material->SetupResidualJacobianSuboperators = RatelMaterialSetupResidualJacobianSuboperators_FEM;

  // -- Output operators
  material->SetupEnergySuboperator        = RatelMaterialSetupEnergySuboperator_FEM;
  material->SetupDiagnosticSuboperators   = RatelMaterialSetupDiagnosticSuboperators_FEM;
  material->SetupSurfaceForceSuboperators = RatelMaterialSetupSurfaceForceSuboperators_FEM;
  material->SetupMMSErrorSuboperator      = RatelMaterialSetupMMSErrorSuboperator_FEM;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Create a RatelMaterial object for a finite element model

  @param[in,out]  ratel  Ratel context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSetup_FEM(Ratel ratel) {
  CeedMemType     mem_type_backend;
  VecType         vec_type;
  PetscInt        num_active_fields = 0, num_comp_diagnostic, num_comp_energy = 1, num_comp_force = 6;
  const PetscInt *active_field_sizes;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DM Setup FEM");

  // Get VecType
  RatelCeedCall(ratel, CeedGetPreferredMemType(ratel->ceed, &mem_type_backend));
  RatelDebug(ratel, "---- Memory type: %s", CeedMemTypes[mem_type_backend]);
  switch (mem_type_backend) {
    case CEED_MEM_HOST:
      vec_type = VECSTANDARD;
      break;
    case CEED_MEM_DEVICE: {
      const char *ceed_resource;

      RatelCeedCall(ratel, CeedGetResource(ratel->ceed, &ceed_resource));
      if (strstr(ceed_resource, "/gpu/cuda")) vec_type = VECCUDA;
      else if (strstr(ceed_resource, "/gpu/hip")) vec_type = VECKOKKOS;
      else vec_type = VECSTANDARD;
    }
  }
  ratel->is_ceed_backend_gpu = mem_type_backend == CEED_MEM_DEVICE;

  // Determine active fields
  {
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
    for (PetscInt i = 1; i < ratel->num_materials; i++) {
      PetscInt        num_active_fields_current = 0;
      const PetscInt *active_field_sizes_current;

      PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[i], &num_active_fields_current, &active_field_sizes_current));
      if (num_active_fields != num_active_fields_current) SETERRQ(ratel->comm, PETSC_ERR_LIB, "Mixed number of active fields not supported");
      for (PetscInt j = 0; j < num_active_fields_current; j++) {
        if (active_field_sizes[j] != active_field_sizes_current[j]) SETERRQ(ratel->comm, PETSC_ERR_LIB, "Mixed active field sizes not supported");
      }
    }
  }
  if (num_active_fields != ratel->num_active_fields) SETERRQ(ratel->comm, PETSC_ERR_LIB, "Insufficient field orders provided");
  // Determine num_comp_diagnostic
  PetscCall(RatelMaterialGetNumDiagnosticComponents(ratel->materials[0], &num_comp_diagnostic));
  for (PetscInt i = 1; i < ratel->num_materials; i++) {
    PetscInt num_comp_diagnostic_current;

    PetscCall(RatelMaterialGetNumDiagnosticComponents(ratel->materials[i], &num_comp_diagnostic_current));
    if (num_comp_diagnostic != num_comp_diagnostic_current) SETERRQ(ratel->comm, PETSC_ERR_LIB, "Mixed number of components not supported");
  }

  // Setup DM
  // -- Parallel distributed DM template for other DMs
  PetscCall(RatelDMCreateFromOptions(ratel, vec_type, &ratel->dm_orig));
  {
    PetscInt cStart, cEnd, count;

    PetscCall(DMPlexGetHeightStratum(ratel->dm_orig, 0, &cStart, &cEnd));
    count = cEnd - cStart;
    PetscCall(MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPIU_INT, MPI_SUM, ratel->comm));
    PetscCheck(count >= 1, ratel->comm, PETSC_ERR_USER, "Cannot use DM with %" PetscInt_FMT " cells; check -dm_plex_filename or -dm_plex_box_faces",
               count);
  }
  PetscCall(DMGetVecType(ratel->dm_orig, &vec_type));

  // -- Volumetric domain labels
  {
    PetscBool has_label = PETSC_FALSE;

    // ---- Check if label exists if not
    PetscCall(DMHasLabel(ratel->dm_orig, ratel->material_volume_label_name, &has_label));
    if (!has_label && ratel->num_materials == 1) {
      // ---- Create label containing all elements
      PetscInt  c_start, c_end;
      PetscInt *label_values;

      PetscCall(DMCreateLabel(ratel->dm_orig, ratel->material_volume_label_name));
      PetscCall(DMPlexGetHeightStratum(ratel->dm_orig, 0, &c_start, &c_end));

      PetscCall(RatelMaterialGetLabelValues(ratel->materials[0], NULL, &label_values));
      for (PetscInt c = c_start; c < c_end; c++) {
        PetscCall(DMSetLabelValue(ratel->dm_orig, ratel->material_volume_label_name, c, label_values[0]));
      }
    } else if (!has_label && ratel->num_materials > 1) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Domain label doesn't exist for multiple materials!");
      // LCOV_EXCL_STOP
    }
  }

  // -- Fine grid residual and Jacobian DM
  RatelDebug(ratel, "---- Residual and Jacobian DM");
  PetscCall(PetscCalloc1(1, &ratel->dm_hierarchy));
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_hierarchy[0]));
  {
    PetscInt field_orders[RATEL_MAX_FIELDS];

    for (PetscInt i = 0; i < ratel->num_active_fields; i++) field_orders[i] = ratel->multigrid_fine_orders[i];
    PetscCall(RatelDMSetupByOrder_FEM(ratel, true, field_orders, RATEL_DECIDE, ratel->q_extra, num_active_fields, active_field_sizes,
                                      ratel->dm_hierarchy[0]));
  }
  PetscCall(DMSetVecType(ratel->dm_hierarchy[0], vec_type));
  {
    MatType   mat_type = MATSHELL;
    char      mat_type_cl[25];
    PetscBool is_mat_type_cl = PETSC_FALSE;

    if (ratel->multigrid_fine_orders[0] == ratel->multigrid_coarse_orders[0]) {
      if (strstr(vec_type, VECCUDA)) mat_type = MATAIJCUSPARSE;
      else if (strstr(vec_type, VECKOKKOS)) mat_type = MATAIJKOKKOS;
      else mat_type = MATAIJ;
    }

    PetscOptionsBegin(ratel->comm, NULL, "", NULL);
    PetscCall(PetscOptionsString("-fine_dm_mat_type", "fine grid DM MatType", NULL, mat_type_cl, mat_type_cl, sizeof(mat_type_cl), &is_mat_type_cl));
    PetscOptionsEnd();

    PetscCall(DMSetMatType(ratel->dm_hierarchy[0], is_mat_type_cl ? mat_type_cl : mat_type));
    PetscCall(DMSetMatrixPreallocateSkip(ratel->dm_hierarchy[0], PETSC_TRUE));
    PetscCall(DMSetOptionsPrefix(ratel->dm_hierarchy[0], "fine_"));

    PetscCall(DMGetMatType(ratel->dm_hierarchy[0], &mat_type));
    RatelDebug(ratel, "------ Fine DM MatType: %s", mat_type);
  }

  // ---- Label field components for viewing
  {
    // Empty name for conserved field (because there is only one field)
    PetscSection section;
    const char **field_names;

    PetscCall(DMGetLocalSection(ratel->dm_hierarchy[0], &section));
    PetscCall(PetscSectionSetFieldName(section, 0, ""));
    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names));
    for (PetscInt i = 0, j = 0; j < num_active_fields; j++) {
      for (PetscInt k = 0; k < active_field_sizes[j]; k++) {
        PetscCall(PetscSectionSetComponentName(section, j, k, field_names[i]));
        i++;
      }
    }
  }

  // ---- Add labels on DM faces for FE face numbers for surface force computation
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    RatelMaterial material = ratel->materials[i];

    material->num_surface_grad_diagnostic_dm_face_ids = ratel->surface_force_face_count;
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->surface_grad_diagnostic_dm_face_ids));
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->surface_grad_diagnostic_label_names));
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->q_data_surface_grad_diagnostic));
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->restrictions_q_data_surface_grad_diagnostic));
    for (PetscInt j = 0; j < ratel->surface_force_face_count; j++) {
      PetscInt num_face_orientations;

      material->surface_grad_diagnostic_dm_face_ids[j] = ratel->surface_force_dm_faces[j];
      PetscCall(RatelDMFaceLabelCreate(ratel, ratel->dm_hierarchy[0], material, material->surface_grad_diagnostic_dm_face_ids[j],
                                       &num_face_orientations, &material->surface_grad_diagnostic_label_names[j]));
      if (material->num_face_orientations == -1 && num_face_orientations != 0) {
        material->num_face_orientations = num_face_orientations;
      } else {
        if (num_face_orientations != 0 && material->num_face_orientations != num_face_orientations) {
          // LCOV_EXCL_START
          SETERRQ(ratel->comm, PETSC_ERR_SUP, "Conflicting numbers of face orientations for material %s", material->name);
          // LCOV_EXCL_STOP
        }
      }
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->q_data_surface_grad_diagnostic[j]));
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->restrictions_q_data_surface_grad_diagnostic[j]));
    }
    // ---- Add labels on DM faces for FE face numbers for platen boundary condition
    material->num_surface_grad_dm_face_ids = ratel->bc_platen_count;
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->surface_grad_dm_face_ids));
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->surface_grad_label_names));
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->q_data_surface_grad));
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->restrictions_q_data_surface_grad));
    for (PetscInt j = 0; j < ratel->bc_platen_count; j++) {
      PetscInt num_face_orientations;

      material->surface_grad_dm_face_ids[j] = ratel->bc_platen_faces[j];
      PetscCall(RatelDMFaceLabelCreate(ratel, ratel->dm_hierarchy[0], material, material->surface_grad_dm_face_ids[j], &num_face_orientations,
                                       &material->surface_grad_label_names[j]));
      if (material->num_face_orientations == -1 && num_face_orientations != 0) {
        material->num_face_orientations = num_face_orientations;
      } else {
        if (num_face_orientations != 0 && material->num_face_orientations != num_face_orientations) {
          // LCOV_EXCL_START
          SETERRQ(ratel->comm, PETSC_ERR_SUP, "Conflicting numbers of face orientations for material %s", material->name);
          // LCOV_EXCL_STOP
        }
      }
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->q_data_surface_grad[j]));
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->restrictions_q_data_surface_grad[j]));
    }
  }
  // ---- Copy label to coordinate DM
  {
    DM dm_coord;

    PetscCall(DMGetCoordinateDM(ratel->dm_hierarchy[0], &dm_coord));
    PetscCall(DMCopyLabels(ratel->dm_hierarchy[0], dm_coord, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
  }

  // ---- Set Ratel as DM application context
  PetscCall(DMSetApplicationContext(ratel->dm_hierarchy[0], ratel));
  PetscCall(DMSetOptionsPrefix(ratel->dm_hierarchy[0], "final_"));
  PetscCall(DMViewFromOptions(ratel->dm_hierarchy[0], NULL, "-dm_view"));

  // -- State field DM
  {
    PetscInt num_state_fields = 0;

    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscInt num_state_fields_current;

      PetscCall(RatelMaterialGetNumStateFields(ratel->materials[i], &num_state_fields_current));
      if (num_state_fields_current != 0) {
        if (num_state_fields != 0 && num_state_fields != num_state_fields_current) {
          // LCOV_EXCL_START
          SETERRQ(ratel->comm, PETSC_ERR_SUP, "Multiple materials with different numbers of state fields not supported");
          // LCOV_EXCL_STOP
        }
        num_state_fields = num_state_fields_current;
      }
    }

    if (num_state_fields != 0) {
      PetscCall(DMClone(ratel->dm_orig, &ratel->dm_state_fields));
      PetscCall(RatelDMSetupByOrder_FEM(ratel, false, &ratel->highest_fine_order, RATEL_DECIDE, RATEL_DECIDE, 1, &num_state_fields,
                                        ratel->dm_state_fields));
      PetscCall(DMSetVecType(ratel->dm_state_fields, vec_type));

      {
        // Set state field names for output
        PetscSection section;
        const char **field_names;

        // -- Get names from material model
        // -- TODO: Handle different field names for different material models
        PetscCall(RatelMaterialGetStateFieldNames(ratel->materials[0], &field_names));
        for (PetscInt i = 0; i < ratel->num_materials; i++) {
          PetscInt num_state_fields_current;

          PetscCall(RatelMaterialGetNumStateFields(ratel->materials[i], &num_state_fields_current));
          if (num_state_fields_current != 0) {
            PetscCall(RatelMaterialGetStateFieldNames(ratel->materials[i], &field_names));
            break;
          }
        }

        // -- Set names
        PetscCall(DMGetLocalSection(ratel->dm_state_fields, &section));
        PetscCall(PetscSectionSetFieldName(section, 0, ""));
        for (PetscInt i = 0; i < num_state_fields; i++) PetscCall(PetscSectionSetComponentName(section, 0, i, field_names[i]));
      }
    }
  }

  // -- Energy and diagnostic information DMs
  RatelDebug(ratel, "---- Energy DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_energy));
  PetscCall(RatelDMSetupByOrder_FEM(ratel, false, &ratel->highest_fine_order, RATEL_DECIDE, RATEL_DECIDE, 1, &num_comp_energy, ratel->dm_energy));
  PetscCall(DMSetVecType(ratel->dm_energy, vec_type));
  RatelDebug(ratel, "---- Diagnostic information DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_diagnostic));
  PetscCall(RatelDMSetupByOrder_FEM(ratel, false, &ratel->diagnostic_order, ratel->diagnostic_geometry_order, RATEL_DECIDE, 1, &num_comp_diagnostic,
                                    ratel->dm_diagnostic));
  PetscCall(DMSetVecType(ratel->dm_diagnostic, vec_type));
  // ---- Label field components for viewing
  {
    // Empty name for conserved field (because there is only one field)
    PetscSection section;
    const char **field_names;

    PetscCall(DMGetLocalSection(ratel->dm_diagnostic, &section));
    PetscCall(PetscSectionSetFieldName(section, 0, ""));
    PetscCall(RatelMaterialGetDiagnosticFieldNames(ratel->materials[0], &field_names));
    for (PetscInt i = 0; i < num_comp_diagnostic; i++) PetscCall(PetscSectionSetComponentName(section, 0, i, field_names[i]));
  }
  RatelDebug(ratel, "---- Surface force DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_surface_force));
  PetscCall(RatelDMSetupByOrder_FEM(ratel, false, &ratel->highest_fine_order, RATEL_DECIDE, ratel->q_extra_surface_force, 1, &num_comp_force,
                                    ratel->dm_surface_force));
  PetscCall(DMSetVecType(ratel->dm_surface_force, vec_type));
  RatelDebug(ratel, "---- Surface displacement DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_surface_displacement));
  PetscCall(RatelDMSetupByOrder_FEM(ratel, false, &ratel->highest_fine_order, RATEL_DECIDE, ratel->q_extra_surface_force, 1, active_field_sizes,
                                    ratel->dm_surface_displacement));
  PetscCall(DMSetVecType(ratel->dm_surface_displacement, vec_type));
  // ---- Copy labels to surface force DMs
  {
    DM dm_coord;

    PetscCall(DMGetCoordinateDM(ratel->dm_surface_displacement, &dm_coord));
    PetscCall(DMCopyLabels(ratel->dm_hierarchy[0], ratel->dm_surface_displacement, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
    PetscCall(DMCopyLabels(ratel->dm_hierarchy[0], dm_coord, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DM Setup FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup DM with FE space of appropriate degree

  @param[in]   ratel           Ratel context
  @param[in]   setup_boundary  Flag to add Dirichlet boundary
  @param[in]   orders          Polynomial orders of field
  @param[in]   coord_order     Polynomial order of coordinate basis, or RATEL_DECIDE for default
  @param[in]   q_extra         Additional quadrature order, or RATEL_DECIDE for default
  @param[in]   num_fields      Number of fields in solution vector
  @param[in]   field_sizes     Array of number of components for each field
  @param[out]  dm              DM to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSetupByOrder_FEM(Ratel ratel, PetscBool setup_boundary, PetscInt *orders, PetscInt coord_order, PetscInt q_extra,
                                       PetscInt num_fields, const PetscInt *field_sizes, DM dm) {
  PetscInt  dim, q_order = ratel->highest_fine_order + (q_extra == RATEL_DECIDE ? ratel->q_extra : q_extra);
  PetscBool is_simplex = PETSC_TRUE;
  PetscFE   fe;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DMPlex Setup by Degree FEM");

  // Check if simplex or tensor-product mesh
  PetscCall(DMPlexIsSimplex(dm, &is_simplex));

  // Setup DM
  PetscCall(DMGetDimension(dm, &dim));
  for (PetscInt i = 0; i < num_fields; i++) {
    PetscCall(PetscFECreateLagrange(ratel->comm, dim, field_sizes[i], is_simplex, orders[i], q_order, &fe));
    PetscCall(DMAddField(dm, NULL, (PetscObject)fe));
    PetscCall(PetscFEDestroy(&fe));
  }
  PetscCall(DMCreateDS(dm));

  // Project coordinates to enrich quadrature space
  {
    DM             dm_coord;
    PetscDS        ds_coord;
    PetscFE        fe_coord_current, fe_coord_new;
    PetscDualSpace fe_coord_dual_space;
    PetscInt       fe_coord_order, num_comp_coord;

    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(DMGetCoordinateDim(dm, &num_comp_coord));
    PetscCall(DMGetRegionDS(dm_coord, NULL, NULL, &ds_coord));
    PetscCall(PetscDSGetDiscretization(ds_coord, 0, (PetscObject *)&fe_coord_current));
    PetscCall(PetscFEGetDualSpace(fe_coord_current, &fe_coord_dual_space));
    PetscCall(PetscDualSpaceGetOrder(fe_coord_dual_space, &fe_coord_order));

    // Create FE for coordinates
    if (coord_order != RATEL_DECIDE) fe_coord_order = coord_order;
    PetscCall(PetscFECreateLagrange(ratel->comm, dim, num_comp_coord, is_simplex, fe_coord_order, q_order, &fe_coord_new));
    PetscCall(DMProjectCoordinates(dm, fe_coord_new));
    PetscCall(PetscFEDestroy(&fe_coord_new));
  }
  if (setup_boundary) {
    // Dirichlet boundaries
    RatelDebug(ratel, "---- Dirichlet boundaries");
    PetscCall(RatelDMAddBoundariesDirichlet(ratel, dm));
    // Slip boundaries
    RatelDebug(ratel, "---- Slip boundaries");
    PetscCall(RatelDMAddBoundariesSlip(ratel, dm));
  }
  if (!is_simplex) {
    DM dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(DMPlexSetClosurePermutationTensor(dm, PETSC_DETERMINE, NULL));
    PetscCall(DMPlexSetClosurePermutationTensor(dm_coord, PETSC_DETERMINE, NULL));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DMPlex Setup by Degree FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute CeedOperator volumetric QData for RatelMaterial

  @param[in]   material     RatelMaterial context
  @param[in]   label_name   DMPlex label name for volume
  @param[in]   label_value  DMPlex label value for volume
  @param[out]  restriction  CeedElemRestriction for QData
  @param[out]  q_data       CeedVector holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupVolumeQData_FEM(RatelMaterial material, const char *label_name, PetscInt label_value,
                                                 CeedElemRestriction *restriction, CeedVector *q_data) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Volume QData FEM");

  DM                  dm_coord;
  DMLabel             domain_label;
  Vec                 X_loc;
  const PetscScalar  *x_array;
  CeedInt             num_elem, num_qpts, num_comp_x;
  CeedInt             q_data_size = material->model_data->q_data_volume_size;
  PetscInt            dim;
  Ceed                ceed = ratel->ceed;
  CeedVector          x_loc;
  CeedElemRestriction restriction_x;
  CeedBasis           basis_x;

  // DM information
  PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));
  num_comp_x = dim;
  PetscCall(DMGetCoordinateDM(ratel->dm_hierarchy[0], &dm_coord));
  PetscCall(DMGetLabel(ratel->dm_hierarchy[0], label_name, &domain_label));

  // libCEED objects
  // -- Coordinate basis
  RatelDebug(ratel, "---- Coordinate basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, domain_label, label_value, 0, 0, &basis_x));
  // -- Coordinate restriction
  RatelDebug(ratel, "---- Coordinate restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, domain_label, label_value, 0, 0, &restriction_x));
  // ---- QData restriction
  RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(restriction_x, &num_elem));
  RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_x, &num_qpts));
  RatelDebug(ratel, "---- QData restriction: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT " DoF",
             num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
  RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                        CEED_STRIDES_BACKEND, restriction));

  // Element coordinates
  RatelDebug(ratel, "---- Retreiving element coordinates");
  PetscCall(DMGetCoordinatesLocal(ratel->dm_hierarchy[0], &X_loc));
  PetscCall(VecGetArrayRead(X_loc, &x_array));
  RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x, &x_loc, NULL));
  RatelCeedCall(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_USE_POINTER, (CeedScalar *)x_array));

  // libCEED vectors
  RatelDebug(ratel, "---- QData vector");
  // -- Geometric data vector
  RatelCeedCall(ratel, CeedVectorCreate(ceed, num_elem * num_qpts * q_data_size, q_data));

  // Geometric factor computation
  {
    RatelDebug(ratel, "---- Computing volumetric qdata");
    CeedQFunction qf_setup_q_data_volume;
    CeedOperator  op_setup_q_data_volume;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, material->model_data->setup_q_data_volume,
                                                     material->model_data->setup_q_data_volume_loc, &qf_setup_q_data_volume));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_q_data_volume, "dx", num_comp_x * dim, CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_q_data_volume, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup_q_data_volume, "q data", q_data_size, CEED_EVAL_NONE));

    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup_q_data_volume, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_q_data_volume));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric geometric data", op_setup_q_data_volume));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_q_data_volume, "dx", restriction_x, basis_x, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_q_data_volume, "weight", CEED_ELEMRESTRICTION_NONE, basis_x, CEED_VECTOR_NONE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_q_data_volume, "q data", *restriction, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));

    // -- Compute the quadrature data
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup_q_data_volume, stdout));
    RatelCeedCall(ratel, CeedOperatorApply(op_setup_q_data_volume, x_loc, *q_data, CEED_REQUEST_IMMEDIATE));

    // -- Cleanup
    PetscCall(VecRestoreArrayRead(X_loc, &x_array));
    RatelCeedCall(ratel, CeedVectorDestroy(&x_loc));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x));
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup_q_data_volume));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup_q_data_volume));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Volume QData FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute CeedOperator surface gradient QData for RatelMaterial

  @param[in]   material     RatelMaterial context
  @param[in]   dm           DMPlex grid
  @param[in]   label_name   DMPlex label name for surface
  @param[in]   label_value  DMPlex label value for surface
  @param[out]  restriction  CeedElemRestriction for QData
  @param[out]  q_data       CeedVector holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSurfaceGradientQData_FEM(RatelMaterial material, DM dm, const char *label_name, PetscInt label_value,
                                                          CeedElemRestriction *restriction, CeedVector *q_data) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Surface Gradient QData FEM");

  DM                  dm_coord;
  DMLabel             domain_label;
  Vec                 X_loc;
  const PetscScalar  *x_array;
  CeedInt             num_elem, num_qpts, num_comp_x;
  CeedInt             q_data_size = material->model_data->q_data_surface_grad_size;
  PetscInt            height_cell = 0, height_face = 1, dim;
  Ceed                ceed = ratel->ceed;
  CeedVector          x_loc;
  CeedElemRestriction restriction_x_cell, restriction_x_face;
  CeedBasis           basis_x_cell_to_face, basis_x_face;

  // DM information
  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;
  PetscCall(DMGetCoordinateDM(dm, &dm_coord));
  PetscCall(DMGetLabel(dm, label_name, &domain_label));

  // libCEED objects
  // -- Coordinate bases
  RatelDebug(ratel, "---- Coordinate bases");
  PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, domain_label, label_value, height_face, 0, &basis_x_face));
  PetscCall(RatelOrientedBasisCreateCellToFaceFromPlex(ratel, dm_coord, domain_label, label_value, label_value, height_cell, &basis_x_cell_to_face));
  // -- Coordinate restrictions
  RatelDebug(ratel, "---- Coordinate restrictions");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, domain_label, label_value, height_cell, 0, &restriction_x_cell));
  PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, domain_label, label_value, height_face, 0, &restriction_x_face));
  // ---- QData restriction
  RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(restriction_x_cell, &num_elem));
  RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_x_face, &num_qpts));
  RatelDebug(ratel, "---- QData restriction: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT " DoF",
             num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
  RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                        CEED_STRIDES_BACKEND, restriction));

  // Element coordinates
  RatelDebug(ratel, "---- Retreiving element coordinates");
  PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
  PetscCall(VecGetArrayRead(X_loc, &x_array));
  RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_cell, &x_loc, NULL));
  RatelCeedCall(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_USE_POINTER, (CeedScalar *)x_array));

  // libCEED vectors
  RatelDebug(ratel, "---- QData vector");
  // -- Geometric data vector
  RatelCeedCall(ratel, CeedVectorCreate(ceed, num_elem * num_qpts * q_data_size, q_data));

  // Geometric factor computation
  {
    RatelDebug(ratel, "---- Computing surface gradient qdata");
    CeedQFunction qf_setup_q_data_surface_grad;
    CeedOperator  op_setup_q_data_surface_grad;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, material->model_data->setup_q_data_surface_grad,
                                                     material->model_data->setup_q_data_surface_grad_loc, &qf_setup_q_data_surface_grad));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface_grad, "dx cell", num_comp_x * (dim - height_cell), CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface_grad, "dx face", num_comp_x * (dim - height_face), CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface_grad, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup_q_data_surface_grad, "q data", q_data_size, CEED_EVAL_NONE));

    // -- Operator
    RatelCeedCall(ratel,
                  CeedOperatorCreate(ceed, qf_setup_q_data_surface_grad, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_q_data_surface_grad));
    PetscCall(RatelMaterialSetOperatorName(material, "surface gradient geometric data", op_setup_q_data_surface_grad));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_q_data_surface_grad, "dx cell", restriction_x_cell, basis_x_cell_to_face, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_q_data_surface_grad, "dx face", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_q_data_surface_grad, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_q_data_surface_grad, "q data", *restriction, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));

    // -- Compute the quadrature data
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup_q_data_surface_grad, stdout));
    RatelCeedCall(ratel, CeedOperatorApply(op_setup_q_data_surface_grad, x_loc, *q_data, CEED_REQUEST_IMMEDIATE));

    // -- Cleanup
    PetscCall(VecRestoreArrayRead(X_loc, &x_array));
    RatelCeedCall(ratel, CeedVectorDestroy(&x_loc));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_cell_to_face));
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_cell));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup_q_data_surface_grad));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup_q_data_surface_grad));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Surface Gradient QData FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup Residual and Jacobian CeedOperators for RatelMaterial

  @param[in]   material         RatelMaterial context
  @param[out]  op_residual_u    Composite residual u term CeedOperator to add RatelMaterial suboperator
  @param[out]  op_residual_ut   Composite residual u_t term CeedOperator to add RatelMaterial suboperator
  @param[out]  op_residual_utt  Composite residual u_tt term CeedOperator to add RatelMaterial suboperator
  @param[out]  op_jacobian      Composite Jacobian CeedOperator to add RatelMaterial suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupResidualJacobianSuboperators_FEM(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                                  CeedOperator op_residual_utt, CeedOperator op_jacobian) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Residual and Jacobian Suboperators FEM");

  RatelModelData       model_data = material->model_data;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values, num_active_fields;
  const PetscInt      *active_field_sizes;
  CeedInt              num_elem, num_qpts, q_data_size = model_data->q_data_volume_size;
  Ceed                 ceed                    = ratel->ceed;
  CeedEvalMode        *active_field_eval_modes = model_data->active_field_eval_modes;
  CeedVector           q_data                  = NULL, quadrature_fields[RATEL_MAX_FIELDS];
  CeedBasis           *bases_u                 = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restrictions_quadrature_fields[RATEL_MAX_FIELDS];

  // DM information
  PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));
  PetscCall(DMGetLabel(ratel->dm_hierarchy[0], material->volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Solution basis
  RatelDebug(ratel, "---- Solution bases");
  PetscCall(RatelMaterialGetLabelValues(material, NULL, &domain_values));
  PetscCall(PetscCalloc1(num_active_fields, &bases_u));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_hierarchy[0], domain_label, domain_values[0], 0, i, &bases_u[i]));
  }
  // -- Solution restriction
  RatelDebug(ratel, "---- Solution restrictions");
  PetscCall(PetscCalloc1(num_active_fields, &restrictions_u));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_hierarchy[0], domain_label, domain_values[0], 0, i, &restrictions_u[i]));
  }
  // -- Stored data at quadrature points
  RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(restrictions_u[0], &num_elem));
  RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(bases_u[0], &num_qpts));
  if (model_data->num_quadrature_fields) RatelDebug(ratel, "---- Stored field data restrictions");
  for (CeedInt i = 0; i < model_data->num_quadrature_fields; i++) {
    RatelDebug(ratel, "------ Restriction: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT " DoF",
               num_elem, num_qpts, model_data->quadrature_field_sizes[i], num_elem * num_qpts * model_data->quadrature_field_sizes[i]);
    RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, model_data->quadrature_field_sizes[i],
                                                          num_elem * num_qpts * model_data->quadrature_field_sizes[i], CEED_STRIDES_BACKEND,
                                                          &restrictions_quadrature_fields[i]));
  }
  // -- Stored field vectors
  RatelDebug(ratel, "---- Stored field data vectors");
  for (CeedInt i = 0; i < model_data->num_quadrature_fields; i++) {
    RatelCeedCall(ratel, CeedVectorCreate(ceed, num_elem * num_qpts * model_data->quadrature_field_sizes[i], &quadrature_fields[i]));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Local residual evaluator
  {
    RatelDebug(ratel, "---- Setting up local residual evaluator, u term");
    CeedQFunction qf_residual_u;
    CeedOperator  sub_op_residual_u;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_u, model_data->residual_u_loc, &qf_residual_u));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_u, "q data", q_data_size, CEED_EVAL_NONE));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode       = active_field_eval_modes[i];
      CeedInt      quadrature_size = RatelGetQuadratureSize(eval_mode, dim, active_field_sizes[i]);

      if (quadrature_size <= 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_u, field_name, quadrature_size, eval_mode));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%sv, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_residual_u, field_name, quadrature_size, eval_mode));
    }
    for (CeedInt i = 0; i < model_data->num_quadrature_fields; i++) {
      RatelCeedCall(
          ratel, CeedQFunctionAddOutput(qf_residual_u, model_data->quadrature_field_names[i], model_data->quadrature_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_residual_u, material->ctx_physics));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_residual_u, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_residual_u, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_u));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_u));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_u, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode = active_field_eval_modes[i];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_u, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%sv, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_u, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    for (CeedInt i = 0; i < model_data->num_quadrature_fields; i++) {
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_u, model_data->quadrature_field_names[i], restrictions_quadrature_fields[i],
                                                CEED_BASIS_COLLOCATED, quadrature_fields[i]));
    }
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_residual_u, sub_op_residual_u));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_residual_u, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_residual_u));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_residual_u));
  }

  if (material->model_data->has_ut_term) {
    RatelDebug(ratel, "---- Setting up local residual evaluator, u_t term");
    ratel->has_ut_term = PETSC_TRUE;
    CeedQFunction qf_residual_ut;
    CeedOperator  sub_op_residual_ut;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_ut, model_data->residual_ut_loc, &qf_residual_ut));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_ut, "q data", q_data_size, CEED_EVAL_NONE));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u_t, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_ut, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "v_t, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_residual_ut, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
    }
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_residual_ut, material->ctx_physics));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_residual_ut, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_residual_ut, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_ut));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_ut));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_ut, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u_t, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_ut, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "v_t, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_ut, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_residual_ut, sub_op_residual_ut));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_residual_ut, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_residual_ut));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_residual_ut));
  }

  // Scaled mass matrix for local residual evaluator:
  if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
    RatelDebug(ratel, "---- Setting up local residual evaluator, u_tt term");
    ScaledMassContext   *data_scaled_mass;
    CeedQFunctionContext ctx_scaled_mass;
    CeedQFunction        qf_residual_utt;
    CeedOperator         sub_op_residual_utt;

    // -- Check for support
    if (!model_data->residual_utt) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Material %s does not support u_tt term", material->name);

    // -- QFunctionContext
    PetscCall(PetscMalloc(sizeof(ScaledMassContext) + num_active_fields * sizeof(CeedInt), &data_scaled_mass));
    data_scaled_mass->num_fields = (CeedInt)num_active_fields;
    for (PetscInt i = 0; i < num_active_fields; i++) data_scaled_mass->field_sizes[i] = active_field_sizes[i];
    {
      CeedScalar *data_physics;
      RatelCeedCall(ratel, CeedQFunctionContextGetDataRead(material->ctx_physics, CEED_MEM_HOST, &data_physics));
      data_scaled_mass->rho = data_physics[RATEL_COMMON_PARAMETER_RHO];
      RatelCeedCall(ratel, CeedQFunctionContextRestoreDataRead(material->ctx_physics, &data_physics));
    }
    RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_scaled_mass, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*data_scaled_mass), data_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionContextRegisterInt32(ctx_scaled_mass, "num fields", offsetof(ScaledMassContext, num_fields), 1,
                                                           "number of fields in solution vector"));
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_scaled_mass, "rho", offsetof(ScaledMassContext, rho), 1, "material density"));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_scaled_mass, stdout));
    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_utt, model_data->residual_utt_loc, &qf_residual_utt));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_utt, "q data", q_data_size, CEED_EVAL_NONE));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u_tt, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_utt, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "v, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_residual_utt, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
    }
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_residual_utt, ctx_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_residual_utt, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_residual_utt, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_utt));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_utt));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_utt, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u_tt, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_utt, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "v, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_residual_utt, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_residual_utt, sub_op_residual_utt));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_residual_utt, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_residual_utt));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_residual_utt));
  }

  // Jacobian evaluator
  {
    RatelDebug(ratel, "---- Setting up Jacobian evaluator");
    CeedQFunction qf_jacobian;
    CeedOperator  sub_op_jacobian;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->jacobian, model_data->jacobian_loc, &qf_jacobian));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_jacobian, "q data", q_data_size, CEED_EVAL_NONE));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode       = active_field_eval_modes[i];
      CeedInt      quadrature_size = RatelGetQuadratureSize(eval_mode, dim, active_field_sizes[i]);

      if (quadrature_size <= 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta %su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_jacobian, field_name, quadrature_size, eval_mode));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta %sv, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_jacobian, field_name, quadrature_size, eval_mode));
    }
    if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
      for (PetscInt i = 0; i < num_active_fields; i++) {
        if (active_field_eval_modes[i] != CEED_EVAL_INTERP) {
          char field_name[PETSC_MAX_PATH_LEN];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta u, field %" PetscInt_FMT, i));
          RatelCeedCall(ratel, CeedQFunctionAddInput(qf_jacobian, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta v, field %" PetscInt_FMT, i));
          RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_jacobian, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
        }
      }
    }
    for (CeedInt i = 0; i < model_data->num_quadrature_fields; i++) {
      RatelCeedCall(ratel,
                    CeedQFunctionAddInput(qf_jacobian, model_data->quadrature_field_names[i], model_data->quadrature_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_jacobian, material->ctx_physics));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_jacobian, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_jacobian, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_jacobian));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_jacobian));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode = active_field_eval_modes[i];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta %su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta %sv, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    for (CeedInt i = 0; i < model_data->num_quadrature_fields; i++) {
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, model_data->quadrature_field_names[i], restrictions_quadrature_fields[i],
                                                CEED_BASIS_COLLOCATED, quadrature_fields[i]));
    }
    if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
      for (PetscInt i = 0; i < num_active_fields; i++) {
        if (active_field_eval_modes[i] != CEED_EVAL_INTERP) {
          char field_name[PETSC_MAX_PATH_LEN];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta u, field %" PetscInt_FMT, i));
          RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "delta v, field %" PetscInt_FMT, i));
          RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    // -- FLOPs counting
    CeedSize flops = model_data->flops_qf_jacobian_u + model_data->flops_qf_jacobian_ut;
    if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) flops += model_data->flops_qf_jacobian_utt;
    RatelCeedCall(ratel, CeedQFunctionSetUserFlopsEstimate(qf_jacobian, flops));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_jacobian, sub_op_jacobian));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_jacobian, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_jacobian));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_jacobian));
  }

  // Cleanup
  RatelDebug(ratel, "---- Cleaning up libCEED objects");
  RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
  for (PetscInt i = 0; i < model_data->num_quadrature_fields; i++) RatelCeedCall(ratel, CeedVectorDestroy(&quadrature_fields[i]));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < model_data->num_quadrature_fields; i++) {
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restrictions_quadrature_fields[i]));
  }
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCeedCall(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Residual and Jacobian Suboperators FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid level CeedOperators for RatelMaterial

  @param[in]      material                RatelMaterial context
  @param[in]      level                   Multigrid level to setup
  @param[in]      m_loc                   libCEED vector holding multiplicity
  @param[in]      sub_op_jacobian_fine    Fine grid Jacobian CeedOperator
  @param[in,out]  op_jacobian_coarse      Composite Jacobian CeedOperator to add RatelMaterial suboperators
  @param[in,out]  op_prolong              Composite prolongation CeedOperator to add RatelMaterial suboperators
  @param[in,out]  op_restrict             Composite restriction CeedOperator to add RatelMaterial suboperators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianMultigridLevel_FEM(RatelMaterial material, PetscInt level, CeedVector m_loc,
                                                            CeedOperator sub_op_jacobian_fine, CeedOperator op_jacobian_coarse,
                                                            CeedOperator op_prolong, CeedOperator op_restrict) {
  Ratel    ratel = material->ratel;
  PetscInt dim;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Multigrid Level");

  PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));

  // Setup level operators
  DMLabel             domain_label;
  PetscInt           *label_values;
  CeedElemRestriction restriction_u;
  CeedBasis           basis_u;
  CeedOperator        sub_op_jacobian_coarse, sub_op_prolong, sub_op_restrict;

  // -- Domain information
  PetscCall(RatelMaterialGetLabelValues(material, NULL, &label_values));
  PetscCall(DMGetLabel(ratel->dm_orig, ratel->material_volume_label_name, &domain_label));

  // --  libCEED objects
  RatelDebug(ratel, "---- Setting up libCEED restriction for level");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_hierarchy[level], domain_label, label_values[0], 0, 0, &restriction_u));
  RatelDebug(ratel, "---- Setting up libCEED basis for level");
  PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_hierarchy[level], domain_label, label_values[0], 0, 0, &basis_u));

  // -- Create coarse grid, prolongation, and restriction operators
  RatelDebug(ratel, "---- Setting up level Jacobian, prolongation, and restriction operators");
  RatelCeedCall(ratel, CeedOperatorMultigridLevelCreate(sub_op_jacobian_fine, m_loc, restriction_u, basis_u, &sub_op_jacobian_coarse, &sub_op_prolong,
                                                        &sub_op_restrict));

  // -- Add to composite operators
  RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_jacobian_coarse, sub_op_jacobian_coarse));
  RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_prolong, sub_op_prolong));
  RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_restrict, sub_op_restrict));

  // -- Cleanup
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_u));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_u));
  RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_jacobian_coarse));
  RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_prolong));
  RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_restrict));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Multigrid Level Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup energy CeedOperator for RatelMaterial

  @param[in]   material   RatelMaterial context
  @param[out]  op_energy  Composite energy CeedOperator to add RatelMaterial suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupEnergySuboperator_FEM(RatelMaterial material, CeedOperator op_energy) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Energy Suboperator FEM");

  RatelModelData       model_data = material->model_data;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values, num_active_fields;
  const PetscInt      *active_field_sizes;
  CeedInt              num_comp_energy = 1, q_data_size = model_data->q_data_volume_size;
  Ceed                 ceed                    = ratel->ceed;
  CeedEvalMode        *active_field_eval_modes = model_data->active_field_eval_modes;
  CeedVector           q_data                  = NULL;
  CeedBasis           *bases_u                 = NULL, basis_energy;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_energy;

  // DM information
  PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));
  PetscCall(RatelMaterialGetLabelValues(material, NULL, &domain_values));
  PetscCall(DMGetLabel(ratel->dm_hierarchy[0], material->volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Energy basis
  RatelDebug(ratel, "------ Energy basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_energy, domain_label, domain_values[0], 0, 0, &basis_energy));
  // -- Energy restriction
  RatelDebug(ratel, "------ Energy restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_energy, domain_label, domain_values[0], 0, 0, &restriction_energy));
  // -- Solution objects from suboperator
  PetscCall(RatelMaterialGetSolutionData(material, &num_active_fields, &restrictions_u, &bases_u));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Local energy computation
  {
    RatelDebug(ratel, "---- Setting up local energy computation");
    CeedQFunction qf_energy;
    CeedOperator  sub_op_energy;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->energy, model_data->energy_loc, &qf_energy));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_energy, "q data", q_data_size, CEED_EVAL_NONE));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode       = active_field_eval_modes[i];
      CeedInt      quadrature_size = RatelGetQuadratureSize(eval_mode, dim, active_field_sizes[i]);

      if (quadrature_size <= 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_energy, field_name, quadrature_size, eval_mode));
    }
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_energy, "energy", num_comp_energy, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_energy, material->ctx_physics));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_energy, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_energy, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_energy));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_energy));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_energy, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode = active_field_eval_modes[i];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_energy, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_energy, "energy", restriction_energy, basis_energy, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_energy, sub_op_energy));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_energy, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_energy));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_energy));
  }

  // Cleanup
  RatelDebug(ratel, "---- Cleaning up libCEED objects");
  RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_energy));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCeedCall(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_energy));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Energy Suboperator FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup diagnostic value CeedOperator for RatelMaterial

  @param[in]   material            RatelMaterial context
  @param[out]  op_mass_diagnostic  Composite diagnostic value projection CeedOperator to add RatelMaterial suboperator
  @param[out]  op_diagnostic       Composite diagnostic value CeedOperator to add RatelMaterial suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupDiagnosticSuboperators_FEM(RatelMaterial material, CeedOperator op_mass_diagnostic, CeedOperator op_diagnostic) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Diagnostic Suboperator FEM");

  RatelModelData       model_data = material->model_data;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values, num_active_fields;
  const PetscInt      *active_field_sizes;
  CeedInt              num_comp_diagnostic = model_data->num_comp_diagnostic, q_data_size = model_data->q_data_volume_size;
  Ceed                 ceed           = ratel->ceed;
  CeedVector           q_data         = NULL;
  CeedBasis           *bases_u        = NULL, basis_diagnostic;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_diagnostic;

  // DM information
  PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));
  PetscCall(RatelMaterialGetLabelValues(material, NULL, &domain_values));
  PetscCall(DMGetLabel(ratel->dm_hierarchy[0], material->volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects  // -- Diagnostic basis
  RatelDebug(ratel, "------ Diagnostics basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_diagnostic, domain_label, domain_values[0], 0, 0, &basis_diagnostic));
  // -- Diagnostic data restriction
  RatelDebug(ratel, "------ Diagnostics restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_diagnostic, domain_label, domain_values[0], 0, 0, &restriction_diagnostic));
  // -- Solution objects from suboperator
  PetscCall(RatelMaterialGetSolutionData(material, &num_active_fields, &restrictions_u, &bases_u));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Diagnostic value computation
  {
    RatelDebug(ratel, "---- Setting up diagnostic value computation");
    CeedQFunctionContext ctx_mass_diagnostic;
    CeedQFunction        qf_mass_diagnostic, qf_diagnostic;
    CeedOperator         sub_op_mass_diagnostic, sub_op_diagnostic;

    // Mass matrix
    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass_diagnostic));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass_diagnostic, "u", num_comp_diagnostic, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_mass_diagnostic, "v", num_comp_diagnostic, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_mass_diagnostic));
    RatelCeedCall(
        ratel, CeedQFunctionContextSetData(ctx_mass_diagnostic, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(num_comp_diagnostic), &num_comp_diagnostic));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_mass_diagnostic, ctx_mass_diagnostic));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_mass_diagnostic, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_mass_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_mass_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_mass_diagnostic));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "u", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "v", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_mass_diagnostic, sub_op_mass_diagnostic));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_mass_diagnostic, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_mass_diagnostic));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_mass_diagnostic));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_mass_diagnostic));

    // Diagnostic quantities
    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->diagnostic, model_data->diagnostic_loc, &qf_diagnostic));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i] * dim, CEED_EVAL_GRAD));
    }
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_diagnostic, "diagnostic", num_comp_diagnostic, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_diagnostic, material->ctx_physics));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_diagnostic, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_diagnostic));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_diagnostic, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_diagnostic, "diagnostic", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_diagnostic, sub_op_diagnostic));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_diagnostic, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_diagnostic));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_diagnostic));
  }

  // Cleanup
  RatelDebug(ratel, "---- Cleaning up libCEED objects");
  RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_diagnostic));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCeedCall(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_diagnostic));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Diagnostic Suboperator FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup surface force CeedOperator for RatelMaterial

  @param[in]   material           RatelMaterial context
  @param[out]  ops_surface_force  Composite surface force CeedOperators to add RatelMaterial suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSurfaceForceSuboperators_FEM(RatelMaterial material, CeedOperator *ops_surface_force) {
  Ratel           ratel      = material->ratel;
  RatelModelData  model_data = material->model_data;
  PetscInt        dim, num_active_fields;
  const PetscInt *active_field_sizes;
  Ceed            ceed = ratel->ceed;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Surface Force Suboperator FEM");

  // DM information
  PetscCall(DMGetDimension(ratel->dm_surface_displacement, &dim));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // Surface force computation
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscInt dm_face = ratel->surface_force_dm_faces[i];
    RatelDebug(ratel, "---- Setting up surface %" PetscInt_FMT " force computation", dm_face);

    CeedInt             q_data_size = model_data->q_data_surface_grad_size, num_comp_u = active_field_sizes[0], num_comp_force = num_comp_u + dim;
    CeedBasis           basis_u_face, basis_u_cell_to_face, basis_surface_force;
    CeedElemRestriction restriction_u_face, restriction_u_cell_to_face, restriction_surface_force;
    CeedQFunction       qf_surface_force;
    CeedOperator        sub_op_surface_force;

    // Domain label for Face Sets
    DMLabel     face_domain_label      = NULL;
    const char *face_domain_label_name = NULL;

    PetscCall(RatelMaterialGetSurfaceGradientDiagnosticLabelName(material, dm_face, &face_domain_label_name));
    if (!face_domain_label_name) continue;
    PetscCall(DMGetLabel(ratel->dm_surface_displacement, face_domain_label_name, &face_domain_label));
    if (!face_domain_label) SETERRQ(ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "No DM label corresponding to name %s found", face_domain_label_name);

    // Loop over all face orientations
    IS              is_face_domain_values;
    PetscInt        num_face_domain_values;
    const PetscInt *face_domain_values;

    PetscCall(DMLabelGetNonEmptyStratumValuesIS(face_domain_label, &is_face_domain_values));
    PetscCall(ISGetSize(is_face_domain_values, &num_face_domain_values));
    PetscCall(ISGetIndices(is_face_domain_values, &face_domain_values));
    for (PetscInt orientation = 0; orientation < num_face_domain_values; orientation++) {
      PetscInt            face_domain_value  = face_domain_values[orientation];
      CeedVector          q_data             = NULL;
      CeedElemRestriction restriction_q_data = NULL;

      RatelDebug(ratel, "------ Face orientation %" PetscInt_FMT, face_domain_value);

      // Check for face on process
      PetscInt height_face = 1, height_cell = 0;
      {
        PetscInt first_point;
        PetscInt ids[1] = {face_domain_value};

        PetscCall(DMGetFirstLabeledPoint(ratel->dm_surface_displacement, ratel->dm_surface_displacement, face_domain_label, 1, ids, height_cell,
                                         &first_point, NULL));
        if (first_point == -1) continue;
      }

      // -- QData
      PetscCall(RatelMaterialGetSurfaceGradientDiagnosticQData(material, dm_face, face_domain_value, &restriction_q_data, &q_data));

      // Diagnostic quantities
      RatelDebug(ratel, "-------- Setting up surface force operator");
      // -- Bases
      PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value, height_face, 0, &basis_u_face));
      PetscCall(RatelOrientedBasisCreateCellToFaceFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value,
                                                           face_domain_value, height_cell, &basis_u_cell_to_face));
      PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_surface_force, face_domain_label, face_domain_value, height_face, 0, &basis_surface_force));
      // -- ElemRestriction
      PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value, height_face, 0,
                                               &restriction_u_face));
      PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value, height_cell, 0,
                                               &restriction_u_cell_to_face));
      PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_surface_force, face_domain_label, face_domain_value, height_face, 0,
                                               &restriction_surface_force));
      // -- QFunction
      RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->surface_force, model_data->surface_force_loc, &qf_surface_force));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_surface_force, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_surface_force, "u", num_comp_u, CEED_EVAL_INTERP));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_surface_force, "du", num_comp_u * (dim - height_cell), CEED_EVAL_GRAD));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_surface_force, "diagnostic", num_comp_force, CEED_EVAL_INTERP));
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_surface_force, material->ctx_physics));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_surface_force, false));
      // -- Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_surface_force, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_surface_force));
      {
        char operator_name[30];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "surface %" PetscInt_FMT " force computation", dm_face));
        PetscCall(RatelMaterialSetOperatorName(material, operator_name, sub_op_surface_force));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_surface_force, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_surface_force, "u", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_surface_force, "du", restriction_u_cell_to_face, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel,
                    CeedOperatorSetField(sub_op_surface_force, "diagnostic", restriction_surface_force, basis_surface_force, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ops_surface_force[i], sub_op_surface_force));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_surface_force, stdout));

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_face));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_cell_to_face));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_surface_force));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_u_cell_to_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_surface_force));
      RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_surface_force));
      RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_surface_force));
    }
    PetscCall(ISRestoreIndices(is_face_domain_values, &face_domain_values));
    PetscCall(ISDestroy(&is_face_domain_values));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Surface Force Suboperator FEM Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup MMS error CeedOperator for RatelMaterial

  @param[in]   material      RatelMaterial context
  @param[out]  op_mms_error  Composite MMS error CeedOperator to add RatelMaterial suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMMSErrorSuboperator_FEM(RatelMaterial material, CeedOperator op_mms_error) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup MMS Error Suboperator FEM");

  RatelModelData       model_data = material->model_data;
  DM                   dm_coord;
  DMLabel              domain_label;
  Vec                  X;
  const PetscScalar   *x_array;
  PetscInt             dim, *domain_values, num_active_fields;
  const PetscInt      *active_field_sizes;
  CeedInt              num_comp_x, q_data_size = model_data->q_data_volume_size;
  Ceed                 ceed           = ratel->ceed;
  CeedVector           q_data         = NULL, x;
  CeedBasis           *bases_u        = NULL, basis_x;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_x;

  // DM information
  PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));
  num_comp_x = dim;
  PetscCall(DMGetCoordinateDM(ratel->dm_hierarchy[0], &dm_coord));
  PetscCall(RatelMaterialGetLabelValues(material, NULL, &domain_values));
  PetscCall(DMGetLabel(ratel->dm_hierarchy[0], material->volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Coordinate basis
  RatelDebug(ratel, "---- Coordinate basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, domain_label, domain_values[0], 0, 0, &basis_x));
  // -- Coordinate restriction
  RatelDebug(ratel, "---- Coordinate restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, domain_label, domain_values[0], 0, 0, &restriction_x));
  // -- Solution objects from suboperator
  PetscCall(RatelMaterialGetSolutionData(material, &num_active_fields, &restrictions_u, &bases_u));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Element coordinates
  RatelDebug(ratel, "---- Retrieving element coordinates");
  PetscCall(DMGetCoordinatesLocal(ratel->dm_hierarchy[0], &X));
  PetscCall(VecGetArrayRead(X, &x_array));
  RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x, &x, NULL));
  RatelCeedCall(ratel, CeedVectorSetArray(x, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
  PetscCall(VecRestoreArrayRead(X, &x_array));

  // Error calculation, for MMS
  if (model_data->mms_error) {
    RatelDebug(ratel, "---- Setting up error computation for MMS");
    CeedQFunction qf_mms_error;
    CeedOperator  sub_op_mms_error;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->mms_error, model_data->mms_error_loc, &qf_mms_error));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mms_error, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mms_error, "x", num_comp_x, CEED_EVAL_INTERP));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mms_error, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "l2 error, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_mms_error, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
    }
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_mms_error, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_mms_error));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_mms_error));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_mms_error, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_mms_error, "x", restriction_x, basis_x, x));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_mms_error, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "l2 error, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_mms_error, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_mms_error, sub_op_mms_error));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_mms_error, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_mms_error));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_mms_error));
  }

  // Cleanup
  RatelDebug(ratel, "---- Cleaning up libCEED objects");
  RatelCeedCall(ratel, CeedVectorDestroy(&x));
  RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCeedCall(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_x));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup MMS Error Suboperator FEM Success!");
  PetscFunctionReturn(0);
}

/// @}
