/// @file
/// Setup Neo-Hookean physics context objects

#include <ceed.h>
#include <math.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <stddef.h>

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Process command line options for Neo-Hookean physics

  @param[in]   material  RatelMaterial to setup physics context
  @param[in]   nu        Poisson's ratio
  @param[in]   E         Young's Modulus
  @param[in]   rho       Density for scaled mass matrix
  @param[out]  ctx       libCEED QFunctionContext for Neo-Hookean physics

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedPhysicsContextCreate_NeoHookean(RatelMaterial material, CeedScalar nu, CeedScalar E, CeedScalar rho,
                                                               CeedQFunctionContext *ctx) {
  Ratel              ratel = material->ratel;
  NeoHookeanPhysics *physics;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Physics Context Neo-Hookean");

  // Create context data with correct scaling
  PetscCall(PetscCalloc1(1, &physics));
  physics->nu                                            = nu;
  physics->E                                             = E * ratel->material_units->Pascal;
  physics->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (-ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);

  // Create context object
  RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*physics), physics));
  PetscCall(PetscFree(physics));

  // Common parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "rho", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_RHO]), 1,
                                                          "density"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift v", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift a", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));

  // Neo-Hookean specific parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "nu", offsetof(NeoHookeanPhysics, nu), 1, "Poisson's ratio"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "E", offsetof(NeoHookeanPhysics, E), 1, "Young's Modulus"));

  // Debugging output
  if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx, stdout));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Physics Context Neo-Hookean Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Build libCEED QFunctionContext with Neo-Hookean physics

  @param[in]   material  RatelMaterial to setup physics context
  @param[out]  ctx       libCEED QFunctionContext for Neo-Hookean physics

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialPhysicsContextCreate_NeoHookean(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel      = material->ratel;
  PetscBool  nu_flag    = PETSC_FALSE;
  PetscBool  Young_flag = PETSC_FALSE;
  CeedScalar nu = 0.0, E = 0.0, rho = 1.0;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Context Neo-Hookean");

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Neo-Hookean physical parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, &nu_flag));
    RatelDebug(ratel, "---- nu: %f", nu);

    PetscCall(PetscOptionsScalar("-E", "Young's Modulus", NULL, E, &E, &Young_flag));
    RatelDebug(ratel, "---- E: %f", E);

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    if (rho < 0.0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    RatelDebug(ratel, "---- rho: %f", rho);

    PetscOptionsEnd();  // End of setting Physics
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  if (!nu_flag) SETERRQ(ratel->comm, PETSC_ERR_SUP, "-nu option needed");
  if (!Young_flag) SETERRQ(ratel->comm, PETSC_ERR_SUP, "-E option needed");

  // Create context
  PetscCall(RatelCeedPhysicsContextCreate_NeoHookean(material, nu, E, rho, ctx));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Context Neo-Hookean Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup data for libCEED QFunctionContext for smoother with Neo-Hookean physics

  @param[in]   material      RatelMaterial to setup physics context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialPhysicsSmootherDataSetup_NeoHookean(RatelMaterial material) {
  Ratel      ratel = material->ratel;
  CeedScalar nu = 0.0, nu_smoother = 0.0;
  PetscBool  nu_smoother_flag = PETSC_FALSE;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Smoother Context Neo-Hookean");

  // Get parameter values
  if (material->num_physics_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Neo-Hookean physical parameters for smoother", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, NULL));
    RatelDebug(ratel, "---- nu: %f", nu);

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &nu_smoother_flag));
    if (nu_smoother_flag) RatelDebug(ratel, "---- nu: %f", nu_smoother);
    else RatelDebug(ratel, "---- No Nu for smoother set");

    PetscOptionsEnd();  // End of setting Physics
    PetscCall(PetscFree(cl_prefix));

    // Create context, if needed
    if (nu_smoother_flag) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = strlen(label_name) + 1;

      material->num_physics_smoother_values = 1;
      PetscCall(PetscCalloc1(1, &material->ctx_physics_values));
      PetscCall(PetscCalloc1(1, &material->ctx_physics_smoother_values));
      material->ctx_physics_values[0]          = nu;
      material->ctx_physics_smoother_values[0] = nu_smoother;
      PetscCall(PetscCalloc1(1, &material->ctx_physics_label_names));
      PetscCall(PetscCalloc1(label_name_len, &material->ctx_physics_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_physics_label_names[0], label_name, label_name_len));
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Smoother Context Neo-Hookean Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Build libCEED QFunctionContext for platen BC with Neo-Hookean physics

  @param[in]   material    RatelMaterial to setup platen context
  @param[in]   normal      Normal vector to platen surface
  @param[in]   center      Center of platen surface
  @param[in]   distance    Displacement of platen along normal
  @param[in]   gamma       Nitsche's method penalty parameter
  @param[in]   final_time  Final time to complete platen movement
  @param[out]  ctx         libCEED QFunctionContext for platen

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialPlatenContextCreate_NeoHookean(RatelMaterial material, CeedScalar normal[3], CeedScalar center[3], CeedScalar distance,
                                                           CeedScalar gamma, CeedScalar final_time, CeedQFunctionContext *ctx) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Platen Context Neo-Hookean");

  NeoHookeanPhysics *physics = NULL;
  RatelCeedCall(ratel, CeedQFunctionContextGetDataRead(material->ctx_physics, CEED_MEM_HOST, &physics));
  NeoHookeanPlatenData platen_data = {
      .normal[0]  = normal[0],
      .normal[1]  = normal[1],
      .normal[2]  = normal[2],
      .center[0]  = center[0],
      .center[1]  = center[1],
      .center[2]  = center[2],
      .distance   = distance,
      .gamma      = gamma,
      .time       = RATEL_UNSET_INITIAL_TIME,
      .final_time = final_time,
      .material   = *physics,
  };

  // Create context object
  RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(platen_data), &platen_data));
  RatelCeedCall(ratel, CeedQFunctionContextRestoreDataRead(material->ctx_physics, &physics));

  // Set up context
  // Platen parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "normal", offsetof(NeoHookeanPlatenData, normal), 3, "external normal of platen"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "center", offsetof(NeoHookeanPlatenData, center), 3, "center of platen"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "distance", offsetof(NeoHookeanPlatenData, distance), 1,
                                                          "displacement of platen along normal"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "time", offsetof(NeoHookeanPlatenData, time), 1, "current solver time"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "final time", offsetof(NeoHookeanPlatenData, final_time), 1, "final solver time"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "gamma", offsetof(NeoHookeanPlatenData, gamma), 1, "Nitche's method parameter"));

  // Physics parameters
  // Common parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "rho", offsetof(NeoHookeanPlatenData, material.common_parameters[RATEL_COMMON_PARAMETER_RHO]), 1, "density"));
  RatelCeedCall(ratel,
                CeedQFunctionContextRegisterDouble(
                    *ctx, "shift v", offsetof(NeoHookeanPlatenData, material.common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
  RatelCeedCall(
      ratel, CeedQFunctionContextRegisterDouble(
                 *ctx, "shift a", offsetof(NeoHookeanPlatenData, material.common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));

  // Neo-Hookean specific parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "nu", offsetof(NeoHookeanPlatenData, material.nu), 1, "Poisson's ratio"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "E", offsetof(NeoHookeanPlatenData, material.E), 1, "Young's Modulus"));

  // Debugging output
  if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx, stdout));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Platen Context Neo-Hookean Success!");
  PetscFunctionReturn(0);
}

/// @}
