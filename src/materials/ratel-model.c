/// @file
/// Setup Ratel models

#include <ceed.h>
#include <petsc.h>
#include <ratel-model.h>
#include <ratel.h>
#include <string.h>

#define RATEL_MODEL(model_cl_argument, model_postfix) \
  RATEL_INTERN PetscErrorCode RatelRegisterModel_##model_postfix(Ratel, const char *, PetscFunctionList *);

#include "ratel-model-list.h"
#undef RATEL_MODEL

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Register setup functions for models

  @param[in]   ratel                      Ratel context
  @param[out]  material_create_functions  Function list for creating RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModels(Ratel ratel, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Register Models");

#define RATEL_MODEL(model_cl_argument, model_postfix) \
  PetscCall(RatelRegisterModel_##model_postfix(ratel, model_cl_argument, material_create_functions));

#include "ratel-model-list.h"
#undef RATEL_MODEL

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Register Models Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Verify all QFunction paths are relative to Ratel JiT root directory

  @param[in]      ratel       Ratel context
  @param[in,out]  model_data  Model data to check QFunction paths

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelModelDataVerifyRelativePath(Ratel ratel, RatelModelData model_data) {
  PetscFunctionBeginUser;

  model_data->setup_q_data_volume_loc = RatelQFunctionRelativePath(model_data->setup_q_data_volume_loc);
  if (model_data->setup_q_data_surface_loc) model_data->setup_q_data_surface_loc = RatelQFunctionRelativePath(model_data->setup_q_data_surface_loc);
  if (model_data->setup_q_data_surface_grad_loc) {
    model_data->setup_q_data_surface_grad_loc = RatelQFunctionRelativePath(model_data->setup_q_data_surface_grad_loc);
  }
  model_data->residual_u_loc = RatelQFunctionRelativePath(model_data->residual_u_loc);
  if (model_data->residual_ut_loc) model_data->residual_ut_loc = RatelQFunctionRelativePath(model_data->residual_ut_loc);
  if (model_data->residual_utt_loc) model_data->residual_utt_loc = RatelQFunctionRelativePath(model_data->residual_utt_loc);
  model_data->jacobian_loc = RatelQFunctionRelativePath(model_data->jacobian_loc);
  if (model_data->energy_loc) model_data->energy_loc = RatelQFunctionRelativePath(model_data->energy_loc);
  model_data->diagnostic_loc = RatelQFunctionRelativePath(model_data->diagnostic_loc);
  if (model_data->surface_force_loc) model_data->surface_force_loc = RatelQFunctionRelativePath(model_data->surface_force_loc);
  if (model_data->mms_error_loc) model_data->mms_error_loc = RatelQFunctionRelativePath(model_data->mms_error_loc);
  if (model_data->mms_forcing_loc) model_data->mms_forcing_loc = RatelQFunctionRelativePath(model_data->mms_forcing_loc);
  if (model_data->platen_residual_u_loc) model_data->platen_residual_u_loc = RatelQFunctionRelativePath(model_data->platen_residual_u_loc);
  if (model_data->platen_jacobian_loc) model_data->platen_jacobian_loc = RatelQFunctionRelativePath(model_data->platen_jacobian_loc);

  PetscFunctionReturn(0);
}

/// @}
