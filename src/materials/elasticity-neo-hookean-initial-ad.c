/// @file
/// Hyperelastic material at finite strain using Neo-Hookean model in initial configuration

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <ratel/qfunctions/geometry/surface-force-geometry.h>
#include <ratel/qfunctions/geometry/surface-geometry.h>
#include <ratel/qfunctions/geometry/volumetric-geometry.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-initial-ad.h>
#include <ratel/qfunctions/scaled-mass.h>

static const PetscInt    active_field_sizes[]     = {3};
static const char *const active_field_names[]     = {"displacement_x", "displacement_y", "displacement_z"};
static const char *const diagnostic_field_names[] = {"displacement_x",
                                                     "displacement_y",
                                                     "displacement_z",
                                                     "Cauchy_stress_xx",
                                                     "Cauchy_stress_xy",
                                                     "Cauchy_stress_xz",
                                                     "Cauchy_stress_yy",
                                                     "Cauchy_stress_yz",
                                                     "Cauchy_stress_zz",
                                                     "pressure",
                                                     "volumetric_strain",
                                                     "trace_E2",
                                                     "J",
                                                     "strain_energy_density",
                                                     "von_Mises_stress"};
static const char *const quadrature_field_names[] = {"grad u", "S_Voigt", "tape"};
static CeedInt           quadrature_field_sizes[] = {9, 6, RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE};

struct RatelModelData_private elasticity_Neo_Hookean_initial_ad_data_private = {
    .name                          = "hyperelasticity at finite strain, initial configuration Neo-Hookean using Enzyme-AD",
    .type                          = RATEL_MODEL_FEM,
    .setup_q_data_volume           = SetupVolumeGeometry,
    .setup_q_data_volume_loc       = SetupVolumeGeometry_loc,
    .setup_q_data_surface          = SetupSurfaceGeometry,
    .setup_q_data_surface_loc      = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad     = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size            = 10,
    .q_data_surface_size           = 4,
    .q_data_surface_grad_size      = 13,
    .num_active_fields             = 1,
    .active_field_sizes            = active_field_sizes,
    .active_field_names            = active_field_names,
    .active_field_eval_modes       = {CEED_EVAL_GRAD},
    .num_comp_diagnostic           = 15,
    .diagnostic_field_names        = diagnostic_field_names,
    .quadrature_mode               = CEED_GAUSS,
    .residual_u                    = ElasticityResidual_NeoHookeanInitial_AD,
    .residual_u_loc                = ElasticityResidual_NeoHookeanInitial_AD_loc,
    .residual_utt                  = ScaledMass,
    .residual_utt_loc              = ScaledMass_loc,
    .num_quadrature_fields         = sizeof(quadrature_field_sizes) / sizeof(*quadrature_field_sizes),
    .quadrature_field_names        = quadrature_field_names,
    .quadrature_field_sizes        = quadrature_field_sizes,
    .jacobian                      = ElasticityJacobian_NeoHookeanInitial_AD,
    .jacobian_loc                  = ElasticityJacobian_NeoHookeanInitial_AD_loc,
    .energy                        = Energy_NeoHookean,
    .energy_loc                    = Energy_NeoHookean_loc,
    .diagnostic                    = Diagnostic_NeoHookean,
    .diagnostic_loc                = Diagnostic_NeoHookean_loc,
    .surface_force                 = SurfaceForce_NeoHookean,
    .surface_force_loc             = SurfaceForce_NeoHookean_loc,
    .flops_qf_jacobian_u           = 802,
    .flops_qf_jacobian_utt         = 9,
};
RatelModelData elasticity_Neo_Hookean_initial_ad_data = &elasticity_Neo_Hookean_initial_ad_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
@brief Helper function to check allocated Enzyme tape size

@param[in] ratel Ratel context

@return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCheckTapeSize_ElasticityNeoHookeanInitialAD(Ratel ratel) {
  PetscFunctionBegin;

  PetscInt tape_scalars_needed =
      __enzyme_augmentsize((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_dup, enzyme_dup, enzyme_const, enzyme_const) / sizeof(CeedScalar);
  PetscInt tape_scalars_allocated = RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE;

  if (tape_scalars_allocated > tape_scalars_needed) {
    PetscCall(PetscPrintf(ratel->comm, "Warning: allocated %" PetscInt_FMT " CeedScalars for Enzyme tape when %" PetscInt_FMT " needed\n",
                          tape_scalars_allocated, tape_scalars_needed));
  } else if (tape_scalars_allocated < tape_scalars_needed) {
    SETERRQ(ratel->comm, PETSC_ERR_SUP, "Error: allocated %" PetscInt_FMT " CeedScalars for Enzyme tape when %" PetscInt_FMT " needed\n",
            tape_scalars_allocated, tape_scalars_needed);
  }

  PetscFunctionReturn(0);
}

/**
  @brief Create RatelMaterial for Neo-Hookean hyperelasticity at finite strain in initial configuration

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityNeoHookeanInitialAD(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Neo-Hookean Initial AD");

  // Tape size check
  PetscCall(RatelCheckTapeSize_ElasticityNeoHookeanInitialAD(ratel));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_Neo_Hookean_initial_ad_data));
  material->model_data = elasticity_Neo_Hookean_initial_ad_data;

  // QFunction contexts
  PetscCall(RatelMaterialPhysicsContextCreate_NeoHookean(material, &material->ctx_physics));
  PetscCall(RatelMaterialPhysicsSmootherDataSetup_NeoHookean(material));
  material->RatelMaterialPlatenContextCreate = RatelMaterialPlatenContextCreate_NeoHookean;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Neo-Hookean Initial AD Success!");
  PetscFunctionReturn(0);
}

/**
@brief Register Neo-Hookean hyperelasticity at finite strain in initial configuration using Enzyme-AD

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityNeoHookeanInitialAD(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityNeoHookeanInitialAD);

  PetscFunctionReturn(0);
}

/// @}