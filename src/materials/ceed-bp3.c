/// @file
/// CEED BP 3 - scalar Poisson

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/qfunctions/error/ceed-scalar-bps-manufactured-error.h>
#include <ratel/qfunctions/forcing/ceed-scalar-bps-manufactured-force.h>
#include <ratel/qfunctions/geometry/volumetric-symmetric-geometry.h>
#include <ratel/qfunctions/models/ceed-bp3.h>

static const PetscInt    field_sizes[] = {1};
static const char *const field_names[] = {"scalar_value"};

struct RatelModelData_private ceed_bp3_data_private = {
    .name                    = "CEED BP3",
    .type                    = RATEL_MODEL_FEM,
    .setup_q_data_volume     = SetupVolumeGeometrySymmetric,
    .setup_q_data_volume_loc = SetupVolumeGeometrySymmetric_loc,
    .q_data_volume_size      = 7,
    .num_active_fields       = 1,
    .active_field_sizes      = field_sizes,
    .active_field_names      = field_names,
    .active_field_eval_modes = {CEED_EVAL_GRAD},
    .num_comp_diagnostic     = 1,
    .diagnostic_field_names  = field_names,
    .quadrature_mode         = CEED_GAUSS,
    .residual_u              = Residual_CEED_BP3,
    .residual_u_loc          = Residual_CEED_BP3_loc,
    .num_quadrature_fields   = 0,
    .jacobian                = Jacobian_CEED_BP3,
    .jacobian_loc            = Jacobian_CEED_BP3_loc,
    .diagnostic              = Diagnostic_CEED_BP3,
    .diagnostic_loc          = Diagnostic_CEED_BP3_loc,
    .mms_error               = MMSError_CEED_ScalarBPs,
    .mms_error_loc           = MMSError_CEED_ScalarBPs_loc,
    .mms_forcing             = MMSForce_CEED_ScalarBPs,
    .mms_forcing_loc         = MMSForce_CEED_ScalarBPs_loc,
    .flops_qf_jacobian_u     = 15,
};
RatelModelData ceed_bp3_data = &ceed_bp3_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create RatelMaterial for CEED BP3

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_CEED_BP3(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create CEED BP3");

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, ceed_bp3_data));
  material->model_data = ceed_bp3_data;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create CEED BP3 Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Register CEED BP3 model

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_CEED_BP3(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, CEED_BP3);

  PetscFunctionReturn(0);
}

/// @}
