/// @file
/// Hyperelastic material at finite strain using Neo-Hookean model in current configuration

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <ratel/qfunctions/geometry/surface-force-geometry.h>
#include <ratel/qfunctions/geometry/surface-geometry.h>
#include <ratel/qfunctions/geometry/volumetric-geometry.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-current-ad.h>
#include <ratel/qfunctions/scaled-mass.h>

static const PetscInt    active_field_sizes[]     = {3};
static const char *const active_field_names[]     = {"displacement_x", "displacement_y", "displacement_z"};
static const char *const diagnostic_field_names[] = {"displacement_x",
                                                     "displacement_y",
                                                     "displacement_z",
                                                     "Cauchy_stress_xx",
                                                     "Cauchy_stress_xy",
                                                     "Cauchy_stress_xz",
                                                     "Cauchy_stress_yy",
                                                     "Cauchy_stress_yz",
                                                     "Cauchy_stress_zz",
                                                     "pressure",
                                                     "volumetric_strain",
                                                     "trace_E2",
                                                     "J",
                                                     "strain_energy_density",
                                                     "von_Mises_stress"};
static const char *const quadrature_field_names[] = {"dXdx", "e"};
static CeedInt           quadrature_field_sizes[] = {9, 6};

struct RatelModelData_private elasticity_Neo_Hookean_current_ad_data_private = {
    .name                          = "hyperelasticity at finite strain, current configuration Neo-Hookean using Enzyme-AD",
    .type                          = RATEL_MODEL_FEM,
    .setup_q_data_volume           = SetupVolumeGeometry,
    .setup_q_data_volume_loc       = SetupVolumeGeometry_loc,
    .setup_q_data_surface          = SetupSurfaceGeometry,
    .setup_q_data_surface_loc      = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad     = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size            = 10,
    .q_data_surface_size           = 4,
    .q_data_surface_grad_size      = 13,
    .num_active_fields             = 1,
    .active_field_sizes            = active_field_sizes,
    .active_field_names            = active_field_names,
    .active_field_eval_modes       = {CEED_EVAL_GRAD},
    .num_comp_diagnostic           = 15,
    .diagnostic_field_names        = diagnostic_field_names,
    .quadrature_mode               = CEED_GAUSS,
    .residual_u                    = ElasticityResidual_NeoHookeanCurrent_AD,
    .residual_u_loc                = ElasticityResidual_NeoHookeanCurrent_AD_loc,
    .residual_utt                  = ScaledMass,
    .residual_utt_loc              = ScaledMass_loc,
    .num_quadrature_fields         = sizeof(quadrature_field_sizes) / sizeof(*quadrature_field_sizes),
    .quadrature_field_names        = quadrature_field_names,
    .quadrature_field_sizes        = quadrature_field_sizes,
    .jacobian                      = ElasticityJacobian_NeoHookeanCurrent_AD,
    .jacobian_loc                  = ElasticityJacobian_NeoHookeanCurrent_AD_loc,
    .energy                        = Energy_NeoHookean,
    .energy_loc                    = Energy_NeoHookean_loc,
    .diagnostic                    = Diagnostic_NeoHookean,
    .diagnostic_loc                = Diagnostic_NeoHookean_loc,
    .surface_force                 = SurfaceForce_NeoHookean,
    .surface_force_loc             = SurfaceForce_NeoHookean_loc,
    .flops_qf_jacobian_u           = 288,
    .flops_qf_jacobian_utt         = 9,
};
RatelModelData elasticity_Neo_Hookean_current_ad_data = &elasticity_Neo_Hookean_current_ad_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create RatelMaterial for Neo-Hookean hyperelasticity at finite strain in current configuration

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityNeoHookeanCurrentAD(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Neo-Hookean Current AD");

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_Neo_Hookean_current_ad_data));
  material->model_data = elasticity_Neo_Hookean_current_ad_data;

  // QFunction contexts
  PetscCall(RatelMaterialPhysicsContextCreate_NeoHookean(material, &material->ctx_physics));
  PetscCall(RatelMaterialPhysicsSmootherDataSetup_NeoHookean(material));
  material->RatelMaterialPlatenContextCreate = RatelMaterialPlatenContextCreate_NeoHookean;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Create Elasticity Neo-Hookean Current AD Success!");
  PetscFunctionReturn(0);
}

/**
@brief Register Neo-Hookean hyperelasticity at finite strain in current configuration using Enzyme-AD

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityNeoHookeanCurrentAD(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityNeoHookeanCurrentAD);

  PetscFunctionReturn(0);
}

/// @}
