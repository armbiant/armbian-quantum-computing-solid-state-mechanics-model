/// @file
/// Setup Mooney-Rivlin physics context objects

#include <ceed.h>
#include <math.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/mooney-rivlin.h>
#include <stddef.h>

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Process command line options for Neo-Hookean physics

  @param[in]   material  RatelMaterial to setup physics context
  @param[in]   nu        Poisson's ratio
  @param[in]   mu_1      Material Property mu_1
  @param[in]   mu_2      Material Property mu_2
  @param[in]   rho       Density for scaled mass matrix
  @param[out]  ctx       libCEED QFunctionContext for Neo-Hookean physics

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedPhysicsContextCreate_MooneyRivlin(RatelMaterial material, CeedScalar nu, CeedScalar mu_1, CeedScalar mu_2,
                                                                 CeedScalar rho, CeedQFunctionContext *ctx) {
  Ratel                ratel = material->ratel;
  MooneyRivlinPhysics *physics;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Physics Context Mooney-Rivlin");

  // Create context data with correct scaling
  PetscCall(PetscCalloc1(1, &physics));
  physics->mu_1                                          = mu_1 * ratel->material_units->Pascal;
  physics->mu_2                                          = mu_2 * ratel->material_units->Pascal;
  physics->lambda                                        = 2 * (mu_1 + mu_2) * nu / (1 - 2 * nu) * ratel->material_units->Pascal;
  physics->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (-ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);

  // Create context object
  RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*physics), physics));
  PetscCall(PetscFree(physics));

  // Common parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "rho", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                                                          1, "density"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift v", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift a", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));

  // Mooney Rivlin specific parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "mu 1", offsetof(MooneyRivlinPhysics, mu_1), 1, "first model parameter"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "mu 2", offsetof(MooneyRivlinPhysics, mu_2), 1, "second model parameter"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "lambda", offsetof(MooneyRivlinPhysics, lambda), 1, "first Lamé parameter"));

  // Debugging output
  if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx, stdout));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Physics Context Mooney-Rivlin Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Build libCEED QFunctionContext for Mooney-Rivlin physics

  @param[in]   material  RatelMaterial to setup physics context
  @param[out]  ctx       libCEED QFunctionContext for Mooney-Rivlin physics

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialPhysicsContextCreate_MooneyRivlin(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel = material->ratel;
  CeedScalar nu = -1.0, mu_1 = -1.0, mu_2 = -1.0, rho = 1.0;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Context Mooney-Rivlin");

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Mooney-Rivlin physical parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, NULL));
    if (nu < 0 || nu >= 0.5) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires Poisson ratio -nu option in [0, 0.5)");
    RatelDebug(ratel, "---- nu: %f", nu);

    PetscCall(PetscOptionsScalar("-mu_1", "Material Property mu_1", NULL, mu_1, &mu_1, NULL));
    if (mu_1 < 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_1 option (Pa)");
    RatelDebug(ratel, "---- mu_1: %f", mu_1);

    PetscCall(PetscOptionsScalar("-mu_2", "Material Property mu_2", NULL, mu_2, &mu_2, NULL));
    if (mu_2 < 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_2 option (Pa)");
    RatelDebug(ratel, "---- mu_2: %f", mu_2);

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    if (rho < 0.0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    RatelDebug(ratel, "---- rho: %f", rho);

    PetscOptionsEnd();  // End of setting Physics
    PetscCall(PetscFree(cl_prefix));
  }

  // Create context
  PetscCall(RatelCeedPhysicsContextCreate_MooneyRivlin(material, nu, mu_1, mu_2, rho, ctx));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Context Mooney-Rivlin Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup data for libCEED QFunctionContext for smoother with Mooney-Rivlin physics

  @param[in]   material      RatelMaterial to setup physics context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialPhysicsSmootherDataSetup_MooneyRivlin(RatelMaterial material) {
  Ratel      ratel = material->ratel;
  CeedScalar nu = -1.0, nu_smoother = -1.0, mu_1 = -1.0, mu_2 = -1.0;
  PetscBool  nu_smoother_flag = PETSC_FALSE;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Smoother Context Mooney-Rivlin");

  // Get parameter values
  if (material->num_physics_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Mooney-Rivlin physical parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, NULL));
    if (nu < 0 || nu >= 0.5) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires Poisson ratio -nu option in [0, 0.5)");
    RatelDebug(ratel, "---- nu: %f", nu);

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &nu_smoother_flag));
    if (nu_smoother_flag && (nu_smoother < 0 || nu_smoother >= 0.5)) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires Poisson ratio -nu option in [0, 0.5)");
      // LCOV_EXCL_STOP
    }
    if (nu_smoother_flag) RatelDebug(ratel, "---- nu: %f", nu_smoother);
    else RatelDebug(ratel, "---- No Nu for smoother set");

    PetscCall(PetscOptionsScalar("-mu_1", "Material Property mu_1", NULL, mu_1, &mu_1, NULL));
    if (mu_1 < 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_1 option (Pa)");
    RatelDebug(ratel, "---- mu_1: %f", mu_1);

    PetscCall(PetscOptionsScalar("-mu_2", "Material Property mu_2", NULL, mu_2, &mu_2, NULL));
    if (mu_2 < 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_2 option (Pa)");
    RatelDebug(ratel, "---- mu_2: %f", mu_2);

    PetscOptionsEnd();  // End of setting Physics
    PetscCall(PetscFree(cl_prefix));

    // Create context
    if (nu_smoother_flag) {
      const char *label_name     = "lambda";
      PetscSizeT  label_name_len = strlen(label_name) + 1;

      material->num_physics_smoother_values = 1;
      PetscCall(PetscCalloc1(1, &material->ctx_physics_values));
      PetscCall(PetscCalloc1(1, &material->ctx_physics_smoother_values));
      material->ctx_physics_values[0]          = 2 * (mu_1 + mu_2) * nu / (1 - 2 * nu) * ratel->material_units->Pascal;
      material->ctx_physics_smoother_values[0] = 2 * (mu_1 + mu_2) * nu_smoother / (1 - 2 * nu_smoother) * ratel->material_units->Pascal;
      PetscCall(PetscCalloc1(1, &material->ctx_physics_label_names));
      PetscCall(PetscCalloc1(label_name_len, &material->ctx_physics_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_physics_label_names[0], label_name, label_name_len));
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Setup Physics Smoother Context Mooney-Rivlin Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Build libCEED QFunctionContext for platen BC with Mooney-Rivlin physics

  @param[in]   material    RatelMaterial to setup platen context
  @param[in]   normal      Normal vector to platen surface
  @param[in]   center      Center of platen surface
  @param[in]   distance    Displacement of platen along normal
  @param[in]   gamma       Nitsche's method penalty parameter
  @param[in]   final_time  Final time to complete platen movement
  @param[out]  ctx         libCEED QFunctionContext for platen

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialPlatenContextCreate_MooneyRivlin(RatelMaterial material, CeedScalar normal[3], CeedScalar center[3], CeedScalar distance,
                                                             CeedScalar gamma, CeedScalar final_time, CeedQFunctionContext *ctx) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Platen Context Mooney-Rivlin");

  MooneyRivlinPhysics *physics = NULL;
  RatelCeedCall(ratel, CeedQFunctionContextGetDataRead(material->ctx_physics, CEED_MEM_HOST, &physics));
  MooneyRivlinPlatenData platen_data = {
      .normal[0]  = normal[0],
      .normal[1]  = normal[1],
      .normal[2]  = normal[2],
      .center[0]  = center[0],
      .center[1]  = center[1],
      .center[2]  = center[2],
      .distance   = distance,
      .gamma      = gamma,
      .time       = RATEL_UNSET_INITIAL_TIME,
      .final_time = final_time,
      .material   = *physics,
  };

  // Create context object
  RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(platen_data), &platen_data));
  RatelCeedCall(ratel, CeedQFunctionContextRestoreDataRead(material->ctx_physics, &physics));

  // Set up context
  // Platen parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "normal", offsetof(MooneyRivlinPlatenData, normal), 3, "external normal of platen"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "center", offsetof(MooneyRivlinPlatenData, center), 3, "center of platen"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "distance", offsetof(MooneyRivlinPlatenData, distance), 1,
                                                          "displacement of platen along normal"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "time", offsetof(MooneyRivlinPlatenData, time), 1, "current solver time"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "final time", offsetof(MooneyRivlinPlatenData, final_time), 1, "final solver time"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "gamma", offsetof(MooneyRivlinPlatenData, gamma), 1, "Nitche's method parameter"));

  // Physics parameters
  // Common parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "rho", offsetof(MooneyRivlinPlatenData, material.common_parameters[RATEL_COMMON_PARAMETER_RHO]), 1, "density"));
  RatelCeedCall(
      ratel, CeedQFunctionContextRegisterDouble(
                 *ctx, "shift v", offsetof(MooneyRivlinPlatenData, material.common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
  RatelCeedCall(
      ratel, CeedQFunctionContextRegisterDouble(
                 *ctx, "shift a", offsetof(MooneyRivlinPlatenData, material.common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));

  // Mooney Rivlin specific parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "mu 1", offsetof(MooneyRivlinPlatenData, material.mu_1), 1, "first model parameter"));
  RatelCeedCall(ratel,
                CeedQFunctionContextRegisterDouble(*ctx, "mu 2", offsetof(MooneyRivlinPlatenData, material.mu_2), 1, "second model parameter"));
  RatelCeedCall(ratel,
                CeedQFunctionContextRegisterDouble(*ctx, "lambda", offsetof(MooneyRivlinPlatenData, material.lambda), 1, "first Lamé parameter"));

  // Debugging output
  if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx, stdout));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Create Platen Context Mooney-Rivlin Success!");
  PetscFunctionReturn(0);
}

/// @}
