/// @file
/// Provide weak stubs for Ratel models that could be missing

#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>

#define RATEL_MODEL(model_cl_argument, model_postfix)                                                                                                  \
  RATEL_INTERN PetscErrorCode RatelMaterialCreate_##model_postfix(Ratel, RatelMaterial) __attribute__((weak));                                         \
  PetscErrorCode              RatelMaterialCreate_##model_postfix(Ratel ratel, RatelMaterial material) {                                               \
                 PetscFunctionBegin;                                                                                                                   \
                 SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Model data for " model_cl_argument " not compiled");                                         \
                 PetscFunctionReturn(0);                                                                                                               \
  }                                                                                                                                                    \
                                                                                                                                                       \
  RATEL_INTERN PetscErrorCode RatelRegisterModel_##model_postfix(Ratel, const char *, PetscFunctionList *) __attribute__((weak));                      \
  PetscErrorCode              RatelRegisterModel_##model_postfix(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) { \
                 PetscFunctionBegin;                                                                                                                   \
                 RatelDebug256(ratel, RATEL_DEBUG_ORANGE, "---- Weak Registration of model \"%s\"", cl_argument);                                      \
                 PetscCall(PetscFunctionListAdd(material_create_functions, model_cl_argument, RatelMaterialCreate_##model_postfix));                   \
                 PetscFunctionReturn(0);                                                                                                               \
  }

#include "ratel-model-list.h"
#undef RATEL_MODEL
