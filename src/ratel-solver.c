/// @file
/// Implementation of Ratel solver management interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-monitor.h>
#include <ratel-petsc-ops.h>
#include <ratel-solver.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <string.h>

/// @ingroup RatelSolvers
/// @{

/**
  @brief Setup context data for Jacobian evaluation

  @param[in]   ratel              Ratel context
  @param[out]  ctx_form_jacobian  Context data for Jacobian evaluation

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelSetupFormJacobianCtx(Ratel ratel, RatelFormJacobianContext ctx_form_jacobian) {
  VecType vec_type;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Setup Form Jacobian Context");

  ctx_form_jacobian->ratel             = ratel;
  ctx_form_jacobian->num_levels        = ratel->num_multigrid_levels;
  ctx_form_jacobian->ctx_jacobian_fine = ratel->ctx_jacobian[ratel->num_multigrid_levels - 1];

  // Vec type
  PetscCall(DMGetVecType(ratel->dm_orig, &vec_type));
  ctx_form_jacobian->vec_type = vec_type;

  // PETSc objects
  ctx_form_jacobian->mat_jacobian        = ratel->mat_jacobian;
  ctx_form_jacobian->mat_jacobian_coarse = ratel->mat_jacobian_coarse;

  // libCEED objects
  ctx_form_jacobian->op_coarse  = ratel->ctx_jacobian[0]->op;
  ctx_form_jacobian->coo_values = ratel->coo_values_ceed;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Setup Form Jacobian Context Success");
  PetscFunctionReturn(0);
}

/**
  @brief Setup solver contexts

  @param[in,out]  ratel  Ratel context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSetupSolver(Ratel ratel) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DM Setup Solver");

  // Residual and Jacobian operators
  {
    PetscInt     X_loc_size;
    Vec          X_loc, X_loc_residual;
    CeedInt      num_sub_operators;
    CeedVector   x_loc, y_loc;
    CeedOperator op_residual_u, op_residual_ut, op_residual_utt, op_jacobian, op_dirichlet;

    // -- Work vectors
    RatelDebug(ratel, "---- Setting up work vectors");
    ratel->num_multigrid_levels_setup = 1;
    PetscCall(DMCreateLocalVector(ratel->dm_hierarchy[0], &X_loc));
    PetscCall(VecDuplicate(X_loc, &X_loc_residual));
    PetscCall(VecGetSize(X_loc, &X_loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &x_loc));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &y_loc));

    // -- libCEED operators
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_residual_u));
    RatelCeedCall(ratel, CeedOperatorSetName(op_residual_u, "Residual evaluator, u term"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_residual_ut));
    RatelCeedCall(ratel, CeedOperatorSetName(op_residual_ut, "Residual evaluator, ut term"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_residual_utt));
    RatelCeedCall(ratel, CeedOperatorSetName(op_residual_utt, "Residual evaluator, utt term"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_jacobian));
    RatelCeedCall(ratel, CeedOperatorSetName(op_jacobian, "Jacobian"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_dirichlet));
    RatelCeedCall(ratel, CeedOperatorSetName(op_dirichlet, "Dirichlet boundary conditions"));

    // -- Residual and Jacobian
    RatelDebug(ratel, "---- Residual and Jacobian operators");
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupResidualJacobianSuboperators(ratel->materials[i], op_residual_u, op_residual_ut, op_residual_utt, op_jacobian));
    }
    RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyReuse(op_jacobian, true));

    // -- Boundaries
    // ---- Neumann boundaries
    RatelDebug(ratel, "---- Neumann boundaries");
    PetscCall(RatelCeedAddBoundariesNeumann(ratel, ratel->dm_hierarchy[0], op_residual_u));
    // ---- Platen boundaries
    RatelDebug(ratel, "---- Platen boundaries");
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      if (ratel->materials[i]->model_data->platen_residual_u_loc && ratel->materials[i]->model_data->platen_jacobian_loc) {
        PetscCall(RatelMaterialSetupPlatenSuboperators(ratel->materials[i], op_residual_u, op_jacobian));
      }
    }

    // ---- Dirichlet boundaries
    RatelDebug(ratel, "---- Dirichlet boundaries");
    {
      Vec boundary_mask, X;

      PetscCall(DMGetNamedLocalVector(ratel->dm_hierarchy[0], "boundary mask", &boundary_mask));
      PetscCall(VecZeroEntries(boundary_mask));
      PetscCall(DMGetGlobalVector(ratel->dm_hierarchy[0], &X));
      PetscCall(VecSet(X, 1.0));
      PetscCall(DMGlobalToLocal(ratel->dm_hierarchy[0], X, INSERT_VALUES, boundary_mask));
      PetscCall(DMRestoreNamedLocalVector(ratel->dm_hierarchy[0], "boundary mask", &boundary_mask));
      PetscCall(DMRestoreGlobalVector(ratel->dm_hierarchy[0], &X));
    }
    PetscCall(RatelCeedAddBoundariesDirichletClamp(ratel, ratel->dm_hierarchy[0], op_dirichlet));
    RatelCeedCall(ratel, CeedOperatorReferenceCopy(op_dirichlet, &ratel->op_dirichlet));

    // -- Get context labels
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(op_dirichlet, "time", &ratel->op_dirichlet_time_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(op_residual_u, "time", &ratel->op_residual_u_time_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(op_jacobian, "time", &ratel->op_jacobian_time_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(op_jacobian, "shift v", &ratel->op_jacobian_shift_v_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(op_jacobian, "shift a", &ratel->op_jacobian_shift_a_label));

    // -- Setup contexts
    RatelDebug(ratel, "---- Creating contexts");
    // ---- Residual
    PetscCall(RatelOperatorApplyContextCreate(ratel, "residual u", ratel->dm_hierarchy[0], ratel->dm_hierarchy[0], X_loc_residual, NULL, x_loc, y_loc,
                                              op_residual_u, &ratel->ctx_residual_u));
    RatelCeedCall(ratel, CeedCompositeOperatorGetNumSub(op_residual_ut, &num_sub_operators));
    PetscCall(RatelOperatorApplyContextCreate(ratel, "residual u_t", ratel->dm_hierarchy[0], ratel->dm_hierarchy[0], X_loc, NULL, x_loc, y_loc,
                                              op_residual_ut, &ratel->ctx_residual_ut));
    PetscCall(RatelOperatorApplyContextCreate(ratel, "residual u_tt", ratel->dm_hierarchy[0], ratel->dm_hierarchy[0], X_loc, NULL, x_loc, y_loc,
                                              op_residual_utt, &ratel->ctx_residual_utt));
    // ---- Jacobian
    PetscCall(PetscCalloc1(1, &ratel->ctx_jacobian));
    PetscCall(PetscCalloc1(1, &ratel->ctx_form_jacobian));
    PetscCall(RatelOperatorApplyContextCreate(ratel, "Jacobian, fine level", ratel->dm_hierarchy[0], ratel->dm_hierarchy[0], X_loc, NULL, x_loc,
                                              y_loc, op_jacobian, &ratel->ctx_jacobian[0]));
    PetscCall(RatelSetupFormJacobianCtx(ratel, ratel->ctx_form_jacobian));
    PetscCall(PetscCalloc1(1, &ratel->mat_jacobian));
    {
      Vec      X;
      VecType  vec_type;
      PetscInt X_l_size, X_g_size;

      PetscCall(DMGetVecType(ratel->dm_hierarchy[0], &vec_type));
      PetscCall(DMGetGlobalVector(ratel->dm_hierarchy[0], &X));
      PetscCall(VecGetSize(X, &X_g_size));
      PetscCall(VecGetLocalSize(X, &X_l_size));
      PetscCall(DMRestoreGlobalVector(ratel->dm_hierarchy[0], &X));
      PetscCall(MatCreateShell(ratel->comm, X_l_size, X_l_size, X_g_size, X_g_size, ratel->ctx_jacobian[0], &ratel->mat_jacobian[0]));
      PetscCall(MatShellSetContextDestroy(ratel->mat_jacobian[0], (PetscErrorCode(*)(void *))RatelOperatorApplyContextDestroy));
      PetscCall(MatShellSetOperation(ratel->mat_jacobian[0], MATOP_MULT, (void (*)(void))RatelApplyJacobian));
      PetscCall(MatShellSetOperation(ratel->mat_jacobian[0], MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
      PetscCall(MatShellSetVecType(ratel->mat_jacobian[0], vec_type));
    }

    // -- Forcing term
    RatelDebug(ratel, "---- Forcing operators");
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupForcingSuboperator(ratel->materials[i], op_residual_u));
    }

    // -- Debugging information
    if (ratel->is_debug) {
      // LCOV_EXCL_START
      RatelDebug(ratel, "---- Final residual operator");
      RatelCeedCall(ratel, CeedOperatorView(op_residual_u, stdout));
      if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
        RatelDebug(ratel, "---- Final scaled mass operator for residual");
        RatelCeedCall(ratel, CeedOperatorView(op_residual_utt, stdout));
      }
      RatelDebug(ratel, "---- Final Jacobian operator");
      RatelCeedCall(ratel, CeedOperatorView(op_jacobian, stdout));
      // LCOV_EXCL_STOP
    }

    // -- Cleanup
    PetscCall(VecDestroy(&X_loc));
    PetscCall(VecDestroy(&X_loc_residual));
    RatelCeedCall(ratel, CeedVectorDestroy(&x_loc));
    RatelCeedCall(ratel, CeedVectorDestroy(&y_loc));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_residual_u));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_residual_ut));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_residual_utt));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_jacobian));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_dirichlet));
  }

  // -- Energy
  {
    CeedOperator op_energy;
    PetscInt     Y_loc_size;
    Vec          Y_loc;
    CeedVector   y_loc;

    // -- Create vectors
    PetscCall(DMCreateLocalVector(ratel->dm_energy, &Y_loc));
    PetscCall(VecGetSize(Y_loc, &Y_loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, Y_loc_size, &y_loc));

    // -- libCEED operators
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_energy));
    RatelCeedCall(ratel, CeedOperatorSetName(op_energy, "energy"));

    RatelDebug(ratel, "---- Energy operator");
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupEnergySuboperator(ratel->materials[i], op_energy));
    }

    // -- Context
    PetscCall(RatelOperatorApplyContextCreate(ratel, "energy", ratel->dm_hierarchy[0], ratel->dm_energy, ratel->ctx_residual_u->X_loc, Y_loc,
                                              ratel->ctx_residual_u->x_loc, y_loc, op_energy, &ratel->ctx_energy));

    // -- Debugging information
    if (ratel->is_debug) {
      // LCOV_EXCL_START
      RatelDebug(ratel, "---- Final energy operator");
      RatelCeedCall(ratel, CeedOperatorView(op_energy, stdout));
      // LCOV_EXCL_STOP
    }

    // -- Cleanup
    PetscCall(VecDestroy(&Y_loc));
    RatelCeedCall(ratel, CeedVectorDestroy(&y_loc));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_energy));
  }

  // -- Diagnostic
  {
    PetscInt     X_loc_size;
    Vec          X_loc;
    CeedVector   x_loc, y_loc;
    CeedOperator op_diagnostic, op_mass_diagnostic;

    // -- Create vectors
    PetscCall(DMGetLocalVector(ratel->dm_diagnostic, &X_loc));
    PetscCall(VecGetSize(X_loc, &X_loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &x_loc));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &y_loc));
    PetscCall(DMRestoreLocalVector(ratel->dm_diagnostic, &X_loc));

    // -- libCEED operators
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_diagnostic));
    RatelCeedCall(ratel, CeedOperatorSetName(op_diagnostic, "diagnostic values"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_mass_diagnostic));
    RatelCeedCall(ratel, CeedOperatorSetName(op_mass_diagnostic, "mass operator for diagnostic value projection"));

    RatelDebug(ratel, "---- Diagnostic operator");
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupDiagnosticSuboperators(ratel->materials[i], op_mass_diagnostic, op_diagnostic));
    }

    // -- Setting up diagnostic quantities context
    RatelDebug(ratel, "---- Setting up diagnostic quantities context");
    PetscCall(RatelOperatorApplyContextCreate(ratel, "diagnostic quantities", ratel->dm_hierarchy[0], ratel->dm_diagnostic,
                                              ratel->ctx_residual_u->X_loc, NULL, ratel->ctx_residual_u->x_loc, y_loc, op_diagnostic,
                                              &ratel->ctx_diagnostic));

    // -- Setting up diagnostic projection context
    RatelDebug(ratel, "---- Setting up diagnostic projection context");
    PetscCall(RatelOperatorApplyContextCreate(ratel, "diagnostic projection", ratel->dm_diagnostic, ratel->dm_diagnostic, NULL, NULL, x_loc, y_loc,
                                              op_mass_diagnostic, &ratel->ctx_diagnostic_projection));

    // -- Setting up diagnostic projection KSP
    RatelDebug(ratel, "---- Setting up diagnostic projection KSP");
    {
      PetscInt l_size, g_size;
      KSP      ksp;
      PC       pc;
      Mat      mat;
      Vec      X;

      // ---- Projection operator
      PetscCall(DMGetGlobalVector(ratel->dm_diagnostic, &X));
      PetscCall(VecGetSize(X, &g_size));
      PetscCall(VecGetLocalSize(X, &l_size));
      PetscCall(MatCreateShell(ratel->comm, l_size, l_size, g_size, g_size, ratel->ctx_diagnostic_projection, &mat));
      PetscCall(MatShellSetOperation(mat, MATOP_MULT, (void (*)(void))RatelApplyOperator));
      PetscCall(MatShellSetOperation(mat, MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
      {
        VecType vec_type;

        PetscCall(VecGetType(X, &vec_type));
        PetscCall(MatShellSetVecType(mat, vec_type));
      }
      PetscCall(DMRestoreGlobalVector(ratel->dm_diagnostic, &X));

      // ---- Projection KSP
      PetscCall(KSPCreate(ratel->comm, &ksp));
      PetscCall(KSPSetOptionsPrefix(ksp, "projection_"));
      PetscCall(KSPSetType(ksp, KSPCG));
      PetscCall(KSPSetOperators(ksp, mat, mat));
      PetscCall(KSPSetTolerances(ksp, 1e-04, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
      PetscCall(KSPGetPC(ksp, &pc));
      PetscCall(PCSetType(pc, PCJACOBI));
      PetscCall(KSPSetFromOptions(ksp));
      ratel->ksp_diagnostic_projection = ksp;

      // ---- Cleanup
      PetscCall(MatDestroy(&mat));
    }

    // -- Debugging information
    if (ratel->is_debug) {
      // LCOV_EXCL_START
      RatelDebug(ratel, "---- Final diagnostic operator");
      RatelCeedCall(ratel, CeedOperatorView(op_diagnostic, stdout));
      RatelDebug(ratel, "---- Final diagnostic projection operator");
      RatelCeedCall(ratel, CeedOperatorView(op_mass_diagnostic, stdout));
      // LCOV_EXCL_STOP
    }

    // ---- Cleanup
    RatelCeedCall(ratel, CeedVectorDestroy(&x_loc));
    RatelCeedCall(ratel, CeedVectorDestroy(&y_loc));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_diagnostic));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_mass_diagnostic));
  }

  // -- Surface force
  if (ratel->surface_force_face_count != 0) {
    PetscInt      Y_loc_size;
    Vec           Y_loc;
    CeedVector    y_loc;
    CeedOperator *ops_surface_force;

    // -- Create vectors
    PetscCall(DMCreateLocalVector(ratel->dm_surface_force, &Y_loc));
    PetscCall(VecGetSize(Y_loc, &Y_loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, Y_loc_size, &y_loc));

    // -- libCEED operators
    PetscCall(PetscCalloc1(ratel->surface_force_face_count, &ops_surface_force));
    for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
      char operator_name[30];

      RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ops_surface_force[i]));
      PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "surface force on face %" PetscInt_FMT, ratel->surface_force_dm_faces[i]));
      RatelCeedCall(ratel, CeedOperatorSetName(ops_surface_force[i], operator_name));
    }

    RatelDebug(ratel, "---- Surface force operator");
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupSurfaceForceSuboperators(ratel->materials[i], ops_surface_force));
    }
    ratel->ops_surface_force = ops_surface_force;

    // -- Context
    PetscCall(RatelOperatorApplyContextCreate(ratel, "surface force", ratel->dm_hierarchy[0], ratel->dm_surface_force, ratel->ctx_residual_u->X_loc,
                                              Y_loc, ratel->ctx_residual_u->x_loc, y_loc, ops_surface_force[0], &ratel->ctx_surface_force));

    // -- Debugging information
    if (ratel->is_debug) {
      // LCOV_EXCL_START
      RatelDebug(ratel, "---- Final surface force operators");
      for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) RatelCeedCall(ratel, CeedOperatorView(ops_surface_force[i], stdout));
      // LCOV_EXCL_STOP
    }

    // -- Cleanup
    PetscCall(VecDestroy(&Y_loc));
    RatelCeedCall(ratel, CeedVectorDestroy(&y_loc));
  }

  // -- Surface force centroids
  RatelDebug(ratel, "---- Face centroids");
  PetscCall(RatelSetupSurfaceForceCentroids(ratel, ratel->dm_hierarchy[0]));

  // -- MMS error
  {
    PetscBool has_mms = PETSC_TRUE;

    // ---- Check for MMS support
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscBool has_mms_current;

      PetscCall(RatelMaterialHasMMS(ratel->materials[i], &has_mms_current));
      has_mms = has_mms && has_mms_current;
    }
    ratel->has_mms = has_mms;
  }
  if (ratel->has_mms) {
    CeedOperator op_mms_error;

    // -- libCEED operators
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_mms_error));
    RatelCeedCall(ratel, CeedOperatorSetName(op_mms_error, "MMS error"));

    RatelDebug(ratel, "---- MMS Error operator");
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupMMSErrorSuboperator(ratel->materials[i], op_mms_error));
    }

    // -- Context
    RatelDebug(ratel, "---- Creating operator context data");
    PetscCall(RatelOperatorApplyContextCreate(ratel, "MMS error", ratel->dm_hierarchy[0], ratel->dm_hierarchy[0], ratel->ctx_residual_u->X_loc,
                                              ratel->ctx_residual_u->Y_loc, ratel->ctx_residual_u->x_loc, ratel->ctx_residual_u->y_loc, op_mms_error,
                                              &ratel->ctx_mms_error));

    // -- Debugging information
    if (ratel->is_debug) {
      // LCOV_EXCL_START
      RatelDebug(ratel, "---- Final MMS error operator");
      RatelCeedCall(ratel, CeedOperatorView(op_mms_error, stdout));
      // LCOV_EXCL_STOP
    }

    // -- Cleanup
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_mms_error));
  }

  // -- Initial boundary values for residual evaluation
  PetscCall(RatelUpdateTimeAndBoundaryValues(ratel, 1.0));

  // -- FLOPs counting
  {
    PetscCall(PetscCalloc1(1, &ratel->flops_jacobian));
    CeedSize ceed_flops_estimate = 0;

    RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(ratel->ctx_jacobian[0]->op, &ceed_flops_estimate));
    ratel->flops_jacobian[0] = ceed_flops_estimate;
  }

  // -- Jacobian event
  PetscCall(PetscClassIdRegister("libCEED", &ratel->libceed_class_id));
  PetscCall(PetscLogEventRegister("RatelJacobianApp", ratel->libceed_class_id, &ratel->event_jacobian));

  // -- Solver context
  switch (ratel->solver_type) {
    case RATEL_SOLVER_STATIC:
      RatelDebug(ratel, "---- Setting SNES contexts");
      // -- SNES Function
      PetscCall(DMSNESSetFunction(ratel->dm_hierarchy[0], RatelSNESFormResidual, ratel->ctx_residual_u));
      // -- SNES Jacobian
      PetscCall(DMSNESSetJacobian(ratel->dm_hierarchy[0], RatelSNESFormJacobian, ratel->ctx_form_jacobian));
      break;
    case RATEL_SOLVER_QUASISTATIC:
      RatelDebug(ratel, "---- Setting TS contexts");
      // -- TS IFunction
      PetscCall(DMTSSetIFunction(ratel->dm_hierarchy[0], RatelTSFormIResidual, ratel->ctx_residual_ut));
      // -- TS IJacobian
      PetscCall(DMTSSetIJacobian(ratel->dm_hierarchy[0], RatelTSFormIJacobian, ratel->ctx_form_jacobian));
      break;
    case RATEL_SOLVER_DYNAMIC:
      // -- TS I2Function
      PetscCall(DMTSSetI2Function(ratel->dm_hierarchy[0], RatelTSFormI2Residual, ratel->ctx_residual_utt));
      // -- TS IJacobian
      PetscCall(DMTSSetI2Jacobian(ratel->dm_hierarchy[0], RatelTSFormI2Jacobian, ratel->ctx_form_jacobian));
      break;
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DM Setup Solver Success!");
  PetscFunctionReturn(0);
}

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Setup default TS options, the DM, and options from command line.
         Note: Sets SNES defaults from `RatelSNESSetDefaults()`.

  @param[in]      ratel  Ratel context
  @param[in,out]  ts     TS object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetup(Ratel ratel, TS ts) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Setup");

  // Set DM & default TS options
  if (!ratel->dm_hierarchy) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Must set up Ratel DM with RatelDMCreate before calling RatelTSSetup");
  PetscCall(TSSetDM(ts, ratel->dm_hierarchy[0]));
  PetscCall(RatelTSSetDefaults(ratel, ts));

  // Set additional command line options
  PetscCall(RatelTSSetFromOptions(ratel, ts));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Setup Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup default TS options.
         Note: Sets SNES defaults from `RatelSNESSetDefaults()`.

  @param[in]      ratel  Ratel context
  @param[in,out]  ts     TS object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetDefaults(Ratel ratel, TS ts) {
  SNES snes;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Set Defaults");

  // TS defaults
  PetscCall(TSSetTimeStep(ts, 1.0));
  if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
    PetscCall(TSSetType(ts, TSALPHA2));
  }

  // SNES defaults
  PetscCall(TSGetSNES(ts, &snes));
  PetscCall(RatelSNESSetDefaults(ratel, snes));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Set Defaults Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Set additional Ratel specific TS options.

         `-ts_monitor_strain_energy` sets TSMonitor function for strain energy
         `-ts_monitor_surface_force viewer:filename.extension` sets TSMonitor function for surface forces
         `-ts_monitor_diagnostic_quantities viewer:filename.extension` sets TSMonitor function for diagnostic quantities
         `-ts_monitor_checkpoint viewer:filename.extension` sets TSMonitor function for solution checkpoint binaries

  @param[in]      ratel  Ratel context
  @param[in,out]  ts     TS object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetFromOptions(Ratel ratel, TS ts) {
  PetscInt  max_strings           = 1;
  PetscBool monitor_strain_energy = PETSC_FALSE, monitor_surface_forces = PETSC_FALSE, monitor_diagnostic_quantities = PETSC_FALSE,
            monitor_checkpoint = PETSC_FALSE;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Set From Options");

  PetscOptionsBegin(ratel->comm, NULL, "Ratel TS options", NULL);
  PetscCall(PetscOptionsViewer("-ts_monitor_strain_energy", "Set TSMonitor for strain energy", NULL, &ratel->monitor_viewer_strain_energy, NULL,
                               &monitor_strain_energy));
  PetscCall(PetscOptionsViewer("-ts_monitor_surface_force", "Set TSMonitor for surface forces", NULL, &ratel->monitor_viewer_surface_forces, NULL,
                               &monitor_surface_forces));
  PetscCall(PetscOptionsViewer("-ts_monitor_diagnostic_quantities", "Set TSMonitor for diagnostic quantities", NULL,
                               &ratel->monitor_viewer_diagnostic_quantities, NULL, &monitor_diagnostic_quantities));
  PetscCall(PetscOptionsStringArray("-ts_monitor_checkpoint", "Set TSMonitor for checkpoints", NULL, &ratel->monitor_checkpoint_file_name,
                                    &max_strings, &monitor_checkpoint));
  PetscOptionsEnd();

  // Check for supported viewer for strain energy
  if (monitor_strain_energy) {
    PetscViewerType viewer_type;

    PetscCall(PetscViewerGetType(ratel->monitor_viewer_strain_energy, &viewer_type));
    if (strcmp(viewer_type, PETSCVIEWERASCII)) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Only ASCII viewer supported for -ts_monitor_strain_energy");
      // LCOV_EXCL_STOP
    }
  }

  // Check for supported viewer for surface forces
  if (monitor_surface_forces) {
    PetscViewerType viewer_type;
    const char     *file_name;

    PetscCall(PetscViewerGetType(ratel->monitor_viewer_surface_forces, &viewer_type));
    if (strcmp(viewer_type, PETSCVIEWERASCII)) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Only ASCII viewer supported for ts_monitor_surface_force");
      // LCOV_EXCL_STOP
    }

    // Header row
    PetscCall(PetscViewerFileGetName(ratel->monitor_viewer_surface_forces, &file_name));
    if (strcmp(file_name, "stdout")) {
      PetscCall(PetscViewerASCIIPrintf(ratel->monitor_viewer_surface_forces,
                                       "time, face_id, centroid_x, centroid_y, centroid_z, force_x, force_y, force_z\n"));
    }
  }

  // Get filename information if using VTK/VTU viewer
  // TODO: If the PETSc VTK/VTU viewer supports time series in the future, this can be dropped
  if (monitor_diagnostic_quantities) {
    const char *file_name;

    PetscCall(PetscViewerFileGetName(ratel->monitor_viewer_diagnostic_quantities, &file_name));
    if (file_name) {
      char *file_extension;

      PetscCall(PetscStrrchr(file_name, '.', &file_extension));
      ratel->is_monitor_vtk = !strcmp(file_extension, "vtu") || !strcmp(file_extension, "vtk");
      if (ratel->is_monitor_vtk) {
        size_t file_name_len      = file_extension - file_name;
        size_t file_extension_len = 0;

        PetscCall(PetscStrlen(file_extension, &file_extension_len));
        file_extension_len += 1;
        PetscCall(PetscCalloc1(file_name_len + 1, &ratel->monitor_vtk_file_name));
        PetscCall(PetscSNPrintf(ratel->monitor_vtk_file_name, file_name_len, "%s", file_name));
        PetscCall(PetscCalloc1(file_extension_len + 1, &ratel->monitor_vtk_file_extension));
        PetscCall(PetscSNPrintf(ratel->monitor_vtk_file_extension, file_extension_len, "%s", file_extension));
      }
    }
  }

  // Get checkpoint frequency
  if (monitor_checkpoint) {
    ratel->monitor_checkpoint_interval = 1;
    PetscOptionsBegin(ratel->comm, NULL, "Ratel TS options", NULL);
    PetscCall(PetscOptionsInt("-ts_monitor_checkpoint_interval", "Set TSMonitor checkpoint interval", NULL, ratel->monitor_checkpoint_interval,
                              &ratel->monitor_checkpoint_interval, NULL));
    PetscOptionsEnd();
  }

  // Set monitors
  if (monitor_strain_energy || monitor_surface_forces || monitor_diagnostic_quantities || monitor_checkpoint)
    PetscCall(RatelTSMonitorSet(ratel, ts, RatelTSMonitor));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Set From Options Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup default SNES options, the DM, and options from command line.

  @param[in]      ratel  Ratel context
  @param[in,out]  snes   SNES object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetup(Ratel ratel, SNES snes) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Setup");

  // Set DM & default SNES options
  if (!ratel->dm_hierarchy) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Must set up Ratel DM with RatelDMCreate before calling RatelSNESSetup");
  PetscCall(SNESSetDM(snes, ratel->dm_hierarchy[0]));
  PetscCall(RatelSNESSetDefaults(ratel, snes));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Setup Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup default SNES options.
         Note: Sets default SNES line search to critical point method.

  @param[in]      ratel  Ratel context
  @param[in,out]  snes   SNES object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetDefaults(Ratel ratel, SNES snes) {
  SNESLineSearch line_search;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Set Defaults");

  // Default linesearch
  PetscCall(SNESGetLineSearch(snes, &line_search));
  PetscCall(SNESLineSearchSetType(line_search, SNESLINESEARCHCP));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Set Defaults Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid preconditioner from Ratel context

  @param[in]   ratel           Ratel context
  @param[in]   multigrid_type  Multigrid strategy
  @param[out]  ts              TS object with multigrid preconditioner

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, TS ts) {
  SNES snes;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Setup PCMG");

  // Setup on SNES
  PetscCall(TSGetSNES(ts, &snes));
  PetscCall(RatelSNESSetupPCMG(ratel, multigrid_type, snes));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Setup PCMG Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid preconditioner from Ratel context

  @param[in]   ratel           Ratel context
  @param[in]   multigrid_type  Multigrid strategy
  @param[out]  snes            SNES object with multigrid preconditioner

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, SNES snes) {
  KSP ksp;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Setup PCMG");

  // Setup KSP and PC
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(RatelKSPSetupPCMG(ratel, multigrid_type, ksp));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Setup PCMG Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup libCEED operators for multigrid prolongation, restriction, and coarse
           grid Jacobian evaluation

  @param[in]      ratel        Ratel context
  @param[in]      M_loc        PETSc Vec holding multiplicity data
  @param[in]      m_loc        libCEED vector for multiplicity data
  @param[in]      level        Multigrid level to set up
  @param[in,out]  op_jacobian  Composite CeedOperator for Jacobian
  @param[in,out]  op_prolong   Composite CeedOperator for prolongation
  @param[in,out]  op_restrict  Composite CeedOperator for restriction

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelSetupMultigridLevel(Ratel ratel, Vec M_loc, CeedVector m_loc, PetscInt level, CeedOperator op_jacobian,
                                               CeedOperator op_prolong, CeedOperator op_restrict) {
  PetscScalar *m;
  PetscMemType m_mem_type;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Setup Multigrid Level");
  RatelDebug(ratel, "---- Level: %" PetscInt_FMT, level);

  // Multiplicity
  PetscCall(VecGetArrayAndMemType(M_loc, &m, &m_mem_type));
  RatelCeedCall(ratel, CeedVectorSetArray(m_loc, RatelMemTypeP2C(m_mem_type), CEED_USE_POINTER, (CeedScalar *)m));
  RatelCeedCall(ratel, CeedCompositeOperatorGetMultiplicity(ratel->ctx_jacobian[level + 1]->op, ratel->num_jacobian_multiplicity_skip_indices,
                                                            ratel->jacobian_multiplicity_skip_indices, ratel->ctx_jacobian[level + 1]->y_loc));
  RatelCeedCall(ratel, CeedVectorPointwiseMult(m_loc, m_loc, ratel->ctx_jacobian[level + 1]->y_loc));

  // Setup level operators
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupMultigridLevel(ratel->materials[i], level, m_loc, ratel->ctx_jacobian[level + 1]->op, op_jacobian, op_prolong,
                                               op_restrict));
  }

  // Restore PETSc vector
  RatelCeedCall(ratel, CeedVectorTakeArray(m_loc, RatelMemTypeP2C(m_mem_type), (CeedScalar **)&m));
  PetscCall(VecRestoreArrayAndMemType(M_loc, &m));

  // Debugging output
  if (ratel->is_debug) {
    // LCOV_EXCL_START
    RatelCeedCall(ratel, CeedOperatorView(op_jacobian, stdout));
    RatelCeedCall(ratel, CeedOperatorView(op_prolong, stdout));
    RatelCeedCall(ratel, CeedOperatorView(op_restrict, stdout));
    // LCOV_EXCL_STOP
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Setup Multigrid Level Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid preconditioner from Ratel context.
           Note: Sets default KSP to Conjugate Gradient for multigrid with multiple levels
                 or no multigrid and "preconditioner only" with AMG for multigrid with one level.

  @param[in]   ratel           Ratel context
  @param[in]   multigrid_type  Multigrid strategy
  @param[out]  ksp             KSP object with multigrid preconditioner

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelKSPSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, KSP ksp) {
  PetscLogStage stage_pmg_setup;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel pMG Setup");
  PetscCall(PetscLogStageGetId("Ratel pMG Setup", &stage_pmg_setup));
  if (stage_pmg_setup == -1) PetscCall(PetscLogStageRegister("Ratel pMG Setup", &stage_pmg_setup));
  PetscCall(PetscLogStagePush(stage_pmg_setup));

  // Setup level degrees
  if (ratel->is_cl_multigrid_type) ratel->multigrid_type = ratel->cl_multigrid_type;
  else ratel->multigrid_type = multigrid_type;

  // Determine number of levels
  switch (ratel->multigrid_type) {
    case RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC:
      ratel->num_multigrid_levels = ceil(log(ratel->multigrid_fine_orders[0]) / log(2)) - floor(log(ratel->multigrid_coarse_orders[0]) / log(2)) + 1;
      for (PetscInt i = 1; i < ratel->num_active_fields; i++) {
        ratel->num_multigrid_levels = RatelIntMax(ratel->num_multigrid_levels, ceil(log(ratel->multigrid_fine_orders[i]) / log(2)) -
                                                                                   floor(log(ratel->multigrid_coarse_orders[i]) / log(2)) + 1);
      }
      break;
    case RATEL_MULTIGRID_P_COARSENING_UNIFORM:
      ratel->num_multigrid_levels = ratel->multigrid_fine_orders[0] - ratel->multigrid_coarse_orders[0] + 1;
      for (PetscInt i = 1; i < ratel->num_active_fields; i++) {
        ratel->num_multigrid_levels =
            RatelIntMax(ratel->num_multigrid_levels, ratel->multigrid_fine_orders[i] - ratel->multigrid_coarse_orders[i] + 1);
      }
      break;
    case RATEL_MULTIGRID_P_COARSENING_USER:
      // Nothing to do
      break;
    case RATEL_MULTIGRID_AMG_ONLY:
    case RATEL_MULTIGRID_NONE:
      ratel->num_multigrid_levels = 1;
      break;
  }
  CeedInt fine_level = ratel->num_multigrid_levels - 1;

  // Note: currently don't support p-multigrid with multiple active fields
  if (ratel->num_multigrid_levels > 1 && ratel->num_active_fields > 1) {
    // LCOV_EXCL_START
    SETERRQ(ratel->comm, PETSC_ERR_SUP, "P-multigrid with multiple active fields not currently supported");
    // LCOV_EXCL_STOP
  }

  // Note: currently don't support assembly multiple active fields
  if (ratel->multigrid_type == RATEL_MULTIGRID_AMG_ONLY && ratel->num_active_fields > 1) {
    // LCOV_EXCL_START
    SETERRQ(ratel->comm, PETSC_ERR_SUP, "libCEED operator assembly with multiple active fields not currently supported");
    // LCOV_EXCL_STOP
  }

  // Populate array of degrees for each level for multigrid
  switch (ratel->multigrid_type) {
    case RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC:
      for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
        ratel->multigrid_level_orders[i][0]          = ratel->multigrid_coarse_orders[i];
        ratel->multigrid_level_orders[i][fine_level] = ratel->multigrid_fine_orders[i];
        for (CeedInt j = fine_level - 1; j > 0; j--) {
          ratel->multigrid_level_orders[i][j] = RatelIntMax(ceil(ratel->multigrid_level_orders[i][j + 1] / 2.0), ratel->multigrid_coarse_orders[i]);
        }
      }
      break;
    case RATEL_MULTIGRID_P_COARSENING_UNIFORM:
      for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
        ratel->multigrid_level_orders[i][fine_level] = ratel->multigrid_fine_orders[i];
        for (CeedInt j = fine_level - 1; j >= 0; j--) {
          ratel->multigrid_level_orders[i][j] = RatelIntMax(ratel->multigrid_level_orders[i][j + 1] - 1, ratel->multigrid_coarse_orders[i]);
        }
      }
      break;
    case RATEL_MULTIGRID_P_COARSENING_USER:
      for (PetscInt i = 1; i < ratel->num_active_fields; i++) {
        if (ratel->multigrid_fine_orders[i] == ratel->multigrid_coarse_orders[i]) {
          // No coarsening provided, use fine order for all levels
          for (PetscInt j = 1; j < ratel->num_multigrid_levels; j++) ratel->multigrid_level_orders[i][j] = ratel->multigrid_coarse_orders[i];
        }
      }
      break;
    case RATEL_MULTIGRID_AMG_ONLY:
    case RATEL_MULTIGRID_NONE:
      for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
        ratel->multigrid_level_orders[i][0] = ratel->multigrid_fine_orders[i];
      }
      break;
  }

  // -- Realloc arrays
  RatelDebug(ratel, "---- Reallocating arrays");
  {
    // ---- DM
    DM dm = ratel->dm_hierarchy[0];
    PetscCall(PetscFree(ratel->dm_hierarchy));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->dm_hierarchy));
    ratel->dm_hierarchy[fine_level] = dm;
    // ---- FLOPs counting
    PetscLogDouble flops_jacobian   = ratel->flops_jacobian[0];
    PetscCall(PetscFree(ratel->flops_jacobian));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->flops_jacobian));
    ratel->flops_jacobian[fine_level] = flops_jacobian;
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->event_prolong));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->flops_prolong));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->event_restrict));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->flops_restrict));
    // ---- Operator contexts
    RatelOperatorApplyContext ctx_jacobian = ratel->ctx_jacobian[0];
    PetscCall(PetscFree(ratel->ctx_jacobian));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->ctx_jacobian));
    ratel->ctx_jacobian[fine_level] = ctx_jacobian;
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->ctx_prolong_restrict));
    // ---- MatShells
    Mat mat_jacobian = ratel->mat_jacobian[0];
    PetscCall(PetscFree(ratel->mat_jacobian));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->mat_jacobian));
    ratel->mat_jacobian[fine_level] = mat_jacobian;
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->mat_prolong_restrict));
  }

  // Setup DMs for multigrid levels
  switch (ratel->model_type) {
    case RATEL_MODEL_FEM: {
      RatelDebug(ratel, "---- Mesh: FEM");
      VecType         vec_type;
      PetscInt        num_active_fields;
      const PetscInt *active_field_sizes;
      PetscInt        field_orders[RATEL_MAX_FIELDS];

      PetscCall(DMGetVecType(ratel->dm_orig, &vec_type));
      PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
      for (CeedInt i = 0; i < ratel->num_multigrid_levels - 1; i++) {
        for (PetscInt j = 0; j < ratel->num_active_fields; j++) field_orders[j] = ratel->multigrid_level_orders[j][i];
        PetscCall(DMClone(ratel->dm_orig, &ratel->dm_hierarchy[i]));
        PetscCall(RatelDMSetupByOrder_FEM(ratel, true, field_orders, RATEL_DECIDE, RATEL_DECIDE, num_active_fields, active_field_sizes,
                                          ratel->dm_hierarchy[i]));
        PetscCall(DMSetVecType(ratel->dm_hierarchy[i], vec_type));
      }
      break;
    }
  }

  // libCEED operators for each level
  RatelDebug(ratel, "---- Setup libCEED operators");
  for (PetscInt i = ratel->num_multigrid_levels - 2; i >= 0; i--) {
    RatelDebug(ratel, "------ Multigrid level: %" CeedInt_FMT, i);
    PetscInt     X_loc_size;
    Vec          X_loc;
    CeedVector   x_loc, y_loc;
    CeedOperator op_jacobian, op_prolong, op_restrict;

    // -- Vectors
    PetscCall(DMCreateLocalVector(ratel->dm_hierarchy[i], &X_loc));
    PetscCall(VecGetSize(X_loc, &X_loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &x_loc));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &y_loc));
    // -- Composite operators
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_jacobian));
    RatelCeedCall(ratel, CeedOperatorSetName(op_jacobian, "Jacobian"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_prolong));
    {
      char op_name_prolong[25];

      PetscCall(PetscSNPrintf(op_name_prolong, sizeof(op_name_prolong), "prolongation %" PetscInt_FMT, i + 1));
      RatelCeedCall(ratel, CeedOperatorSetName(op_prolong, op_name_prolong));
    }
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_restrict));
    {
      char op_name_restrict[25];

      PetscCall(PetscSNPrintf(op_name_restrict, sizeof(op_name_restrict), "restriction %" PetscInt_FMT, i + 1));
      RatelCeedCall(ratel, CeedOperatorSetName(op_restrict, op_name_restrict));
    }
    // -- Compute Multiplicity
    {
      RatelDebug(ratel, "-------- Compute multiplicity");
      Vec X;

      PetscCall(DMGetGlobalVector(ratel->dm_hierarchy[i + 1], &X));
      PetscCall(VecZeroEntries(X));
      PetscCall(VecSet(ratel->ctx_jacobian[i + 1]->X_loc, 1.0));
      PetscCall(DMLocalToGlobal(ratel->dm_hierarchy[i + 1], ratel->ctx_jacobian[i + 1]->X_loc, ADD_VALUES, X));
      PetscCall(DMGlobalToLocal(ratel->dm_hierarchy[i + 1], X, INSERT_VALUES, ratel->ctx_jacobian[i + 1]->X_loc));
      PetscCall(DMRestoreGlobalVector(ratel->dm_hierarchy[i + 1], &X));
    }
    // -- Sub-operators for volumetric terms
    PetscCall(RatelSetupMultigridLevel(ratel, ratel->ctx_jacobian[i + 1]->X_loc, ratel->ctx_jacobian[i + 1]->x_loc, i, op_jacobian, op_prolong,
                                       op_restrict));
    // -- Clean up multiplicity values in local vectors
    PetscCall(VecZeroEntries(ratel->ctx_jacobian[i + 1]->X_loc));
    // -- FLOPs counting
    {
      CeedSize ceed_flops_estimate = 0;

      RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(op_jacobian, &ceed_flops_estimate));
      ratel->flops_jacobian[i] = ceed_flops_estimate;
      RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(op_prolong, &ceed_flops_estimate));
      ratel->flops_prolong[i + 1] = ceed_flops_estimate;
      RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(op_restrict, &ceed_flops_estimate));
      ratel->flops_restrict[i + 1] = ceed_flops_estimate;

      char event_name_prolong[25];
      PetscCall(PetscSNPrintf(event_name_prolong, sizeof(event_name_prolong), "RatelProlong %" PetscInt_FMT, i + 1));
      PetscCall(PetscLogEventRegister(event_name_prolong, ratel->libceed_class_id, &ratel->event_prolong[i + 1]));

      char event_name_restrict[26];
      PetscCall(PetscSNPrintf(event_name_restrict, sizeof(event_name_restrict), "RatelRestrict %" PetscInt_FMT, i + 1));
      PetscCall(PetscLogEventRegister(event_name_restrict, ratel->libceed_class_id, &ratel->event_restrict[i + 1]));
    }

    // -- Contexts
    {
      char name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(name, PETSC_MAX_PATH_LEN, "Jacobian, level %" PetscInt_FMT, i));
      PetscCall(RatelOperatorApplyContextCreate(ratel, name, ratel->dm_hierarchy[i], ratel->dm_hierarchy[i], X_loc, NULL, x_loc, y_loc, op_jacobian,
                                                &ratel->ctx_jacobian[i]));
      PetscCall(PetscSNPrintf(name, PETSC_MAX_PATH_LEN, "Prolongation and restriction, level %" PetscInt_FMT, i));
      PetscCall(RatelProlongRestrictContextCreate(ratel, name, ratel->dm_hierarchy[i], ratel->dm_hierarchy[i + 1], X_loc,
                                                  ratel->ctx_jacobian[i + 1]->X_loc, x_loc, ratel->ctx_jacobian[i + 1]->x_loc, y_loc,
                                                  ratel->ctx_jacobian[i + 1]->y_loc, op_prolong, op_restrict, &ratel->ctx_prolong_restrict[i + 1]));
    }

    // -- Cleanup
    PetscCall(VecDestroy(&X_loc));
    RatelCeedCall(ratel, CeedVectorDestroy(&x_loc));
    RatelCeedCall(ratel, CeedVectorDestroy(&y_loc));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_jacobian));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_prolong));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_restrict));
  }

  // MatShells
  RatelDebug(ratel, "---- Setup MatShells");
  for (CeedInt i = 0; i < ratel->num_multigrid_levels; i++) {
    RatelDebug(ratel, "------ Multigrid level: %" CeedInt_FMT, i);
    PetscInt X_l_size, X_g_size;

    // -- Get sizes
    {
      Vec X;

      PetscCall(DMGetGlobalVector(ratel->dm_hierarchy[i], &X));
      PetscCall(VecGetSize(X, &X_g_size));
      PetscCall(VecGetLocalSize(X, &X_l_size));
      PetscCall(DMRestoreGlobalVector(ratel->dm_hierarchy[i], &X));
    }

    // -- Jacobian
    if (i < ratel->num_multigrid_levels - 1) {
      RatelDebug(ratel, "------ Jacobian MatShell");
      PetscCall(MatCreateShell(ratel->comm, X_l_size, X_l_size, X_g_size, X_g_size, ratel->ctx_jacobian[i], &ratel->mat_jacobian[i]));
      PetscCall(MatShellSetContextDestroy(ratel->mat_jacobian[i], (PetscErrorCode(*)(void *))RatelOperatorApplyContextDestroy));
      PetscCall(MatSetOption(ratel->mat_jacobian[i], MAT_SPD, PETSC_TRUE));
      PetscCall(MatShellSetOperation(ratel->mat_jacobian[i], MATOP_MULT, (void (*)(void))RatelApplyJacobian));
      PetscCall(MatShellSetOperation(ratel->mat_jacobian[i], MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
    }

    // -- Prolongation/Restriction
    if (i > 0) {
      RatelDebug(ratel, "------ Prolongation/Restriction MatShell");
      PetscInt X_l_size_coarse, X_g_size_coarse;

      // ---- Get sizes
      {
        Vec X_coarse;

        PetscCall(DMGetGlobalVector(ratel->dm_hierarchy[i - 1], &X_coarse));
        PetscCall(VecGetSize(X_coarse, &X_g_size_coarse));
        PetscCall(VecGetLocalSize(X_coarse, &X_l_size_coarse));
        PetscCall(DMRestoreGlobalVector(ratel->dm_hierarchy[i - 1], &X_coarse));
      }

      // ---- Matshell
      PetscCall(MatCreateShell(ratel->comm, X_l_size, X_l_size_coarse, X_g_size, X_g_size_coarse, ratel->ctx_prolong_restrict[i],
                               &ratel->mat_prolong_restrict[i]));
      PetscCall(MatShellSetContextDestroy(ratel->mat_prolong_restrict[i], (PetscErrorCode(*)(void *))RatelProlongRestrictContextDestroy));
      PetscCall(MatShellSetOperation(ratel->mat_prolong_restrict[i], MATOP_MULT, (void (*)(void))RatelApplyProlongation));
      PetscCall(MatShellSetOperation(ratel->mat_prolong_restrict[i], MATOP_MULT_TRANSPOSE, (void (*)(void))RatelApplyRestriction));
    }
  }

  // Flag number of multigrid levels setup
  ratel->num_multigrid_levels_setup = ratel->num_multigrid_levels;

  // Coarse Jacobian
  if (ratel->multigrid_type != RATEL_MULTIGRID_NONE) {
    // -- Coarse Mat
    {
      VecType   vec_type;
      MatType   mat_type = MATAIJ;
      char      mat_type_cl[25];
      PetscBool is_mat_type_cl = PETSC_FALSE;

      PetscCall(DMGetVecType(ratel->dm_orig, &vec_type));
      if (strstr(vec_type, VECCUDA)) mat_type = MATAIJCUSPARSE;
      else if (strstr(vec_type, VECKOKKOS)) mat_type = MATAIJKOKKOS;
      else mat_type = MATAIJ;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(
          PetscOptionsString("-coarse_dm_mat_type", "coarse grid DM MatType", NULL, mat_type_cl, mat_type_cl, sizeof(mat_type_cl), &is_mat_type_cl));
      PetscOptionsEnd();

      PetscCall(DMSetMatType(ratel->dm_hierarchy[0], is_mat_type_cl ? mat_type_cl : mat_type));
      PetscCall(DMSetMatrixPreallocateSkip(ratel->dm_hierarchy[0], PETSC_TRUE));
      PetscCall(DMSetOptionsPrefix(ratel->dm_hierarchy[0], "coarse_"));

      PetscCall(DMGetMatType(ratel->dm_hierarchy[0], &mat_type));
      RatelDebug(ratel, "------ Coarse DM MatType: %s", mat_type);
      PetscCall(DMViewFromOptions(ratel->dm_hierarchy[0], NULL, "-dm_view"));
    }

    // -- Assemble sparsity pattern
    if (ratel->num_multigrid_levels > 1) {
      PetscInt        num_active_fields;
      const PetscInt *active_field_sizes;

      PetscCall(DMCreateMatrix(ratel->dm_hierarchy[0], &ratel->mat_jacobian_coarse));
      PetscCall(MatSetOption(ratel->mat_jacobian_coarse, MAT_SPD, PETSC_TRUE));
      PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
      if (active_field_sizes[0] >= 3) {
        MatNullSpace near_null;

        PetscCall(DMPlexCreateRigidBody(ratel->dm_hierarchy[0], 0, &near_null));
        PetscCall(MatSetNearNullSpace(ratel->mat_jacobian_coarse, near_null));
        PetscCall(MatNullSpaceDestroy(&near_null));
      }
      PetscCall(RatelMatSetPreallocationCOO(ratel, ratel->ctx_jacobian[0]->op, &ratel->coo_values_ceed, ratel->mat_jacobian_coarse));
    }

    // -- Update Jacobian context
    PetscCall(RatelSetupFormJacobianCtx(ratel, ratel->ctx_form_jacobian));
  }

  // Default KSP type
  PetscCall(KSPSetType(ksp, KSPCG));
  PetscCall(KSPSetNormType(ksp, KSP_NORM_NATURAL));

  // PC setup
  RatelDebug(ratel, "---- Setup PCMG");
  PC pc;
  PetscCall(KSPGetPC(ksp, &pc));

  if (ratel->multigrid_type == RATEL_MULTIGRID_NONE) {
    // ---- No multigrid
    RatelDebug(ratel, "------ No multigrid");
    PetscCall(PCJacobiSetType(pc, PC_JACOBI_DIAGONAL));
  } else if (ratel->multigrid_type == RATEL_MULTIGRID_AMG_ONLY) {
    // ---- AMG only
    RatelDebug(ratel, "------ AMG only");
    PetscCall(PCSetType(pc, PCGAMG));
  } else {
    // ---- P-multigrid with PCMG
    RatelDebug(ratel, "------ Full p-multigrid");
    PetscCall(PCSetType(pc, PCMG));
    PetscCall(PCMGSetLevels(pc, ratel->num_multigrid_levels, NULL));
    // ------ Loop over levels
    for (PetscInt i = 0; i < ratel->num_multigrid_levels; i++) {
      RatelDebug(ratel, "-------- Setup PCMG level: %" PetscInt_FMT, i);
      // -------- Smoother
      KSP ksp_smoother;
      PC  pc_smoother;
      // ---------- Smoother KSP
      PetscCall(PCMGGetSmoother(pc, i, &ksp_smoother));
      PetscCall(KSPSetDM(ksp_smoother, ratel->dm_hierarchy[i]));
      PetscCall(KSPSetDMActive(ksp_smoother, PETSC_FALSE));
      // ---------- Chebyshev options
      PetscCall(KSPSetType(ksp_smoother, KSPCHEBYSHEV));
      PetscCall(KSPChebyshevEstEigSet(ksp_smoother, 0, 0.1, 0, 1.1));
      PetscCall(KSPChebyshevEstEigSetUseNoisy(ksp_smoother, PETSC_TRUE));
      PetscCall(KSPSetOperators(ksp_smoother, ratel->mat_jacobian[i], ratel->mat_jacobian[i]));
      // ---------- Smoother preconditioner
      PetscCall(KSPGetPC(ksp_smoother, &pc_smoother));
      PetscCall(PCSetType(pc_smoother, PCJACOBI));
      PetscCall(PCJacobiSetType(pc_smoother, PC_JACOBI_DIAGONAL));
      // -------- Work vector
      // if (i != fine_level) PetscCall(PCMGSetX(pc, i, ratel->X[i]));
      // -------- Level prolongation/restriction operator
      if (i > 0) {
        PetscCall(PCMGSetInterpolation(pc, i, ratel->mat_prolong_restrict[i]));
        PetscCall(PCMGSetRestriction(pc, i, ratel->mat_prolong_restrict[i]));
      }
    }
    // ------ PCMG coarse solve
    RatelDebug(ratel, "-------- Setup PCMG coarse solve");
    KSP ksp_coarse;
    PC  pc_coarse;
    // -------- Coarse KSP
    PetscCall(PCMGGetCoarseSolve(pc, &ksp_coarse));
    PetscCall(KSPSetType(ksp_coarse, KSPPREONLY));
    PetscCall(KSPSetOperators(ksp_coarse, ratel->mat_jacobian_coarse, ratel->mat_jacobian_coarse));
    // -------- Coarse preconditioner
    PetscCall(KSPGetPC(ksp_coarse, &pc_coarse));
    PetscCall(PCSetType(pc_coarse, PCGAMG));
    // ------ PCMG options
    PetscCall(PCMGSetType(pc, PC_MG_MULTIPLICATIVE));
    PetscCall(PCMGSetNumberSmooth(pc, 3));
    PetscCall(PCMGSetCycleType(pc, PC_MG_CYCLE_V));
  }

  PetscLogStagePop();
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel KSP Setup PCMG Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup inital conditions for TS based upon command line options

  @param[in]      ratel  Ratel context
  @param[in,out]  ts     TS to setup
  @param[out]     U      Global vector to set with initial conditions

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetupInitialCondition(Ratel ratel, TS ts, Vec U) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Setup Initial Condition");

  switch (ratel->initial_condition_type) {
    case RATEL_INITIAL_CONDITION_ZERO:
      PetscCall(VecSet(U, 0.0));
      PetscCall(TSSetTime(ts, 0.0));
      PetscCall(TSSetStepNumber(ts, 0));
      break;
    case RATEL_INITIAL_CONDITION_CONTINUE: {
      PetscViewer viewer;
      PetscInt    step_number, checkpoint_version;
      PetscReal   time;

      // -- Open file
      PetscCall(PetscViewerBinaryOpen(ratel->comm, ratel->continue_file_name, FILE_MODE_READ, &viewer));

      // -- Read restart data
      PetscCall(PetscViewerBinaryRead(viewer, &checkpoint_version, 1, NULL, PETSC_INT));
      PetscCall(PetscViewerBinaryRead(viewer, &step_number, 1, NULL, PETSC_INT));
      PetscCall(TSSetStepNumber(ts, step_number));
      PetscCall(PetscViewerBinaryRead(viewer, &time, 1, NULL, PETSC_REAL));
      PetscCall(TSSetTime(ts, time));
      PetscCall(VecLoad(U, viewer));

      // -- Cleanup
      PetscCall(PetscViewerDestroy(&viewer));
      break;
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Setup Initial Condition Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup inital conditions for SNES based upon command line options

  @param[in]      ratel  Ratel context
  @param[in,out]  snes   SNES to setup
  @param[out]     U      Global vector to set with initial conditions

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetupInitialCondition(Ratel ratel, SNES snes, Vec U) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Setup Initial Condition");

  switch (ratel->initial_condition_type) {
    case RATEL_INITIAL_CONDITION_ZERO:
      PetscCall(VecSet(U, 0.0));
      break;
    case RATEL_INITIAL_CONDITION_CONTINUE: {
      PetscViewer viewer;
      PetscInt    step_number, checkpoint_version;
      PetscReal   time;

      // -- Open file
      PetscCall(PetscViewerBinaryOpen(ratel->comm, ratel->continue_file_name, FILE_MODE_READ, &viewer));

      // -- Read restart data, discard time and step number
      PetscCall(PetscViewerBinaryRead(viewer, &checkpoint_version, 1, NULL, PETSC_INT));
      PetscCall(PetscViewerBinaryRead(viewer, &step_number, 1, NULL, PETSC_INT));
      PetscCall(PetscViewerBinaryRead(viewer, &time, 1, NULL, PETSC_REAL));
      PetscCall(VecLoad(U, viewer));

      // -- Cleanup
      PetscCall(PetscViewerDestroy(&viewer));
      break;
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Setup Initial Condition Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Checkpoint final solution for TS

  @param[in]   ratel  Ratel context
  @param[in]   ts     TS to checkpoint
  @param[out]  U      Global vector to checkpoint

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSCheckpointFinalSolutionFromOptions(Ratel ratel, TS ts, Vec U) {
  PetscReal time;
  PetscInt  num_steps, monitor_checkpoint_interval;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Checkpoint Final Solution");

  // Get filename if not set
  if (!ratel->monitor_checkpoint_file_name) {
    PetscInt max_strings = 1;

    PetscOptionsBegin(ratel->comm, NULL, "Ratel options", NULL);
    PetscCall(PetscOptionsStringArray("-checkpoint_final_solution", "Checkpoint final solution for continuation", NULL,
                                      &ratel->monitor_checkpoint_file_name, &max_strings, NULL));
    PetscOptionsEnd();

    if (ratel->monitor_checkpoint_file_name) ratel->monitor_checkpoint_interval = -1;
  }

  // Checkpoint
  if (ratel->monitor_checkpoint_file_name) {
    monitor_checkpoint_interval        = ratel->monitor_checkpoint_interval;
    ratel->monitor_checkpoint_interval = 1;

    PetscCall(TSGetSolveTime(ts, &time));
    PetscCall(TSGetStepNumber(ts, &num_steps));
    PetscCall(RatelTSMonitorCheckpoint(ts, num_steps, time, U, ratel));

    ratel->monitor_checkpoint_interval = monitor_checkpoint_interval;
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Checkpoint Final Solution Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Checkpoint final solution for SNES

  @param[in]   ratel  Ratel context
  @param[in]   snes   SNES to checkpoint
  @param[out]  U      Global vector to checkpoint

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESCheckpointFinalSolutionFromOptions(Ratel ratel, SNES snes, Vec U) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Checkpoint Final Solution");

  // Get filename if not set
  if (!ratel->monitor_checkpoint_file_name) {
    PetscInt max_strings = 1;

    PetscOptionsBegin(ratel->comm, NULL, "Ratel options", NULL);
    PetscCall(PetscOptionsStringArray("-checkpoint_final_solution", "Checkpoint final solution for continuation", NULL,
                                      &ratel->monitor_checkpoint_file_name, &max_strings, NULL));
    PetscOptionsEnd();

    if (ratel->monitor_checkpoint_file_name) ratel->monitor_checkpoint_interval = -1;
  }

  // Checkpoint
  if (ratel->monitor_checkpoint_file_name) {
    PetscCall(RatelTSMonitorCheckpoint(NULL, 0, 0.0, U, ratel));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel SNES Checkpoint Final Solution Success!");
  PetscFunctionReturn(0);
}

/// @}
