/// @file
/// Implementation of Ratel diagnostic data interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-diagnostic.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-petsc-ops.h>
#include <ratel.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Internal code for computing diagnostic quantities

  @param[in]   ratel  Ratel context
  @param[in]   U      Computed solution vector
  @param[in]   time   Final time value, or 1.0 for SNES solution
  @param[out]  D      Computed diagonstic quantities vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeDiagnosticQuantities_Internal(Ratel ratel, Vec U, PetscScalar time, Vec D) {
  PetscFunctionBeginUser;

  // Compute quantities
  RatelDebug(ratel, "---- Computing diagnostic quantities");
  PetscCall(RatelUpdateTimeAndBoundaryValues(ratel, time));
  PetscCall(RatelApplyLocalCeedOp(U, D, ratel->ctx_diagnostic));

  // Project quantities
  PetscCall(KSPSolve(ratel->ksp_diagnostic_projection, D, D));

  PetscFunctionReturn(0);
}

/**
  @brief Internal routine for computing diagnostic quantities on mesh faces

  @param[in]   ratel           Ratel context
  @param[in]   U               Computed solution vector
  @param[in]   time            Current time value, or 1.0 for SNES solution
  @param[out]  surface_forces  Computed face forces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForces_Internal(Ratel ratel, Vec U, PetscScalar time, PetscScalar *surface_forces) {
  PetscInt dim = 3, num_comp_force = 6;

  PetscFunctionBeginUser;

  if (ratel->surface_force_face_count == 0) PetscFunctionReturn(0);

  // Get number of solution components
  {
    const PetscInt *field_sizes;

    PetscCall(DMGetDimension(ratel->ctx_surface_force->dm_y, &dim));
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_force = 2 * field_sizes[0];  // First field should be displacement
  }

  // Compute quantities
  RatelDebug(ratel, "---- Computing surface forces");
  PetscCall(RatelUpdateTimeAndBoundaryValues(ratel, time));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    RatelDebug(ratel, "------ Face %" PetscInt_FMT, ratel->surface_force_dm_faces[i]);
    PetscScalar local_buffer[num_comp_force];

    ratel->ctx_surface_force->op = ratel->ops_surface_force[i];
    PetscCall(RatelApplyLocalCeedOp(U, NULL, ratel->ctx_surface_force));
    PetscCall(VecStrideSumAll(ratel->ctx_surface_force->Y_loc, local_buffer));
    for (PetscInt j = 0; j < dim; j++) local_buffer[j] /= ratel->surface_force_face_area[i];
    PetscCall(MPIU_Allreduce(local_buffer, &surface_forces[num_comp_force * i], num_comp_force, MPIU_REAL, MPI_SUM, ratel->comm));
    for (PetscInt j = 0; j < dim; j++) surface_forces[num_comp_force * i + j] += ratel->surface_force_face_centroid[i][j];
    RatelDebug(ratel, "------   Centroid: [%f, %f, %f]", surface_forces[num_comp_force * i + 0], surface_forces[num_comp_force * i + 1],
               dim >= 3 ? surface_forces[num_comp_force * i + 2] : 0.0);
    RatelDebug(ratel, "------   Forces:   [%f, %f, %f]", surface_forces[num_comp_force * i + dim + 0], surface_forces[num_comp_force * i + dim + 1],
               dim >= 3 ? surface_forces[num_comp_force * i + dim + 2] : 0.0);
  }
  ratel->ctx_surface_force->op = ratel->ops_surface_force[0];

  PetscFunctionReturn(0);
}

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Determine if Ratel context has MMS solution

  @param[in]   ratel    Ratel context
  @param[out]  has_mms  Boolean flag if Ratel context has MMS solution

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelHasMMS(Ratel ratel, PetscBool *has_mms) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Checking MMS Status");

  *has_mms = ratel->has_mms;
  RatelDebug(ratel, "---- MMS status: %s", *has_mms ? "has mms" : "no mms");

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Checking MMS status success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute the L2 error from the manufactured solution

  Note: User is responsible for freeing array of L2 error values

  @param[in]   ratel       Ratel context
  @param[in]   U           Computed solution vector
  @param[out]  num_fields  Number of L2 error values
  @param[out]  l2_error    Computed L2 error for solution compared to MMS

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeMMSL2Error(Ratel ratel, Vec U, PetscInt *num_fields, PetscScalar **l2_error) {
  Vec E;
  IS *is_field;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute MMS L2 Error");

  if (!ratel->has_mms) SETERRQ(ratel->comm, PETSC_ERR_SUP, "No manufactured solution available");

  // Work vectors
  RatelDebug(ratel, "---- Creating work vectors");
  PetscCall(VecDuplicate(U, &E));

  // Field ISs
  PetscCall(DMCreateFieldDecomposition(ratel->dm_hierarchy[0], num_fields, NULL, &is_field, NULL));

  // Allocate results array
  PetscCall(PetscCalloc1(*num_fields, l2_error));

  // Integrate L2 error
  RatelDebug(ratel, "---- Computing MMS error");
  PetscCall(RatelApplyLocalCeedOp(U, E, ratel->ctx_mms_error));

  // Compute L2 error per field
  for (PetscInt i = 0; i < *num_fields; i++) {
    Vec         E_field;
    PetscScalar l2_error_field = 1.0;

    PetscCall(VecGetSubVector(E, is_field[i], &E_field));
    PetscCall(VecSum(E_field, &l2_error_field));
    PetscCall(VecDestroy(&E_field));
    (*l2_error)[i] = sqrt(l2_error_field);
    RatelDebug(ratel, "---- L2 Error, field %" PetscInt_FMT ": %f", i, (*l2_error)[i]);
  }

  // Cleanup
  RatelDebug(ratel, "---- Cleaning up");
  PetscCall(VecDestroy(&E));
  for (PetscInt i = 0; i < *num_fields; i++) PetscCall(ISDestroy(&is_field[i]));
  PetscFree(is_field);

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute MMS L2 Error Success!");

  PetscFunctionReturn(0);
}

/**
  @brief Retrieve expected strain energy, or `NULL` if no strain energy set

  @param[in]   ratel                   Ratel context
  @param[out]  expected_strain_energy  Expected strain energy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetExpectedStrainEnergy(Ratel ratel, PetscScalar *expected_strain_energy) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Expected Strain Energy");

  *expected_strain_energy = ratel->expected_strain_energy;
  RatelDebug(ratel, "---- expected strain energy: %f", *expected_strain_energy);

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Expected Strain Energy Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute the final strain energy in the computed solution

  @param[in]   ratel          Ratel context
  @param[in]   U              Computed solution vector
  @param[in]   time           Final time value, or 1.0 for SNES solution
  @param[out]  strain_energy  Computed strain energy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeStrainEnergy(Ratel ratel, Vec U, PetscScalar time, PetscScalar *strain_energy) {
  PetscScalar local_strain_energy = 0;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute Strain Energy");

  // Compute strain energy
  RatelDebug(ratel, "---- Computing Strain Energy");
  PetscCall(RatelUpdateTimeAndBoundaryValues(ratel, time));
  PetscCall(RatelApplyLocalCeedOp(U, NULL, ratel->ctx_energy));

  // Reduce max error
  PetscCall(VecSum(ratel->ctx_energy->Y_loc, &local_strain_energy));
  PetscCall(MPIU_Allreduce(&local_strain_energy, strain_energy, 1, MPIU_REAL, MPI_SUM, ratel->comm));
  RatelDebug(ratel, "---- strain energy: %f", *strain_energy);

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute Strain Energy Success!");
  PetscFunctionReturn(0);
}

/**
  @brief List mesh face numbers for diagnostic force computation

  @param[in]   ratel         Ratel context
  @param[out]  num_faces     Number of faces where forces are computed
  @param[out]  face_numbers  DMPlex mesh face numbers

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetSurfaceForceFaces(Ratel ratel, PetscInt *num_faces, const PetscInt **face_numbers) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Get Surface Force Faces");

  *num_faces    = ratel->surface_force_face_count;
  *face_numbers = (const PetscInt *)ratel->surface_force_dm_faces;

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Get Surface Force Faces Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute diagnostic quantities on mesh faces

  @param[in]   ratel           Ratel context
  @param[in]   U               Computed solution vector
  @param[in]   time            Final time value, or 1.0 for SNES solution
  @param[out]  surface_forces  Computed face forces - caller is responsible for freeing

  Note: The component `i` of the centroid for face `f` is at index `surface_forces[(2 * num_comp) * f + i]`.
        The component `i` of the force for face `f` is at index `surface_forces[(2 * num_comp + 1) * f + i]`.

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForces(Ratel ratel, Vec U, PetscScalar time, PetscScalar **surface_forces) {
  PetscInt num_comp_u = 3;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute Surface Forces");

  // Get number of solution components
  {
    const PetscInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  // Work array
  RatelDebug(ratel, "---- Creating surface force array");
  PetscCall(PetscCalloc1(ratel->surface_force_face_count * 2 * num_comp_u, surface_forces));

  // Internal routine
  PetscCall(RatelComputeSurfaceForces_Internal(ratel, U, time, *surface_forces));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute Surface Forces Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute diagnostic quantities

  @param[in]   ratel  Ratel context
  @param[in]   U      Computed solution vector
  @param[in]   time   Final time value, or 1.0 for SNES solution
  @param[out]  D      Computed diagonstic quantities vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeDiagnosticQuantities(Ratel ratel, Vec U, PetscScalar time, Vec *D) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute Diagnostic Quantities");

  // Work vectors
  RatelDebug(ratel, "---- Creating diagnostic quantity vector");
  PetscCall(DMCreateGlobalVector(ratel->dm_diagnostic, D));
  PetscCall(PetscObjectSetName((PetscObject)*D, "diagnostic_quantities"));

  // Internal routine
  PetscCall(RatelComputeDiagnosticQuantities_Internal(ratel, U, time, *D));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Compute Diagnostic Quantities Success!");
  PetscFunctionReturn(0);
}

/// @}
