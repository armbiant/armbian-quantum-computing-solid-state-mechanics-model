/// @file
/// Display and command line option strings for Ratel enums

#include <ratel.h>

const char *const RatelSolverTypes[] = {
    [RATEL_SOLVER_STATIC]      = "static solver",
    [RATEL_SOLVER_QUASISTATIC] = "quasistatic solver",
    [RATEL_SOLVER_DYNAMIC]     = "dynamic solver",
};
const char *const RatelModelTypes[] = {
    [RATEL_MODEL_FEM] = "finite element model",
};

const char *const RatelForcingTypes[] = {
    [RATEL_FORCING_NONE]     = "none",
    [RATEL_FORCING_CONSTANT] = "constant",
    [RATEL_FORCING_MMS]      = "manufactured solution",
};

const char *const RatelForcingTypesCL[] = {
    "none", "constant", "mms", "RatelForcingType", "RATEL_FORCE_", 0,
};

const char *const RatelBoundaryTypes[] = {
    [RATEL_BOUNDARY_CLAMP]    = "Dirichlet clamp",
    [RATEL_BOUNDARY_MMS]      = "Dirichlet mms",
    [RATEL_BOUNDARY_TRACTION] = "Neumann traction",
    [RATEL_BOUNDARY_PLATEN]   = "Nitsche contact (platen)",
};

const char *const RatelInitialConditionTypes[] = {
    [RATEL_INITIAL_CONDITION_ZERO]     = "zero initial condition",
    [RATEL_INITIAL_CONDITION_CONTINUE] = "continue initial condition",
};

const char *const RatelMutigridTypes[] = {
    [RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC] = "p-multigrid, logarithmic coarsening",
    [RATEL_MULTIGRID_P_COARSENING_UNIFORM]     = "p-multigrid, uniform coarsening",
    [RATEL_MULTIGRID_P_COARSENING_USER]        = "p-multigrid, user defined coarsening",
    [RATEL_MULTIGRID_AMG_ONLY]                 = "AMG only",
    [RATEL_MULTIGRID_NONE]                     = "no multigrid",
};

const char *const RatelMultigridTypesCL[] = {
    "p_logarithmic", "p_uniform", "p_user", "amg_only", "none", "RatelMultigridTypes", "RATEL_MULTIGRID_", 0,
};
