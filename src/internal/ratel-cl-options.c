/// @file
/// Command line option processing for solid mechanics example using PETSc

#include <math.h>
#include <ratel-cl-options.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-types.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Process general command line options

  @param[in,out]  ratel  Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProcessCommandLineOptions(Ratel ratel) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Process Command Line Options");

  PetscOptionsBegin(ratel->comm, NULL, "Ratel library with PETSc and libCEED", NULL);

  // libCEED resource
  {
    PetscBool ceed_set    = PETSC_FALSE;
    PetscInt  max_strings = 1;

    PetscCall(PetscOptionsStringArray("-ceed", "CEED resource specifier", NULL, &ratel->ceed_resource, &max_strings, &ceed_set));
    RatelDebug(ratel, "---- Ceed resource requested by user: %s", ceed_set ? ratel->ceed_resource : "none");

    // -- Provide default libCEED resource if not specified
    if (!ceed_set) {
      const char *default_ceed_resource     = "/cpu/self";
      size_t      default_ceed_resource_len = strlen(default_ceed_resource) + 1;

      PetscCall(PetscCalloc1(default_ceed_resource_len, &ratel->ceed_resource));
      PetscCall(PetscStrncpy(ratel->ceed_resource, default_ceed_resource, default_ceed_resource_len));
    }
    RatelDebug(ratel, "---- Ceed resource to initialize: %s", ratel->ceed_resource);

    // Initalize GPU
    // Note: We make sure PETSc has initialized its device (which is MPI-aware in case multiple devices are available) before CeedInit.
    //       This ensures that PETSc and libCEED agree about which device to use.
    if (strncmp(ratel->ceed_resource, "/gpu", 4) == 0) PetscCall(PetscDeviceInitialize(PETSC_DEVICE_DEFAULT()));

    // Initalize Ceed
    {
      PetscErrorCode ierr;
      Ceed           ceed;

      // -- Create Ceed
      ierr = CeedInit(ratel->ceed_resource, &ceed);
      if (ierr) SETERRQ(ratel->comm, PETSC_ERR_LIB, "libCEED resource initialization failed");
      ratel->ceed = ceed;

      // -- Set error handler
      ierr = CeedSetErrorHandler(ratel->ceed, CeedErrorStore);
      if (ierr) SETERRQ(ratel->comm, PETSC_ERR_LIB, "Setting libCEED error handler failed");

      // -- Set default JiT source root
      RatelDebug(ratel, "---- Adding Ratel JiT root: %s", RatelJitSourceRootDefault);
      RatelCeedCall(ratel, CeedAddJitSourceRoot(ratel->ceed, (char *)RatelJitSourceRootDefault));

      // -- View ceed
      if (ratel->is_debug) {
        // LCOV_EXCL_START
        const char *ceed_resource;
        RatelCeedCall(ratel, CeedGetResource(ratel->ceed, &ceed_resource));
        RatelDebug(ratel, "---- libCEED resource initalized: %s", ceed_resource);
        // LCOV_EXCL_STOP
      }
    }
  }

  // Multigrid options
  // -- Type of multigrid and coarsening
  ratel->multigrid_type    = RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC;
  ratel->cl_multigrid_type = RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC;
  PetscCall(PetscOptionsEnum("-multigrid", "Set multigrid type option", NULL, RatelMultigridTypesCL, (PetscEnum)ratel->cl_multigrid_type,
                             (PetscEnum *)&ratel->cl_multigrid_type, &ratel->is_cl_multigrid_type));

  // -- Multigrid level orders
  {
    PetscBool found_orders_option = PETSC_FALSE;

    ratel->multigrid_fine_orders[0] = 3;
    ratel->num_active_fields        = RATEL_MAX_FIELDS;
    PetscCall(PetscOptionsIntArray("-orders", "Polynomial orders of basis functions", NULL, ratel->multigrid_fine_orders, &ratel->num_active_fields,
                                   &found_orders_option));
    if (!found_orders_option) {
      ratel->num_active_fields = 1;
      PetscCall(PetscOptionsIntArray("-order", "Polynomial orders of basis functions", NULL, ratel->multigrid_fine_orders, &ratel->num_active_fields,
                                     &found_orders_option));
    }

    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      char     option_name[PETSC_MAX_PATH_LEN];
      PetscInt num_multigrid_levels = RATEL_MAX_MULTIGRID_LEVELS;

      if (ratel->multigrid_fine_orders[i] <= 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Polynomial order must be positive integer");

      PetscCall(PetscSNPrintf(option_name, PETSC_MAX_PATH_LEN, "-multigrid_orders_field_%" PetscInt_FMT, i));
      PetscCall(PetscOptionsIntArray(option_name, "Polynomial orders of basis functions for multigrid", NULL, ratel->multigrid_level_orders[i],
                                     &num_multigrid_levels, &found_orders_option));
      if (!found_orders_option && i == 0) {
        num_multigrid_levels = RATEL_MAX_MULTIGRID_LEVELS - 1;
        PetscCall(PetscOptionsIntArray("-multigrid_orders", "Polynomial orders of basis functions for multigrid", NULL,
                                       ratel->multigrid_level_orders[i], &num_multigrid_levels, &found_orders_option));
      }

      if (i == 0) ratel->num_multigrid_levels = found_orders_option ? num_multigrid_levels : 1;
      if (found_orders_option && ratel->num_multigrid_levels != num_multigrid_levels) {
        // LCOV_EXCL_START
        SETERRQ(ratel->comm, PETSC_ERR_SUP, "Consistent number of polynomial orders must be provided for each field");
        // LCOV_EXCL_STOP
      }
      if (found_orders_option) {
        for (PetscInt j = 1; j < num_multigrid_levels; j++) {
          if (ratel->multigrid_level_orders[i][j] <= 0) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Polynomial order must be positive integer");
          if (ratel->multigrid_level_orders[i][j] < ratel->multigrid_level_orders[i][j - 1]) {
            // LCOV_EXCL_START
            SETERRQ(ratel->comm, PETSC_ERR_SUP, "Polynomial orders for multigrid levels must be provided in ascending order");
            // LCOV_EXCL_STOP
          }
        }
      } else {
        for (PetscInt j = 1; j < num_multigrid_levels + 1; j++) ratel->multigrid_level_orders[i][j] = ratel->multigrid_level_orders[i][0];
      }
    }
  }
  ratel->num_multigrid_levels_setup = 0;
  ratel->highest_fine_order         = ratel->multigrid_fine_orders[0];
  RatelDebug(ratel, "---- Active fields found: %" PetscInt_FMT, ratel->num_active_fields);
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    ratel->highest_fine_order = RatelIntMax(ratel->highest_fine_order, ratel->multigrid_fine_orders[i]);
    RatelDebug(ratel, "---- Fine grid polynomial order field %" PetscInt_FMT ": %" PetscInt_FMT, i, ratel->multigrid_fine_orders[i]);
  }

  // -- Coarse grid order
  {
    PetscBool found_orders_option = PETSC_FALSE;
    PetscInt  num_coarse_orders   = RATEL_MAX_FIELDS;

    ratel->multigrid_coarse_orders[0] = 1;
    PetscCall(PetscOptionsIntArray("-coarse_orders", "Polynomial orders of coarse grid basis functions", NULL, ratel->multigrid_coarse_orders,
                                   &num_coarse_orders, &found_orders_option));
    if (found_orders_option) {
      if (num_coarse_orders > 1 && num_coarse_orders != ratel->num_active_fields) {
        // LCOV_EXCL_START
        SETERRQ(ratel->comm, PETSC_ERR_SUP, "Coarse grid polynomial orders should be supplied for the only the first field or all fields");
        // LCOV_EXCL_STOP
      }
      if (num_coarse_orders == 1) {
        for (PetscInt i = 1; i < ratel->num_active_fields; i++) ratel->multigrid_coarse_orders[i] = ratel->multigrid_fine_orders[i];
      }
    }
    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      // LCOV_EXCL_START
      RatelDebug(ratel, "---- Coarse grid polynomial order field %" PetscInt_FMT ": %" PetscInt_FMT, i, ratel->multigrid_coarse_orders[i]);
      // LCOV_EXCL_STOP
    }
  }

  // Additional quadrature points
  ratel->q_extra = 0;
  PetscCall(PetscOptionsInt("-q_extra", "Number of extra quadrature points", NULL, ratel->q_extra, &ratel->q_extra, NULL));
  RatelDebug(ratel, "---- Extra quadrature points: %" PetscInt_FMT, ratel->q_extra);

  // Additional quadrature points
  ratel->q_extra_surface_force = ratel->q_extra > 0 ? ratel->q_extra : 1;
  PetscCall(PetscOptionsInt("-q_extra_surface_force", "Number of extra quadrature points for surface force computation", NULL,
                            ratel->q_extra_surface_force, &ratel->q_extra_surface_force, NULL));
  RatelDebug(ratel, "---- Extra quadrature points for surface force computation: %" PetscInt_FMT, ratel->q_extra_surface_force);

  // Diagnostic quantities order
  ratel->diagnostic_order = ratel->highest_fine_order;
  PetscCall(
      PetscOptionsInt("-diagnostic_order", "Polynomial order of diagnostic output", NULL, ratel->diagnostic_order, &ratel->diagnostic_order, NULL));
  RatelDebug(ratel, "---- Diagnostic value order: %" PetscInt_FMT, ratel->diagnostic_order);

  // Geometry order for viewer
  ratel->diagnostic_geometry_order = RATEL_DECIDE;
  PetscCall(PetscOptionsInt("-diagnostic_geometry_order", "Geometry order for diagnostic values mesh", NULL, ratel->diagnostic_geometry_order,
                            &ratel->diagnostic_geometry_order, NULL));
  PetscBool is_diagnostic_geometry_ratel_decide = ratel->diagnostic_geometry_order == RATEL_DECIDE;
  if (!is_diagnostic_geometry_ratel_decide && ratel->diagnostic_geometry_order < 1) {
    // LCOV_EXCL_START
    SETERRQ(ratel->comm, PETSC_ERR_SUP, "Geometry order must be a positive value: %" PetscInt_FMT, ratel->diagnostic_geometry_order);
    // LCOV_EXCL_STOP
  }
  // LCOV_EXCL_START
  if (is_diagnostic_geometry_ratel_decide) {
    RatelDebug(ratel, "---- Geometry order for diagnostic values mesh: use solution mesh geometric order");
  } else {
    RatelDebug(ratel, "---- Geometry order for diagnostic values mesh: %" PetscInt_FMT, ratel->diagnostic_geometry_order);
  }
  // LCOV_EXCL_STOP

  // Physical scaling options
  {
    RatelUnits units;
    PetscCall(PetscCalloc1(1, &units));

    units->meter = 1.0;
    PetscCall(PetscOptionsScalar("-units_meter", "1 meter in scaled length units", NULL, units->meter, &units->meter, NULL));
    units->meter = fabs(units->meter);
    RatelDebug(ratel, "---- units meter: %f", units->meter);

    units->second = 1.0;
    PetscCall(PetscOptionsScalar("-units_second", "1 second in scaled time units", NULL, units->second, &units->second, NULL));
    units->second = fabs(units->second);
    RatelDebug(ratel, "---- units second: %f", units->second);

    units->kilogram = 1.0;
    PetscCall(PetscOptionsScalar("-units_kilogram", "1 kilogram in scaled mass units", NULL, units->kilogram, &units->kilogram, NULL));
    units->kilogram = fabs(units->kilogram);
    RatelDebug(ratel, "---- units kilogram: %f", units->kilogram);

    // Define derived units
    units->Pascal = units->kilogram / (units->meter * PetscSqr(units->second));

    ratel->material_units = units;
  }

  // Material regions
  {
    PetscBool found_material_option = PETSC_FALSE;
    char     *material_names[RATEL_MAX_MATERIALS];

    // -- Get number and names of materials
    ratel->num_materials = RATEL_MAX_MATERIALS;
    PetscCall(PetscOptionsStringArray("-material", "List of user-defined material names", __func__, material_names, &ratel->num_materials,
                                      &found_material_option));
    if (!found_material_option) {
      ratel->num_materials = 1;
      PetscCall(PetscCalloc1(1, &material_names[0]));
    }
    PetscCall(PetscCalloc1(ratel->num_materials, &ratel->materials));

    // -- Create materials
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialCreate(ratel, material_names[i], &ratel->materials[i]));
      PetscCall(PetscFree(material_names[i]));
    }
  }

  // Forcing function
  ratel->forcing_choice = RATEL_FORCING_NONE;  // Default - no forcing term
  PetscCall(PetscOptionsEnum("-forcing", "Set forcing function option", NULL, RatelForcingTypesCL, (PetscEnum)ratel->forcing_choice,
                             (PetscEnum *)&ratel->forcing_choice, NULL));
  RatelDebug(ratel, "---- Forcing function: %s", RatelForcingTypes[ratel->forcing_choice]);

  if (ratel->forcing_choice == RATEL_FORCING_CONSTANT) {
    for (CeedInt i = 0; i < ratel->num_materials; i++) {
      const char *model_name;

      PetscCall(RatelMaterialGetModelName(ratel->materials[i], &model_name));
      if (strcmp(model_name, "elasticity-linear")) {
        // LCOV_EXCL_START
        SETERRQ(ratel->comm, PETSC_ERR_SUP,
                "Cannot use constant forcing and finite strain formulation. Constant forcing in reference frame currently unavailable.");
        // LCOV_EXCL_STOP
      }
    }
  }

  // Dirichlet boundary conditions
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    char option_name[PETSC_MAX_PATH_LEN];

    ratel->bc_clamp_count[i] = RATEL_MAX_BOUNDARY_FACES;
    PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_field_%" PetscInt_FMT, i));
    PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply incremental Dirichlet BC", NULL, ratel->bc_clamp_faces[i],
                                   &ratel->bc_clamp_count[i], NULL));
    if (i == 0 && ratel->bc_clamp_count[i] == 0) {
      ratel->bc_clamp_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray("-bc_clamp", "Face IDs to apply incremental Dirichlet BC", NULL, ratel->bc_clamp_faces[i],
                                     &ratel->bc_clamp_count[i], NULL));
    }
  }

  // MMS boundary conditions
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    char option_name[PETSC_MAX_PATH_LEN];

    ratel->bc_mms_count[i] = RATEL_MAX_BOUNDARY_FACES;
    PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_mms_field_%" PetscInt_FMT, i));
    PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply MMS Dirichlet BC", NULL, ratel->bc_mms_faces[i], &ratel->bc_mms_count[i], NULL));
    if (i == 0 && ratel->bc_mms_count[i] == 0) {
      ratel->bc_mms_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray("-bc_mms", "Face IDs to apply MMS Dirichlet BC", NULL, ratel->bc_mms_faces[i], &ratel->bc_mms_count[i], NULL));
    }

    if (ratel->bc_mms_count[i] > 0 && ratel->forcing_choice == RATEL_FORCING_MMS && ratel->num_materials > 1) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Cannot verify the manufactured solution with more than one material");
      // LCOV_EXCL_STOP
    }
  }
  {
    PetscInt bc_mms_count = 0;

    for (PetscInt i = 0; i < ratel->num_active_fields; i++) bc_mms_count += ratel->bc_mms_count[i];
    if ((bc_mms_count > 0 || ratel->forcing_choice == RATEL_FORCING_MMS) && (bc_mms_count == 0 || ratel->forcing_choice != RATEL_FORCING_MMS)) {
      // LCOV_EXCL_START
      PetscCall(PetscPrintf(ratel->comm, "WARNING: '-bc_mms' and '-forcing mms' typically should be used together"));
      // LCOV_EXCL_STOP
    }
  }

  // Slip boundary conditions
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    char option_name[PETSC_MAX_PATH_LEN];

    ratel->bc_slip_count[i] = RATEL_MAX_BOUNDARY_FACES;
    PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_slip_field_%" PetscInt_FMT, i));
    PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply slip BC", NULL, ratel->bc_slip_faces[i], &ratel->bc_slip_count[i], NULL));
    if (i == 0 && ratel->bc_slip_count[i] == 0) {
      ratel->bc_slip_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray("-bc_slip", "Face IDs to apply slip BC", NULL, ratel->bc_slip_faces[i], &ratel->bc_slip_count[i], NULL));
    }

    // -- Set components for each slip BC
    if (ratel->bc_slip_count[i]) RatelDebug(ratel, "---- Slip Boundaries, field %" PetscInt_FMT ": ", i);
    for (PetscInt j = 0; j < ratel->bc_slip_count[i]; j++) {
      // ---- Translation vector
      ratel->bc_slip_components_count[i][j] = sizeof(ratel->bc_slip_components[i][0]) / sizeof(ratel->bc_slip_components[i][0][0]);
      for (PetscInt k = 0; k < ratel->bc_slip_components_count[i][j]; k++) ratel->bc_slip_components[i][j][k] = 0.;

      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_slip_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_components", i,
                              ratel->bc_slip_faces[i][j]));
      PetscCall(PetscOptionsIntArray(option_name, "Components to constrain with slip BC", NULL, ratel->bc_slip_components[i][j],
                                     &ratel->bc_slip_components_count[i][j], NULL));
      if (i == 0 && ratel->bc_slip_components_count[i][j] == 0) {
        ratel->bc_slip_components_count[i][j] = sizeof(ratel->bc_slip_components[i][0]) / sizeof(ratel->bc_slip_components[i][0][0]);
        PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_slip_%" PetscInt_FMT "_components", ratel->bc_slip_faces[i][j]));
        PetscCall(PetscOptionsIntArray(option_name, "Components to constrain with slip BC", NULL, ratel->bc_slip_components[i][j],
                                       &ratel->bc_slip_components_count[i][j], NULL));
      }

      if (ratel->bc_slip_components_count[i][j] == 0) {
        // LCOV_EXCL_START
        SETERRQ(ratel->comm, PETSC_ERR_SUP, "Must set slip components for face %" PetscInt_FMT, ratel->bc_slip_faces[i][j]);
        // LCOV_EXCL_STOP
      }

      if (ratel->is_debug) {
        // LCOV_EXCL_START
        char    components[8] = "";
        CeedInt head          = 0;
        for (CeedInt k = 0; k < ratel->bc_slip_components_count[i][j]; k++) {
          components[head] = ratel->bc_slip_components[i][j][k] == 0 ? 'x' : ratel->bc_slip_components[i][j][k] == 1 ? 'y' : 'z';
          head++;
          if (k < ratel->bc_slip_components_count[i][j] - 1) {
            components[head]     = ',';
            components[head + 1] = ' ';
            head += 2;
          }
        }
        RatelDebug(ratel, "----   Slip Boundary %" PetscInt_FMT ": components %s", ratel->bc_slip_faces[i][j], components);
        // LCOV_EXCL_STOP
      }
    }
  }

  // Neumann boundary conditions
  ratel->bc_traction_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-bc_traction", "Face IDs to apply traction (Neumann) BC", NULL, ratel->bc_traction_faces, &ratel->bc_traction_count,
                                 NULL));

  // Platen boundary conditions
  ratel->bc_platen_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-bc_platen", "Face IDs to apply platen BC", NULL, ratel->bc_platen_faces, &ratel->bc_platen_count, NULL));

  // Initial condition type
  ratel->initial_condition_type = RATEL_INITIAL_CONDITION_ZERO;
  {
    PetscInt  max_strings    = 1;
    PetscBool is_continue_ic = PETSC_FALSE;

    PetscCall(PetscOptionsStringArray("-continue_file", "Filepath for initial condition binary file", NULL, &ratel->continue_file_name, &max_strings,
                                      &is_continue_ic));
    if (is_continue_ic) ratel->initial_condition_type = RATEL_INITIAL_CONDITION_CONTINUE;
  }

  // Face diagnostics
  ratel->surface_force_face_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-surface_force_faces", "Face IDs to compute surface force", NULL, ratel->surface_force_dm_faces,
                                 &ratel->surface_force_face_count, NULL));

  // Set target strain energy
  {
    PetscBool expected_strain_energy_set = PETSC_FALSE;

    ratel->expected_strain_energy = 0.;
    PetscCall(PetscOptionsReal("-expected_strain_energy", "Expected final strain energy", NULL, ratel->expected_strain_energy,
                               &ratel->expected_strain_energy, &expected_strain_energy_set));
    if (!expected_strain_energy_set) ratel->expected_strain_energy = 0.0;
  }

  PetscOptionsEnd();  // End of setting AppCtx

  // Check for all required values set
  {
    PetscInt fixed_bc_count = 0;

    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      fixed_bc_count += ratel->bc_clamp_count[i] + ratel->bc_mms_count[i] + ratel->bc_slip_count[i];
    }
    if (fixed_bc_count == 0 && ratel->forcing_choice != RATEL_FORCING_MMS) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "At least one fixed boundary condition needed");
      // LCOV_EXCL_STOP
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Process Command Line Options Success!");
  PetscFunctionReturn(0);
}

/// @}
