/// @file
/// Ratel matrix shell and SNES/TS operators

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscsnes.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-petsc-ops.h>
#include <ratel-utils.h>
#include <ratel.h>

/// @addtogroup RatelInternal
/// @{

// -----------------------------------------------------------------------------
// Context data
// -----------------------------------------------------------------------------
/**
  @brief Setup context data for operator application

  @param[in]   ratel  Ratel context
  @param[in]   name   Context name
  @param[in]   dm_x   Input DM
  @param[in]   dm_y   Output DM
  @param[in]   X_loc  Input PETSc local vector
  @param[in]   Y_loc  Output PETSc local vector
  @param[in]   x_loc  Input libCEED local vector
  @param[in]   y_loc  Output libCEED local vector
  @param[in]   op     libCEED operator
  @param[out]  ctx    Context data for operator evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelOperatorApplyContextCreate(Ratel ratel, const char *name, DM dm_x, DM dm_y, Vec X_loc, Vec Y_loc, CeedVector x_loc,
                                               CeedVector y_loc, CeedOperator op, RatelOperatorApplyContext *ctx) {
  PetscFunctionBeginUser;

  // Allocate
  PetscCall(PetscCalloc1(1, ctx));

  // Ratel
  (*ctx)->ratel = ratel;

  // Name
  {
    char  *name_copy = NULL;
    size_t name_len  = name ? strlen(name) : 0;

    if (name_len > 0) {
      PetscCall(PetscCalloc1(name_len + 1, &name_copy));
      PetscCall(PetscMemcpy(name_copy, name, name_len));
    }
    (*ctx)->name = name_copy;
  }

  // PETSc objects
  PetscCall(PetscObjectReference((PetscObject)dm_x));
  (*ctx)->dm_x = dm_x;
  PetscCall(PetscObjectReference((PetscObject)dm_y));
  (*ctx)->dm_y = dm_y;
  if (X_loc) PetscCall(PetscObjectReference((PetscObject)X_loc));
  else RatelDebug256(ratel, RATEL_DEBUG_ORANGE, "---- No input local vector for context \"%s\", global vector required to apply", name ? name : "");
  (*ctx)->X_loc = X_loc;
  if (Y_loc) PetscCall(PetscObjectReference((PetscObject)Y_loc));
  else RatelDebug256(ratel, RATEL_DEBUG_ORANGE, "---- No output local vector for context \"%s\", global vector required to apply", name ? name : "");
  (*ctx)->Y_loc = Y_loc;

  // libCEED objects
  RatelCeedCall(ratel, CeedVectorReferenceCopy(x_loc, &(*ctx)->x_loc));
  RatelCeedCall(ratel, CeedVectorReferenceCopy(y_loc, &(*ctx)->y_loc));
  RatelCeedCall(ratel, CeedOperatorReferenceCopy(op, &(*ctx)->op));

  PetscFunctionReturn(0);
}

/**
  @brief Destroy context data for operator application

  @param[in,out]  ctx  Context data for operator evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelOperatorApplyContextDestroy(RatelOperatorApplyContext ctx) {
  PetscFunctionBeginUser;

  if (!ctx) PetscFunctionReturn(0);

  Ratel ratel = ctx->ratel;

  // Name
  PetscCall(PetscFree(ctx->name));

  // PETSc objects
  PetscCall(DMDestroy(&ctx->dm_x));
  PetscCall(DMDestroy(&ctx->dm_y));
  PetscCall(VecDestroy(&ctx->X_loc));
  PetscCall(VecDestroy(&ctx->Y_loc));

  // libCEED objects
  RatelCeedCall(ratel, CeedVectorDestroy(&ctx->x_loc));
  RatelCeedCall(ratel, CeedVectorDestroy(&ctx->y_loc));
  RatelCeedCall(ratel, CeedOperatorDestroy(&ctx->op));

  // Deallocate
  PetscCall(PetscFree(ctx));

  PetscFunctionReturn(0);
}

/**
  @brief Setup context data for prolongation/restriction

  @param[in]   ratel        Ratel context
  @param[in]   name         Context name
  @param[in]   dm_c         Coarse grid DM
  @param[in]   dm_f         Fine grid DM
  @param[in]   X_loc_c      Coarse grid input PETSc local vector
  @param[in]   X_loc_f      Fine grid input PETSc local vector
  @param[in]   x_loc_c      Coarse grid input libCEED local vector
  @param[in]   x_loc_f      Fine grid input libCEED local vector
  @param[in]   y_loc_c      Coarse grid output libCEED local vector
  @param[in]   y_loc_f      Fine grid output libCEED local vector
  @param[in]   op_prolong   libCEED prolongation operator
  @param[in]   op_restrict  libCEED restriction operator
  @param[out]  ctx          Context data for prolongation/restriction evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProlongRestrictContextCreate(Ratel ratel, const char *name, DM dm_c, DM dm_f, Vec X_loc_c, Vec X_loc_f, CeedVector x_loc_c,
                                                 CeedVector x_loc_f, CeedVector y_loc_c, CeedVector y_loc_f, CeedOperator op_prolong,
                                                 CeedOperator op_restrict, RatelProlongRestrictContext *ctx) {
  PetscFunctionBeginUser;

  // Allocate
  PetscCall(PetscCalloc1(1, ctx));

  // Ratel
  (*ctx)->ratel = ratel;

  // Name
  {
    char  *name_copy = NULL;
    size_t name_len  = name ? strlen(name) : 0;

    if (name_len > 0) {
      PetscCall(PetscCalloc1(name_len + 1, &name_copy));
      PetscCall(PetscMemcpy(name_copy, name, name_len));
    }
    (*ctx)->name = name_copy;
  }

  // PETSc objects
  PetscCall(PetscObjectReference((PetscObject)dm_c));
  (*ctx)->dm_c = dm_c;
  PetscCall(PetscObjectReference((PetscObject)dm_f));
  (*ctx)->dm_f = dm_f;
  PetscCall(PetscObjectReference((PetscObject)X_loc_c));
  (*ctx)->X_loc_c = X_loc_c;
  PetscCall(PetscObjectReference((PetscObject)X_loc_f));
  (*ctx)->X_loc_f = X_loc_f;

  // libCEED objects
  RatelCeedCall(ratel, CeedVectorReferenceCopy(x_loc_f, &(*ctx)->x_loc_f));
  RatelCeedCall(ratel, CeedVectorReferenceCopy(x_loc_c, &(*ctx)->x_loc_c));
  RatelCeedCall(ratel, CeedVectorReferenceCopy(y_loc_f, &(*ctx)->y_loc_f));
  RatelCeedCall(ratel, CeedVectorReferenceCopy(y_loc_c, &(*ctx)->y_loc_c));
  RatelCeedCall(ratel, CeedOperatorReferenceCopy(op_prolong, &(*ctx)->op_prolong));
  RatelCeedCall(ratel, CeedOperatorReferenceCopy(op_restrict, &(*ctx)->op_restrict));

  PetscFunctionReturn(0);
}

/**
  @brief Destroy context data for prolongation/restriction

  @param[in,out]  ctx  Context data for prolongation/restriction evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProlongRestrictContextDestroy(RatelProlongRestrictContext ctx) {
  PetscFunctionBeginUser;

  if (!ctx) PetscFunctionReturn(0);

  Ratel ratel = ctx->ratel;

  // Name
  PetscCall(PetscFree(ctx->name));

  // PETSc objects
  PetscCall(DMDestroy(&ctx->dm_c));
  PetscCall(DMDestroy(&ctx->dm_f));
  PetscCall(VecDestroy(&ctx->X_loc_c));
  PetscCall(VecDestroy(&ctx->X_loc_f));

  // libCEED objects
  RatelCeedCall(ratel, CeedVectorDestroy(&ctx->x_loc_f));
  RatelCeedCall(ratel, CeedVectorDestroy(&ctx->x_loc_c));
  RatelCeedCall(ratel, CeedVectorDestroy(&ctx->y_loc_f));
  RatelCeedCall(ratel, CeedVectorDestroy(&ctx->y_loc_c));
  RatelCeedCall(ratel, CeedOperatorDestroy(&ctx->op_prolong));
  RatelCeedCall(ratel, CeedOperatorDestroy(&ctx->op_restrict));

  // Deallocate
  PetscCall(PetscFree(ctx));

  PetscFunctionReturn(0);
}

// -----------------------------------------------------------------------------
// Preconditioning
// -----------------------------------------------------------------------------
/**
  @brief Compute the diagonal of an operator via libCEED

  @param[in]   A  Operator MatShell
  @param[out]  D  Vector holding operator diagonal

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetDiagonal(Mat A, Vec D) {
  Ratel                     ratel;
  RatelOperatorApplyContext ctx;
  Vec                       D_loc;
  PetscMemType              mem_type;
  CeedOperator             *sub_operators;
  PetscBool                 has_smoother_parameter = PETSC_FALSE, is_smoother_parameter_changed = PETSC_FALSE;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // Get sub-operators
  for (PetscInt i = 0; i < ratel->num_multigrid_levels_setup; i++) has_smoother_parameter |= ctx->op == ratel->ctx_jacobian[i]->op;
  if (has_smoother_parameter) {
    RatelCeedCall(ratel, CeedCompositeOperatorGetSubList(ctx->op, &sub_operators));

    // Set smoother physics context
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscBool is_current_changed = PETSC_FALSE;

      PetscCall(RatelMaterialSetJacobianSootherContext(ratel->materials[i], PETSC_TRUE, ctx->op, &is_current_changed));
      is_smoother_parameter_changed |= is_current_changed;
    }
    if (is_smoother_parameter_changed) {
      RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ctx->op, true));
    }
  }

  // Place PETSc vector in libCEED vector
  PetscCall(DMGetLocalVector(ctx->dm_x, &D_loc));
  PetscCall(RatelVecP2C(ratel, D_loc, &mem_type, ctx->x_loc));

  // Compute Diagonal
  RatelCeedCall(ratel, CeedOperatorLinearAssembleDiagonal(ctx->op, ctx->x_loc, CEED_REQUEST_IMMEDIATE));

  // Reset physics context
  if (is_smoother_parameter_changed) {
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetJacobianSootherContext(ratel->materials[i], PETSC_FALSE, ctx->op, NULL));
    }
    RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ctx->op, true));
  }

  // Restore PETSc vector
  PetscCall(RatelVecC2P(ratel, ctx->x_loc, mem_type, D_loc));

  // Local-to-Global
  PetscCall(VecZeroEntries(D));
  PetscCall(DMLocalToGlobal(ctx->dm_x, D_loc, ADD_VALUES, D));
  PetscCall(DMRestoreLocalVector(ctx->dm_x, &D_loc));

  PetscFunctionReturn(0);
}

/**
  @brief Set COO preallocation for Mat associated with a CeedOperator

  @param[in]      ratel       Ratel context
  @param[in]      op          CeedOperator to assemble sparsity pattern
  @param[out]     coo_values  CeedVector to hold COO values
  @param[in,out]  A           Sparse matrix to hold assembled CeedOperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMatSetPreallocationCOO(Ratel ratel, CeedOperator op, CeedVector *coo_values, Mat A) {
  PetscLogStage stage_amg_setup;
  PetscCount    num_entries;
  CeedInt      *rows_ceed, *cols_ceed;
  PetscInt     *rows_petsc = NULL, *cols_petsc = NULL;

  PetscFunctionBeginUser;

  // -- Assemble sparsity pattern
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel AMG Setup");
  PetscCall(PetscLogStageGetId("Ratel AMG Setup", &stage_amg_setup));
  if (stage_amg_setup == -1) {
    PetscCall(PetscLogStageRegister("Ratel AMG Setup", &stage_amg_setup));
  }
  PetscCall(PetscLogStagePush(stage_amg_setup));
  RatelCeedCall(ratel, CeedOperatorLinearAssembleSymbolic(op, &num_entries, &rows_ceed, &cols_ceed));
  PetscCall(RatelIntArrayC2P(num_entries, &rows_ceed, &rows_petsc));
  PetscCall(RatelIntArrayC2P(num_entries, &cols_ceed, &cols_petsc));
  PetscCall(MatSetPreallocationCOOLocal(A, num_entries, rows_petsc, cols_petsc));
  free(rows_petsc);
  free(cols_petsc);
  RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, num_entries, coo_values));

  PetscCall(PetscLogStagePop());
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel AMG Setup Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Set COO values for Mat associated with a CeedOperator

  @param[in]      ratel       Ratel context
  @param[in]      op          CeedOperator to assemble
  @param[in]      coo_values  CeedVector to hold COO values
  @param[in]      mem_type    MemType to pass COO values
  @param[in,out]  A           Sparse matrix to hold assembled CeedOperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMatSetValuesCOO(Ratel ratel, CeedOperator op, CeedVector coo_values, CeedMemType mem_type, Mat A) {
  const CeedScalar *values;

  PetscFunctionBeginUser;

  // -- Assemble matrix values
  RatelCeedCall(ratel, CeedOperatorLinearAssemble(op, coo_values));
  RatelCeedCall(ratel, CeedVectorGetArrayRead(coo_values, mem_type, &values));
  PetscCall(MatSetValuesCOO(A, values, INSERT_VALUES));
  PetscCall(MatSetOption(A, MAT_SPD, PETSC_TRUE));
  RatelCeedCall(ratel, CeedVectorRestoreArrayRead(coo_values, &values));

  PetscFunctionReturn(0);
}

// -----------------------------------------------------------------------------
// Operator application
// -----------------------------------------------------------------------------
/**
  @brief Apply the local action of a libCEED operator and store result in PETSc vector
         i.e. compute A X = Y

  @param[in]   X    Input PETSc vector
  @param[out]  Y    Output PETSc vector, or NULL to only write into local vector
  @param[in]   ctx  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyLocalCeedOp(Vec X, Vec Y, RatelOperatorApplyContext ctx) {
  PetscFunctionBeginUser;

  // Zero target vector
  if (Y) PetscCall(VecZeroEntries(Y));

  // Sum into target vector
  PetscCall(RatelApplyAddLocalCeedOp(X, Y, ctx));

  PetscFunctionReturn(0);
}

/**
  @brief Apply the local action of a libCEED operator and store result in PETSc vector
         i.e. compute A X = Y

  @param[in]   X    Input PETSc vector
  @param[out]  Y    Output PETSc vector, or NULL to only write into local vector
  @param[in]   ctx  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyAddLocalCeedOp(Vec X, Vec Y, RatelOperatorApplyContext ctx) {
  Ratel        ratel = ctx->ratel;
  Vec          X_loc = ctx->X_loc, Y_loc = ctx->Y_loc;
  PetscMemType x_mem_type, y_mem_type;

  PetscFunctionBeginUser;

  // Vec validity check
  if ((!X && !ctx->X_loc) || (!Y && !ctx->Y_loc)) {
    // LCOV_EXCL_START
    SETERRQ(ratel->comm, PETSC_ERR_SUP, "A local or global Vec must be provided for the input and output");
    // LCOV_EXCL_STOP
  }

  // Get local vectors, as needed
  if (!ctx->X_loc) PetscCall(DMGetLocalVector(ctx->dm_x, &X_loc));
  if (!ctx->Y_loc) PetscCall(DMGetLocalVector(ctx->dm_y, &Y_loc));

  // Global-to-local
  PetscCall(DMGlobalToLocal(ctx->dm_x, X, INSERT_VALUES, X_loc));

  // Setup libCEED vectors
  PetscCall(RatelVecReadP2C(ratel, X_loc, &x_mem_type, ctx->x_loc));
  PetscCall(RatelVecP2C(ratel, Y_loc, &y_mem_type, ctx->y_loc));

  // Apply libCEED operator
  RatelCeedCall(ratel, CeedOperatorApply(ctx->op, ctx->x_loc, ctx->y_loc, CEED_REQUEST_IMMEDIATE));

  // Restore PETSc vectors
  PetscCall(RatelVecReadC2P(ratel, ctx->x_loc, x_mem_type, X_loc));
  PetscCall(RatelVecC2P(ratel, ctx->y_loc, y_mem_type, Y_loc));

  // Local-to-global
  if (Y) PetscCall(DMLocalToGlobal(ctx->dm_y, Y_loc, ADD_VALUES, Y));

  // Restore local vectors, as needed
  if (!ctx->X_loc) PetscCall(DMRestoreLocalVector(ctx->dm_x, &X_loc));
  if (!ctx->Y_loc) PetscCall(DMRestoreLocalVector(ctx->dm_y, &Y_loc));

  PetscFunctionReturn(0);
}

/**
  @brief Get the current time used in libCEED operators

  @param[in]  ratel  Ratel context to get time
  @param[out]  time  Current time

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelGetCurrentTime(Ratel ratel, PetscReal *current_time) {
  size_t        num_values;
  const double *context_time;

  PetscFunctionBeginUser;

  // Set default
  *current_time = RATEL_UNSET_INITIAL_TIME;

  // Update contexts as needed
  if (ratel->op_dirichlet_time_label) {
    RatelCeedCall(ratel, CeedOperatorContextGetDoubleRead(ratel->op_dirichlet, ratel->op_dirichlet_time_label, &num_values, &context_time));
    *current_time = context_time[0];
    RatelCeedCall(ratel, CeedOperatorContextRestoreDoubleRead(ratel->op_dirichlet, ratel->op_dirichlet_time_label, &context_time));
    PetscFunctionReturn(0);
  }
  if (ratel->op_residual_u_time_label) {
    RatelCeedCall(ratel, CeedOperatorContextGetDoubleRead(ratel->ctx_residual_u->op, ratel->op_residual_u_time_label, &num_values, &context_time));
    *current_time = context_time[0];
    RatelCeedCall(ratel, CeedOperatorContextRestoreDoubleRead(ratel->ctx_residual_u->op, ratel->op_residual_u_time_label, &context_time));
    PetscFunctionReturn(0);
  }
  if (ratel->op_jacobian_time_label) {
    RatelCeedCall(ratel, CeedOperatorContextGetDoubleRead(ratel->ctx_jacobian[0]->op, ratel->op_jacobian_time_label, &num_values, &context_time));
    *current_time = context_time[0];
    RatelCeedCall(ratel, CeedOperatorContextRestoreDoubleRead(ratel->ctx_jacobian[0]->op, ratel->op_jacobian_time_label, &context_time));
    PetscFunctionReturn(0);
  }

  PetscFunctionReturn(0);
}

/**
  @brief Update the current time used in libCEED operators

  @param[in,out]  ratel  Ratel context to update boundary conditions
  @param[in]      time   Current time

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelUpdateTimeContexts(Ratel ratel, PetscReal time) {
  PetscFunctionBeginUser;

  // Update contexts as needed
  if (ratel->op_dirichlet_time_label) {
    RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_dirichlet, ratel->op_dirichlet_time_label, &time));
  }
  if (ratel->op_residual_u_time_label) {
    RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->ctx_residual_u->op, ratel->op_residual_u_time_label, &time));
  }
  if (ratel->op_jacobian_time_label) {
    RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->ctx_jacobian[0]->op, ratel->op_jacobian_time_label, &time));
  }

  PetscFunctionReturn(0);
}

/**
  @brief Compute the Dirichlet boundary values via libCEED

  @param[in]   dm                 DM to insert boundary values on
  @param[in]   insert_essential   Boolean flag for Dirichlet or Neumann boundary conditions
  @param[out]  U_loc              Local vector to insert the boundary values into
  @param[in]   time               Current time
  @param[in]   face_geometry_FVM  Face geometry data for finite volume discretizations
  @param[in]   cell_geometry_FVM  Cell geometry data for finite volume discretizations
  @param[in]   grad_FVM           Gradient reconstruction data for finite volume discretizations

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexInsertBoundaryValues(DM dm, PetscBool insert_essential, Vec U_loc, PetscReal time, Vec face_geometry_FVM,
                                               Vec cell_geometry_FVM, Vec grad_FVM) {
  Ratel        ratel;
  PetscMemType x_mem_type;
  Vec          boundary_mask;

  PetscFunctionBeginUser;

  PetscCall(DMGetApplicationContext(dm, &ratel));
  PetscBool have_nonzero_bcs = ratel->bc_clamp_nonzero_count > 0;

  // Update time context information
  PetscCall(RatelUpdateTimeContexts(ratel, time));

  // Update boundary values
  Vec X_loc = U_loc;
  while (X_loc) {
    // -- Mask Dirichlet entries
    PetscCall(DMGetNamedLocalVector(dm, "boundary mask", &boundary_mask));
    PetscCall(VecPointwiseMult(X_loc, X_loc, boundary_mask));
    PetscCall(DMRestoreNamedLocalVector(dm, "boundary mask", &boundary_mask));

    // -- Compute boundary values
    if (have_nonzero_bcs) {
      // ---- Setup libCEED vector
      PetscCall(RatelVecP2C(ratel, X_loc, &x_mem_type, ratel->ctx_residual_u->x_loc));

      // ---- Apply libCEED operator
      RatelCeedCall(ratel, CeedOperatorApplyAdd(ratel->op_dirichlet, CEED_VECTOR_NONE, ratel->ctx_residual_u->x_loc, CEED_REQUEST_IMMEDIATE));

      // ---- Restore PETSc vectors
      PetscCall(RatelVecC2P(ratel, ratel->ctx_residual_u->x_loc, x_mem_type, X_loc));
    }

    // -- Update cached boundary values, if needed
    Vec X_loc_residual = ratel->ctx_residual_u->X_loc;
    if (X_loc != X_loc_residual) X_loc = X_loc_residual;
    else X_loc = NULL;
  }

  PetscFunctionReturn(0);
}

/**
  @brief Update time contexts and boundary values

  @param[in,out]  ratel  Ratel context to update boundary conditions
  @param[in]      time   Current time

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelUpdateTimeAndBoundaryValues(Ratel ratel, PetscReal time) {
  CeedScalar current_time;

  PetscFunctionBeginUser;

  // Check for time mismatch
  PetscCall(RatelGetCurrentTime(ratel, &current_time));

  // Used cached values if time unchanged
  if (current_time != time) {
    PetscCall(RatelUpdateTimeContexts(ratel, time));
    PetscCall(DMPlexInsertBoundaryValues(ratel->ctx_residual_u->dm_x, PETSC_TRUE, ratel->ctx_residual_u->X_loc, time, NULL, NULL, NULL));
  }

  PetscFunctionReturn(0);
}

/**
  @brief Compute the action of the matrix-free operator libCEED

  @param[in]   A  Operator MatShell
  @param[in]   X  Input PETSc vector
  @param[out]  Y  Output PETSc vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyOperator(Mat A, Vec X, Vec Y) {
  RatelOperatorApplyContext ctx;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  PetscCall(RatelApplyLocalCeedOp(X, Y, ctx));

  PetscFunctionReturn(0);
}

/**
  @brief Compute the action of the Jacobian for a SNES operator via libCEED

  @param[in]   A  Jacobian operator MatShell
  @param[in]   X  Input PETSc vector
  @param[out]  Y  Output PETSc vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyJacobian(Mat A, Vec X, Vec Y) {
  Ratel                     ratel;
  RatelOperatorApplyContext ctx;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // libCEED for local action of Jacobian
  // Note: X_loc should be zeroed already at this point
  PetscCall(PetscLogEventBegin(ratel->event_jacobian, A, X, Y, 0));

  PetscCall(RatelApplyLocalCeedOp(X, Y, ctx));
  if (ratel->is_ceed_backend_gpu) PetscCall(PetscLogGpuFlops(ctx->flops));
  else PetscCall(PetscLogFlops(ctx->flops));

  PetscCall(PetscLogEventEnd(ratel->event_jacobian, A, X, Y, 0));

  PetscFunctionReturn(0);
}

/**
  @brief Compute action of a p-multigrid prolongation operator via libCEED

  @param[in]   A  Prolongation operator MatShell
  @param[in]   C  Input PETSc vector on coarse grid
  @param[out]  F  Output PETSc vector on fine grid

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyProlongation(Mat A, Vec C, Vec F) {
  Ratel                       ratel;
  RatelProlongRestrictContext ctx;
  Vec                         X_loc_c, Y_loc_f;
  PetscMemType                x_mem_type, y_mem_type;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // FLOPs counting
  PetscCall(PetscLogEventBegin(ctx->event_prolong, A, C, F, 0));
  if (ratel->is_ceed_backend_gpu) PetscCall(PetscLogGpuFlops(ctx->flops_prolong));
  else PetscCall(PetscLogFlops(ctx->flops_prolong));

  // Get local vectors, as needed
  X_loc_c = ctx->X_loc_c;
  PetscCall(DMGetLocalVector(ctx->dm_f, &Y_loc_f));

  // Global-to-local
  // Note: X_loc_c should be zeroed already at this point
  PetscCall(DMGlobalToLocal(ctx->dm_c, C, INSERT_VALUES, X_loc_c));

  // Setup libCEED vectors
  PetscCall(RatelVecReadP2C(ratel, X_loc_c, &x_mem_type, ctx->x_loc_c));
  PetscCall(RatelVecP2C(ratel, Y_loc_f, &y_mem_type, ctx->y_loc_f));

  // Apply libCEED operator
  RatelCeedCall(ratel, CeedOperatorApply(ctx->op_prolong, ctx->x_loc_c, ctx->y_loc_f, CEED_REQUEST_IMMEDIATE));

  // Restore PETSc vectors
  PetscCall(RatelVecReadC2P(ratel, ctx->x_loc_c, x_mem_type, X_loc_c));
  PetscCall(RatelVecC2P(ratel, ctx->y_loc_f, y_mem_type, Y_loc_f));

  // Local-to-global
  PetscCall(VecZeroEntries(F));
  PetscCall(DMLocalToGlobal(ctx->dm_f, Y_loc_f, ADD_VALUES, F));

  // Restore local vectors, as needed
  PetscCall(DMRestoreLocalVector(ctx->dm_f, &Y_loc_f));

  PetscCall(PetscLogEventEnd(ctx->event_prolong, A, C, F, 0));
  PetscFunctionReturn(0);
}

/**
  @brief Compute action of a p-multigrid restriction operator via libCEED

  @param[in]   A  Restriction operator MatShell
  @param[in]   F  Input PETSc vector on fine grid
  @param[out]  C  Output PETSc vector on coarse grid

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyRestriction(Mat A, Vec F, Vec C) {
  Ratel                       ratel;
  RatelProlongRestrictContext ctx;
  Vec                         X_loc_f, Y_loc_c;
  PetscMemType                x_mem_type, y_mem_type;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // FLOPs counting
  PetscCall(PetscLogEventBegin(ctx->event_restrict, A, F, C, 0));
  if (ratel->is_ceed_backend_gpu) PetscCall(PetscLogGpuFlops(ctx->flops_restrict));
  else PetscCall(PetscLogFlops(ctx->flops_restrict));

  // Get local vectors, as needed
  X_loc_f = ctx->X_loc_f;
  PetscCall(DMGetLocalVector(ctx->dm_c, &Y_loc_c));

  // Global-to-local
  // Note: X_loc_f should be zeroed already at this point
  PetscCall(DMGlobalToLocal(ctx->dm_f, F, INSERT_VALUES, ctx->X_loc_f));

  // Setup libCEED vectors
  PetscCall(RatelVecReadP2C(ratel, X_loc_f, &x_mem_type, ctx->x_loc_f));
  PetscCall(RatelVecP2C(ratel, Y_loc_c, &y_mem_type, ctx->y_loc_c));

  // Apply libCEED operator
  RatelCeedCall(ratel, CeedOperatorApply(ctx->op_restrict, ctx->x_loc_f, ctx->y_loc_c, CEED_REQUEST_IMMEDIATE));

  // Restore PETSc vectors
  PetscCall(RatelVecReadC2P(ratel, ctx->x_loc_f, x_mem_type, X_loc_f));
  PetscCall(RatelVecC2P(ratel, ctx->y_loc_c, y_mem_type, Y_loc_c));

  // Local-to-global
  PetscCall(VecZeroEntries(C));
  PetscCall(DMLocalToGlobal(ctx->dm_c, Y_loc_c, ADD_VALUES, C));

  // Restore local vectors, as needed
  PetscCall(DMRestoreLocalVector(ctx->dm_c, &Y_loc_c));

  PetscCall(PetscLogEventEnd(ctx->event_restrict, A, F, C, 0));
  PetscFunctionReturn(0);
}

// -----------------------------------------------------------------------------
// Solver callbacks
// -----------------------------------------------------------------------------
/**
  @brief Compute the non-linear residual for a SNES operator via libCEED

  @param[in]   snes            Non-linear solver
  @param[in]   X               Current state vector
  @param[out]  Y               Residual vector
  @param[in]   ctx_residual_u  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESFormResidual(SNES snes, Vec X, Vec Y, void *ctx_residual_u) {
  RatelOperatorApplyContext ctx   = (RatelOperatorApplyContext)ctx_residual_u;
  Ratel                     ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Update time context and Dirichlet values
  PetscCall(RatelUpdateTimeAndBoundaryValues(ratel, 1.0));

  // libCEED for local action of residual evaluator
  PetscCall(RatelApplyLocalCeedOp(X, Y, ctx));

  // Mark QFunction as requiring re-assembly
  RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ctx->ratel->ctx_jacobian[0]->op, true));

  PetscFunctionReturn(0);
}

/**
  @brief Assemble SNES Jacobian for each level of multigrid hierarchy

  @param[in]      snes               Non-linear solver
  @param[in]      X                  Current non-linear residual
  @param[out]     J                  Fine grid Jacobian operator
  @param[out]     J_pre              Fine grid Jacobian operator for preconditioning
  @param[in,out]  ctx_form_jacobian  Jacobian context, holding Jacobian operators for each level of multigrid hierarchy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESFormJacobian(SNES snes, Vec X, Mat J, Mat J_pre, void *ctx_form_jacobian) {
  RatelFormJacobianContext ctx                 = (RatelFormJacobianContext)ctx_form_jacobian;
  Ratel                    ratel               = ctx->ratel;
  PetscInt                 num_levels          = ctx->num_levels;
  Mat                     *mat_jacobian        = ctx->mat_jacobian;
  Mat                      mat_jacobian_coarse = num_levels > 1 ? ctx->mat_jacobian_coarse : NULL;
  MatType                  mat_type;

  PetscFunctionBeginUser;

  // Set context for fine level
  PetscCall(MatGetType(J, &mat_type));
  if (!strcmp(mat_type, MATSHELL)) {
    PetscCall(MatShellSetContext(J, ctx->ctx_jacobian_fine));
    PetscCall(MatShellSetOperation(J, MATOP_MULT, (void (*)(void))RatelApplyJacobian));
    PetscCall(MatShellSetOperation(J, MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
    PetscCall(MatShellSetVecType(J, ctx->vec_type));
    PetscCall(MatSetUp(J));
  } else {
    mat_jacobian_coarse = J;
  }

  // Update Jacobian on each level
  for (PetscInt level = 1; level < num_levels; level++) {
    PetscCall(MatAssemblyBegin(mat_jacobian[level], MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(mat_jacobian[level], MAT_FINAL_ASSEMBLY));
    PetscCall(MatSetOption(mat_jacobian[level], MAT_SPD, PETSC_TRUE));
  }

  // Form coarse assembled matrix
  if (mat_jacobian_coarse) {
    const PetscScalar *x;
    PetscMemType       mem_type;

    if (!ctx->coo_values) {
      PetscCall(RatelMatSetPreallocationCOO(ratel, ctx->op_coarse, &ctx->coo_values, mat_jacobian_coarse));
    }
    PetscCall(VecGetArrayReadAndMemType(X, &x, &mem_type));
    PetscCall(VecRestoreArrayReadAndMemType(X, &x));
    PetscCall(RatelMatSetValuesCOO(ratel, ctx->op_coarse, ctx->coo_values, RatelMemTypeP2C(mem_type), mat_jacobian_coarse));
  }

  // J_pre might be AIJ (e.g., when using coloring), so we need to assemble it
  PetscCall(MatAssemblyBegin(J, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(J, MAT_FINAL_ASSEMBLY));
  PetscCall(MatSetOption(J, MAT_SPD, PETSC_TRUE));
  if (J != J_pre) {
    PetscCall(MatAssemblyBegin(J_pre, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(J_pre, MAT_FINAL_ASSEMBLY));
    PetscCall(MatSetOption(J_pre, MAT_SPD, PETSC_TRUE));
  }

  PetscFunctionReturn(0);
}

/**
  @brief Compute the non-linear residual for a TS operator via libCEED

  @param[in]      ts               Time-stepper
  @param[in]      time             Current time
  @param[in]      X                Current state vector
  @param[in]      X_t              Time derivative of current state vector
  @param[out]     Y                Function vector
  @param[in,out]  ctx_residual_ut  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormIResidual(TS ts, PetscReal time, Vec X, Vec X_t, Vec Y, void *ctx_residual_ut) {
  RatelOperatorApplyContext ctx   = (RatelOperatorApplyContext)ctx_residual_ut;
  Ratel                     ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Update time context and Dirichlet values
  PetscCall(RatelUpdateTimeAndBoundaryValues(ratel, time));

  // libCEED for local action of residual evaluator
  PetscCall(RatelApplyLocalCeedOp(X, Y, ratel->ctx_residual_u));

  // Add contribution of X_t term, if required
  if (ratel->has_ut_term) {
    PetscCall(RatelApplyAddLocalCeedOp(X_t, Y, ctx));
  }

  // Mark QFunction as requiring re-assembly
  RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ratel->ctx_jacobian[0]->op, true));

  PetscFunctionReturn(0);
}

/**
  @brief Assemble TS Jacobian for each level of multigrid hierarchy

  @param[in]      ts                 Time-stepper
  @param[in]      time               Current time
  @param[in]      X                  Current non-linear residual
  @param[in]      X_t                Time derivative of current non-linear residual
  @param[in]      v                  Shift
  @param[out]     J                  Fine grid Jacobian operator
  @param[out]     J_pre              Fine grid Jacobian operator for preconditioning
  @param[in,out]  ctx_form_jacobian  Jacobian context, holding Jacobian operators for each level of multigrid hierarchy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormIJacobian(TS ts, PetscReal time, Vec X, Vec X_t, PetscReal v, Mat J, Mat J_pre, void *ctx_form_jacobian) {
  RatelFormJacobianContext ctx   = (RatelFormJacobianContext)ctx_form_jacobian;
  Ratel                    ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Update time context and Dirichlet values
  if (ratel->has_ut_term) {
    RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->ctx_jacobian[0]->op, ratel->op_jacobian_shift_v_label, &v));
  }

  // Apply
  PetscCall(RatelSNESFormJacobian(NULL, X, J, J_pre, ctx_form_jacobian));

  PetscFunctionReturn(0);
}

/**
  @brief Compute the non-linear residual for a TS operator via libCEED

  @param[in]      ts                Time-stepper
  @param[in]      time              Current time
  @param[in]      X                 Current state vector
  @param[in]      X_t               Time derivative of current state vector
  @param[in]      X_tt              Second time derivative of current state vector
  @param[out]     Y                 Function vector
  @param[in,out]  ctx_residual_utt  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormI2Residual(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, Vec Y, void *ctx_residual_utt) {
  RatelOperatorApplyContext ctx   = (RatelOperatorApplyContext)ctx_residual_utt;
  Ratel                     ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Update time context and Dirichlet values
  PetscCall(RatelUpdateTimeAndBoundaryValues(ratel, time));

  // Apply the portion that only depends upon X, X_t
  PetscCall(RatelTSFormIResidual(ts, time, X, X_t, Y, ratel->ctx_residual_ut));

  // libCEED for local action of scaled mass matrix
  PetscCall(RatelApplyAddLocalCeedOp(X_tt, Y, ctx));

  PetscFunctionReturn(0);
}

/**
  @brief Assemble TS Jacobian for each level of multigrid hierarchy

  @param[in]      ts                 Time-stepper
  @param[in]      time               Current time
  @param[in]      X                  Current non-linear residual
  @param[in]      X_t                Time derivative of current non-linear residual
  @param[in]      X_tt               Second time derivative of current non-linear residual
  @param[in]      v                  Shift for X_t
  @param[in]      a                  Shift for X_tt
  @param[out]     J                  Fine grid Jacobian operator
  @param[out]     J_pre              Fine grid Jacobian operator for preconditioning
  @param[in,out]  ctx_form_jacobian  Jacobian context, holding Jacobian operators for each level of multigrid hierarchy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormI2Jacobian(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, PetscReal v, PetscReal a, Mat J, Mat J_pre,
                                     void *ctx_form_jacobian) {
  RatelFormJacobianContext ctx   = (RatelFormJacobianContext)ctx_form_jacobian;
  Ratel                    ratel = ctx->ratel;

  PetscFunctionBeginUser;

  RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->ctx_jacobian[0]->op, ratel->op_jacobian_shift_a_label, &a));
  PetscCall(RatelTSFormIJacobian(ts, time, X, X_t, v, J, J_pre, ctx_form_jacobian));

  PetscFunctionReturn(0);
}

/// @}
