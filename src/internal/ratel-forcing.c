/// @file
/// Ratel forcing function support

#include <ceed.h>
#include <ceed/backend.h>
#include <ratel-dm.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/forcing/constant-force.h>

/**
  @brief Add forcing term to residual operator

  @param[in]   material     RatelMaterial context
  @param[out]  op_residual  Composite residual u term CeedOperator to add RatelMaterial sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupForcingSuboperator(RatelMaterial material, CeedOperator op_residual) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Forcing Sub-operator");

  // Create the QFunction and Operator that computes the forcing term for the residual evaluator
  if (ratel->forcing_choice != RATEL_FORCING_NONE) {
    RatelDebug(ratel, "---- Setting up forcing term");
    RatelModelData       model_data = material->model_data;
    DMLabel              domain_label;
    PetscInt             dim, *domain_values, num_active_fields;
    const PetscInt      *active_field_sizes = model_data->active_field_sizes;
    CeedInt              q_data_size        = model_data->q_data_volume_size;
    CeedVector           q_data             = NULL;
    CeedBasis           *bases_u            = NULL;
    CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL;
    CeedQFunctionContext ctx_forcing = NULL;
    CeedQFunction        qf_forcing  = NULL;
    CeedOperator         sub_op_forcing;

    // DM information
    PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));
    PetscCall(RatelMaterialGetLabelValues(material, NULL, &domain_values));
    PetscCall(DMGetLabel(ratel->dm_hierarchy[0], material->volume_label_name, &domain_label));

    // libCEED objects
    // -- Solution objects from sub-operator
    PetscCall(RatelMaterialGetSolutionData(material, &num_active_fields, &restrictions_u, &bases_u));
    // -- QData
    PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

    // Forcing choice specific options
    switch (ratel->forcing_choice) {
      case RATEL_FORCING_NONE:
        break;  // Excluded above
      case RATEL_FORCING_CONSTANT: {
        PetscInt             max_n        = 3;
        ForcingVectorContext forcing_data = {.direction[0] = 0, .direction[1] = -1, .direction[2] = 0, .time = RATEL_UNSET_INITIAL_TIME};

        // -- Read in forcing vector
        PetscOptionsBegin(ratel->comm, NULL, "", NULL);

        PetscCall(PetscOptionsScalarArray("-forcing_vec", "Direction to apply constant force", NULL, forcing_data.direction, &max_n, NULL));
        RatelDebug(ratel, "---- Constant forcing vector: [%.3f, %.3f, %.3f]", forcing_data.direction[0], forcing_data.direction[1],
                   forcing_data.direction[2]);
        PetscOptionsEnd();  // End of reading in forcing vector

        // -- QFunctionContext
        RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_forcing));
        RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_forcing, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(ForcingVectorContext), &forcing_data));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "forcing x", offsetof(ForcingVectorContext, direction[0]), 1,
                                                                "forcing in x direction"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "forcing y", offsetof(ForcingVectorContext, direction[1]), 1,
                                                                "forcing in y direction"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "forcing z", offsetof(ForcingVectorContext, direction[2]), 1,
                                                                "forcing in z direction"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "time", offsetof(ForcingVectorContext, time), 1, "forcing time"));

        // -- QFunction
        RatelCeedCall(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, ConstantForce, RatelQFunctionRelativePath(ConstantForce_loc), &qf_forcing));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_forcing, "q data", q_data_size, CEED_EVAL_NONE));
        break;
      }
      case RATEL_FORCING_MMS: {
        // -- Check support
        if (!model_data->mms_forcing) SETERRQ(ratel->comm, 1, "MMS forcing function for material '%s' not found", material->name);

        // -- QFunctionContext
        if (material->ctx_physics) {
          RatelCeedCall(ratel, CeedQFunctionContextReference(material->ctx_physics));
          ctx_forcing = material->ctx_physics;
        }

        // -- QFunction
        RatelCeedCall(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, model_data->mms_forcing, model_data->mms_forcing_loc, &qf_forcing));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_forcing, "q data", q_data_size, CEED_EVAL_NONE));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_forcing, "x", dim, CEED_EVAL_INTERP));
        break;
      }
    }
    // -- Common QFunction output
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_forcing, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
    }
    // -- QFunctionContext
    if (ctx_forcing) {
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_forcing, ctx_forcing));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_forcing, false));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_forcing, stdout));
      RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_forcing))
    }

    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ratel->ceed, qf_forcing, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_forcing));
    PetscCall(RatelMaterialSetOperatorName(material, "forcing", sub_op_forcing));
    RatelCeedCall(ratel, CeedOperatorSetField(sub_op_forcing, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force, field %" PetscInt_FMT, i));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_forcing, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    if (ratel->forcing_choice == RATEL_FORCING_MMS) {
      DM                  dm_coord;
      Vec                 X_loc;
      const PetscScalar  *x_array;
      CeedElemRestriction restriction_x;
      CeedBasis           basis_x;
      CeedVector          x_loc;

      // -- Coordinate DM
      PetscCall(DMGetCoordinateDM(ratel->dm_hierarchy[0], &dm_coord));

      // libCEED objects
      // -- Coordinate basis
      RatelDebug(ratel, "---- Coordinate basis");
      PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, domain_label, domain_values[0], 0, 0, &basis_x));
      // -- Coordinate restriction
      RatelDebug(ratel, "---- Coordinate restriction");
      PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, domain_label, domain_values[0], 0, 0, &restriction_x));
      // Element coordinates
      RatelDebug(ratel, "---- Retrieving element coordinates");
      PetscCall(DMGetCoordinatesLocal(ratel->dm_hierarchy[0], &X_loc));
      PetscCall(VecGetArrayRead(X_loc, &x_array));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x, &x_loc, NULL));
      RatelCeedCall(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
      PetscCall(VecRestoreArrayRead(X_loc, &x_array));

      // -- Input field
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_forcing, "x", restriction_x, basis_x, x_loc));

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&x_loc));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_x));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x));
    }

    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_residual, sub_op_forcing));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_forcing, stdout));

    // -- Cleanup
    RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      RatelCeedCall(ratel, CeedBasisDestroy(&bases_u[i]));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    }
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_forcing));
    RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_forcing));
    PetscCall(PetscFree(restrictions_u));
    PetscCall(PetscFree(bases_u));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Material Setup Forcing Sub-operator Success!");
  PetscFunctionReturn(0);
}
