/// @file
/// Boundary condition functions for Ratel

#include <ceed.h>
#include <math.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-petsc-ops.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <ratel/qfunctions/boundaries/clamp-boundary.h>
#include <ratel/qfunctions/boundaries/platen-boundary.h>
#include <ratel/qfunctions/boundaries/traction-boundary.h>
#include <ratel/qfunctions/geometry/surface-geometry.h>
#include <ratel/qfunctions/mass.h>
#include <ratel/qfunctions/surface-centroid.h>
#include <ratel/qfunctions/utils.h>
#include <stdio.h>
#include <string.h>

/// @addtogroup RatelBoundary
/// @{

// -----------------------------------------------------------------------------
// Setup DM
// -----------------------------------------------------------------------------

/**
  @brief Create boundary label

  @param[in,out]  dm    DM to add boundary label
  @param[in]      name  Name for new boundary label

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreateBCLabel(DM dm, const char name[]) {
  DMLabel label;

  PetscFunctionBeginUser;

  PetscCall(DMCreateLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  PetscCall(DMPlexMarkBoundaryFaces(dm, PETSC_DETERMINE, label));
  PetscCall(DMPlexLabelComplete(dm, label));

  PetscFunctionReturn(0);
};

/**
  @brief Add Dirichlet boundaries to DM

  @param[in]   ratel  Ratel context
  @param[out]  dm     DM to update with Dirichlet boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMAddBoundariesDirichlet(Ratel ratel, DM dm) {
  PetscBool   has_label;
  DMLabel     label;
  const char *name = "Face Sets";

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DMPlex Add Boundaries Dirichlet");

  // DM label
  PetscCall(DMHasLabel(dm, name, &has_label));
  if (!has_label) PetscCall(RatelCreateBCLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    RatelDebug(ratel, "---- Dirichlet boundaries for field %" PetscInt_FMT, i);

    // MMS specified BCs
    RatelDebug(ratel, "------ Dirichlet MMS boundaries");
    for (PetscInt j = 0; j < ratel->bc_mms_count[i]; j++) {
      char bcName[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(bcName, sizeof bcName, "mms_field_%" PetscInt_FMT "_face_%" PetscInt_FMT, i, ratel->bc_mms_faces[i][j]));
      PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bcName, label, 1, &ratel->bc_mms_faces[i][j], i, 0, NULL,
                              (void (*)(void))ratel->materials[0]->model_data->BoundaryMMS[i], NULL, NULL, NULL));
    }

    // User specified BCs
    RatelDebug(ratel, "------ Dirichlet clamp boundaries");
    for (PetscInt j = 0; j < ratel->bc_clamp_count[i]; j++) {
      char bcName[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(bcName, sizeof bcName, "clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT, i, ratel->bc_clamp_faces[i][j]));
      PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bcName, label, 1, &ratel->bc_clamp_faces[i][j], i, 0, NULL, NULL, NULL, NULL, NULL));
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DMPlex Add Boundaries Dirichlet Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Add Slip boundaries to DM

  @param[in]   ratel  Ratel context
  @param[out]  dm     DM to update with Dirichlet boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMAddBoundariesSlip(Ratel ratel, DM dm) {
  DMLabel     label;
  PetscBool   has_label;
  const char *name = "Face Sets";

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DMPlex Add Boundaries Slip");

  // Set slip BCs in each direction
  PetscCall(DMHasLabel(dm, name, &has_label));
  if (!has_label) PetscCall(RatelCreateBCLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    RatelDebug(ratel, "---- Dirichlet boundaries for field %" PetscInt_FMT, i);
    for (PetscInt j = 0; j < ratel->bc_slip_count[i]; j++) {
      char bcName[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(bcName, sizeof bcName, "slip_field_%" PetscInt_FMT "_face_%" PetscInt_FMT, i, ratel->bc_slip_faces[i][j]));
      PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bcName, label, 1, &ratel->bc_slip_faces[i][j], i, ratel->bc_slip_components_count[i][j],
                              ratel->bc_slip_components[i][j], (void (*)(void))NULL, NULL, NULL, NULL));
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DMPlex Add Boundaries Slip Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Add Dirichlet clamp boundaries to Dirichlet operator

  @param[in]   ratel         Ratel context
  @param[in]   dm            DM to use for Dirichlet boundaries
  @param[out]  op_dirichlet  Composite Dirichlet operator to add sub-operators to

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesDirichletClamp(Ratel ratel, DM dm, CeedOperator op_dirichlet) {
  PetscBool            has_dirichlet = PETSC_FALSE;
  Ceed                 ceed          = ratel->ceed;
  CeedInt              Q             = ratel->highest_fine_order + 1 + ratel->q_extra;
  PetscInt             dim;
  CeedInt              num_comp_x, num_comp_u = 3, num_elem, elem_size, height = 1;
  Vec                  coords;
  const PetscScalar   *coord_array;
  CeedVector           x_coord = NULL, multiplicity, x_stored, scale_stored;
  CeedBasis            basis_x_face;
  CeedQFunctionContext ctx_clamp;
  CeedQFunction        qf_setup, qf_clamp;
  CeedOperator         op_setup, sub_op_clamp;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Ceed Add Boundaries Dirichlet Clamp");

  // Get number of solution components and dimension
  {
    const PetscInt *field_sizes;

    PetscCall(DMGetDimension(dm, &dim));
    num_comp_x = dim;
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  // Coordinate Basis
  RatelDebug(ratel, "---- Setting up libCEED coordinate basis");
  DM dm_coord;
  PetscCall(DMGetCoordinateDM(dm, &dm_coord));
  PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, 0, 0, height, 0, &basis_x_face));

  // Setup QFunction
  RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupClampBCs, RatelQFunctionRelativePath(SetupClampBCs_loc), &qf_setup));
  RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "x", num_comp_x, CEED_EVAL_INTERP));
  RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "multiplicity", num_comp_u, CEED_EVAL_NONE));
  RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "x stored", num_comp_x, CEED_EVAL_NONE));
  RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "scale", 1, CEED_EVAL_NONE));

  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    CeedBasis basis_u_face = NULL, basis_x_to_u_face = NULL;

    if (ratel->bc_clamp_count[i] > 0) {
      RatelDebug(ratel, "---- Dirichlet boundaries for field %" PetscInt_FMT, i);
      has_dirichlet = PETSC_TRUE;

      DMLabel domain_label;
      PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
      PetscCall(DMGetDimension(dm, &dim));

      // Basis
      RatelDebug(ratel, "------ Setting up libCEED bases");
      PetscCall(RatelBasisCreateFromPlex(ratel, dm, 0, 0, height, i, &basis_u_face));
      PetscCall(CeedBasisCreateProjection(basis_x_face, basis_u_face, &basis_x_to_u_face));

      // Compute contribution on each boundary face
      for (CeedInt j = 0; j < ratel->bc_clamp_count[i]; j++) {
        CeedElemRestriction restriction_x_face, restriction_u_face, restriction_x_stored, restriction_scale;

        RatelDebug(ratel, "------ Setting up clamp boundary: %" CeedInt_FMT, j);

        // -- Set up context data
        BCClampData clamp_data = {
            .translation[0]         = 0.0,
            .translation[1]         = 0.0,
            .translation[2]         = 0.0,
            .rotation_axis[0]       = 0.0,
            .rotation_axis[1]       = 0.0,
            .rotation_axis[2]       = 0.0,
            .rotation_polynomial[0] = 0.0,
            .rotation_polynomial[1] = 0.0,
            .time                   = RATEL_UNSET_INITIAL_TIME,
            .final_time             = 1.0,
        };

        // ---- Read CL options for clamp conditions
        PetscOptionsBegin(ratel->comm, NULL, "", NULL);

        // ------ Translation vector
        PetscBool is_option_found = PETSC_FALSE;
        char      option_name[PETSC_MAX_PATH_LEN];

        PetscInt max_n = 3;
        PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_translate", i,
                                ratel->bc_clamp_faces[i][j]));
        PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, clamp_data.translation, &max_n, &is_option_found));
        if (i == 0 && !is_option_found) {
          PetscInt max_n = 3;
          PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_translate", ratel->bc_clamp_faces[i][j]));
          PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, clamp_data.translation, &max_n, NULL));
        }

        // ------ Rotation vector
        is_option_found         = PETSC_FALSE;
        max_n                   = 5;
        PetscScalar rotation[5] = {0, 0, 0, 0, 0};
        PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_rotate", i,
                                ratel->bc_clamp_faces[i][j]));
        PetscCall(
            PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &max_n, &is_option_found));
        if (i == 0 && !is_option_found) {
          PetscInt max_n = 5;
          PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_rotate", ratel->bc_clamp_faces[i][j]));
          PetscCall(PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &max_n, NULL));
        }

        // ------ Normalize
        CeedScalar norm_rotation = RatelNorm3((CeedScalar *)rotation);
        if (fabs(norm_rotation) < CEED_EPSILON) norm_rotation = 1;
        for (PetscInt k = 0; k < 3; k++) clamp_data.rotation_axis[k] = rotation[k] / norm_rotation;
        clamp_data.rotation_polynomial[0] = rotation[3];
        clamp_data.rotation_polynomial[1] = rotation[4];
        RatelDebug(ratel, "----   Clamp Boundary %" PetscInt_FMT ": [tx: %.3f, ty: %.3f, tz: %.3f, rx: %.3f, ry: %.3f, rz: %.3f, rt: %.3f]",
                   ratel->bc_clamp_faces[i][j], clamp_data.translation[0], clamp_data.translation[1], clamp_data.translation[2],
                   clamp_data.rotation_axis[0], clamp_data.rotation_axis[1], clamp_data.rotation_axis[2], clamp_data.rotation_polynomial[0]);

        clamp_data.final_time = 1.0;
        PetscCall(PetscOptionsScalar("-ts_max_time", "final time", NULL, clamp_data.final_time, &clamp_data.final_time, NULL));

        PetscOptionsEnd();  // End of setting clamp conditions

        // -- Skip if zero BC
        CeedScalar norm_translation = RatelNorm3(clamp_data.translation);
        CeedScalar norm_rotation    = RatelNorm3(clamp_data.rotation_axis);
        if (fabs(norm_translation) < CEED_EPSILON && fabs(norm_rotation) < CEED_EPSILON) {
          RatelDebug(ratel, "---- Zero Dirichlet clamp boundary, no operator needed");
          continue;
        }
        ratel->bc_clamp_nonzero_count++;

        // -- Restrictions
        RatelDebug(ratel, "---- Setting up libCEED restrictions");
        PetscCall(RatelGetRestrictionForDomain(ratel, dm, domain_label, ratel->bc_clamp_faces[i][j], height, i, Q, 0, &restriction_u_face,
                                               &restriction_x_face, NULL));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_u_face, &multiplicity, NULL));
        RatelCeedCall(ratel, CeedElemRestrictionGetMultiplicity(restriction_u_face, multiplicity));
        RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(restriction_u_face, &num_elem));
        RatelCeedCall(ratel, CeedElemRestrictionGetElementSize(restriction_u_face, &elem_size));
        RatelDebug(ratel,
                   "---- Restriction, stored coordinates: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT
                   " fields for %" CeedInt_FMT " DoF",
                   num_elem, elem_size, num_comp_x, num_elem * elem_size * num_comp_x);
        RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, elem_size, num_comp_x, num_elem * elem_size * num_comp_x,
                                                              CEED_STRIDES_BACKEND, &restriction_x_stored));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_stored, &x_stored, NULL));
        RatelCeedCall(ratel,
                      CeedElemRestrictionCreateStrided(ceed, num_elem, elem_size, 1, num_elem * elem_size, CEED_STRIDES_BACKEND, &restriction_scale));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_scale, &scale_stored, NULL));

        // Coordinates vector
        if (!x_coord) {
          RatelDebug(ratel, "---- Retrieving element coordinates");
          PetscCall(DMGetCoordinatesLocal(dm, &coords));
          PetscCall(VecGetArrayRead(coords, &coord_array));
          RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &x_coord, NULL));
          RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_USE_POINTER, (CeedScalar *)coord_array));
        }

        // -- Setup Operator
        RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
        RatelCeedCall(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
        RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "x", restriction_x_face, basis_x_to_u_face, CEED_VECTOR_ACTIVE));
        RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "multiplicity", restriction_u_face, CEED_BASIS_COLLOCATED, multiplicity));
        RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "x stored", restriction_x_stored, CEED_BASIS_COLLOCATED, x_stored));
        RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "scale", restriction_scale, CEED_BASIS_COLLOCATED, scale_stored));

        // -- Compute geometric factors
        RatelDebug(ratel, "---- Projecting coordinates and computing multiplicity scaling factor");
        if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup, stdout));
        RatelCeedCall(ratel, CeedOperatorApply(op_setup, x_coord, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

        // -- Apply QFunction
        RatelDebug(ratel, "---- Setting up Dirichlet clamp operator");
        RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, ApplyClampBCs, RatelQFunctionRelativePath(ApplyClampBCs_loc), &qf_clamp));

        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_clamp, "x", num_comp_x, CEED_EVAL_NONE));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_clamp, "scale", 1, CEED_EVAL_NONE));
        RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_clamp, "u", num_comp_u, CEED_EVAL_NONE));
        RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_clamp));
        RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_clamp, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(BCClampData), &clamp_data));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation x", offsetof(BCClampData, translation[0]), 1,
                                                                "translation in x direction"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation y", offsetof(BCClampData, translation[1]), 1,
                                                                "translation in y direction"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation z", offsetof(BCClampData, translation[2]), 1,
                                                                "translation in z direction"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation x", offsetof(BCClampData, rotation_axis[0]), 1,
                                                                "x component for axis of rotation"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation y", offsetof(BCClampData, rotation_axis[1]), 1,
                                                                "y component for axis of rotation"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation z", offsetof(BCClampData, rotation_axis[2]), 1,
                                                                "z component for axis of rotation"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation c_0", offsetof(BCClampData, rotation_polynomial[0]), 1,
                                                                "coefficient 0 for rotation polynomial"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation c_1", offsetof(BCClampData, rotation_polynomial[1]), 1,
                                                                "coefficient 1 for rotation polynomial"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "time", offsetof(BCClampData, time), 1, "current solver time"));
        RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "final time", offsetof(BCClampData, final_time), 1, "final solver time"));
        RatelCeedCall(ratel, CeedQFunctionSetContext(qf_clamp, ctx_clamp));
        RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_clamp, false));
        if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_clamp, stdout));

        // -- Apply operator
        RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_clamp, NULL, NULL, &sub_op_clamp));
        {
          char operator_name[25];

          PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "clamp - face %" PetscInt_FMT " field %" PetscInt_FMT,
                                  ratel->bc_clamp_faces[i][j], i));
          RatelCeedCall(ratel, CeedOperatorSetName(sub_op_clamp, operator_name));
        }
        RatelCeedCall(ratel, CeedOperatorSetField(sub_op_clamp, "x", restriction_x_stored, CEED_BASIS_COLLOCATED, x_stored));
        RatelCeedCall(ratel, CeedOperatorSetField(sub_op_clamp, "scale", restriction_scale, CEED_BASIS_COLLOCATED, scale_stored));
        RatelCeedCall(ratel, CeedOperatorSetField(sub_op_clamp, "u", restriction_u_face, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));
        RatelCeedCall(ratel, CeedOperatorSetNumQuadraturePoints(sub_op_clamp, elem_size));
        // -- Add to composite operator
        RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_dirichlet, sub_op_clamp));
        if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_clamp, stdout));

        // -- Cleanup
        RatelCeedCall(ratel, CeedVectorDestroy(&x_stored));
        RatelCeedCall(ratel, CeedVectorDestroy(&scale_stored));
        RatelCeedCall(ratel, CeedVectorDestroy(&multiplicity));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_stored));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_scale));
        RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_clamp));
        RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_clamp));
        RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup));
        RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_clamp));
      }
    }
    // -- Cleanup
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_to_u_face));
  }
  // Cleanup
  if (x_coord) PetscCall(VecRestoreArrayRead(coords, &coord_array));
  RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
  RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup));

  if (has_dirichlet) {
    // Override DMPlexInsertBoundaryValues
    RatelDebug(ratel, "---- Overriding DMPlexInsertBoundaryValues_C");
    PetscCall(PetscObjectComposeFunction((PetscObject)dm, "DMPlexInsertBoundaryValues_C", RatelDMPlexInsertBoundaryValues));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Ceed Add Boundaries Dirichlet Clamp Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Add Neumann boundaries to residual operator

  @param[in]   ratel        Ratel context
  @param[in]   dm           DM to use for Neumann boundaries
  @param[out]  op_residual  Composite residual u term CeedOperator to add Neumann sub-operators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesNeumann(Ratel ratel, DM dm, CeedOperator op_residual) {
  Ceed                 ceed = ratel->ceed;
  CeedInt              Q    = ratel->highest_fine_order + 1 + ratel->q_extra;
  PetscInt             dim;
  CeedInt              num_comp_x, num_comp_u = 3, q_data_size = 4, num_elem, num_qpts;
  Vec                  coords;
  const PetscScalar   *coord_array;
  CeedVector           x_coord = NULL, q_data;
  CeedBasis            basis_x_face, basis_u_face;
  CeedElemRestriction  restriction_x_face, restriction_u_face, restriction_q_data;
  CeedQFunctionContext ctx_traction;
  CeedQFunction        qf_setup, qf_traction;
  CeedOperator         op_setup, sub_op_traction;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Ceed Add Boundaries Neumann");

  // Get number of solution components and dimension
  {
    const PetscInt *field_sizes;

    PetscCall(DMGetDimension(dm, &dim));
    num_comp_x = dim;
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  if (ratel->bc_traction_count > 0) {
    DMLabel domain_label;
    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
    PetscCall(DMGetDimension(dm, &dim));

    // Basis
    RatelDebug(ratel, "---- Setting up libCEED bases");
    CeedInt height = 1;
    DM      dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, 0, 0, height, 0, &basis_x_face));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm, 0, 0, height, 0, &basis_u_face));

    // Setup QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSurfaceGeometry, RatelQFunctionRelativePath(SetupSurfaceGeometry_loc), &qf_setup));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "dx", num_comp_x * (num_comp_x - height), CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "q data", q_data_size, CEED_EVAL_NONE));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->bc_traction_count; i++) {
      RatelDebug(ratel, "---- Setting up traction boundary: %" CeedInt_FMT, i);

      // -- Set up context data
      BCTractionData traction_data = {.direction[0] = 0, .direction[1] = 0, .direction[2] = 0, .time = RATEL_UNSET_INITIAL_TIME, .final_time = 1.0};

      // ---- Read CL options for traction conditions
      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      // ------ Translation vector
      char option_name[25];

      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_traction_%" PetscInt_FMT, ratel->bc_traction_faces[i]));
      PetscInt  max_n = 3;
      PetscBool set   = false;
      PetscCall(PetscOptionsScalarArray(option_name, "Traction vector for constrained face", NULL, traction_data.direction, &max_n, &set));

      if (!set) SETERRQ(ratel->comm, PETSC_ERR_SUP, "Traction vector must be set for all traction boundary conditions.");
      RatelDebug(ratel, "----   Traction Boundary %" PetscInt_FMT ": [tx: %.3f, ty: %.3f, tz: %.3f]", ratel->bc_traction_faces[i],
                 traction_data.direction[0], traction_data.direction[1], traction_data.direction[2]);

      PetscCall(PetscOptionsScalar("-ts_max_time", "final time", NULL, traction_data.final_time, &traction_data.final_time, NULL));

      PetscOptionsEnd();  // End of setting traction conditions

      // -- Restrictions
      RatelDebug(ratel, "---- Setting up libCEED restrictions");
      PetscCall(RatelGetRestrictionForDomain(ratel, dm, domain_label, ratel->bc_traction_faces[i], height, 0, Q, 0, &restriction_u_face,
                                             &restriction_x_face, NULL));
      RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_u_face, &num_qpts));
      RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(restriction_x_face, &num_elem));
      RatelDebug(ratel,
                 "---- Restriction, geometric data: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT
                 " DoF",
                 num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
      RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                            CEED_STRIDES_BACKEND, &restriction_q_data));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_q_data, &q_data, NULL));

      // Coordinates vector
      if (!x_coord) {
        RatelDebug(ratel, "---- Retrieving element coordinates");
        PetscCall(DMGetCoordinatesLocal(dm, &coords));
        PetscCall(VecGetArrayRead(coords, &coord_array));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &x_coord, NULL));
        RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_USE_POINTER, (CeedScalar *)coord_array));
      }

      // -- Setup Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
      RatelCeedCall(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "dx", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));

      // -- Compute geometric factors
      RatelDebug(ratel, "---- Computing geometric factors");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_setup, x_coord, q_data, CEED_REQUEST_IMMEDIATE));

      // -- Apply QFunction
      RatelDebug(ratel, "---- Setting up Neumann traction operator");
      RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, ApplyTractionBCs, RatelQFunctionRelativePath(ApplyTractionBCs_loc), &qf_traction));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_traction, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_traction, "v", num_comp_u, CEED_EVAL_INTERP));
      RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_traction));
      RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_traction, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(BCTractionData), &traction_data));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction x", offsetof(BCTractionData, direction[0]), 1,
                                                              "traction in x direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction y", offsetof(BCTractionData, direction[1]), 1,
                                                              "traction in y direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction z", offsetof(BCTractionData, direction[2]), 1,
                                                              "traction in z direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "time", offsetof(BCTractionData, time), 1, "current solver time"));
      RatelCeedCall(ratel,
                    CeedQFunctionContextRegisterDouble(ctx_traction, "final time", offsetof(BCTractionData, final_time), 1, "final solver time"));
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_traction, ctx_traction));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_traction, false));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_traction, stdout));

      // -- Apply operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_traction, NULL, NULL, &sub_op_traction));
      {
        char operator_name[25];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "traction - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCeedCall(ratel, CeedOperatorSetName(sub_op_traction, operator_name));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_traction, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(sub_op_traction, "v", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_residual, sub_op_traction));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_traction, stdout));

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_traction));
      RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_traction));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup));
      RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_traction));
    }
    // Cleanup
    if (x_coord) PetscCall(VecRestoreArrayRead(coords, &coord_array));
    RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Ceed Add Boundaries Neumann Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup surface force face centroid computation

  @param[in]  ratel  Ratel context
  @param[in]  dm     DM with surface force faces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupSurfaceForceCentroids(Ratel ratel, DM dm) {
  Ceed                 ceed = ratel->ceed;
  CeedInt              Q    = ratel->highest_fine_order + 1 + ratel->q_extra;
  PetscInt             dim  = 3;
  CeedInt              num_comp_x, q_data_size = 4, num_elem, num_qpts;
  Vec                  coords;
  const PetscScalar   *coord_array;
  PetscScalar          centroid_local[3], surface_area_local;
  CeedVector           x_coord = NULL, q_data, centroid, one;
  CeedBasis            basis_x_face;
  CeedElemRestriction  restriction_x_face, restriction_q_data;
  CeedQFunctionContext ctx_mass;
  CeedQFunction        qf_setup, qf_mass, qf_centroid;
  CeedOperator         op_setup, op_mass, op_centroid;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Setup Surface Force Centroids");

  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;

  if (ratel->surface_force_face_count > 0) {
    DMLabel domain_label;
    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));

    // Basis
    RatelDebug(ratel, "---- Setting up libCEED bases");
    CeedInt height = 1;
    DM      dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, 0, 0, height, 0, &basis_x_face));

    // Setup QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSurfaceGeometry, RatelQFunctionRelativePath(SetupSurfaceGeometry_loc), &qf_setup));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "dx", num_comp_x * (num_comp_x - height), CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "q data", q_data_size, CEED_EVAL_NONE));

    // Surface area QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass, "u", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_mass, "v", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_mass));
    RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_mass, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(CeedScalar), &dim));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_mass, ctx_mass));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_mass, false));

    // Centroid QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, ComputeCentroid, RatelQFunctionRelativePath(ComputeCentroid_loc), &qf_centroid));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_centroid, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_centroid, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_centroid, "centroid", num_comp_x, CEED_EVAL_INTERP));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->surface_force_face_count; i++) {
      RatelDebug(ratel, "---- Setting up centroid on surface face %" CeedInt_FMT, i);

      // -- Restrictions
      RatelDebug(ratel, "---- Setting up libCEED restrictions");
      PetscCall(
          RatelGetRestrictionForDomain(ratel, dm, domain_label, ratel->surface_force_dm_faces[i], height, 0, Q, 0, NULL, &restriction_x_face, NULL));
      RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_x_face, &num_qpts));
      RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(restriction_x_face, &num_elem));
      RatelDebug(ratel,
                 "---- Restriction, geometric data: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT
                 " DoF",
                 num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
      RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                            CEED_STRIDES_BACKEND, &restriction_q_data));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_q_data, &q_data, NULL));

      // Coordinates vector
      if (!x_coord) {
        RatelDebug(ratel, "---- Retrieving element coordinates");
        PetscCall(DMGetCoordinatesLocal(dm, &coords));
        PetscCall(VecGetArrayRead(coords, &coord_array));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &x_coord, NULL));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &centroid, NULL));
        RatelCeedCall(ratel, CeedVectorSetValue(centroid, 0.0));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &one, NULL));
        RatelCeedCall(ratel, CeedVectorSetValue(one, 1.0));
        RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_USE_POINTER, (CeedScalar *)coord_array));
      }

      // -- Setup Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
      RatelCeedCall(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "dx", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));

      // -- Compute geometric factors
      RatelDebug(ratel, "---- Computing geometric factors");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_setup, x_coord, q_data, CEED_REQUEST_IMMEDIATE));

      // -- Surface area operator
      RatelDebug(ratel, "---- Setting surface area operator");
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_mass, NULL, NULL, &op_mass));
      {
        char operator_name[25];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "surface area - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCeedCall(ratel, CeedOperatorSetName(op_mass, operator_name));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "u", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "v", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));

      // -- Compute surface area
      RatelDebug(ratel, "---- Computing surface area");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_mass, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_mass, one, centroid, CEED_REQUEST_IMMEDIATE));
      {
        const CeedScalar *array;
        CeedSize          length;

        RatelCeedCall(ratel, CeedVectorGetLength(centroid, &length));
        RatelCeedCall(ratel, CeedVectorGetArrayRead(centroid, CEED_MEM_HOST, &array));
        surface_area_local = 0.0;
        for (CeedInt j = 0; j < length / dim; j++) {
          for (CeedInt d = 0; d < dim; d++) surface_area_local += array[j * dim + d];
        }
        RatelCeedCall(ratel, CeedVectorRestoreArrayRead(centroid, &array));
      }
      PetscCall(MPIU_Allreduce(&surface_area_local, &ratel->surface_force_face_area[i], 1, MPIU_REAL, MPI_SUM, ratel->comm));
      RatelDebug(ratel, "------   Surface area: %0.10e", ratel->surface_force_face_area[i]);

      // -- Centroid QFunction
      RatelDebug(ratel, "---- Setting up centroid operator");
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_centroid, NULL, NULL, &op_centroid));
      {
        char operator_name[25];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "centroid - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCeedCall(ratel, CeedOperatorSetName(op_centroid, operator_name));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(op_centroid, "x", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_centroid, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(op_centroid, "centroid", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));

      // -- Compute initial centroid
      RatelDebug(ratel, "---- Computing initial centroid");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_centroid, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_centroid, x_coord, centroid, CEED_REQUEST_IMMEDIATE));
      {
        const CeedScalar *array;
        CeedSize          length;

        RatelCeedCall(ratel, CeedVectorGetLength(centroid, &length));
        RatelCeedCall(ratel, CeedVectorGetArrayRead(centroid, CEED_MEM_HOST, &array));
        for (CeedInt d = 0; d < dim; d++) centroid_local[d] = 0.0;
        for (CeedInt j = 0; j < length / dim; j++) {
          for (CeedInt d = 0; d < dim; d++) centroid_local[d] += array[j * dim + d];
        }
        RatelCeedCall(ratel, CeedVectorRestoreArrayRead(centroid, &array));
      }
      PetscCall(MPIU_Allreduce(centroid_local, &ratel->surface_force_face_centroid[i], dim, MPIU_REAL, MPI_SUM, ratel->comm));
      for (CeedInt d = 0; d < dim; d++) ratel->surface_force_face_centroid[i][d] /= ratel->surface_force_face_area[i];
      RatelDebug(ratel, "------   Centroid: [%0.10e, %0.10e, %0.10e]", ratel->surface_force_face_centroid[i][0],
                 ratel->surface_force_face_centroid[i][1], ratel->surface_force_face_centroid[i][2]);

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_centroid));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_mass));
    }
    // Cleanup
    if (x_coord) PetscCall(VecRestoreArrayRead(coords, &coord_array));
    RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
    RatelCeedCall(ratel, CeedVectorDestroy(&centroid));
    RatelCeedCall(ratel, CeedVectorDestroy(&one));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_mass));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_centroid));
    RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_mass));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Setup Surface Force Centroid Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Add platen contact boundaries to residual operator

  @param[in]   material     RatelMaterial context
  @param[out]  op_residual  Composite residual u term CeedOperator to add RatelMaterial sub-operator
  @param[out]  op_jacobian  Composite Jacobian CeedOperator to add RatelMaterial sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCeedAddBoundariesPlaten(RatelMaterial material, CeedOperator op_residual, CeedOperator op_jacobian) {
  // ---- Get material data
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  DM                   dm         = ratel->dm_hierarchy[0];
  CeedInt              Q          = ratel->highest_fine_order + 1 + ratel->q_extra;
  CeedInt              num_comp_x, num_comp_u;
  Vec                  coords;
  const PetscScalar   *coord_array;
  PetscInt             dim;
  Ceed                 ceed    = ratel->ceed;
  CeedVector           x_coord = NULL;
  CeedBasis            basis_x_face;
  CeedQFunctionContext ctx_platen = NULL;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Ceed Add Boundaries Platen");
  if (ratel->bc_platen_count > 0) {
    if (!model_data->platen_residual_u_loc || !model_data->platen_jacobian_loc) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_ARG_INCOMP, "--- Platen BC unsupported for %s", material->name);
      // LCOV_EXCL_STOP
    }
    num_comp_u = model_data->active_field_sizes[0];
    // Setup Bases
    RatelDebug(ratel, "---- Setting up libCEED bases");
    PetscCall(DMGetDimension(dm, &dim));
    num_comp_x     = dim;
    CeedInt height = 1;
    DM      dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, 0, 0, height, 0, &basis_x_face));

    for (PetscInt i = 0; i < ratel->bc_platen_count; i++) {
      PetscInt dm_face = ratel->bc_platen_faces[i];
      RatelDebug(ratel, "---- Setting up platen BC on face %" PetscInt_FMT, dm_face);

      CeedScalar normal[3] = {0., 0., -1.}, center[3] = {0., 0., 0.}, distance = 0., gamma = 1., final_time = 1.;
      // ---- Read CL options for platen conditions
      {
        char cl_prefix[20];
        PetscCall(PetscSNPrintf(cl_prefix, sizeof cl_prefix, "bc_platen_%" PetscInt_FMT "_", dm_face));
        PetscOptionsBegin(ratel->comm, cl_prefix, "Platen BC Parameters", NULL);

        PetscInt max_n = 3;
        // ------ Normal vector
        PetscCall(PetscOptionsScalarArray("-normal", "Exterior normal to the half-space platen", NULL, normal, &max_n, NULL));
        // ------ Center point
        PetscCall(PetscOptionsScalarArray("-center", "Point on the surface of the half-plane platen", NULL, center, &max_n, NULL));
        // ------ Total distance
        PetscCall(PetscOptionsScalar("-distance", "Total distance to move platen along normal vector", NULL, distance, &distance, NULL));
        // ------ Nitsche parameter
        PetscCall(PetscOptionsScalar("-gamma", "Nitsche's method penalty parameter", NULL, gamma, &gamma, NULL));

        // ------ Normalize
        CeedScalar norm_normal = RatelNorm3((CeedScalar *)normal);
        if (fabs(norm_normal) < CEED_EPSILON) norm_normal = 1;
        for (PetscInt j = 0; j < 3; j++) normal[j] /= norm_normal;

        RatelDebug(ratel,
                   "----   Platen Boundary %" PetscInt_FMT ": [nx: %.3f, ny: %.3f, nz: %.3f, cx: %.3f, cy: %.3f, cz: %.3f, dist: %.3f, gamma: %3f]",
                   ratel->bc_platen_faces[i], normal[0], normal[1], normal[2], center[0], center[1], center[2], distance, gamma);

        PetscOptionsEnd();                               // End of setting platen conditions
        PetscOptionsBegin(ratel->comm, NULL, "", NULL);  // get final time
        // ------ Final time
        PetscCall(PetscOptionsScalar("-ts_max_time", "final time", NULL, final_time, &final_time, NULL));
        PetscOptionsEnd();
      }

      // Domain label for Face Sets
      DMLabel     face_domain_label      = NULL;
      const char *face_domain_label_name = NULL;

      PetscCall(RatelMaterialGetSurfaceGradientLabelName(material, dm_face, &face_domain_label_name));
      if (!face_domain_label_name) continue;
      PetscCall(DMGetLabel(dm, face_domain_label_name, &face_domain_label));
      if (!face_domain_label) SETERRQ(ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "No DM label corresponding to name %s found", face_domain_label_name);

      // Create context
      PetscCall((*material->RatelMaterialPlatenContextCreate)(material, normal, center, distance, gamma, final_time, &ctx_platen));

      // Loop over all domain values
      IS              is_face_domain_values;
      PetscInt        num_face_domain_values;
      const PetscInt *face_domain_values;

      PetscCall(DMLabelGetNonEmptyStratumValuesIS(face_domain_label, &is_face_domain_values));
      PetscCall(ISGetSize(is_face_domain_values, &num_face_domain_values));
      PetscCall(ISGetIndices(is_face_domain_values, &face_domain_values));

      for (PetscInt orientation = 0; orientation < num_face_domain_values; orientation++) {
        CeedInt             num_elem, num_qpts, q_data_size = model_data->q_data_surface_grad_size, num_fields = model_data->num_quadrature_fields;
        CeedVector          stored_fields[RATEL_MAX_FIELDS + 1];
        CeedBasis           basis_x_cell_to_face, basis_u_cell_to_face;
        CeedElemRestriction restriction_x_face, restriction_x_cell, restriction_u_cell, restriction_stored_fields[RATEL_MAX_FIELDS + 1];
        CeedQFunction       qf_platen;
        CeedOperator        op_platen;
        PetscInt            face_domain_value  = face_domain_values[orientation];
        CeedVector          q_data             = NULL;
        CeedElemRestriction restriction_q_data = NULL;
        RatelDebug(ratel, "------ Face orientation %" PetscInt_FMT, face_domain_value);

        // Check for face on process
        {
          PetscInt first_point;
          PetscInt ids[1] = {face_domain_value};

          PetscCall(DMGetFirstLabeledPoint(dm_coord, dm_coord, face_domain_label, 1, ids, 0, &first_point, NULL));
          if (first_point == -1) continue;
        }
        // Diagnostic quantities
        RatelDebug(ratel, "------ Setting up platen BC operator");

        // -- Bases
        PetscCall(RatelMaterialGetSurfaceGradientQData(material, dm_face, face_domain_value, &restriction_q_data, &q_data));
        PetscCall(RatelOrientedBasisCreateCellToFaceFromPlex(ratel, dm_coord, face_domain_label, face_domain_value, face_domain_value, 0,
                                                             &basis_x_cell_to_face));
        PetscCall(
            RatelOrientedBasisCreateCellToFaceFromPlex(ratel, dm, face_domain_label, face_domain_value, face_domain_value, 0, &basis_u_cell_to_face));

        // -- ElemRestriction
        PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, face_domain_label, face_domain_value, 0, 0, &restriction_x_cell));
        PetscCall(RatelRestrictionCreateFromPlex(ratel, dm, face_domain_label, face_domain_value, 0, 0, &restriction_u_cell));
        RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(restriction_u_cell, &num_elem));
        PetscCall(RatelGetRestrictionForDomain(ratel, dm, face_domain_label, ratel->bc_platen_faces[i], 1, 0, Q, 0, NULL, &restriction_x_face, NULL));
        RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_x_face, &num_qpts));
        // Coordinates vector
        if (!x_coord) {
          RatelDebug(ratel, "---- Retrieving element coordinates");
          PetscCall(DMGetCoordinatesLocal(dm, &coords));
          PetscCall(VecGetArrayRead(coords, &coord_array));
          RatelCeedCall(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &x_coord, NULL));
          RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)coord_array));
          PetscCall(VecRestoreArrayRead(coords, &coord_array));
        }

        // -- Stored field vectors
        for (CeedInt i = 0; i < num_fields; i++) {
          RatelCeedCall(ratel, CeedVectorCreate(ceed, num_elem * num_qpts * model_data->quadrature_field_sizes[i], &stored_fields[i]));
        }
        RatelCeedCall(ratel, CeedVectorCreate(ceed, num_elem * num_qpts, &stored_fields[num_fields]));

        // ---- Stored field restrictions
        for (CeedInt i = 0; i < num_fields; i++) {
          RatelDebug(ratel,
                     "------ Restriction: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT " DoF",
                     num_elem, num_qpts, model_data->quadrature_field_sizes[i], num_elem * num_qpts * model_data->quadrature_field_sizes[i]);
          RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, model_data->quadrature_field_sizes[i],
                                                                num_elem * num_qpts * model_data->quadrature_field_sizes[i], CEED_STRIDES_BACKEND,
                                                                &restriction_stored_fields[i]));
        }
        RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, 1, num_elem * num_qpts, CEED_STRIDES_BACKEND,
                                                              &restriction_stored_fields[num_fields]));

        // -- QFunction
        RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->platen_residual_u, model_data->platen_residual_u_loc, &qf_platen));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_platen, "q data", q_data_size, CEED_EVAL_NONE));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_platen, "du", num_comp_u * dim, CEED_EVAL_GRAD));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_platen, "u", num_comp_u, CEED_EVAL_INTERP));
        RatelCeedCall(ratel, CeedQFunctionAddInput(qf_platen, "x", num_comp_x, CEED_EVAL_INTERP));
        RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_platen, "v", num_comp_u, CEED_EVAL_INTERP));
        for (CeedInt i = 0; i < num_fields; i++) {
          RatelCeedCall(
              ratel, CeedQFunctionAddOutput(qf_platen, model_data->quadrature_field_names[i], model_data->quadrature_field_sizes[i], CEED_EVAL_NONE));
        }
        // P_1_gamma has to be at the end of the outputs in order for P_* functions to work
        RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_platen, "P_1_gamma", 1, CEED_EVAL_NONE));
        // -- Add Context
        RatelCeedCall(ratel, CeedQFunctionSetContext(qf_platen, ctx_platen));
        RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_platen, false));
        // -- Operator
        RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_platen, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_platen));
        {
          char operator_name[30];

          PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "platen BC %" PetscInt_FMT ".%" PetscInt_FMT, dm_face, face_domain_value));
          PetscCall(RatelMaterialSetOperatorName(material, operator_name, op_platen));
        }
        RatelCeedCall(ratel, CeedOperatorSetField(op_platen, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
        RatelCeedCall(ratel, CeedOperatorSetField(op_platen, "du", restriction_u_cell, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));
        RatelCeedCall(ratel, CeedOperatorSetField(op_platen, "u", restriction_u_cell, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));
        RatelCeedCall(ratel, CeedOperatorSetField(op_platen, "x", restriction_x_cell, basis_x_cell_to_face, x_coord));
        for (CeedInt i = 0; i < num_fields; i++) {
          RatelCeedCall(ratel, CeedOperatorSetField(op_platen, model_data->quadrature_field_names[i], restriction_stored_fields[i],
                                                    CEED_BASIS_COLLOCATED, stored_fields[i]));
        }
        RatelCeedCall(ratel, CeedOperatorSetField(op_platen, "P_1_gamma", restriction_stored_fields[num_fields], CEED_BASIS_COLLOCATED,
                                                  stored_fields[num_fields]));

        RatelCeedCall(ratel, CeedOperatorSetField(op_platen, "v", restriction_u_cell, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));
        // -- Add to composite operator
        RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_residual, op_platen));

        // Set up Jacobian
        {
          RatelDebug(ratel, "---- Setting up Jacobian evaluator");
          CeedQFunction sub_qf_jacobian;
          CeedOperator  sub_op_jacobian;
          // -- QFunction
          RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->platen_jacobian, model_data->platen_jacobian_loc, &sub_qf_jacobian));
          RatelCeedCall(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "q data", q_data_size, CEED_EVAL_NONE));
          RatelCeedCall(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "delta du", num_comp_u * dim, CEED_EVAL_GRAD));
          RatelCeedCall(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "delta u", num_comp_u, CEED_EVAL_INTERP));
          // P_1_gamma has to be before other passive inputs so that dP_* for different models can use same offset
          RatelCeedCall(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "P_1_gamma", 1, CEED_EVAL_NONE));
          for (CeedInt i = 0; i < num_fields; i++) {
            RatelCeedCall(ratel, CeedQFunctionAddInput(sub_qf_jacobian, model_data->quadrature_field_names[i], model_data->quadrature_field_sizes[i],
                                                       CEED_EVAL_NONE));
          }
          RatelCeedCall(ratel, CeedQFunctionAddOutput(sub_qf_jacobian, "delta v", num_comp_u, CEED_EVAL_INTERP));
          // -- Context
          RatelCeedCall(ratel, CeedQFunctionSetContext(sub_qf_jacobian, ctx_platen));
          RatelCeedCall(ratel, CeedQFunctionSetContextWritable(sub_qf_jacobian, false));
          // -- Operator
          RatelCeedCall(ratel, CeedOperatorCreate(ceed, sub_qf_jacobian, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_jacobian));
          {
            char operator_name[30];

            PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "platen BC jacobian %" PetscInt_FMT ".%" PetscInt_FMT, dm_face,
                                    face_domain_value));
            PetscCall(RatelMaterialSetOperatorName(material, operator_name, sub_op_jacobian));
          }
          RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, "q data", restriction_q_data, CEED_BASIS_COLLOCATED, q_data));
          RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, "delta du", restriction_u_cell, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));
          RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, "P_1_gamma", restriction_stored_fields[num_fields], CEED_BASIS_COLLOCATED,
                                                    stored_fields[num_fields]));
          for (CeedInt i = 0; i < num_fields; i++) {
            RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, model_data->quadrature_field_names[i], restriction_stored_fields[i],
                                                      CEED_BASIS_COLLOCATED, stored_fields[i]));
          }
          RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, "delta u", restriction_u_cell, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));
          RatelCeedCall(ratel, CeedOperatorSetField(sub_op_jacobian, "delta v", restriction_u_cell, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));

          CeedInt flops = model_data->flops_qf_jacobian_u - 36;
          RatelCeedCall(ratel, CeedQFunctionSetUserFlopsEstimate(sub_qf_jacobian, flops));

          // -- Add to composite operator
          RatelCeedCall(ratel, CeedCompositeOperatorAddSub(op_jacobian, sub_op_jacobian));
          if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(sub_op_jacobian, stdout));
          // -- Cleanup
          RatelCeedCall(ratel, CeedQFunctionDestroy(&sub_qf_jacobian));
          RatelCeedCall(ratel, CeedOperatorDestroy(&sub_op_jacobian));
        }

        if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_platen, stdout));
        // -- Cleanup
        for (PetscInt i = 0; i < num_fields + 1; i++) {
          RatelCeedCall(ratel, CeedVectorDestroy(&stored_fields[i]));
        }
        RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
        RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_cell_to_face));
        RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_cell_to_face));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_x_cell));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_u_cell));
        RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
        RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_platen));
        RatelCeedCall(ratel, CeedOperatorDestroy(&op_platen));
        for (PetscInt i = 0; i < num_fields + 1; i++) {
          RatelCeedCall(ratel, CeedElemRestrictionDestroy(&restriction_stored_fields[i]));
        }
      }
      PetscCall(ISRestoreIndices(is_face_domain_values, &face_domain_values));
      PetscCall(ISDestroy(&is_face_domain_values));
      RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_platen));
    }
    RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel Ceed Add Boundaries Platen Success!");
  PetscFunctionReturn(0);
}

/// @}
