/// @file
/// Implementation of Ratel solver monitor interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-diagnostic.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-monitor.h>
#include <ratel.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief RatelTSMonitor to allow multiple monitor outputs

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitor(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor");

  // Selectively call RatelTSMonitor functions
  //  based upon if monitor viewers exist
  if (ratel->monitor_viewer_strain_energy) PetscCall(RatelTSMonitorStrainEnergy(ts, steps, time, U, ctx));
  if (ratel->monitor_viewer_surface_forces) PetscCall(RatelTSMonitorSurfaceForces(ts, steps, time, U, ctx));
  if (ratel->monitor_viewer_diagnostic_quantities) PetscCall(RatelTSMonitorDiagnosticQuantities(ts, steps, time, U, ctx));
  if (ratel->monitor_checkpoint_file_name) PetscCall(RatelTSMonitorCheckpoint(ts, steps, time, U, ctx));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Success!");
  PetscFunctionReturn(0);
}

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief RatelTSMonitor function for strain energy

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorStrainEnergy(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel       ratel         = (Ratel)ctx;
  PetscScalar strain_energy = 0;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Strain Energy");

  PetscCall(RatelComputeStrainEnergy(ratel, U, time, &strain_energy));
  PetscCall(
      PetscViewerASCIIPrintf(ratel->monitor_viewer_strain_energy, "%" PetscInt_FMT " TS time %g strain energy %0.12e\n", steps, time, strain_energy));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Strain Energy Success!");
  PetscFunctionReturn(0);
}

/**
  @brief RatelTSMonitor function for surface forces

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSurfaceForces(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel    ratel = (Ratel)ctx;
  PetscInt dim = 3, num_comp_u = 3;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Surface Forces");

  // Get number of solution components
  {
    const PetscInt *field_sizes;

    PetscCall(DMGetDimension(ratel->ctx_surface_force->dm_y, &dim));
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  // Compute surface forces
  PetscScalar *surface_forces;
  PetscCall(RatelComputeSurfaceForces(ratel, U, time, &surface_forces));

  // View
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscCall(PetscViewerASCIIPrintf(
        ratel->monitor_viewer_surface_forces, "%0.12e, %" PetscInt_FMT ", %0.12e, %0.12e, %0.12e, %0.12e, %0.12e, %0.12e\n", time,
        ratel->surface_force_dm_faces[i], surface_forces[2 * i * num_comp_u + 0], dim >= 3 ? surface_forces[2 * i * num_comp_u + 1] : 0.0,
        surface_forces[2 * i * num_comp_u + 2], surface_forces[(2 * i + 1) * num_comp_u + 0], surface_forces[(2 * i + 1) * num_comp_u + 1],
        dim >= 3 ? surface_forces[(2 * i + 1) * num_comp_u + 2] : 0.0));
  }

  // Cleanup
  PetscCall(PetscFree(surface_forces));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Surface Forces Success!");
  PetscFunctionReturn(0);
}

/**
  @brief RatelTSMonitor function for diagnostic quantities

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorDiagnosticQuantities(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Diagnostic Quantities");

  // Cached diagnostics vector
  Vec D;
  PetscCall(DMGetNamedLocalVector(ratel->dm_diagnostic, "diagnostic quantities", &D));
  PetscCall(PetscObjectSetName((PetscObject)D, "diagnostic_quantities"));

  // Compute and store diagnostic quantities
  PetscCall(RatelComputeDiagnosticQuantities_Internal(ratel, U, time, D));
  PetscCall(DMSetOutputSequenceNumber(ratel->dm_diagnostic, steps, time));

  // Update VTK/VTU output filename, if needed
  if (ratel->is_monitor_vtk) {
    char output_filename[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(output_filename, sizeof output_filename, "%s_%" PetscInt_FMT ".%s", ratel->monitor_vtk_file_name, steps,
                            ratel->monitor_vtk_file_extension));
    PetscCall(PetscViewerFileSetName(ratel->monitor_viewer_diagnostic_quantities, output_filename));
  }

  // View
  PetscCall(VecView(D, ratel->monitor_viewer_diagnostic_quantities));

  // Cleanup
  PetscCall(DMRestoreNamedLocalVector(ratel->dm_diagnostic, "diagnostic quantities", &D));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Diagnostic Quantities Success!");
  PetscFunctionReturn(0);
}

/**
  @brief RatelTSMonitor function for binary checkpoints

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorCheckpoint(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel          ratel              = (Ratel)ctx;
  const PetscInt checkpoint_version = RATEL_CHECKPOINT_VERSION;
  char           file_path[PETSC_MAX_PATH_LEN];
  PetscViewer    viewer;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Checkpoint");

  if (steps % ratel->monitor_checkpoint_interval == 0) {
    // Setup viewer
    PetscCall(PetscSNPrintf(file_path, sizeof file_path, "%s-%" PetscInt_FMT ".bin", ratel->monitor_checkpoint_file_name, steps));
    PetscCall(PetscViewerBinaryOpen(ratel->comm, file_path, FILE_MODE_WRITE, &viewer));

    // Write checkpoint
    PetscCall(PetscViewerBinaryWrite(viewer, &checkpoint_version, 1, PETSC_INT));
    PetscCall(PetscViewerBinaryWrite(viewer, &steps, 1, PETSC_INT));
    time /= ratel->material_units->second;  // Dimensionalize time back
    PetscCall(PetscViewerBinaryWrite(viewer, &time, 1, PETSC_REAL));
    PetscCall(VecView(U, viewer));

    // Cleanup
    PetscCall(PetscViewerDestroy(&viewer));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Checkpoint Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Set Ratel TSMonitor function

  @param[in]  ratel    Ratel context
  @param[in]  ts       TS object to monitor
  @param[in]  monitor  Monitor function to set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSet(Ratel ratel, TS ts, RatelTSMonitorFunction monitor) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Set");

  // Set default ASCII monitor, if needed
  if (monitor != RatelTSMonitor) {
    PetscViewer *monitor_viewer = NULL;

    if (monitor == RatelTSMonitorStrainEnergy) monitor_viewer = &ratel->monitor_viewer_strain_energy;
    else if (monitor == RatelTSMonitorSurfaceForces) monitor_viewer = &ratel->monitor_viewer_surface_forces;
    else monitor_viewer = &ratel->monitor_viewer_diagnostic_quantities;

    if (!(*monitor_viewer)) {
      PetscCall(PetscViewerCreate(ratel->comm, monitor_viewer));
      PetscCall(PetscViewerSetType(*monitor_viewer, PETSCVIEWERASCII));
    }
  }

  // Set Ratel monitor
  PetscCall(TSMonitorSet(ts, RatelTSMonitor, ratel, NULL));

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel TS Monitor Set Success!");
  PetscFunctionReturn(0);
}

/// @}
