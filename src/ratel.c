/// @file
/// Implementation of Core Ratel interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-cl-options.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel-petsc-ops.h>
#include <ratel-solver.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/qfunctions/utils.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Print Ratel debugging information in color

  @param[in]  ratel      Ratel context
  @param[in]  all_ranks  Boolean flag to force printing on all ranks
  @param[in]  color      Color to print
  @param[in]  format     Printing format
**/
// LCOV_EXCL_START
void RatelDebugImpl256(const Ratel ratel, PetscBool all_ranks, const unsigned char color, const char *format, ...) {
  if (!ratel->is_debug && !all_ranks) return;
  if (all_ranks) {
    if (color != RATEL_DEBUG_NO_COLOR) PetscSynchronizedFPrintf(ratel->comm, PETSC_STDOUT, "\033[38;5;%dm", color);
    if (all_ranks) PetscSynchronizedFPrintf(ratel->comm, PETSC_STDOUT, "Printing from rank %" PetscInt_FMT "\n", ratel->comm_rank);
    va_list args;
    va_start(args, format);
    PetscSynchronizedFPrintf(ratel->comm, PETSC_STDOUT, format, args);
    va_end(args);
    if (color != RATEL_DEBUG_NO_COLOR) PetscSynchronizedFPrintf(ratel->comm, PETSC_STDOUT, "\033[m");
    PetscSynchronizedFPrintf(ratel->comm, PETSC_STDOUT, "\n");
    PetscSynchronizedFlush(ratel->comm, PETSC_STDOUT);
  } else {
    if (color != RATEL_DEBUG_NO_COLOR) PetscFPrintf(ratel->comm, PETSC_STDOUT, "\033[38;5;%dm", color);
    va_list args;
    va_start(args, format);
    PetscVFPrintf(PETSC_STDOUT, format, args);
    va_end(args);
    if (color != RATEL_DEBUG_NO_COLOR) PetscFPrintf(ratel->comm, PETSC_STDOUT, "\033[m");
    PetscPrintf(ratel->comm, "\n");
    fflush(PETSC_STDOUT);
  }
}
// LCOV_EXCL_STOP

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Get Ratel library version info

  Ratel version numbers have the form major.minor.patch.
  Non-release versions may contain unstable interfaces.

  @param[out]  major    Major version of the library
  @param[out]  minor    Minor version of the library
  @param[out]  patch    Patch (subminor) version of the library
  @param[out]  release  True for releases; false for development branches.

  The caller may pass NULL for any arguments that are not needed.

  @sa RATEL_VERSION_GE()

  @return An error code: 0 - success, otherwise - failure
**/
// LCOV_EXCL_START
PetscErrorCode RatelGetVersion(int *major, int *minor, int *patch, PetscBool *release) {
  if (major) *major = RATEL_VERSION_MAJOR;
  if (minor) *minor = RATEL_VERSION_MINOR;
  if (patch) *patch = RATEL_VERSION_PATCH;
  if (release) *release = RATEL_VERSION_RELEASE;
  return 0;
}
// LCOV_EXCL_STOP

/**
  @brief Setup Ratel context object
         Note: This function call initializes the libCEED context.

  @param[in]   comm          MPI communication object
  @param[out]  ratel         Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelInit(MPI_Comm comm, Ratel *ratel) {
  PetscLogStage stage_ratel_setup;

  PetscFunctionBeginUser;
  PetscCall(PetscLogStageGetId("Ratel Setup", &stage_ratel_setup));
  if (stage_ratel_setup == -1) PetscCall(PetscLogStageRegister("Ratel Setup", &stage_ratel_setup));
  PetscCall(PetscLogStagePush(stage_ratel_setup));

  PetscCall(PetscCalloc1(1, ratel));
  (*ratel)->comm = comm;

  // Record env variables RATEL_DEBUG or DBG
  {
    PetscMPIInt comm_rank = -1;

    PetscCallMPI(MPI_Comm_rank(comm, &comm_rank));
    (*ratel)->comm_rank = comm_rank;
    (*ratel)->is_debug  = comm_rank == 0 && (!!getenv("RATEL_DEBUG") || !!getenv("DEBUG") || !!getenv("DBG"));
    RatelDebug256(*ratel, RATEL_DEBUG_GREEN, "---------- Ratel debugging mode active ----------");
    RatelDebug256(*ratel, RATEL_DEBUG_GREEN, "-- Ratel Init");
  }

  // Register models
  RatelDebug(*ratel, "---- Registering models");
  (*ratel)->material_create_functions = NULL;
  PetscCall(RatelRegisterModels(*ratel, &(*ratel)->material_create_functions));

  // Read basic command line options
  RatelDebug(*ratel, "---- Processing command line options");
  PetscCall(RatelProcessCommandLineOptions(*ratel));

  PetscCall(PetscLogStagePop());
  RatelDebug256(*ratel, RATEL_DEBUG_GREEN, "-- Ratel Init Success!");
  PetscFunctionReturn(0);
}

/**
  @brief View a Ratel context
         Note: This function can be called with the command line option `-ratel_view`

  @param[in]  ratel   Ratel context to view
  @param[in]  viewer  Optional visualization context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelView(Ratel ratel, PetscViewer viewer) {
  PetscInt fine_level = ratel->num_multigrid_levels - 1;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel View");

  if (!viewer) PetscCall(PetscViewerASCIIGetStdout(ratel->comm, &viewer));

  PetscCall(PetscViewerASCIIPrintf(viewer, "Ratel Context:\n"));
  {
    PetscMPIInt comm_size;
    char        hostname[PETSC_MAX_PATH_LEN];

    PetscCall(MPI_Comm_size(ratel->comm, &comm_size));
    PetscCall(PetscGetHostName(hostname, sizeof hostname));
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  MPI:\n"
                                     "    Hostname: %s\n"
                                     "    Total ranks: %d\n",
                                     hostname, comm_size));
  }
  {
    VecType vec_type;
    MatType mat_type;

    PetscCall(DMGetVecType(ratel->dm_hierarchy[fine_level], &vec_type));
    PetscCall(DMGetMatType(ratel->dm_hierarchy[fine_level], &mat_type));
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  PETSc:\n"
                                     "    VecType: %s\n"
                                     "    MatType: %s\n",
                                     vec_type, mat_type));
  }
  {
    const char *ceed_resource_used;
    CeedMemType mem_type_backend;

    RatelCeedCall(ratel, CeedGetResource(ratel->ceed, &ceed_resource_used));
    RatelCeedCall(ratel, CeedGetPreferredMemType(ratel->ceed, &mem_type_backend));

    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  libCEED:\n"
                                     "    Backend resource: %s\n"
                                     "    Backend MemType: %s\n",
                                     ceed_resource_used, CeedMemTypes[mem_type_backend]));
  }
  {
    PetscInt num_global_dofs;
    PetscInt num_local_dofs;

    {
      Vec U;
      PetscCall(DMGetGlobalVector(ratel->dm_hierarchy[fine_level], &U));
      PetscCall(VecGetSize(U, &num_global_dofs));
      PetscCall(VecGetLocalSize(U, &num_local_dofs));
      PetscCall(DMRestoreGlobalVector(ratel->dm_hierarchy[fine_level], &U));
    }
    PetscInt64 num_local_dofs_minmax[] = {num_local_dofs, -num_local_dofs};
    PetscCallMPI(MPI_Allreduce(MPI_IN_PLACE, num_local_dofs_minmax, 2, MPI_INT, MPI_MIN, ratel->comm));
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  Mesh:\n"
                                     "    Global DoFs: %" PetscInt_FMT "\n"
                                     "    Local DoFs: [%" PetscInt64_FMT ", %" PetscInt64_FMT "]\n",
                                     num_global_dofs, num_local_dofs_minmax[0], -num_local_dofs_minmax[1]));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Solver: %s\n", RatelSolverTypes[ratel->solver_type]));
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Materials:\n"));
  for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
    PetscCall(RatelMaterialView(ratel->materials[material_index], viewer));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Forcing: %s\n", RatelForcingTypes[ratel->forcing_choice]));
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    if (ratel->bc_clamp_count[i]) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "  Dirichlet (clamp) boundaries, field %" PetscInt_FMT ":\n", i));
      for (PetscInt j = 0; j < ratel->bc_clamp_count[i]; j++) {
        PetscBool   is_option_found = PETSC_FALSE;
        char        option_name[PETSC_MAX_PATH_LEN];
        PetscScalar translation[3] = {0, 0, 0};
        PetscScalar rotation[5]    = {0, 0, 0, 0, 0};
        PetscInt    max_n          = 3;

        PetscOptionsBegin(ratel->comm, NULL, "", NULL);
        PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_translate",
                                ratel->bc_clamp_faces[i][j], i));
        PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, translation, &max_n, &is_option_found));
        if (i == 0 && !is_option_found) {
          PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_translate", ratel->bc_clamp_faces[i][j]));
          PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, translation, &max_n, NULL));
        }
        max_n           = 5;
        is_option_found = PETSC_FALSE;
        PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_rotate",
                                ratel->bc_clamp_faces[i][j], i));
        PetscCall(
            PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &max_n, &is_option_found));
        if (i == 0 && !is_option_found) {
          PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_rotate", ratel->bc_clamp_faces[i][j]));
          PetscCall(PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &max_n, NULL));
        }

        // ------ Normalize
        PetscScalar norm_rotation = RatelNorm3((CeedScalar *)rotation);
        if (fabs(norm_rotation) < 1e-16) norm_rotation = 1;
        for (PetscInt j = 0; j < 3; j++) rotation[i] /= norm_rotation;

        PetscOptionsEnd();

        PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_clamp_faces[i][j]));
        PetscCall(PetscViewerASCIIPrintf(viewer, "      Translation vector: [%f, %f, %f]\n", translation[0], translation[1], translation[2]));
        PetscCall(PetscViewerASCIIPrintf(viewer, "      Rotation vector: [%f, %f, %f, %f]\n", rotation[0], rotation[1], rotation[2], rotation[3]));
      }
    }
  }
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    if (ratel->bc_mms_count[i]) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "  Dirichlet MMS boundaries, field %" PetscInt_FMT ":\n", i));
      for (PetscInt j = 0; j < ratel->bc_mms_count[i]; j++)
        PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_mms_faces[i][j]));
    }
  }
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    if (ratel->bc_slip_count[i]) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "  Slip boundaries, field %" PetscInt_FMT ":\n", i));
      for (PetscInt j = 0; j < ratel->bc_slip_count[i]; j++) {
        PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_slip_faces[i][j]));

        char    components[8] = "";
        CeedInt head          = 0;
        for (CeedInt k = 0; k < ratel->bc_slip_components_count[i][j]; k++) {
          components[head] = ratel->bc_slip_components[i][j][k] == 0 ? 'x' : ratel->bc_slip_components[i][j][k] == 1 ? 'y' : 'z';
          head++;
          if (k < ratel->bc_slip_components_count[i][j] - 1) {
            components[head]     = ',';
            components[head + 1] = ' ';
            head += 2;
          }
        }
        PetscViewerASCIIPrintf(viewer, "      Components: %s\n", components);
      }
    }
  }
  if (ratel->bc_traction_count) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Neumann (traction) boundaries:\n"));
    for (PetscInt i = 0; i < ratel->bc_traction_count; i++) {
      char        option_name[25];
      PetscScalar traction[3] = {0, 0, 0};
      PetscInt    max_n       = 3;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_traction_%" PetscInt_FMT, ratel->bc_traction_faces[i]));
      PetscCall(PetscOptionsScalarArray(option_name, "Traction vector for constrained face", NULL, traction, &max_n, NULL));
      PetscOptionsEnd();

      PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_traction_faces[i]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Traction vector: [%f, %f, %f]\n", traction[0], traction[1], traction[2]));
    }
  }
  if (ratel->bc_platen_count) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Platen (contact) boundaries:\n"));
    for (PetscInt i = 0; i < ratel->bc_platen_count; i++) {
      char       option_name[25];
      PetscInt   max_n     = 3;
      CeedScalar normal[3] = {0, 0, -1};
      CeedScalar center[3] = {0, 0, 0};
      CeedScalar distance  = 1.0;
      CeedScalar gamma     = 1.0;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_platen_%" PetscInt_FMT "_normal", ratel->bc_platen_faces[i]));
      PetscCall(PetscOptionsScalarArray(option_name, "Exterior normal to the half-space platen", NULL, normal, &max_n, NULL));
      CeedScalar norm_normal = RatelNorm3(normal);
      if (fabs(norm_normal) < CEED_EPSILON) norm_normal = 1;
      for (PetscInt j = 0; j < 3; j++) normal[j] = normal[j] / norm_normal;

      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_platen_%" PetscInt_FMT "_center", ratel->bc_platen_faces[i]));
      PetscCall(PetscOptionsScalarArray(option_name, "Point on the surface of the half-plane platen", NULL, center, &max_n, NULL));

      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_platen_%" PetscInt_FMT "_distance", ratel->bc_platen_faces[i]));
      PetscCall(PetscOptionsScalar(option_name, "Total distance to move platen along normal vector", NULL, distance, &distance, NULL));

      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_platen_%" PetscInt_FMT "_gamma", ratel->bc_platen_faces[i]));
      PetscCall(PetscOptionsScalar(option_name, "Nitsche's method penalty parameter", NULL, gamma, &gamma, NULL));
      PetscOptionsEnd();

      PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_platen_faces[i]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Platen center: [%f, %f, %f]\n", center[0], center[1], center[2]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Platen normal: [%f, %f, %f]\n", normal[0], normal[1], normal[2]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Platen distance: %f\n", distance));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Nitsche method parameter: %f\n", gamma));
    }
  }
  PetscCall(PetscViewerASCIIPrintf(viewer,
                                   "  Initial condition:\n"
                                   "    Type: %s\n",
                                   RatelInitialConditionTypes[ratel->initial_condition_type]));
  switch (ratel->initial_condition_type) {
    case RATEL_INITIAL_CONDITION_ZERO:
      break;
    case RATEL_INITIAL_CONDITION_CONTINUE: {
      PetscViewer continue_viewer;
      PetscInt    step_number, checkpoint_version;
      PetscReal   time;

      PetscCall(PetscViewerBinaryOpen(ratel->comm, ratel->continue_file_name, FILE_MODE_READ, &continue_viewer));
      PetscCall(PetscViewerBinaryRead(continue_viewer, &checkpoint_version, 1, NULL, PETSC_INT));
      PetscCall(PetscViewerBinaryRead(continue_viewer, &step_number, 1, NULL, PETSC_INT));
      PetscCall(PetscViewerBinaryRead(continue_viewer, &time, 1, NULL, PETSC_REAL));
      PetscCall(PetscViewerDestroy(&continue_viewer));

      PetscCall(PetscViewerASCIIPrintf(viewer,
                                       "    Filename: %s\n"
                                       "    Checkpoint version: %X\n"
                                       "    Timestep: %" PetscInt_FMT "\n"
                                       "    Time: %g\n",
                                       ratel->continue_file_name, (int)checkpoint_version, step_number, time));
    }
  }
  PetscCall(PetscViewerASCIIPrintf(viewer,
                                   "  Multigrid:\n"
                                   "    Type: %s\n"
                                   "    Number of levels: %" PetscInt_FMT "\n",
                                   RatelMutigridTypes[ratel->multigrid_type], ratel->num_multigrid_levels));
  if (ratel->num_multigrid_levels > 1) {
    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "    Level orders, field %" PetscInt_FMT ": [", i));
      for (PetscInt j = ratel->num_multigrid_levels - 1; j > 0; j--) {
        PetscCall(PetscViewerASCIIPrintf(viewer, "%s%" PetscInt_FMT ",", j == fine_level ? "" : " ", ratel->multigrid_level_orders[i][j]));
      }
      PetscCall(PetscViewerASCIIPrintf(viewer, " %" PetscInt_FMT "]\n", ratel->multigrid_level_orders[i][0]));
    }
  }
  {
    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      PetscCall(PetscViewerASCIIPrintf(viewer,
                                       "  Finite elements, field %" PetscInt_FMT ":\n"
                                       "    Polynomial order: %" PetscInt_FMT "\n"
                                       "    Additional quadrature points: %" PetscInt_FMT "\n",
                                       i, ratel->multigrid_fine_orders[i], ratel->q_extra));
      if (ratel->dm_hierarchy && ratel->dm_hierarchy[fine_level]) {
        DM             dm = ratel->dm_hierarchy[fine_level];
        DMLabel        depth_label;
        DMPolytopeType cell_type;
        PetscBool      is_simplex = PETSC_FALSE;
        PetscInt       depth = 0, first_point = 0;
        PetscInt       ids[1] = {0};

        PetscCall(DMPlexIsSimplex(dm, &is_simplex));
        PetscCall(PetscViewerASCIIPrintf(viewer, "    Basis application: %s\n", is_simplex ? "simplex" : "tensor product"));

        PetscCall(DMPlexGetDepth(dm, &depth));
        PetscCall(DMPlexGetDepthLabel(dm, &depth_label));
        ids[0] = depth;
        PetscCall(DMGetFirstLabeledPoint(dm, dm, depth_label, 1, ids, 0, &first_point, NULL));
        if (first_point >= 0) {
          PetscCall(DMPlexGetCellType(dm, first_point, &cell_type));
          CeedElemTopology elem_topo = RatelElemTopologyP2C(cell_type);
          PetscCall(PetscViewerASCIIPrintf(viewer, "    Element topology: %s\n", CeedElemTopologies[elem_topo]));
        }
      }
    }
  }
  {
    PetscBool is_diagnostic_geometry_ratel_decide = ratel->diagnostic_geometry_order == RATEL_DECIDE;
    char      diagnostic_geometry_order_string[PETSC_MAX_PATH_LEN];

    if (is_diagnostic_geometry_ratel_decide) {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%s", "use solution mesh geometric order"));
    } else {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%" PetscInt_FMT,
                              ratel->diagnostic_geometry_order));
    }
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Geometry order for diagnostic values mesh: %s\n", diagnostic_geometry_order_string));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Has manufactured solution: %s\n", ratel->has_mms ? "yes" : "no"));
  if (ratel->expected_strain_energy == 0.0) {
    // LCOV_EXCL_START
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Expected strain energy: not set\n"));
    // LCOV_EXCL_STOP
  } else {
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Expected strain energy: %0.12e\n", ratel->expected_strain_energy));
  }

  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel View Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Destroy a Ratel context

  @param[in,out]  ratel  Ratel context object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDestroy(Ratel *ratel) {
  PetscFunctionBeginUser;
  RatelDebug256(*ratel, RATEL_DEBUG_GREEN, "-- Ratel Destroy");

  // View before destroying, if requested
  {
    PetscViewer viewer;
    PetscBool   view_ratel = PETSC_FALSE;

    PetscCall(PetscOptionsGetViewer((*ratel)->comm, NULL, NULL, "-ratel_view", &viewer, NULL, &view_ratel));
    if (view_ratel) PetscCall(RatelView(*ratel, viewer));
  }

  // DMs
  PetscCall(DMDestroy(&(*ratel)->dm_orig));
  PetscCall(DMDestroy(&(*ratel)->dm_state_fields));
  PetscCall(DMDestroy(&(*ratel)->dm_energy));
  PetscCall(DMDestroy(&(*ratel)->dm_diagnostic));
  PetscCall(DMDestroy(&(*ratel)->dm_surface_displacement));
  PetscCall(DMDestroy(&(*ratel)->dm_surface_force));
  for (PetscInt i = 0; i < (*ratel)->num_multigrid_levels_setup; i++) {
    PetscCall(DMDestroy(&(*ratel)->dm_hierarchy[i]));
  }
  PetscCall(PetscFree((*ratel)->dm_hierarchy));

  // Vector
  PetscCall(VecDestroy(&(*ratel)->X_loc_residual_u));

  // KSP
  PetscCall(KSPDestroy(&(*ratel)->ksp_diagnostic_projection));

  // Mats
  for (PetscInt i = 0; i < (*ratel)->num_multigrid_levels_setup; i++) {
    PetscCall(MatDestroy(&(*ratel)->mat_jacobian[i]));
    if (i > 0) PetscCall(MatDestroy(&(*ratel)->mat_prolong_restrict[i]));
  }
  PetscCall(MatDestroy(&(*ratel)->mat_jacobian_coarse));
  PetscCall(PetscFree((*ratel)->mat_jacobian));
  PetscCall(PetscFree((*ratel)->mat_prolong_restrict));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_residual_u));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_residual_ut));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_residual_utt));
  PetscCall(PetscFree((*ratel)->ctx_jacobian));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_energy));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_diagnostic));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_diagnostic_projection));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_surface_force));
  PetscCall(RatelOperatorApplyContextDestroy((*ratel)->ctx_mms_error));
  if ((*ratel)->ctx_form_jacobian) RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->ctx_form_jacobian->coo_values));
  PetscCall(PetscFree((*ratel)->ctx_form_jacobian));
  PetscCall(PetscFree((*ratel)->ctx_prolong_restrict));

  // FLOPs counting
  PetscCall(PetscFree((*ratel)->event_prolong));
  PetscCall(PetscFree((*ratel)->event_restrict));
  PetscCall(PetscFree((*ratel)->flops_jacobian));
  PetscCall(PetscFree((*ratel)->flops_prolong));
  PetscCall(PetscFree((*ratel)->flops_restrict));

  // Materials
  PetscCall(PetscFunctionListDestroy(&(*ratel)->material_create_functions));
  for (PetscInt i = 0; i < ((*ratel)->num_materials); i++) PetscCall(RatelMaterialDestroy(&(*ratel)->materials[i]));
  PetscCall(PetscFree((*ratel)->materials));
  PetscCall(PetscFree((*ratel)->material_volume_label_name));
  PetscCall(PetscFree((*ratel)->material_units));

  // Output options
  PetscCall(PetscViewerDestroy(&(*ratel)->monitor_viewer_strain_energy));
  PetscCall(PetscViewerDestroy(&(*ratel)->monitor_viewer_surface_forces));
  PetscCall(PetscViewerDestroy(&(*ratel)->monitor_viewer_diagnostic_quantities));
  PetscCall(PetscFree((*ratel)->monitor_checkpoint_file_name));
  PetscCall(PetscFree((*ratel)->monitor_vtk_file_name));
  PetscCall(PetscFree((*ratel)->monitor_vtk_file_extension));
  PetscCall(PetscFree((*ratel)->continue_file_name));

  // Ceed
  PetscCall(PetscFree((*ratel)->ceed_resource));
  RatelCeedCall((*ratel), CeedOperatorDestroy(&(*ratel)->op_dirichlet));
  for (PetscInt i = 0; i < (*ratel)->surface_force_face_count; i++) RatelCeedCall((*ratel), CeedOperatorDestroy(&(*ratel)->ops_surface_force[i]));
  PetscCall(PetscFree((*ratel)->ops_surface_force));
  {
    PetscErrorCode ierr;

    ierr = CeedDestroy(&(*ratel)->ceed);
    if (ierr) SETERRQ((*ratel)->comm, PETSC_ERR_LIB, "Destroying Ceed object failed");
  }

  // Ratel itself
  RatelDebug256(*ratel, RATEL_DEBUG_GREEN, "-- Ratel Destroy Success!");
  PetscCall(PetscFree(*ratel));

  PetscFunctionReturn(0);
}

/**
  @brief Create DM with SNES or TS hooks from Ratel context

  @param[in]   ratel        Ratel context
  @param[in]   solver_type  Numerical method to use
  @param[out]  dm           DMPlex object with SNES and TS hooks

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMCreate(Ratel ratel, RatelSolverType solver_type, DM *dm) {
  PetscLogStage stage_dm_setup;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DM Create");
  PetscCall(PetscLogStageGetId("Ratel DM Setup", &stage_dm_setup));
  if (stage_dm_setup == -1) PetscCall(PetscLogStageRegister("Ratel DM Setup", &stage_dm_setup));
  PetscCall(PetscLogStagePush(stage_dm_setup));

  // Model specific setup
  PetscCall(RatelMaterialGetModelType(ratel->materials[0], &ratel->model_type));
  for (PetscInt i = 1; i < ratel->num_materials; i++) {
    RatelModelType model_type;

    PetscCall(RatelMaterialGetModelType(ratel->materials[i], &model_type));
    if (model_type != ratel->model_type) SETERRQ(ratel->comm, PETSC_ERR_LIB, "Mixed material model types not supported");
  }
  switch (ratel->model_type) {
    case RATEL_MODEL_FEM:
      RatelDebug(ratel, "---- Mesh: FEM");
      PetscCall(RatelDMSetup_FEM(ratel));
      break;
  }

  // Solver specific setup
  ratel->solver_type = solver_type;
  PetscCall(RatelDMSetupSolver(ratel));

  // Copy DM reference
  PetscCall(PetscObjectReference((PetscObject)ratel->dm_hierarchy[0]));
  *dm = ratel->dm_hierarchy[0];

  PetscCall(PetscLogStagePop());
  RatelDebug256(ratel, RATEL_DEBUG_GREEN, "-- Ratel DM Create Success!");
  PetscFunctionReturn(0);
}

/// @}
