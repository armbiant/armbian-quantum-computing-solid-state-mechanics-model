#ifndef ratel_solver_h
#define ratel_solver_h

#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelDMSetupSolver(Ratel ratel);

#endif  // ratel_solver_h
