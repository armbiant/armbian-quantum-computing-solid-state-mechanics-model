#ifndef ratel_material_h
#define ratel_material_h

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>

// Material
RATEL_INTERN PetscErrorCode RatelMaterialCreate(Ratel ratel, const char *material_name, RatelMaterial *material);
RATEL_INTERN PetscErrorCode RatelMaterialView(RatelMaterial material, PetscViewer viewer);
RATEL_INTERN PetscErrorCode RatelMaterialDestroy(RatelMaterial *material);
RATEL_INTERN PetscErrorCode RatelMaterialGetActiveFieldSizes(RatelMaterial material, PetscInt *num_active_fields,
                                                             const PetscInt **active_field_sizes);
RATEL_INTERN PetscErrorCode RatelMaterialGetNumDiagnosticComponents(RatelMaterial material, PetscInt *num_components);
RATEL_INTERN PetscErrorCode RatelMaterialGetNumStateFields(RatelMaterial material, PetscInt *num_state_fields);
RATEL_INTERN PetscErrorCode RatelMaterialGetActiveFieldNames(RatelMaterial material, const char ***field_names);
RATEL_INTERN PetscErrorCode RatelMaterialGetDiagnosticFieldNames(RatelMaterial material, const char ***field_names);
RATEL_INTERN PetscErrorCode RatelMaterialGetStateFieldNames(RatelMaterial material, const char ***field_names);
RATEL_INTERN PetscErrorCode RatelMaterialGetLabelValues(RatelMaterial material, PetscInt *num_label_values, PetscInt **label_values);
RATEL_INTERN PetscErrorCode RatelMaterialGetModelType(RatelMaterial material, RatelModelType *model_type);
RATEL_INTERN PetscErrorCode RatelMaterialGetMaterialName(RatelMaterial material, const char **material_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetModelName(RatelMaterial material, const char **model_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name);
RATEL_INTERN PetscErrorCode RatelMaterialIsOnProcess(RatelMaterial material, PetscBool *is_on_process);
RATEL_INTERN PetscErrorCode RatelMaterialHasMMS(RatelMaterial material, PetscBool *has_mms);

// Utilities
RATEL_INTERN PetscErrorCode RatelMaterialGetCLPrefix(RatelMaterial material, char **cl_prefix);
RATEL_INTERN PetscErrorCode RatelMaterialGetCLMessage(RatelMaterial material, char **cl_message);
RATEL_INTERN PetscErrorCode RatelMaterialSetOperatorName(RatelMaterial material, const char *base_name, CeedOperator op);

// Geometry
RATEL_INTERN PetscErrorCode RatelMaterialGetSolutionData(RatelMaterial material, PetscInt *num_active_fields, CeedElemRestriction **restrictions,
                                                         CeedBasis **bases);
RATEL_INTERN PetscErrorCode RatelMaterialGetVolumeQData(RatelMaterial material, CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientQData(RatelMaterial material, PetscInt dm_face, PetscInt orientation,
                                                                 CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticQData(RatelMaterial material, PetscInt dm_face, PetscInt orientation,
                                                                           CeedElemRestriction *restriction, CeedVector *q_data);
// Residual and Jacobian
RATEL_INTERN PetscErrorCode RatelMaterialSetupResidualJacobianSuboperators(RatelMaterial material, CeedOperator op_residual_u,
                                                                           CeedOperator op_residual_ut, CeedOperator op_residual_utt,
                                                                           CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelMaterialSetupForcingSuboperator(RatelMaterial material, CeedOperator op_residual_u);
RATEL_INTERN PetscErrorCode RatelMaterialSetupMultigridLevel(RatelMaterial material, PetscInt level, CeedVector m_loc, CeedOperator op_jacobian_fine,
                                                             CeedOperator op_jacobian_coarse, CeedOperator op_prolong, CeedOperator op_restrict);
RATEL_INTERN PetscErrorCode RatelMaterialSetJacobianSootherContext(RatelMaterial material, PetscBool set_or_unset, CeedOperator op_jacobian,
                                                                   PetscBool *was_set);
// Platen BC
RATEL_INTERN PetscErrorCode RatelMaterialSetupPlatenSuboperators(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_jacobian);

// Output
RATEL_INTERN PetscErrorCode RatelMaterialSetupEnergySuboperator(RatelMaterial material, CeedOperator op_energy);
RATEL_INTERN PetscErrorCode RatelMaterialSetupDiagnosticSuboperators(RatelMaterial material, CeedOperator op_mass_diagnostic,
                                                                     CeedOperator op_diagnostic);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceForceSuboperators(RatelMaterial material, CeedOperator *ops_surface_force);
RATEL_INTERN PetscErrorCode RatelMaterialSetupMMSErrorSuboperator(RatelMaterial material, CeedOperator op_mms_error);

#endif  // ratel_material_h
