#ifndef ratel_petsc_opts_h
#define ratel_petsc_opts_h

#include <ceed.h>
#include <petsc.h>
#include <petscsnes.h>
#include <ratel-impl.h>
#include <ratel.h>

// Context data
RATEL_INTERN PetscErrorCode RatelOperatorApplyContextCreate(Ratel ratel, const char *name, DM dm_x, DM dm_y, Vec X_loc, Vec Y_loc, CeedVector x_loc,
                                                            CeedVector y_loc, CeedOperator op, RatelOperatorApplyContext *ctx);
RATEL_INTERN PetscErrorCode RatelOperatorApplyContextDestroy(RatelOperatorApplyContext ctx);
RATEL_INTERN PetscErrorCode RatelProlongRestrictContextCreate(Ratel ratel, const char *name, DM dm_c, DM dm_f, Vec X_loc_c, Vec X_loc_f,
                                                              CeedVector x_loc_c, CeedVector x_loc_f, CeedVector y_loc_c, CeedVector y_loc_f,
                                                              CeedOperator op_prolong, CeedOperator op_restrict, RatelProlongRestrictContext *ctx);
RATEL_INTERN PetscErrorCode RatelProlongRestrictContextDestroy(RatelProlongRestrictContext ctx);

// Preconditioning
RATEL_INTERN PetscErrorCode RatelGetDiagonal(Mat A, Vec D);
RATEL_INTERN PetscErrorCode RatelMatSetPreallocationCOO(Ratel ratel, CeedOperator op_ceed, CeedVector *coo_values_ceed, Mat A);
RATEL_INTERN PetscErrorCode RatelMatSetValuesCOO(Ratel ratel, CeedOperator op_ceed, CeedVector coo_values_ceed, CeedMemType mem_type, Mat A);

// Operator application
RATEL_INTERN PetscErrorCode RatelApplyLocalCeedOp(Vec X, Vec Y, RatelOperatorApplyContext ctx);
RATEL_INTERN PetscErrorCode RatelApplyAddLocalCeedOp(Vec X, Vec Y, RatelOperatorApplyContext ctx);
RATEL_INTERN PetscErrorCode RatelDMPlexInsertBoundaryValues(DM dm, PetscBool insert_essential, Vec U_loc, PetscReal time, Vec face_geometry_FVM,
                                                            Vec cell_geometry_FVM, Vec grad_FVM);
RATEL_INTERN PetscErrorCode RatelUpdateTimeAndBoundaryValues(Ratel ratel, PetscReal time);
RATEL_INTERN PetscErrorCode RatelApplyOperator(Mat A, Vec X, Vec Y);
RATEL_INTERN PetscErrorCode RatelApplyJacobian(Mat A, Vec X, Vec Y);
RATEL_INTERN PetscErrorCode RatelApplyProlongation(Mat A, Vec C, Vec F);
RATEL_INTERN PetscErrorCode RatelApplyRestriction(Mat A, Vec F, Vec C);

// Solver callbacks
RATEL_INTERN PetscErrorCode RatelSNESFormResidual(SNES snes, Vec X, Vec Y, void *ctx_residual_u);
RATEL_INTERN PetscErrorCode RatelSNESFormJacobian(SNES snes, Vec X, Mat J, Mat J_pre, void *ctx_form_jacobian);
RATEL_INTERN PetscErrorCode RatelTSFormIResidual(TS ts, PetscReal time, Vec X, Vec X_t, Vec Y, void *ctx_residual_ut);
RATEL_INTERN PetscErrorCode RatelTSFormIJacobian(TS ts, PetscReal time, Vec X, Vec X_t, PetscReal v, Mat J, Mat J_pre, void *ctx_form_jacobian);
RATEL_INTERN PetscErrorCode RatelTSFormI2Residual(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, Vec Y, void *ctx_residual_utt);
RATEL_INTERN PetscErrorCode RatelTSFormI2Jacobian(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, PetscReal v, PetscReal a, Mat J, Mat J_pre,
                                                  void *ctx_form_jacobian);

#endif  // ratel_petsc_opts_h
