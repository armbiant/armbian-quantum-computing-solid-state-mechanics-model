/// @file
/// Ratel Mooney-Rivlin material parameters

#ifndef ratel_mooney_rivlin_h
#define ratel_mooney_rivlin_h

#include <ceed.h>

#include "common-parameters.h"

#ifndef mooney_rivlin_physics
#define mooney_rivlin_physics
typedef struct {
  // Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  // Mooney-Rivlin specific properties
  CeedScalar mu_1;
  CeedScalar mu_2;
  CeedScalar lambda;
} MooneyRivlinPhysics;
#endif

#ifndef mooney_rivlin_platen_data
#define mooney_rivlin_platen_data
typedef struct {
  CeedScalar          normal[3];
  CeedScalar          center[3];
  CeedScalar          distance;
  CeedScalar          time;
  CeedScalar          final_time;
  CeedScalar          gamma;     // Nitche's method parameter
  MooneyRivlinPhysics material;  // Mooney-Rivlin model
} MooneyRivlinPlatenData;
#endif

#endif  // ratel_mooney_rivlin_h
