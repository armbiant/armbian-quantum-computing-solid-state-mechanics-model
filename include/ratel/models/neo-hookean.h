/// @file
/// Ratel Neo-Hookean material parameters

#ifndef ratel_neo_hookean_h
#define ratel_neo_hookean_h

#include <ceed.h>

#include "common-parameters.h"

#ifndef neo_hookean_physics
#define neo_hookean_physics
typedef struct {
  // Common properties
  // Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  // Neo-Hookean specific properties
  CeedScalar nu;  // Poisson's ratio
  CeedScalar E;   // Young's Modulus
} NeoHookeanPhysics;
#endif

#ifndef neo_hookean_platen_data
#define neo_hookean_platen_data
typedef struct {
  CeedScalar        normal[3];
  CeedScalar        center[3];
  CeedScalar        distance;
  CeedScalar        time;
  CeedScalar        final_time;
  CeedScalar        gamma;     // Nitche's method parameter
  NeoHookeanPhysics material;  // isotropic neo-Hookean model
} NeoHookeanPlatenData;
#endif

#endif  // ratel_neo_hookean_h
