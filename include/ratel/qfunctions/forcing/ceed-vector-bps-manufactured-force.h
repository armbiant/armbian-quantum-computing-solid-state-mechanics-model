/// @file
/// Ratel volumetric forcing term for CEED BPs manufactured solution

#ifndef ratel_ceed_vector_bps_manufactured_qf_h
#define ratel_ceed_vector_bps_manufactured_qf_h

#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// -----------------------------------------------------------------------------
// Forcing term for vector BPs manufactured solution
// -----------------------------------------------------------------------------
CEED_QFUNCTION(MMSForce_CEED_VectorBPs)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*force)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Scaling
  const CeedScalar c[3] = {0., 1., 2.};
  const CeedScalar k[3] = {1., 2., 3.};

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar x = coords[0][i], y = coords[1][i], z = coords[2][i];
    const CeedScalar wdetJ = q_data[0][i];

    // Component 1
    const CeedScalar true_solution = sin(M_PI * (c[0] + k[0] * x)) * sin(M_PI * (c[1] + k[1] * y)) * sin(M_PI * (c[2] + k[2] * z));

    // Component 1
    force[0][i] = -wdetJ * M_PI * M_PI * (k[0] * k[0] + k[1] * k[1] + k[2] * k[2]) * true_solution;
    // Component 2
    force[1][i] = 2.0 * force[0][i];
    // Component 3
    force[2][i] = 3.0 * force[0][i];
  }  // End of Quadrature Point Loop
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_ceed_vector_bps_manufactured_qf_h
