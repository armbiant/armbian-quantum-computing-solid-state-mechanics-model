/// @file
/// Ratel mass operator QFunction source

#ifndef ratel_scaled_mass_qf_h
#define ratel_scaled_mass_qf_h

#include <math.h>

#include "../models/scaled-mass.h"
#include "utils.h"

// -----------------------------------------------------------------------------
// Scaled mass operator with full volumetric qdata
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ScaledMass)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  const ScaledMassContext *context = (ScaledMassContext *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Scaled mass operator
    for (CeedInt j = 0; j < context->num_fields; j++) {
      ScaledMassApplyAtQuadraturePoint(Q, i, context->field_sizes[j], -context->rho * q_data[0][i], in[j + 1], out[j]);
    }
  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------

#endif  // ratel_scaled_mass_qf_h
