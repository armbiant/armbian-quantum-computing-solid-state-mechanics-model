/// @file
/// Ratel linear elasticity QFunction source

#ifndef ratel_ceed_bp3_qf_h
#define ratel_ceed_bp3_qf_h

#include <math.h>

#include "../utils.h"

// -----------------------------------------------------------------------------
// Residual evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Residual_CEED_BP3)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[CEED_Q_VLA]     = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*vg)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3] = {ug[0][i], ug[1][i], ug[2][i]};

    // Read q_data (dXdxdXdx_T symmetric matrix)
    const CeedScalar dXdxdXdx_T[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[2][i], q_data[4][i], q_data[5][i]},
        {q_data[3][i], q_data[5][i], q_data[6][i]}
    };

    for (CeedInt j = 0; j < 3; j++) {  // j = direction of vg
      vg[j][i] = (du[0] * dXdxdXdx_T[0][j] + du[1] * dXdxdXdx_T[1][j] + du[2] * dXdxdXdx_T[2][j]);
    }
  }  // End of Quadrature Point Loop
  return 0;
}

// -----------------------------------------------------------------------------
// Jacobian evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Jacobian_CEED_BP3)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[CEED_Q_VLA]     = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*vg)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3] = {ug[0][i], ug[1][i], ug[2][i]};

    // Read q_data (dXdxdXdx_T symmetric matrix)
    const CeedScalar dXdxdXdx_T[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[2][i], q_data[4][i], q_data[5][i]},
        {q_data[3][i], q_data[5][i], q_data[6][i]}
    };

    for (CeedInt j = 0; j < 3; j++) {  // j = direction of vg
      vg[j][i] = (du[0] * dXdxdXdx_T[0][j] + du[1] * dXdxdXdx_T[1][j] + du[2] * dXdxdXdx_T[2][j]);
    }
  }  // End of Quadrature Point Loop
  return 0;
}

// -----------------------------------------------------------------------------
// Nodal diagnostic quantities
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Diagnostic_CEED_BP3)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *u                   = in[1];

  // Outputs
  CeedScalar *diagnostic = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Displacement
    diagnostic[i] = u[i] * wdetJ;
  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_ceed_bp3_qf_h
