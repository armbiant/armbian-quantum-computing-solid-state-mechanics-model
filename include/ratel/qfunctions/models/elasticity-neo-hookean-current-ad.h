/// @file
/// Ratel Neo-Hookean hyperelasticity at finite strain in current configuration QFunction source

#ifndef ratel_elasticity_neo_hookean_current_qf_h
#define ratel_elasticity_neo_hookean_current_qf_h

#include <math.h>

#include "../../models/neo-hookean.h"
#include "../utils.h"
#include "elasticity-common.h"
#include "elasticity-neo-hookean-common.h"

#define UnpackArrayQ(x_packed, x) \
  for (int j = 0; j < sizeof(x_packed) / sizeof(x_packed[0]); j++) x[j][i] = x_packed[j];

#define PackArrayQ(x, x_packed) \
  for (int j = 0; j < sizeof(x_packed) / sizeof(x_packed[0]); j++) x_packed[j] = x[j][i];

// ----------------------------------------------------------------------------
// Strain Energy Function
// ----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar StrainEnergy(CeedScalar e[6], CeedScalar lambda, CeedScalar mu) {
  // J and log(J)
  CeedScalar e2[6];
  for (CeedInt i = 0; i < 6; i++) e2[i] = 2 * e[i];
  CeedScalar detbm1 = RatelVoigtDetAM1(e2);
  CeedScalar J      = sqrt(detbm1 + 1);
  CeedScalar logJ   = RatelLog1pSeries(detbm1) / 2.;

  // trace(e)
  CeedScalar trace_e = RatelVoigtTrace(e);

  return lambda * (J * J - 1) / 4 - lambda * logJ / 2 + mu * (-logJ + trace_e);
};

// -----------------------------------------------------------------------------
//  Compute Kirchhoff stress
// -----------------------------------------------------------------------------
// -- Enzyme-AD
void       __enzyme_autodiff(void *, ...);
void       __enzyme_fwddiff(void *, ...);
extern int enzyme_const;

void *__enzyme_function_like[2] = {(void *)RatelLog1pSeries, (void *)"log1p"};

CEED_QFUNCTION_HELPER void Kirchhofftau_Voigt_NeoHookean_AD(const CeedScalar lambda, const CeedScalar mu, CeedScalar e_Voigt[6],
                                                            CeedScalar tau_Voigt[6]) {
  // dPsi / de
  CeedScalar dPsi_Voigt[6] = {0.};
  __enzyme_autodiff((void *)StrainEnergy, e_Voigt, dPsi_Voigt, enzyme_const, lambda, enzyme_const, mu);
  for (CeedInt i = 3; i < 6; i++) dPsi_Voigt[i] /= 2.;

  // b = 2 e + I
  CeedScalar b_Voigt[6];
  for (CeedInt j = 0; j < 6; j++) b_Voigt[j] = 2 * e_Voigt[j] + (j < 3);

  // tau = (dPsi / de) b
  CeedScalar dPsi[3][3], b[3][3], tau[3][3];
  RatelVoigtUnpack(dPsi_Voigt, dPsi);
  RatelVoigtUnpack(b_Voigt, b);
  RatelMatMatMult(1., dPsi, b, tau);
  RatelVoigtPack(tau, tau_Voigt);
}

CEED_QFUNCTION_HELPER void dtau_fwd(const CeedScalar lambda, const CeedScalar mu, CeedScalar e_Voigt[6], CeedScalar de_Voigt[6],
                                    CeedScalar tau_Voigt[6], CeedScalar dtau_Voigt[6]) {
  __enzyme_fwddiff((void *)Kirchhofftau_Voigt_NeoHookean_AD, enzyme_const, lambda, enzyme_const, mu, e_Voigt, de_Voigt, tau_Voigt, dtau_Voigt);
}

// -----------------------------------------------------------------------------
// Compute Kirchhoff stress
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int P_NeoHookeanCurrent_AD(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                                 const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar P[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*dXdx_stored)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[1];
  CeedScalar(*e_stored)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[2];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;
  const CeedScalar         TwoMu   = E / (1 + nu);
  const CeedScalar         mu      = TwoMu / 2;
  const CeedScalar         K_bulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar         lambda  = (3 * K_bulk - TwoMu) / 3;

  // Formulation Terminology:
  //  I     : 3x3 Identity matrix
  //  b     : left Cauchy-Green tensor
  //  F     : deformation gradient
  //  tau   : Kirchhoff stress (in current config)
  // Formulation:
  //  F =  I + Grad_ue
  //  J = det(F)
  //  b = F*F^{T}
  //  tau = mu*(b-I) +  lambda*logJ*I;

  // Read spatial derivatives of u
  const CeedScalar du[3][3] = {
      {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
      {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
      {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
  };

  // -- Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  const CeedScalar dXdx_initial[3][3] = {
      {q_data[1][i], q_data[6][i], q_data[5][i]},
      {q_data[9][i], q_data[2][i], q_data[4][i]},
      {q_data[8][i], q_data[7][i], q_data[3][i]}
  };
  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // Grad_u = du/dx_initial = du/dX * dX/dx_initial
  CeedScalar Grad_u[3][3];
  RatelMatMatMult(1.0, du, dXdx_initial, Grad_u);

  // Compute the Deformation Gradient : F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  // Compute F^{-1}
  const CeedScalar Jm1  = RatelMatDetAM1(Grad_u);
  const CeedScalar detF = Jm1 + 1.;
  CeedScalar       F_inv_Voigt[9];
  RatelMatComputeInverseNonSymmetric(F, detF, F_inv_Voigt);
  CeedScalar F_inv[3][3];
  RatelVoigtUnpackNonSymmetric(F_inv_Voigt, F_inv);

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatMatMultAtQuadraturePoint(Q, i, 1.0, dXdx_initial, F_inv, dXdx_stored);
  RatelMatPackAtQuadraturePoint(Q, i, dXdx_stored, dXdx);

  // Compute Green Euler Strain tensor (e)
  CeedScalar e_Voigt[6];
  GreenEulerStrain(Grad_u, e_Voigt);

  // Compute Kirchhoff (tau) with Enzyme-AD
  CeedScalar tau_Voigt[6];
  Kirchhofftau_Voigt_NeoHookean_AD(lambda, mu, e_Voigt, tau_Voigt);
  RatelVoigtUnpack(tau_Voigt, P);

  // Store
  UnpackArrayQ(e_Voigt, e_stored);

  return 0;
}

// -----------------------------------------------------------------------------
// Compute linearization of Kirchhoff stress
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int dP_NeoHookeanCurrent_AD(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                                  const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dP[3][3]) {
  // Inputs
  const CeedScalar(*delta_ug)[3][CEED_Q_VLA]    = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*dXdx_stored)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[stored_fields_input_offset + 0];
  const CeedScalar(*e_stored)[CEED_Q_VLA]       = (const CeedScalar(*)[CEED_Q_VLA])in[stored_fields_input_offset + 1];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Constants
  const CeedScalar TwoMu  = E / (1 + nu);
  const CeedScalar mu     = TwoMu / 2;
  const CeedScalar K_bulk = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar lambda = (3 * K_bulk - TwoMu) / 3;

  // Read spatial derivatives of delta_u
  const CeedScalar delta_du[3][3] = {
      {delta_ug[0][0][i], delta_ug[1][0][i], delta_ug[2][0][i]},
      {delta_ug[0][1][i], delta_ug[1][1][i], delta_ug[2][1][i]},
      {delta_ug[0][2][i], delta_ug[1][2][i], delta_ug[2][2][i]}
  };

  // Retreive dXdx
  RatelMatPackAtQuadraturePoint(Q, i, dXdx_stored, dXdx);

  // Compute grad_du = \nabla_x (deltau) = deltau * dX/dx
  CeedScalar grad_du[3][3];
  RatelMatMatMult(1.0, delta_du, dXdx, grad_du);

  // Compute b = 2 e + I
  CeedScalar e_Voigt[6], b_Voigt[6], b[3][3];
  PackArrayQ(e_stored, e_Voigt);
  for (CeedInt j = 0; j < 6; j++) b_Voigt[j] = 2 * e_Voigt[j] + (j < 3);
  RatelVoigtUnpack(b_Voigt, b);

  // Compute de = db / 2 = (grad_du b + b grad_du') / 2
  CeedScalar de[3][3], de_Voigt[6];
  RatelMatMatMultPlusMatMatTransposeMult(0.5, grad_du, b, b, grad_du, de);
  RatelVoigtPack(de, de_Voigt);

  // Compute dtau with Enzyme-AD
  CeedScalar tau_Voigt[6], dtau_Voigt[6], tau[3][3], dtau[3][3];
  dtau_fwd(lambda, mu, e_Voigt, de_Voigt, tau_Voigt, dtau_Voigt);
  RatelVoigtUnpack(tau_Voigt, tau);
  RatelVoigtUnpack(dtau_Voigt, dtau);

  // Compute tau_grad_du = tau * grad_du
  CeedScalar tau_grad_du[3][3];
  RatelMatMatTransposeMult(1.0, tau, grad_du, tau_grad_du);

  // Compute dp = dtau - tau * grad_du
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      dP[j][k] = dtau[j][k] - tau_grad_du[j][k];
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Residual evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityResidual_NeoHookeanCurrent_AD)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, P_NeoHookeanCurrent_AD, in, out);
}

// -----------------------------------------------------------------------------
// Jacobian evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanCurrent_AD)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, dP_NeoHookeanCurrent_AD, in, out);
}
// -----------------------------------------------------------------------------

#endif  // ratel_elasticity_neo_hookean_current_qf_h
