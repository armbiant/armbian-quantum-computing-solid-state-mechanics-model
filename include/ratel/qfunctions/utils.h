/// @file
/// Ratel utility helpers QFunction source

#ifndef ratel_utils_qf_h
#define ratel_utils_qf_h

#include <math.h>

#define RATEL_PI_DOUBLE 3.14159265358979323846

// -----------------------------------------------------------------------------
// Series approximation of log1p()
//  log1p() is not vectorized in libc

// The series expansion up to the sixth term is accurate to 1e-10 relative error in the range 2/3 < J < 3/2.
// This is accurate enough for many hyperelastic applications, but perhaps not for materials like foams that may encounter volumetric strains on the
// order of 1/5 < J < 5.
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelLog1pSeries(CeedScalar x) {
  CeedScalar sum = 0;
  CeedScalar y   = x / (2. + x);
  CeedScalar y2  = y * y;
  sum += y;
  for (CeedInt i = 0; i < 5; i++) {
    y *= y2;
    sum += y / (2 * i + 3);
  }
  return 2 * sum;
}

// -----------------------------------------------------------------------------
// Dot two vectors a . b
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelDot3(const CeedScalar a[3], const CeedScalar b[3]) { return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]; }

// -----------------------------------------------------------------------------
// Compute vector norm
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelNorm3(const CeedScalar a[3]) { return sqrt(RatelDot3(a, a)); }

// -----------------------------------------------------------------------------
// Trace of a matrix
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelTrace3(const CeedScalar A[3][3]) { return A[0][0] + A[1][1] + A[2][2]; }

// -----------------------------------------------------------------------------
// Compute alpha * A * B = C
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatMatMult(const CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3], CeedScalar C[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k] += alpha * A[j][m] * B[m][k];
      }
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Compute alpha * A * B^T = C
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatMatTransposeMult(const CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3], CeedScalar C[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k] += alpha * A[j][m] * B[k][m];
      }
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Compute alpha * A * B^T = C
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatMatMultAtQuadraturePoint(const CeedInt Q, const CeedInt i, const CeedScalar alpha, const CeedScalar A[3][3],
                                                           const CeedScalar B[3][3], CeedScalar C[3][3][CEED_Q_VLA]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k][i] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k][i] += alpha * A[j][m] * B[m][k];
      }
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Compute alpha * A * B^T = C
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatMatTransposeMultAtQuadraturePoint(const CeedInt Q, const CeedInt i, const CeedScalar alpha,
                                                                    const CeedScalar A[3][3], const CeedScalar B[3][3],
                                                                    CeedScalar C[3][3][CEED_Q_VLA]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k][i] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k][i] += alpha * A[j][m] * B[k][m];
      }
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Compute A * B + C * D = E
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatMatMultPlusMatMatMult(const CeedScalar A[3][3], const CeedScalar B[3][3], const CeedScalar C[3][3],
                                                        const CeedScalar D[3][3], CeedScalar E[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      E[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        E[j][k] += A[j][m] * B[m][k] + C[j][m] * D[m][k];
      }
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Compute alpha * (A * B + C * D') = E
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatMatMultPlusMatMatTransposeMult(const CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3],
                                                                 const CeedScalar C[3][3], const CeedScalar D[3][3], CeedScalar E[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      E[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        E[j][k] += alpha * (A[j][m] * B[m][k] + C[j][m] * D[k][m]);
      }
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Compute alpha * A : B = c
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelMatMatContract(const CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3]) {
  CeedScalar c = 0;
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      c += alpha * A[j][k] * B[j][k];
    }
  }

  return c;
}

// -----------------------------------------------------------------------------
// Compute det(F) - 1
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelMatDetAM1(const CeedScalar A[3][3]) {
  return A[0][0] * (A[1][1] * A[2][2] - A[1][2] * A[2][1]) +         /* *NOPAD* */
         A[0][1] * (A[1][2] * A[2][0] - A[1][0] * A[2][2]) +         /* *NOPAD* */
         A[0][2] * (A[1][0] * A[2][1] - A[2][0] * A[1][1]) +         /* *NOPAD* */
         A[0][0] + A[1][1] + A[2][2] +                               /* *NOPAD* */
         A[0][0] * A[1][1] + A[0][0] * A[2][2] + A[1][1] * A[2][2] - /* *NOPAD* */
         A[0][1] * A[1][0] - A[0][2] * A[2][0] - A[1][2] * A[2][1];  /* *NOPAD* */
}

// -----------------------------------------------------------------------------
// Compute A^-1, where A is symmetric, returns array with 6 elements in Voigt notation
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatComputeInverseSymmetric(const CeedScalar A[3][3], const CeedScalar det_A, CeedScalar A_inv[6]) {
  // Compute A^(-1) : A-Inverse
  CeedScalar B[6] = {
      A[1][1] * A[2][2] - A[1][2] * A[2][1], /* *NOPAD* */
      A[0][0] * A[2][2] - A[0][2] * A[2][0], /* *NOPAD* */
      A[0][0] * A[1][1] - A[0][1] * A[1][0], /* *NOPAD* */
      A[0][2] * A[1][0] - A[0][0] * A[1][2], /* *NOPAD* */
      A[0][1] * A[1][2] - A[0][2] * A[1][1], /* *NOPAD* */
      A[0][2] * A[2][1] - A[0][1] * A[2][2]  /* *NOPAD* */
  };
  for (CeedInt m = 0; m < 6; m++) {
    A_inv[m] = B[m] / (det_A);
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Compute A^-1, where A is non-symmetric, returns array with 9 elements
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatComputeInverseNonSymmetric(const CeedScalar A[3][3], const CeedScalar det_A, CeedScalar A_inv[9]) {
  // Compute A^(-1) : A-Inverse
  CeedScalar B[9] = {
      A[1][1] * A[2][2] - A[1][2] * A[2][1], /* *NOPAD* */
      A[0][0] * A[2][2] - A[0][2] * A[2][0], /* *NOPAD* */
      A[0][0] * A[1][1] - A[0][1] * A[1][0], /* *NOPAD* */
      A[0][2] * A[1][0] - A[0][0] * A[1][2], /* *NOPAD* */
      A[0][1] * A[1][2] - A[0][2] * A[1][1], /* *NOPAD* */
      A[0][2] * A[2][1] - A[0][1] * A[2][2], /* *NOPAD* */
      A[0][1] * A[2][0] - A[0][0] * A[2][1], /* *NOPAD* */
      A[1][0] * A[2][1] - A[1][1] * A[2][0], /* *NOPAD* */
      A[1][2] * A[2][0] - A[1][0] * A[2][2]  /* *NOPAD* */
  };
  for (CeedInt m = 0; m < 9; m++) {
    A_inv[m] = B[m] / (det_A);
  }

  return 0;
}

// -----------------------------------------------------------------------------
//  Create symmetric entries from full tensor
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void RatelVoigtPack(const CeedScalar full[3][3], CeedScalar sym[6]) {
  sym[0] = full[0][0];
  sym[1] = full[1][1];
  sym[2] = full[2][2];
  sym[3] = full[1][2];
  sym[4] = full[0][2];
  sym[5] = full[0][1];
}

// -----------------------------------------------------------------------------
//  Create full tensor from symmetric entries
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void RatelVoigtUnpack(const CeedScalar sym[6], CeedScalar full[3][3]) {
  full[0][0] = sym[0];
  full[0][1] = sym[5];
  full[0][2] = sym[4];
  full[1][0] = sym[5];
  full[1][1] = sym[1];
  full[1][2] = sym[3];
  full[2][0] = sym[4];
  full[2][1] = sym[3];
  full[2][2] = sym[2];
}

// -----------------------------------------------------------------------------
// Mass application helper
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int ScaledMassApplyAtQuadraturePoint(const CeedInt Q, const CeedInt i, const CeedInt num_comp, const CeedScalar scaling,
                                                           const CeedScalar *u, CeedScalar *v) {
  for (CeedInt c = 0; c < num_comp; c++) {
    v[c * Q + i] = scaling * u[c * Q + i];
  }
  return 0;
}

// -----------------------------------------------------------------------------
// Trace of a Voigt matrix
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelVoigtTrace(CeedScalar V[6]) { return V[0] + V[1] + V[2]; }

// -----------------------------------------------------------------------------
// Computes det(V) - 1
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelVoigtDetAM1(const double V[6]) {
  return V[0] * (V[1] * V[2] - V[3] * V[3]) +      /* *NOPAD* */
         V[5] * (V[3] * V[4] - V[5] * V[2]) +      /* *NOPAD* */
         V[4] * (V[5] * V[3] - V[4] * V[1]) +      /* *NOPAD* */
         V[0] + V[1] + V[2] +                      /* *NOPAD* */
         V[0] * V[1] + V[0] * V[2] + V[1] * V[2] - /* *NOPAD* */
         V[5] * V[5] - V[4] * V[4] - V[3] * V[3];  /* *NOPAD* */
}

// -----------------------------------------------------------------------------
//  Pack matrix at quadrature point
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelMatPackAtQuadraturePoint(const CeedInt Q, const CeedInt i, const CeedScalar Aq[3][3][CEED_Q_VLA], CeedScalar A[3][3]) {
  A[0][0] = Aq[0][0][i];
  A[0][1] = Aq[0][1][i];
  A[0][2] = Aq[0][2][i];
  A[1][0] = Aq[1][0][i];
  A[1][1] = Aq[1][1][i];
  A[1][2] = Aq[1][2][i];
  A[2][0] = Aq[2][0][i];
  A[2][1] = Aq[2][1][i];
  A[2][2] = Aq[2][2][i];

  return 0;
}

// -----------------------------------------------------------------------------
//  Pack matrix at quadrature point
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int RatelVoigtUnpackQdata(const CeedInt Q, const CeedInt i, const CeedScalar q_data[10][CEED_Q_VLA], CeedScalar full[3][3]) {
  full[0][0] = q_data[1][i];
  full[0][1] = q_data[6][i];
  full[0][2] = q_data[5][i];
  full[1][0] = q_data[9][i];
  full[1][1] = q_data[2][i];
  full[1][2] = q_data[4][i];
  full[2][0] = q_data[8][i];
  full[2][1] = q_data[7][i];
  full[2][2] = q_data[3][i];

  return 0;
}

// -----------------------------------------------------------------------------
// Compute determinant of matrix A
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar RatelComputeDetA(const CeedScalar A[3][3]) {
  // Compute det(A)
  const CeedScalar B11 = A[1][1] * A[2][2] - A[1][2] * A[2][1];
  const CeedScalar B12 = A[0][2] * A[2][1] - A[0][1] * A[2][2];
  const CeedScalar B13 = A[0][1] * A[1][2] - A[0][2] * A[1][1];
  return A[0][0] * B11 + A[1][0] * B12 + A[2][0] * B13;
};

// -----------------------------------------------------------------------------
//  Create full tensor from non-symmetric entries
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void RatelVoigtUnpackNonSymmetric(const CeedScalar nonsym[9], CeedScalar full[3][3]) {
  full[0][0] = nonsym[0];
  full[0][1] = nonsym[5];
  full[0][2] = nonsym[4];
  full[1][0] = nonsym[8];
  full[1][1] = nonsym[1];
  full[1][2] = nonsym[3];
  full[2][0] = nonsym[7];
  full[2][1] = nonsym[6];
  full[2][2] = nonsym[2];
}

// -----------------------------------------------------------------------------
//  Compute e = (b - I)/2 = (grad_u + grad_u^T + grad_u*grad_u^T)/2
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void GreenEulerStrain(const CeedScalar grad_u[3][3], CeedScalar e[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  for (CeedInt m = 0; m < 6; m++) {
    e[m] = (grad_u[ind_j[m]][ind_k[m]] + grad_u[ind_k[m]][ind_j[m]]) * 0.5;
    for (CeedInt n = 0; n < 3; n++) {
      e[m] += (grad_u[ind_j[m]][n] * grad_u[ind_k[m]][n]) * 0.5;
    }
  }
}

// -----------------------------------------------------------------------------
//  Compute E = (C - I)/2 = (grad_u + grad_u^T + grad_u^T*grad_u)/2
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void GreenLagrangeStrain(const CeedScalar grad_u[3][3], CeedScalar E[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  for (CeedInt m = 0; m < 6; m++) {
    E[m] = (grad_u[ind_j[m]][ind_k[m]] + grad_u[ind_k[m]][ind_j[m]]) * 0.5;
    for (CeedInt n = 0; n < 3; n++) {
      E[m] += (grad_u[n][ind_j[m]] * grad_u[n][ind_k[m]]) * 0.5;
    }
  }
}

#endif  // ratel_utils_qf_h
