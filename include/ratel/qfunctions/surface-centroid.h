/// @file
/// Ratel surface centroid computation QFunction source

#ifndef ratel_surface_centroid_qf_h
#define ratel_surface_centroid_qf_h

#include "utils.h"

// -----------------------------------------------------------------------------
// This QFunction computes the centroid of a face
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ComputeCentroid)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*x)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*centroid)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Centroid
    for (CeedInt j = 0; j < dim; j++) centroid[j][i] = x[j][i] * q_data[0][i];
  }  // End of Quadrature Point Loop

  // Return
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_surface_centroid_qf_h
