/// @file
/// Ratel platen boundary condition QFunction source

#ifndef ratel_platen_boundary_qf_h
#define ratel_platen_boundary_qf_h

#include "../models/elasticity-common.h"

// platen boundary context
#ifndef bc_platen_data
#define bc_platen_data
typedef struct {
  CeedScalar  normal[3];
  CeedScalar  center[3];
  CeedScalar  distance;
  CeedScalar  time;
  CeedScalar  final_time;
  CeedScalar  gamma;     // Nitche's method parameter
  CeedScalar *material;  // pointer to material data
} BCPlatenData;
#endif

// -----------------------------------------------------------------------------
// This QFunction computes the surface integral of the user platen vector on
//   the constrained faces.
// Updated to support initial gap using https://hal.archives-ouvertes.fr/hal-00722114/document
//
// Computed:
//   (P_1_gamma < 0) ? (P_1_gamma * n_platen * (w detNb)) : 0
// where P_1_gamma = sigma_N - gamma*(u_N - g)
//         sigma_N = -(P·n)·n_platen   surface force away from platen
//             u_N = -u·n_platen       displacement away from platen
//               g = (X - C)·n_platen  gap between platen and surface in initial config
//
// -----------------------------------------------------------------------------
int CEED_QFUNCTION_HELPER ApplyPlatenBCs(void *ctx, CeedInt Q, RatelComputeFirstPiolaKirchhoff compute_P, CeedInt num_stored_fields,
                                         const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*x)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar *P_1_gamma      = (CeedScalar *)out[num_stored_fields + 1];

  // User context
  const BCPlatenData *context = (BCPlatenData *)ctx;
  const CeedScalar    s       = context->time / context->final_time;

  // Compute current center
  CeedScalar current_center[3];
  for (CeedInt j = 0; j < 3; j++) {
    current_center[j] = context->center[j] + s * context->distance * context->normal[j];
  }

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar    dXdx[3][3], P[3][3];
    const CeedInt stored_fields_input_offset = 2;
    compute_P((void *)&context->material, Q, i, stored_fields_input_offset, in, out, dXdx, P);

    // Normal
    const CeedScalar normal[3] = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Compute u_N = -u . n_platen
    const CeedScalar ui[3] = {u[0][i], u[1][i], u[2][i]};
    const CeedScalar u_N   = -RatelDot3(ui, context->normal);

    // Initial Gap
    const CeedScalar overlap_vec[3] = {
        x[0][i] - current_center[0],
        x[1][i] - current_center[1],
        x[2][i] - current_center[2],
    };
    // And compute gap as magnitude of projection of above vector onto face normal
    const CeedScalar g              = RatelDot3(context->normal, overlap_vec);
    const CeedScalar u_N_minus_g    = u_N - g;
    CeedScalar       sigma_dot_n[3] = {0., 0., 0.};
    CeedScalar       sigma_N        = 0.0;

    // Compute boundary traction
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        sigma_dot_n[j] += P[j][k] * normal[k];
      }
      sigma_N += -sigma_dot_n[j] * context->normal[j];
    }

    P_1_gamma[i] = sigma_N - context->gamma * u_N_minus_g;

    for (CeedInt j = 0; j < 3; j++) {
      v[j][i] = (P_1_gamma[i] < 0) * P_1_gamma[i] * context->normal[j] * q_data[0][i];
    }
  }  // End of Quadrature Point Loop

  // Return
  return 0;
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// This qfunction computes the incremental traction due to contact BC
//
// Computed:
//   (P_1_gamma < 0) ? ((dsigma_N - gamma * du_N) * n_platen * (w detNb)) : 0
// where dsigma_N = -(dP·n)·n_platen  incremental surface force away from platen
//           du_N = -du·n_platen      incremental displacement away from platen
//
// -----------------------------------------------------------------------------
int CEED_QFUNCTION_HELPER ApplyPlatenBCs_Jacobian(void *ctx, CeedInt Q, RatelComputeFirstPiolaKirchhoff compute_dP, const CeedScalar *const *in,
                                                  CeedScalar *const *out) {
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*delta_u)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar *P_1_gamma            = (const CeedScalar *)in[3];
  CeedScalar(*delta_v)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const BCPlatenData *context      = (BCPlatenData *)ctx;
  const CeedScalar   *phys_context = (CeedScalar *)&context->material;

  // Get P_1_gamma
  const CeedInt stored_fields_input_offset = 4;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation and linearized First Piola-Kirchhoff
    CeedScalar dXdx[3][3], dP[3][3];
    compute_dP((void *)phys_context, Q, i, stored_fields_input_offset, in, out, dXdx, dP);

    // Normal
    const CeedScalar normal[3] = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Compute du_N = -du . n_platen
    const CeedScalar du[3] = {delta_u[0][i], delta_u[1][i], delta_u[2][i]};

    const CeedScalar du_N = -RatelDot3(du, context->normal);

    // Apply dXdx ^ T and weight
    CeedScalar dsigma_dot_n[3];

    // Compute delta boundary traction
    for (CeedInt j = 0; j < 3; j++) {
      dsigma_dot_n[j] = RatelDot3(dP[j], normal);
    }
    const CeedScalar dsigma_N = -RatelDot3(dsigma_dot_n, context->normal);

    for (CeedInt j = 0; j < 3; j++) {
      // only evaluate where P_1_gamma was negative, otherwise uniformly zero
      delta_v[j][i] = (P_1_gamma[i] < 0) * (dsigma_N - context->gamma * du_N) * context->normal[j] * wdetJ;
    }
  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------
#endif  // ratel_platen_boundary_qf_h
