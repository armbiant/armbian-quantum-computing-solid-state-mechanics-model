/// @file
/// Ratel surface diagnostic geometric factors computation QFunction source

#ifndef ratel_surface_diagnostic_geometry_qf_h
#define ratel_surface_diagnostic_geometry_qf_h

#include "../utils.h"

// -----------------------------------------------------------------------------
// This QFunction sets up the geometric factors required for integration and coordinate transformations as well as gradient transformation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(SetupSurfaceForceGeometry)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J_cell)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*J_face)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*w)                     = in[2];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    // N_1, N_2, and N_3 are given by the cross product of the columns of dxdX
    CeedScalar normal[3];
    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = J_face[0][j+1]*J_face[1][j+2] - J_face[0][j+2]*J_face[1][j+1]
      normal[j] = J_face[0][(j + 1) % dim][i] * J_face[1][(j + 2) % dim][i] - J_face[0][(j + 2) % dim][i] * J_face[1][(j + 1) % dim][i];
    }
    const CeedScalar detJ_face  = RatelNorm3(normal);
    // Gradient
    const CeedScalar dxdX[3][3] = {
        {J_cell[0][0][i], J_cell[1][0][i], J_cell[2][0][i]},
        {J_cell[0][1][i], J_cell[1][1][i], J_cell[2][1][i]},
        {J_cell[0][2][i], J_cell[1][2][i], J_cell[2][2][i]}
    };
    const CeedScalar detJ_cell = RatelComputeDetA(dxdX);
    CeedScalar       dXdx_voigt[9];
    RatelMatComputeInverseNonSymmetric(dxdX, detJ_cell, dXdx_voigt);
    // Qdata
    // -- Interp-to-Interp q_data
    q_data[0][i] = w[i] * detJ_face;
    for (CeedInt j = 0; j < 9; j++) {
      q_data[j + 1][i] = dXdx_voigt[j];
    }
    // -- Normal vector
    for (CeedInt j = 0; j < 3; j++) q_data[j + 10][i] = normal[j] / detJ_face;

  }  // End of Quadrature Point Loop

  // Return
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_surface_diagnostic_geometry_qf_h
