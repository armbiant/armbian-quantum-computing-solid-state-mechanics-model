/// @file
/// Ratel error computation linear elasticity manufactured solution QFunction source

#ifndef ratel_linear_manufactured_error_qf_h
#define ratel_linear_manufactured_error_qf_h

#include <math.h>

// -----------------------------------------------------------------------------
// True solution for linear elasticity manufactured solution
// -----------------------------------------------------------------------------
CEED_QFUNCTION(MMSError_Linear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*error)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar x = coords[0][i], y = coords[1][i], z = coords[2][i];
    const CeedScalar wdetJ = q_data[0][i];

    // Error
    CeedScalar true_u = exp(2 * x) * sin(3 * y) * cos(4 * z) / 1e8;
    error[0][i]       = (true_u - u[0][i]) * (true_u - u[0][i]) * wdetJ;
    true_u            = exp(3 * y) * sin(4 * z) * cos(2 * x) / 1e8;
    error[1][i]       = (true_u - u[1][i]) * (true_u - u[1][i]) * wdetJ;
    true_u            = exp(4 * z) * sin(2 * x) * cos(3 * y) / 1e8;
    error[2][i]       = (true_u - u[2][i]) * (true_u - u[2][i]) * wdetJ;
  }  // End of Quadrature Point Loop
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_linear_manufactured_error_qf_h
