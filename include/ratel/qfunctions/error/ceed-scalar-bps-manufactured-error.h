/// @file
/// Ratel error computation CEED scalar BPs manufactured solution QFunction source

#ifndef ratel_ceed_scalar_bps_manufactured_error_qf_h
#define ratel_ceed_scalar_bps_manufactured_error_qf_h

#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// -----------------------------------------------------------------------------
// True solution for CEED scalar BPs manufactured solution
// -----------------------------------------------------------------------------
CEED_QFUNCTION(MMSError_CEED_ScalarBPs)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar *u                   = in[2];

  // Outputs
  CeedScalar *error = out[0];

  // Scaling
  const CeedScalar c[3] = {0., 1., 2.};
  const CeedScalar k[3] = {1., 2., 3.};

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar x = coords[0][i], y = coords[1][i], z = coords[2][i];
    const CeedScalar wdetJ = q_data[0][i];

    const CeedScalar true_solution = sin(M_PI * (c[0] + k[0] * x)) * sin(M_PI * (c[1] + k[1] * y)) * sin(M_PI * (c[2] + k[2] * z));

    error[i] = (u[i] - true_solution) * (u[i] - true_solution) * wdetJ;
  }  // End of Quadrature Point Loop
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_ceed_scalar_bps_manufactured_error_qf_h
