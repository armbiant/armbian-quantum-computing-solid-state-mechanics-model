#ifndef ratel_impl_h
#define ratel_impl_h

#ifdef __clang_analyzer__
#define RATEL_INTERN
#else
#define RATEL_INTERN RATEL_EXTERN __attribute__((visibility("hidden")))
#endif

#include <ceed.h>
#include <petsc.h>
#include <ratel.h>

RATEL_INTERN const char *RatelJitSourceRootDefault;

#define RatelQFunctionRelativePath(absolute_path) strstr(absolute_path, "ratel/qfunctions")

#if !(CEED_VERSION_GE(0, 11, 0))
#error "libCEED v0.11.0 or later is required"
#endif

#define RatelCeedChk(ratel, ierr)                                   \
  do {                                                              \
    if (ierr != CEED_ERROR_SUCCESS) {                               \
      const char *error_message;                                    \
      CeedGetErrorMessage((ratel)->ceed, &error_message);           \
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "%s", error_message); \
    }                                                               \
  } while (0);

#define RatelCeedCall(ratel, ...)         \
  do {                                    \
    PetscErrorCode ierr_q_ = __VA_ARGS__; \
    RatelCeedChk(ratel, ierr_q_);         \
  } while (0);

#define RATEL_MAX_MATERIALS 16
#define RATEL_MAX_FIELDS 16
#define RATEL_MAX_BOUNDARY_FACES 16
#define RATEL_MAX_MULTIGRID_LEVELS 16
#define RATEL_MAX_MATERIAL_LABEL_VALUES 128
#define RATEL_CHECKPOINT_VERSION 0XCEEDF00
#define RATEL_UNSET_INITIAL_TIME -42.0

/// RATEL_DEBUG_COLOR default value, forward RatelDebug* declarations & macros
#define RATEL_DEBUG_NO_COLOR 255
#define RATEL_DEBUG_RED 196
#define RATEL_DEBUG_ORANGE 208
#define RATEL_DEBUG_GREEN 108
RATEL_INTERN void RatelDebugImpl256(const Ratel, PetscBool all_ranks, const unsigned char, const char *, ...);
#define RatelDebug256(ratel, color, ...) RatelDebugImpl256(ratel, false, color, __VA_ARGS__)
#define RatelDebug(ratel, ...) RatelDebug256(ratel, (unsigned char)RATEL_DEBUG_NO_COLOR, __VA_ARGS__)
#define RatelParallelDebug256(ratel, color, ...) RatelDebugImpl256(ratel, PETSC_TRUE, color, __VA_ARGS__)
#define RatelParallelDebug(ratel, ...) RatelParallelDebug256(ratel, (unsigned char)RATEL_DEBUG_NO_COLOR, __VA_ARGS__)

// Default value option
#define RATEL_DECIDE -42

// Units
struct RatelUnits_private {
  // Fundamental units
  PetscScalar meter;
  PetscScalar kilogram;
  PetscScalar second;
  // Derived unit
  PetscScalar Pascal;
};

typedef PetscErrorCode RatelBCFunction(PetscInt, PetscReal, const PetscReal *, PetscInt, PetscScalar *, void *);

// Material model
/// @ingroup RatelMaterials
typedef struct RatelModelData_private *RatelModelData;
struct RatelModelData_private {
  const char       *name;
  RatelModelType    type;
  CeedQFunctionUser setup_q_data_volume, setup_q_data_surface, setup_q_data_surface_grad, residual_u, residual_ut, residual_utt, jacobian, energy,
      diagnostic, surface_force, mms_forcing, mms_error, platen_residual_u, platen_jacobian;
  const char *setup_q_data_volume_loc, *setup_q_data_surface_loc, *setup_q_data_surface_grad_loc, *residual_u_loc, *residual_ut_loc,
      *residual_utt_loc, *jacobian_loc, *energy_loc, *diagnostic_loc, *surface_force_loc, *mms_forcing_loc, *mms_error_loc, *platen_residual_u_loc,
      *platen_jacobian_loc;
  CeedInt num_active_fields, num_comp_diagnostic, q_data_volume_size, q_data_surface_size, q_data_surface_grad_size, num_quadrature_fields,
      num_state_fields;
  const char *const *active_field_names;
  const PetscInt    *active_field_sizes;
  CeedEvalMode       active_field_eval_modes[RATEL_MAX_FIELDS];
  const char *const *diagnostic_field_names;
  const char *const *state_field_names;
  const char *const *quadrature_field_names;
  CeedInt           *quadrature_field_sizes;
  CeedQuadMode       quadrature_mode;
  PetscBool          has_ut_term;
  PetscLogDouble     flops_qf_jacobian_u, flops_qf_jacobian_ut, flops_qf_jacobian_utt;
  RatelBCFunction   *BoundaryMMS[RATEL_MAX_FIELDS];
};

// Material region
/// @ingroup RatelMaterials
typedef struct RatelMaterial_private *RatelMaterial;
struct RatelMaterial_private {
  Ratel       ratel;
  const char *name;
  // Mesh
  PetscBool is_on_process, is_on_process_set;
  PetscInt  num_volume_label_values;
  PetscInt *volume_label_values;
  char     *volume_label_name;
  PetscInt  num_face_orientations;
  PetscInt  num_surface_grad_dm_face_ids, num_surface_grad_diagnostic_dm_face_ids;
  PetscInt *surface_grad_dm_face_ids, *surface_grad_diagnostic_dm_face_ids;
  char    **surface_grad_label_names, **surface_grad_diagnostic_label_names;
  // Model
  RatelModelData         model_data;
  CeedQFunctionContext   ctx_physics;
  PetscInt               num_physics_smoother_values;
  CeedScalar            *ctx_physics_values, *ctx_physics_smoother_values;
  char                 **ctx_physics_label_names;
  CeedContextFieldLabel *ctx_physics_labels;
  // Qdata
  CeedVector          q_data_volume, **q_data_surface_grad_diagnostic, **q_data_surface_grad;
  CeedElemRestriction restriction_q_data_volume, **restrictions_q_data_surface_grad_diagnostic, **restrictions_q_data_surface_grad;
  // Operators
  PetscInt residual_index, *jacobian_indices, num_jacobian_indices;
  // Methods
  // -- Geometry
  PetscErrorCode (*SetupVolumeQData)(RatelMaterial, const char *, PetscInt, CeedElemRestriction *, CeedVector *);
  PetscErrorCode (*SetupSurfaceGradientQData)(RatelMaterial, DM, const char *, PetscInt, CeedElemRestriction *, CeedVector *);
  // -- Residual and Jacobian operators
  PetscErrorCode (*SetupResidualJacobianSuboperators)(RatelMaterial, CeedOperator, CeedOperator, CeedOperator, CeedOperator);
  PetscErrorCode (**RatelMaterialSetupMultigridLevels)(RatelMaterial, PetscInt, CeedVector, CeedOperator, CeedOperator, CeedOperator, CeedOperator);
  // -- Output operators
  PetscErrorCode (*SetupEnergySuboperator)(RatelMaterial, CeedOperator);
  PetscErrorCode (*SetupDiagnosticSuboperators)(RatelMaterial, CeedOperator, CeedOperator);
  PetscErrorCode (*SetupSurfaceForceSuboperators)(RatelMaterial, CeedOperator *);
  PetscErrorCode (*SetupMMSErrorSuboperator)(RatelMaterial, CeedOperator);
  // -- Material dependent context
  PetscErrorCode (*RatelMaterialPlatenContextCreate)(RatelMaterial, CeedScalar *, CeedScalar *, CeedScalar, CeedScalar, CeedScalar,
                                                     CeedQFunctionContext *);
};

// PETSc operator contexts
typedef struct RatelOperatorApplyContext_private *RatelOperatorApplyContext;
struct RatelOperatorApplyContext_private {
  Ratel          ratel;
  char          *name;
  DM             dm_x, dm_y;
  Vec            X_loc, Y_loc;
  CeedVector     x_loc, y_loc;
  CeedOperator   op;
  PetscLogDouble flops;
};

typedef struct RatelProlongRestrictContext_private *RatelProlongRestrictContext;
struct RatelProlongRestrictContext_private {
  Ratel          ratel;
  char          *name;
  DM             dm_c, dm_f;
  Vec            X_loc_c, X_loc_f;
  CeedVector     x_loc_c, x_loc_f, y_loc_c, y_loc_f;
  CeedOperator   op_prolong, op_restrict;
  PetscLogDouble flops_prolong, flops_restrict;
  PetscLogEvent  event_prolong, event_restrict;
};

// Jacobian setup context
typedef struct RatelFormJacobianContext_private *RatelFormJacobianContext;
struct RatelFormJacobianContext_private {
  Ratel                     ratel;
  PetscInt                  num_levels;
  RatelOperatorApplyContext ctx_jacobian_fine;
  VecType                   vec_type;
  Mat                      *mat_jacobian, mat_jacobian_coarse;
  CeedVector                coo_values;
  CeedOperator              op_coarse;
};

// Ratel context
struct Ratel_private {
  MPI_Comm        comm;
  PetscInt        comm_rank;
  RatelSolverType solver_type;
  RatelModelType  model_type;
  PetscBool       is_debug;
  PetscBool       is_ceed_backend_gpu;
  // PETSc
  DM                        dm_orig, *dm_hierarchy, dm_state_fields, dm_energy, dm_diagnostic, dm_surface_displacement, dm_surface_force;
  KSP                       ksp_diagnostic_projection;
  Vec                       X_loc_residual_u;
  Mat                      *mat_jacobian, mat_jacobian_coarse, *mat_prolong_restrict;
  RatelOperatorApplyContext ctx_residual_u, ctx_residual_ut, ctx_residual_utt, *ctx_jacobian, ctx_energy, ctx_diagnostic, ctx_diagnostic_projection,
      ctx_surface_force, ctx_mms_error;
  RatelFormJacobianContext     ctx_form_jacobian;
  RatelProlongRestrictContext *ctx_prolong_restrict;
  PetscClassId                 libceed_class_id;
  PetscLogEvent                event_jacobian, *event_prolong, *event_restrict;
  PetscViewer                  monitor_viewer_strain_energy, monitor_viewer_surface_forces, monitor_viewer_diagnostic_quantities;
  char                        *monitor_checkpoint_file_name;
  PetscInt                     monitor_checkpoint_interval;
  PetscBool                    is_monitor_vtk;
  char                        *monitor_vtk_file_name, *monitor_vtk_file_extension;
  // Ceed
  char                 *ceed_resource;
  Ceed                  ceed;
  CeedOperator         *ops_surface_force, op_dirichlet;
  CeedContextFieldLabel op_dirichlet_time_label, op_residual_u_time_label, op_jacobian_time_label, op_jacobian_shift_v_label,
      op_jacobian_shift_a_label;
  CeedVector coo_values_ceed;
  // Materials
  PetscFunctionList material_create_functions;
  PetscInt          num_materials;
  RatelMaterial    *materials;
  char             *material_volume_label_name;
  RatelUnits        material_units;
  PetscBool         has_ut_term;
  // Forcing
  RatelForcingType forcing_choice;
  // Boundaries
  PetscInt    bc_clamp_count[RATEL_MAX_FIELDS];
  PetscInt    bc_clamp_nonzero_count;
  PetscInt    bc_clamp_faces[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_mms_count[RATEL_MAX_FIELDS];
  PetscInt    bc_mms_faces[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_slip_count[RATEL_MAX_FIELDS];
  PetscInt    bc_slip_faces[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_slip_components_count[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_slip_components[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES][3];
  PetscInt    bc_traction_count;
  PetscInt    bc_traction_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_platen_count;
  PetscInt    bc_platen_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscInt    surface_force_face_count;
  PetscInt    surface_force_dm_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscScalar surface_force_face_area[RATEL_MAX_BOUNDARY_FACES];
  PetscScalar surface_force_face_centroid[RATEL_MAX_BOUNDARY_FACES][3];
  // Initial condition
  RatelInitialConditionType initial_condition_type;
  char                     *continue_file_name;
  // Multigrid
  PetscBool          is_cl_multigrid_type;
  RatelMultigridType cl_multigrid_type;
  RatelMultigridType multigrid_type;
  PetscInt           num_active_fields, num_multigrid_levels, num_multigrid_levels_setup;
  PetscInt           multigrid_level_orders[RATEL_MAX_FIELDS][RATEL_MAX_MULTIGRID_LEVELS], multigrid_fine_orders[RATEL_MAX_FIELDS],
      multigrid_coarse_orders[RATEL_MAX_FIELDS];
  CeedInt         num_jacobian_multiplicity_skip_indices, *jacobian_multiplicity_skip_indices;
  PetscLogDouble *flops_jacobian, *flops_prolong, *flops_restrict;
  // Testing
  PetscBool   has_mms;
  PetscScalar expected_strain_energy;
  // Other options
  PetscInt highest_fine_order, diagnostic_order, diagnostic_geometry_order;
  PetscInt q_extra, q_extra_surface_force;
};

#endif  // ratel_impl_h
