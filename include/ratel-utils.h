#ifndef ratel_utils_h
#define ratel_utils_h

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelIntArrayC2P(PetscInt num_entries, CeedInt **array_ceed, PetscInt **array_petsc);
RATEL_INTERN PetscErrorCode RatelIntArrayP2C(PetscInt num_entries, PetscInt **array_petsc, CeedInt **array_ceed);
RATEL_INTERN PetscErrorCode RatelVecP2C(Ratel ratel, Vec X_petsc, PetscMemType *mem_type, CeedVector x_ceed);
RATEL_INTERN PetscErrorCode RatelVecC2P(Ratel ratel, CeedVector x_ceed, PetscMemType mem_type, Vec X_petsc);
RATEL_INTERN PetscErrorCode RatelVecReadP2C(Ratel ratel, Vec X_petsc, PetscMemType *mem_type, CeedVector x_ceed);
RATEL_INTERN PetscErrorCode RatelVecReadC2P(Ratel ratel, CeedVector x_ceed, PetscMemType mem_type, Vec X_petsc);

/**
  @brief Return minimum of two integers

  @param[in]  a  The first integer to compare
  @param[in]  b  The second integer to compare

  @return The minimum of the two integers

**/
/// @ingroup RatelInternal
static inline PetscInt RatelIntMin(PetscInt a, PetscInt b) { return a < b ? a : b; }

/**
  @brief Return maximum of two integers

  @param[in]  a  The first integer to compare
  @param[in]  b  The second integer to compare

  @return The maximum of the two integers

**/
/// @ingroup RatelInternal
static inline PetscInt RatelIntMax(PetscInt a, PetscInt b) { return a > b ? a : b; }

/**
  @brief Return the quadrature size from the eval mode, dimension, and number of components

  @param[in]  eval_mode       The basis evaluation mode
  @param[in]  dim             The basis dimension
  @param[in]  num_components  The basis number of components

  @return The maximum of the two integers

**/
/// @ingroup RatelInternal
static inline CeedInt RatelGetQuadratureSize(CeedEvalMode eval_mode, CeedInt dim, CeedInt num_components) {
  switch (eval_mode) {
    case CEED_EVAL_INTERP:
      return num_components;
    case CEED_EVAL_GRAD:
      return dim * num_components;
    default:
      return -1;
  }
}

/**
  @brief Translate PetscMemType to CeedMemType

  @param[in]  mem_type  PetscMemType

  @return Equivalent CeedMemType
**/
/// @ingroup RatelInternal
static inline CeedMemType RatelMemTypeP2C(PetscMemType mem_type) { return PetscMemTypeDevice(mem_type) ? CEED_MEM_DEVICE : CEED_MEM_HOST; }

/**
  @brief Involute negative indices, essential BC DoFs are encoded in closure indices as -(i+1)

  @param[in]  i  DoF index

  @return involuted index value
**/
static inline PetscInt RatelInvolute(PetscInt i) { return i >= 0 ? i : -(i + 1); }

/**
  @brief Convert from DMPolytopeType to CeedElemTopology

  @param[in]  cell_type  DMPolytopeType for the cell

  @return CeedElemTopology, or 0 if no equivelent CeedElemTopology was found
**/
static inline CeedElemTopology RatelElemTopologyP2C(DMPolytopeType cell_type) {
  switch (cell_type) {
    case DM_POLYTOPE_TRIANGLE:
      return CEED_TOPOLOGY_TRIANGLE;
    case DM_POLYTOPE_QUADRILATERAL:
      return CEED_TOPOLOGY_QUAD;
    case DM_POLYTOPE_TETRAHEDRON:
      return CEED_TOPOLOGY_TET;
    case DM_POLYTOPE_HEXAHEDRON:
      return CEED_TOPOLOGY_HEX;
    default:
      return 0;
  }
}

#endif  // ratel_utils_h
