#ifndef ratel_model_h
#define ratel_model_h

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>
#include <ratel/models/mooney-rivlin.h>
#include <ratel/models/neo-hookean.h>

// Physics options
#define RATEL_MODEL_REGISTER(material_create_functions, model_cl_argument, model_postfix) \
  RatelDebug(ratel, "---- Registration of model \"%s\"", model_cl_argument);              \
  PetscCall(PetscFunctionListAdd(material_create_functions, model_cl_argument, RatelMaterialCreate_##model_postfix));

RATEL_INTERN PetscErrorCode RatelRegisterModels(Ratel ratel, PetscFunctionList *material_create_functions);
RATEL_INTERN PetscErrorCode RatelModelDataVerifyRelativePath(Ratel ratel, RatelModelData model_data);

// Physics options
#define RATEL_PHYSICS(physics_postfix)                                                                                                    \
  RATEL_INTERN PetscErrorCode RatelMaterialPhysicsContextCreate_##physics_postfix(RatelMaterial, CeedQFunctionContext *);                 \
  RATEL_INTERN PetscErrorCode RatelMaterialPhysicsSmootherDataSetup_##physics_postfix(RatelMaterial);                                     \
  RATEL_INTERN PetscErrorCode RatelMaterialPlatenContextCreate_##physics_postfix(RatelMaterial, CeedScalar[3], CeedScalar[3], CeedScalar, \
                                                                                 CeedScalar, CeedScalar, CeedQFunctionContext *);

RATEL_PHYSICS(NeoHookean);
RATEL_PHYSICS(MooneyRivlin);

#endif  // ratel_model_h
