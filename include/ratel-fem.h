#ifndef ratel_fem_h
#define ratel_fem_h

#include <ceed.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscts.h>
#include <ratel-impl.h>
#include <ratel.h>

// Material FEM setup
RATEL_INTERN PetscErrorCode RatelMaterialCreate_FEM(Ratel ratel, RatelMaterial material);
RATEL_INTERN PetscErrorCode RatelDMSetup_FEM(Ratel ratel);
RATEL_INTERN PetscErrorCode RatelDMSetupByOrder_FEM(Ratel ratel, PetscBool setup_boundary, PetscInt *orders, PetscInt coord_order, PetscInt q_extra,
                                                    PetscInt num_fields, const PetscInt *field_sizes, DM dm);

// Geometry
RATEL_INTERN PetscErrorCode RatelMaterialSetupVolumeQData_FEM(RatelMaterial material, const char *label_name, PetscInt label_value,
                                                              CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceQData_FEM(RatelMaterial material, const char *label_name, PetscInt label_value,
                                                               CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceGradientQData_FEM(RatelMaterial material, DM dm, const char *label_name, PetscInt label_value,
                                                                       CeedElemRestriction *restriction, CeedVector *q_data);

// Operators
// -- Residual and Jacobian
RATEL_INTERN PetscErrorCode RatelMaterialSetupResidualJacobianSuboperators_FEM(RatelMaterial material, CeedOperator op_residual_u,
                                                                               CeedOperator op_residual_ut, CeedOperator op_residual_utt,
                                                                               CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianMultigridLevel_FEM(RatelMaterial material, PetscInt level, CeedVector m_loc,
                                                                         CeedOperator sub_op_jacobian_fine, CeedOperator op_jacobian_coarse,
                                                                         CeedOperator op_prolong, CeedOperator op_restrict);

// -- Output
RATEL_INTERN PetscErrorCode RatelMaterialSetupEnergySuboperator_FEM(RatelMaterial material, CeedOperator op_energy);
RATEL_INTERN PetscErrorCode RatelMaterialSetupDiagnosticSuboperators_FEM(RatelMaterial material, CeedOperator op_mass_diagnostic,
                                                                         CeedOperator op_diagnostic);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceForceSuboperators_FEM(RatelMaterial material, CeedOperator *ops_surface_force);
RATEL_INTERN PetscErrorCode RatelMaterialSetupMMSErrorSuboperator_FEM(RatelMaterial material, CeedOperator op_mms_error);

#endif  // ratel_fem_h
