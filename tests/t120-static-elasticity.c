/// @file
/// Test Ratel PCMG setup for SNES

//TESTARGS(name="linear-box") -ceed {ceed_resource} -options_file tests/elasticity-linear.yml
//TESTARGS(name="linear-no-multigrid") -ceed {ceed_resource} -options_file tests/elasticity-linear-no-multigrid.yml
//TESTARGS(name="linear-simplex") -ceed {ceed_resource} -options_file tests/elasticity-linear-simplex.yml
//TESTARGS(name="linear-quad-coords") -ceed {ceed_resource} -options_file tests/elasticity-linear-quad-coords.yml

const char help[] = "Ratel - test case 120\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm     comm;
  Ratel        ratel;
  SNES         snes;
  KSP          ksp;
  DM           dm;
  Vec          U;
  PetscInt     num_fields;
  PetscScalar *l2_error;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(SNESSetDM(snes, dm));
  PetscCall(RatelSNESSetDefaults(ratel, snes));

  // P-multigrid based preconditioning
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(RatelKSPSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, ksp));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(SNESSolve(snes, NULL, U));

  // Verify
  PetscCall(RatelComputeMMSL2Error(ratel, U, &num_fields, &l2_error));
  for (PetscInt i = 0; i < num_fields; i++) {
    if (fabs(l2_error[i]) > 2e-7) printf("Error: L2 norm = %0.5e\n", l2_error[i]);
  }

  // Cleanup
  PetscCall(PetscFree(l2_error));
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
