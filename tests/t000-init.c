/// @file
/// Test Ratel object initialization and destruction

//TESTARGS(name="init") -ceed {ceed_resource} -options_file tests/t000-init.yml

const char help[] = "Ratel - test case 001\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize and destroy Ratel context
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
