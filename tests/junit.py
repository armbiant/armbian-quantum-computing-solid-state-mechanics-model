#!/usr/bin/env python3

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), 'junit-xml')))
from junit_xml import TestCase, TestSuite


def parse_testargs(file):
    if os.path.splitext(file)[1] in ['.c', '.cpp']:
        return sum([[[line.split()[1:], [line.split()[0].strip('//TESTARGS(name=').strip(')')]]]
                    for line in open(file).readlines()
                    if line.startswith('//TESTARGS')], [])
    raise RuntimeError('Unrecognized extension for file: {}'.format(file))


def get_source(test):
    if test.startswith('ex'):
        return os.path.join('examples', test + '.c')
    else:
        return os.path.join('tests', test + '.c')


def get_testargs(source):
    args = parse_testargs(source)
    if not args:
        return [(['-ceed', '{ceed_resource}'], [''])]
    return args


def check_required_failure(test_case, stderr, required):
    if required in stderr:
        test_case.status = 'fails with required: {}'.format(required)
    else:
        test_case.add_failure_info('required: {}'.format(required))


def contains_any(resource, substrings):
    return any((sub in resource for sub in substrings))


def skip_rule(test, nproc, name, petsc_arch, ceed_resource):
    if 'only=' in name[0]:
        conditions = name[0].split('only=')[1]
        if ('serial' in conditions)   and (int(nproc) > 1):            return "serial test with nproc > 1"
        if ('parallel' in conditions) and (int(nproc) == 1):           return "parallel test with nproc = 1"
        if ('int32' in conditions)    and ('int64' in petsc_arch):     return "int32 test with int64 PETSc arch"
        if ('int64' in conditions)    and ('int64' not in petsc_arch): return "int64 test with int32 PETSc arch"
    return False


def run(test, nproc, backends, mode):
    import subprocess
    import time
    import difflib
    source = get_source(test)
    all_args = get_testargs(source)

    if mode.lower() == "tap":
        print('1..' + str(len(all_args) * len(backends)))

    test_cases = []
    my_env = os.environ.copy()
    my_env['CEED_ERROR_HANDLER'] = 'exit'
    petsc_arch = ""
    try:
        petsc_arch = my_env['PETSC_ARCH']
    except:
        petsc_arch = my_env['PETSC_DIR']
    index = 1
    for args, name in all_args:
        for ceed_resource in backends:
            rargs = []
            if int(nproc) > 1:
                rargs = ['mpiexec', '-n', nproc]
            rargs += [os.path.join('build', test)] + args.copy()
            if '{ceed_resource}' in rargs:
                rargs[rargs.index('{ceed_resource}')] = ceed_resource

            # run test
            skip_reason = skip_rule(test, nproc, name, petsc_arch, ceed_resource)
            if skip_reason:
                test_case = TestCase('{} n{} {}'.format(test, nproc, ceed_resource),
                                     elapsed_sec=0,
                                     timestamp=time.strftime('%Y-%m-%d %H:%M:%S %Z', time.localtime()),
                                     stdout='',
                                     stderr='')
                test_case.add_skipped_info(skip_reason)
            else:
                start = time.time()
                proc = subprocess.run(' '.join(rargs),
                                      shell=True,
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE,
                                      env=my_env)
                proc.stdout = proc.stdout.decode('utf-8')
                proc.stderr = proc.stderr.decode('utf-8')

                test_case = TestCase('{} {} n{} {}'.format(test, *name, nproc, ceed_resource),
                                     classname=os.path.dirname(source),
                                     elapsed_sec=time.time() - start,
                                     timestamp=time.strftime('%Y-%m-%d %H:%M:%S %Z', time.localtime(start)),
                                     stdout=proc.stdout,
                                     stderr=proc.stderr)
                ref_stdout = os.path.join('tests/output', test + '.out')

            # classify test results
            if not test_case.is_skipped() and proc.stderr:
                if 'Backend does not implement' in proc.stderr:
                    test_case.add_skipped_info('libCEED backend {} does not implement all methods for {}'.format(test, ceed_resource))
                elif 'PETSC ERROR: Model data for' in proc.stderr:
                    test_case.add_skipped_info('Material model dependencies not installed for {}'.format(test_case.name.split(' ')[1]))
                elif 'You may need to add --download-ctetgen or --download-tetgen' in proc.stderr:
                    test_case.add_skipped_info('Tet mesh generator not installed for {}'.format(test_case.name.split(' ')[1]))

            if not test_case.is_skipped() and not test_case.status:
                if proc.stderr:
                    test_case.add_failure_info('stderr', proc.stderr)
                elif proc.returncode != 0:
                    test_case.add_error_info('returncode = {}'.format(proc.returncode))
                elif os.path.isfile(ref_stdout):
                    with open(ref_stdout) as ref:
                        diff = list(difflib.unified_diff(ref.readlines(),
                                                         proc.stdout.splitlines(keepends=True),
                                                         fromfile=ref_stdout,
                                                         tofile='New'))
                    if diff:
                        test_case.add_failure_info('stdout', output=''.join(diff))
                elif proc.stdout and test[:4] not in ['t001', 't002', 't213']:
                    test_case.add_failure_info('stdout', output=proc.stdout)

            # store result
            test_case.args = ' '.join(rargs)
            test_cases.append(test_case)

            if mode.lower() == "tap":
                # print incremental output if TAP mode
                name = test_case.name.split(' ')[1]
                if ',only=' in name:
                    print('# Test: {}'.format(name.split(',only=')[0]))
                    print('# Only: {}'.format(name.split(',only=')[1]))
                else:
                    print('# Test: {}'.format(name))
                print('# $ {}'.format(test_case.args))
                if test_case.is_error():
                    print('not ok {} - ERROR: {}'.format(index, test_case.errors[0]['message'].strip()))
                    print(test_case.errors[0]['output'].strip())
                elif test_case.is_failure():
                    print('not ok {} - FAIL: {}'.format(index, test_case.failures[0]['message'].strip()))
                    print(test_case.failures[0]['output'].strip())
                elif test_case.is_skipped():
                    print('ok {} - SKIP: {}'.format(index, test_case.skipped[0]['message'].strip()))
                else:
                    print('ok {} - PASS'.format(index))
                sys.stdout.flush()
            else:
                # print error or failure information if JUNIT mode
                if test_case.is_error():
                    print('Test: {} {}'.format(test_case.name.split(' ')[0], test_case.name.split(' ')[1]))
                    print('ERROR: {}'.format(test_case.errors[0]['message'].strip()))
                    print(test_case.errors[0]['output'].strip())
                elif test_case.is_failure():
                    print('Test: {} {}'.format(test_case.name.split(' ')[0], test_case.name.split(' ')[1]))
                    print('FAIL: {}'.format(test_case.failures[0]['message'].strip()))
                    print(test_case.failures[0]['output'].strip())
                sys.stdout.flush()
            index += 1

    return TestSuite(test, test_cases)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('Test runner with JUnit and TAP output')
    parser.add_argument('--mode', help='Output mode, JUnit or TAP', default="JUnit")
    parser.add_argument('--output', help='Output file to write test', default=None)
    parser.add_argument('--gather', help='Gather all *.junit files into XML', action='store_true')
    parser.add_argument('test', help='Test executable', nargs='?')
    args = parser.parse_args()

    if args.gather:
        gather()
    else:
        backends = os.environ['CEED_BACKENDS'].split()
        nproc = os.environ.get('NPROC_TEST', default=1)

        # run tests
        result = run(args.test, nproc, backends, args.mode)

        # build output
        if args.mode.lower() == "junit":
            junit_batch = ''
            try:
                junit_batch = '-' + os.environ['JUNIT_BATCH']
            except BaseException:
                pass
            output = (os.path.join('build', args.test + junit_batch + '.junit')
                      if args.output is None
                      else args.output)

            with open(output, 'w') as fd:
                TestSuite.to_file(fd, [result])
        elif args.mode.lower() != "tap":
            raise Exception("output mode not recognized")

        # check return code
        for t in result.test_cases:
            failures = len([c for c in result.test_cases if c.is_failure()])
            errors = len([c for c in result.test_cases if c.is_error()])
            if failures + errors > 0 and args.mode.lower() != "tap":
                sys.exit(1)
