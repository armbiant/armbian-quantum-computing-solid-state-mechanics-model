# Changelog/Release Notes

On this page we provide a summary of the main API changes, new features and examples for each release of Ratel.

(main)=

## Current `main` branch

### New features

- Update strain energy function to the convex form for both Neo-Hookean and Mooney-Rivlin models.

(v0-2-0)=

## V0.2.1 (12 January 2023)

Bugfix to make tet based tests optional in the test suite.

## V0.2.0 (10 January 2023)

Expanded features and material models.

### New features

- Add `RatelComputeSurfaceForces` for computing final forces on surfaces, identified by DMPlex face id number.
- Add support for computing Jacobians from strain energy density functional from residual evaluation via Automatic Differentiation with Enzyme-AD. Neo-Hookean material models provided in current and initial configuration.
- Add support for platen boundary condition based upon Nitsche's method.
- Add support for restart from saved binaries of state vectors.

### Breaking changes

- Update names and command line arguments for material models for consistency.
- Use PETSc option `options_file` over redundant `yml` option in tests and examples; removed `options_yml` argument from `RatelInit`.
- Docker images built and tested as part of CI process for latest `main`.
- Update Boolean arguments `-view_diagnostic_quantities` and `-ts_monitor_diagnostic_quantities` to use viewer type.
- Remove command line option `-output_dir`; this functionality is provided with setting the output filepath for the viewer via `-view_diagnostic_quantities` and `-ts_monitor_diagnostic_quantities`.
- Remove `RatelVectorView` in favor of base PETSc functionality.
- Rename `RatelSolverType`, `RatelMultigridType`, and some function names for consistency.

### Maintainability

- Internal refactor around `RatelMaterial`, an object representing a region of the mesh given by a material model with consistent physical parameters.
External behavior was not affected by this refactor.

(v0-1-2)=

## v0.1.2 (15 April 2022)

Bugfix for install paths in makefile.

(v0-1-1)=

## v0.1.1 (14 April 2022)

Bugfix for Mass QFunction source file relative path.

(v0-1)=

## v0.1 (11 April 2022)

Initial Ratel API.
The Ratel solid mechanics library is based upon libCEED and PETSc.
libCEED v0.10.1 or later and PETSc v3.17 or later is required.
