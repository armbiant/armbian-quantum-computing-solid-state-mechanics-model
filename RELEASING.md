# Release Procedures

*These notes are meant for a maintainer to create official releases.*

Create a branch to hold pre-release commits.
We ideally want all release mechanics to be in one commit, which will then be tagged.

## Version number and release notes

1. Update the version number.

The version number must be updated in

* `include/ratel/ratel.h`
* `ratel.pc.template`
* `Doxyfile`

2. Update the release notes in `CHANGELOG.md`.

Use `git log --first-parent v0.1..` to get a sense of the pull requests that have been merged and thus might warrant emphasizing in the release notes.
While doing this, gather a couple sentences for key features to highlight on [GitLab releases](https://gitlab.com/micromorph/ratel/-/releases).
The content under the "Current `main` branch" heading needs to be moved to a new heading for the release.

3. Generate the new users manual.

Use `make doc-latexpdf` to build a PDF users manual and inspect it for missing references or formatting problems.
This PDF contains the same content as the documentation website, but it will be archived on Zenodo.

## Quality control and good citizenry

1. If making a minor release, check for API and ABI changes that could break [semantic versioning](https://semver.org/).

The [ABI compliance checker](https://github.com/lvc/abi-compliance-checker) is a useful tool, as is `nm -D ratel.so` and checking for public symbols (capital letters like `T` and `D` that are not namespaced).

2. Double check testing on any architectures that may not be exercised in continuous integration and with users of Ratel.

While unsupported changes do not prevent release, it's polite to make a PR to support the new release, and it's good for quality to test before tagging a Ratel release.

3. Check that `spack install ratel@develop` works prior to tagging.

The Spack `ratel/package.py` file should be updated immediately after tagging a release.

## Archive Users Manual on Zenodo

1. Generate the PDF using `make doc-latexpdf`, click "New version" on the [Zenodo record](https://doi.org/10.5281/zenodo.7521909) and upload.

2. Update author info for new authors or existing authors changing institutions.

3. Save the new upload to get the new DOI for the Zenodo archive.

4. Update the DOI in `README.rst`; then re-generate and re-upload the PDF of the users manual.

## Tagging and releasing on GitLab

0. Confirm all the steps above.

1. `git commit -am'ratel 0.1.0'`

This is amending the commit message on an in-progress commit, after rebasing on latest `main`.

2. `git push` updates the PR holding release.

3. `git switch main && git merge --ff-only HEAD@{1}` fast-forward merge into `main`

4. `git tag --sign -m'ratel 0.1.0'`

5. `git push origin main v0.1.0`

6. Draft a [new release on GitLab](https://gitlab.com/micromorph/ratel/-/releases) with a short summary of the release notes.

7. Publish the updated user guide on Zonodo.

8. Submit a PR to Spack.
