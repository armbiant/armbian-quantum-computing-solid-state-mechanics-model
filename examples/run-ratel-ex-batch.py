# Sample Python runner for Ratel quasistatic elasticity example

import datetime
import functools
import os
import subprocess

# convert options dict to array for command line
def option_dict_to_array(options):
    return [value for pair in (['-' + key, str(options[key])]
                               for key in options) for value in pair]


# run with dictionary of command line options
def run_ratel_with_options(exe_path, options):
    # run configuration
    my_env = os.environ.copy()
    options_array = option_dict_to_array(options)
    proc = subprocess.run(' '.join([exe_path] + options_array),
                          shell=True,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          env=my_env)
    proc.stdout = proc.stdout.decode('utf-8')
    proc.stderr = proc.stderr.decode('utf-8')

    # output logs
    if 'view_diagnostic_quantities' in options:
        base_file = options['view_diagnostic_quantities'].partition(':')[2].rpartition('.')[0]
    else:
        base_file = exe_path.split('/')[-1].split('.')[0]
    date_and_time = datetime.datetime.now().isoformat()
    print('logging console output to ' + base_file + '.out')
    with open(base_file + '.out', 'a') as f:
        f.write('\n--------------------------------------------------------------')
        f.write('\n---------------------- Ratel stdout log ----------------------')
        f.write('\n--------------------------------------------------------------\n')
        f.write('Time: ' + date_and_time)
        f.write('\n--------------------------------------------------------------\n\n')
        f.write('Command line options:\n')
        for key in options:
          f.write(f'-{key} {options[key]}\n')
        f.write('\n--------------------------------------------------------------\n\n')
        f.write(proc.stdout)
        f.write('\n--------------------------------------------------------------')
        f.write('\n---------------------- Ratel stdout log ----------------------')
        f.write('\n--------------------------------------------------------------\n')
        if len(proc.stderr) > 0:
            print('WARNING - OUTPUT TO STDERR')
            f.write('\n------------------------------------------------------------')
            f.write('\n--------------------- Ratel stderr log ---------------------')
            f.write('\n------------------------------------------------------------\n')
            f.write('Time: ' + date_and_time)
            f.write('\n------------------------------------------------------------\n\n')
            f.write(proc.stderr)
            f.write('\n------------------------------------------------------------')
            f.write('\n--------------------- Ratel stderr log ---------------------')
            f.write('\n------------------------------------------------------------\n')


# run static elasticity example
def run_ratel_ex01_with_options(options):
    ex01_path = os.path.join('bin', 'ratel-static')
    run_ratel_with_options(ex01_path, options)


# run quasistatic elasticity example
def run_ratel_ex02_with_options(options):
    ex02_path = os.path.join('bin', 'ratel-quasistatic')
    run_ratel_with_options(ex02_path, options)


# run dynamic elasticity example
def run_ratel_ex03_with_options(options):
    ex03_path = os.path.join('bin', 'ratel-dynamic')
    run_ratel_with_options(ex03_path, options)


# main
if __name__ == '__main__':
    # Ratel basic info
    os.system("make info")

    # simulation arguments
    yml_path = os.path.join(
        'examples',
        'ex02-quasistatic-elasticity-multi-material.yml')
    poissons_ratios = [0.35, 0.375, 0.4, 0.425, 0.45]
    options = {
        # base configuration for rod + binder
        "options_file": yml_path,
        # libCEED backend (CPU, GPU, etc)
        "ceed": "/cpu/self",
        # set binder Poisson's ratio
        "binder_nu": poissons_ratios[0],
        # unset expected strain energy (option for regression tests)
        "expected_strain_energy": 0,
        # output final displacement and derived quantities
        "view_diagnostic_quantities": "vtk:multi-material-binder-nu-" + str(poissons_ratios[0]) + ".vtu",
        # compute final force on surfaces
        "surface_force_faces": "1,2",
        # VTK/VTKU viewer only supports linear mesh geometry
        # If you are using high-order mesh geometry, then you need this option for VTK/VTU viewer
        "diagnostic_geometry_order": 1,
        # typical monitor options
        "ts_monitor_strain_energy": "",
        "ts_monitor_surface_force": "",
    }

    # loop over configurations
    for nu in poissons_ratios:
        print()
        print("-------------------------------------------------------------")
        print("Running Ratel Quasistatic Example with nu = " + str(nu))
        print("-------------------------------------------------------------")

        # run quasistatic elasticity example with current options
        options["binder_nu"] = nu
        options["view_diagnostic_quantities"] = "vtk:multi-material-binder-nu-" + str(nu) + ".vtu"
        run_ratel_ex02_with_options(options)
