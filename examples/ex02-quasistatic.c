/// @file
/// Ratel quasistatic example

//TESTARGS(name="linear-mms") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-linear-mms.yml
//TESTARGS(name="neo-hookean-initial") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-initial.yml
//TESTARGS(name="neo-hookean-initial-ad") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-initial-ad.yml
//TESTARGS(name="neo-hookean-initial-platen") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-initial-platen.yml
//TESTARGS(name="neo-hookean-current-platen") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-current-platen.yml
//TESTARGS(name="neo-hookean-current-platen-half-cylinder") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-current-platen-half-cylinder.yml
//TESTARGS(name="neo-hookean-face-forces") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-current-face-forces.yml
//TESTARGS(name="neo-hookean-face-forces-continue",only="serial,int32") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-current-face-forces-continue.yml
//TESTARGS(name="neo-hookean-current-ad") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-neo-hookean-current-ad.yml
//TESTARGS(name="mooney-rivlin-initial") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-mooney-rivlin-initial.yml
//TESTARGS(name="mooney-rivlin-initial-slip") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-mooney-rivlin-initial-slip.yml
//TESTARGS(name="mooney-rivlin-initial-platen") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-mooney-rivlin-initial-platen.yml
//TESTARGS(name="mooney-rivlin-initial-platen-half-cylinder") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-mooney-rivlin-initial-platen-half-cylinder.yml
//TESTARGS(name="schwarz-pendulum") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-schwarz-pendulum.yml -ts_max_time 0.25 -ts_dt 0.05 -dm_plex_tps_extent 2,1,1  -dm_plex_tps_refine 1 -dm_plex_tps_layers 1 -expected_strain_energy 5.387890807640e-04
//TESTARGS(name="multi-material") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-multi-material.yml
//TESTARGS(name="multi-material-simplex") -ceed {ceed_resource} -quiet -options_file examples/ex02-quasistatic-elasticity-multi-material-simplex.yml

const char help[] = "Ratel - quasistatic example\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  TS          ts;
  DM          dm;
  Vec         U, D;
  PetscScalar final_time = 1.0, l2_tolerance = 2e-7, strain_energy = 0.0, expected_strain_energy = 0.0;
  PetscReal   u_norms[3], *face_forces = NULL;
  PetscBool   quiet = PETSC_FALSE;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));

  // Read command line options
  comm = PETSC_COMM_WORLD;
  PetscOptionsBegin(comm, NULL, "Ratel quasistatic example", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsScalar("-l2_tolerance", "L2 error tolerance", NULL, l2_tolerance, &l2_tolerance, NULL));
  PetscOptionsEnd();

  // Initialize Ratel context and create DM
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(RatelTSSetup(ratel, ts));
  PetscCall(TSSetMaxTime(ts, final_time));
  // Avoid stepping past the final loading condition (because the solution might not be valid there)
  PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_MATCHSTEP));
  PetscCall(TSSetFromOptions(ts));

  // P-multigrid based preconditioning
  PetscCall(RatelTSSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, ts));

  // View Ratel setup
  if (!quiet) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "----- Ratel Quasistatic Example -----\n\n"));
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));
    // LCOV_EXCL_STOP
  }

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));
  // Name vector so it isn't automatically named (via address) in output files
  PetscCall(PetscObjectSetName((PetscObject)U, "U"));

  // Solve
  PetscPreLoadBegin(PETSC_FALSE, "Ratel Solve");
  PetscCall(RatelTSSetupInitialCondition(ratel, ts, U));
  if (PetscPreLoadingOn) {
    // LCOV_EXCL_START
    SNES      snes;
    PetscReal rtol;
    PetscCall(TSGetSNES(ts, &snes));
    PetscCall(SNESGetTolerances(snes, NULL, &rtol, NULL, NULL, NULL));
    PetscCall(SNESSetTolerances(snes, PETSC_DEFAULT, .99, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
    PetscCall(TSSetSolution(ts, U));
    PetscCall(TSStep(ts));
    PetscCall(SNESSetTolerances(snes, PETSC_DEFAULT, rtol, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
    // LCOV_EXCL_STOP
  } else {
    PetscCall(TSSolve(ts, U));
  }
  PetscPreLoadEnd();
  PetscCall(TSGetSolveTime(ts, &final_time));

  // Post solver info
  {
    TSConvergedReason reason;
    PetscInt          num_steps;

    PetscCall(TSGetStepNumber(ts, &num_steps));
    if (!quiet) PetscCall(PetscPrintf(comm, "TS steps: %" PetscInt_FMT "\n", num_steps));
    PetscCall(TSGetConvergedReason(ts, &reason));
    if (!quiet || reason < TS_CONVERGED_ITERATING) PetscCall(PetscPrintf(comm, "TS converged reason: %s\n", TSConvergedReasons[reason]));
  }

  // Verify MMS
  PetscBool has_mms = PETSC_FALSE;
  PetscCall(RatelHasMMS(ratel, &has_mms));
  if (has_mms) {
    PetscInt     num_fields;
    PetscScalar *l2_error;

    PetscCall(RatelComputeMMSL2Error(ratel, U, &num_fields, &l2_error));
    for (PetscInt i = 0; i < num_fields; i++) {
      if (!quiet || l2_error[i] > l2_tolerance) PetscCall(PetscPrintf(comm, "L2 Error, field %" PetscInt_FMT ": %0.12e\n", i, l2_error[i]));
    }
    PetscCall(PetscFree(l2_error));
  }

  // Verify strain energy
  PetscCall(RatelComputeStrainEnergy(ratel, U, final_time, &strain_energy));
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &expected_strain_energy));
  PetscCall(RatelComputeSurfaceForces(ratel, U, final_time, &face_forces));
  PetscCall(VecStrideNormAll(U, NORM_MAX, u_norms));
  if (!quiet) {
    // LCOV_EXCL_START
    PetscInt        num_faces, num_comp_u = 3;
    const PetscInt *faces;

    PetscCall(RatelGetSurfaceForceFaces(ratel, &num_faces, &faces));
    for (PetscInt i = 0; i < num_faces; i++) {
      PetscCall(PetscPrintf(comm, "Surface %" PetscInt_FMT ":\n", faces[i]));
      PetscInt offset = (2 * i) * num_comp_u;
      PetscCall(
          PetscPrintf(comm, "  Centroid: [%0.12e, %0.12e, %0.12e]\n", face_forces[offset + 0], face_forces[offset + 1], face_forces[offset + 2]));
      offset = (2 * i + 1) * num_comp_u;
      PetscCall(
          PetscPrintf(comm, "  Force:    [%0.12e, %0.12e, %0.12e]\n", face_forces[offset + 0], face_forces[offset + 1], face_forces[offset + 2]));
    }

    PetscCall(PetscPrintf(comm, "Computed strain energy: %0.12e\n", strain_energy));
    PetscCall(PetscPrintf(comm, "Max displacements: [%0.12e, %0.12e, %0.12e]\n", u_norms[0], u_norms[1], u_norms[2]));
    // LCOV_EXCL_STOP
  }
  if ((fabs(expected_strain_energy) > 1e-14) && (!quiet || fabs(strain_energy - expected_strain_energy) > 6e-3)) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "Strain energy error: %0.12e\n", fabs(strain_energy - expected_strain_energy)));
    // LCOV_EXCL_STOP
  }

  // Compute diagnostic quantities
  PetscCall(RatelComputeDiagnosticQuantities(ratel, U, final_time, &D));
  PetscCall(VecViewFromOptions(D, NULL, "-view_diagnostic_quantities"));
  PetscCall(VecViewFromOptions(U, NULL, "-view_final_solution"));

  // Checkpoint
  PetscCall(RatelTSCheckpointFinalSolutionFromOptions(ratel, ts, U));

  // Cleanup
  PetscCall(PetscFree(face_forces));
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(VecDestroy(&D));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
