/// @file
/// Ratel static example

//TESTARGS(name="ceed-bp3") -ceed {ceed_resource} -quiet -options_file examples/ex01-ceed-bp3.yml
//TESTARGS(name="ceed-bp4") -ceed {ceed_resource} -quiet -options_file examples/ex01-ceed-bp4.yml
//TESTARGS(name="linear-mms") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-linear-mms.yml
//TESTARGS(name="neo-hookean-current") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-current.yml
//TESTARGS(name="neo-hookean-current-ad") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-current-ad.yml
//TESTARGS(name="neo-hookean-initial") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-initial.yml
//TESTARGS(name="neo-hookean-initial-ad") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-initial-ad.yml
//TESTARGS(name="neo-hookean-initial-platen") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-initial-platen.yml
//TESTARGS(name="mooney-rivlin-initial") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-mooney-rivlin-initial.yml
//TESTARGS(name="neo-hookean-face-forces-continue",only="serial,int32") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-current-face-forces-continue.yml

const char help[] = "Ratel - static example\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm      comm;
  PetscLogStage stage_snes_solve;
  Ratel         ratel;
  SNES          snes;
  DM            dm;
  Vec           U, D;
  PetscScalar   l2_tolerance = 2e-7, strain_energy = 0.0, expected_strain_energy = 0.0;
  PetscBool     quiet = PETSC_FALSE;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));

  // Read command line options
  comm = PETSC_COMM_WORLD;
  PetscOptionsBegin(comm, NULL, "Ratel static example", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsScalar("-l2_tolerance", "L2 error tolerance", NULL, l2_tolerance, &l2_tolerance, NULL));
  PetscOptionsEnd();

  // Initialize Ratel context and create DM
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(RatelSNESSetup(ratel, snes));

  // P-multigrid based preconditioning
  PetscCall(RatelSNESSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, snes));

  // Set additional command line options
  PetscCall(SNESSetFromOptions(snes));

  // View Ratel setup
  if (!quiet) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "----- Ratel Static Example -----\n\n"));
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));
    // LCOV_EXCL_STOP
  }

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(RatelSNESSetupInitialCondition(ratel, snes, U));
  {
    PetscCall(PetscLogStageRegister("Ratel Solve", &stage_snes_solve));
    PetscCall(PetscLogStagePush(stage_snes_solve));
    PetscCall(SNESSolve(snes, NULL, U));
    PetscCall(PetscLogStagePop());
  }

  // Report solver info
  {
    SNESConvergedReason reason;
    PetscInt            num_iterations;

    PetscCall(SNESGetIterationNumber(snes, &num_iterations));
    if (!quiet) PetscCall(PetscPrintf(comm, "SNES iterations: %" PetscInt_FMT "\n", num_iterations));
    PetscCall(SNESGetConvergedReason(snes, &reason));
    if (!quiet || reason < SNES_CONVERGED_ITERATING) PetscCall(PetscPrintf(comm, "SNES converged reason: %s\n", SNESConvergedReasons[reason]));
  }

  // Verify MMS
  PetscBool has_mms = PETSC_FALSE;
  PetscCall(RatelHasMMS(ratel, &has_mms));
  if (has_mms) {
    PetscInt     num_fields;
    PetscScalar *l2_error;

    PetscCall(RatelComputeMMSL2Error(ratel, U, &num_fields, &l2_error));
    for (PetscInt i = 0; i < num_fields; i++) {
      if (!quiet || l2_error[i] > l2_tolerance) PetscCall(PetscPrintf(comm, "L2 Error, field %" PetscInt_FMT ": %0.12e\n", i, l2_error[i]));
    }
    PetscCall(PetscFree(l2_error));
  }

  // Verify strain energy
  PetscCall(RatelComputeStrainEnergy(ratel, U, 1.0, &strain_energy));
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &expected_strain_energy));
  if (!quiet) PetscCall(PetscPrintf(comm, "Computed strain energy: %0.12e\n", strain_energy));
  if ((fabs(expected_strain_energy) > 1e-14) && (!quiet || fabs(strain_energy - expected_strain_energy) > 6e-3)) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "Strain energy error: %0.12e\n", fabs(strain_energy - expected_strain_energy)));
    // LCOV_EXCL_STOP
  }

  // Compute diagnostic quantities
  PetscCall(RatelComputeDiagnosticQuantities(ratel, U, 1.0, &D));
  PetscCall(VecViewFromOptions(D, NULL, "-view_diagnostic_quantities"));
  PetscCall(VecViewFromOptions(U, NULL, "-view_final_solution"));

  // Checkpoint
  PetscCall(RatelSNESCheckpointFinalSolutionFromOptions(ratel, snes, U));

  // Cleanup
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(VecDestroy(&D));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
